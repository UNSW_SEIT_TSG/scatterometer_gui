# Scatterometer GUI

## Development environment configuration
The following instructions assume starting from a clean install of Windows 10 Pro.  At writing I used build version: 10.0.17134 Build 17134.
- Download and install Git for Windows from https://git-scm.com/download/win; I last used: Git-2.19.0-64-bit.exe; use VSCode as Git Editor.
- Download and install Visual Studio Code from https://code.visualstudio.com/download; I last used: VSCodeUserSetup-x64-1.27.2.exe.
- Setup dev machine for git use via ssh via https://gitlab.com/profile/keys
- Clone repo using Git for Windows by executing the command: `git clone git@gitlab.com:UNSW_SEIT_TSG/scatterometer_gui.git`
- Download and install Mono SDK Latest https://download.mono-project.com/archive/5.18.1/windows-installer/mono-5.18.1.0-x64-0.msi
- If the above fails try this version: Mono SDK 5.12.0 of c# mono from https://download.mono-project.com/archive/5.12.0/windows-installer/; I last used: mono-5.12.0.301-x64-0.msi; I tried mono-5.14.0.177-x64-0.msi but it was incompatible.
- Download and extract latest version of Godot Engine from https://godotengine.org/download/windows with mono support; I last used: https://downloads.tuxfamily.org/godotengine/3.1/mono/Godot_v3.1-stable_mono_win64.zip and prior to that Godot_v3.0.6-stable_mono_win64.zip.
- Download and install Build Tools for Visual Studio 2017 from https://visualstudio.microsoft.com/downloads/#build-tools-for-visual-studio-2017; I last used: vs_buildtools__1550157070.1538620282.exe
    - Using Visual Studio Installer install:
        - .NET CORE runtime
        - .NET Framework 3.5 development tools
        - .NET Framework 4 targeting pack
        - .NET Framework 4.5 targeting pack
        - .NET Framework 4.5.1 targeting pack
        - .NET Framework 4.5.2 targeting pack
        - .NET Framework 4.6 targeting pack
        - .NET Framework 4.6.1 SDK
        - .NET Framework 4.6.1 targeting pack
        - .NET Portable Library targeting pack
- Install Visual Studio Code extensions:
    - C# extension 'ms-vscode.csharp';
    - Godot Tools extension 'geequlim.godot-tools'; and the
    - Python extension 'ms-python.python' from the extensions marketplace.
- Set the default godot c# editor to Visual Studio Code (See: http://www.gamefromscratch.com/post/2017/12/21/Godot-Development-Using-Visual-Studio-Code-and-C.aspx for an example).

## Code Conventions
See [CodeStyle](/docs/CodeStyle.md)

## todo (from Joe)
need to save datum for next 'run' of gui code (put in config.cfg)
g2:     70 
g5:     80