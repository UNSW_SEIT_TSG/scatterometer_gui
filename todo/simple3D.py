# 2018 Jul 26, John: This file is redundant, but the 3D model of the scatterometer needs to be migrated to the godot model before the file can be deleted.

# from visual.controls import *
from math import tau as τ, radians, degrees, cos, sin
from vpython import *

class RV240CCHL:
    def __init__(self, offset_x=0, offset_y=0, offset_z=0):
        self.RV240CCHL_base_diameter      = 0.275    #[m]
        self.RV240CCHL_base_height        = 0.016    #[m]
        self.RV240CCHL_upper_diameter     = 0.237    #[m]
        self.RV240CCHL_upper_height       = 0.0585   #[m]
        self.opacity                      = 1
        self.height_above_table = offset_y

        self.RV240CCHL_base = cylinder(
            pos     = vector(0.0+offset_x, offset_y, offset_z),
            axis    = vector(0.0, 1.0, 0.0),
            size    = vector(self.RV240CCHL_base_height, self.RV240CCHL_base_diameter, self.RV240CCHL_base_diameter),
            color   = vector(0.95,0.35,0.35),
            opacity = self.opacity
        )

        self.RV240CCHL_upper = cylinder(
            pos     = vector(0.0+offset_x, self.RV240CCHL_base_height+offset_y, offset_z),
            axis    = vector(0.0, 1.0, 0.0),
            size    = vector(self.RV240CCHL_upper_height, self.RV240CCHL_upper_diameter, self.RV240CCHL_upper_diameter),
            color   = vector(0.75,0.25,0.25),
            opacity = self.opacity
        )
class BaseCylinderLeg:
    def __init__(self, offset_x, offset_y, offset_z, radius=0.1436878, turn=0):
        sx = radius*cos(turn*τ)
        sz = radius*sin(turn*τ)
        self.cyl = cylinder(
            pos     = vector(0.0+sx+offset_x, offset_y, sz+offset_z),
            axis    = vector(0.0, 1.0, 0.0),
            size    = vector(0.0762, 0.0377444, 0.0377444),
            color   = vector(0.95,0.95,0.95)
        )
class SensorArm:
    def __init__(self, offset_x, offset_y, offset_z):
        main_colour = vector(0.55,0.55,0.55)
        self.opacity = 1

        self.base_cylinder_leg_0 = BaseCylinderLeg(offset_x, offset_y, offset_z, 0.1436878, 0/8)
        self.base_cylinder_leg_1 = BaseCylinderLeg(offset_x, offset_y, offset_z, 0.1436878, 1/8)
        self.base_cylinder_leg_2 = BaseCylinderLeg(offset_x, offset_y, offset_z, 0.1436878, 2/8)
        self.base_cylinder_leg_3 = BaseCylinderLeg(offset_x, offset_y, offset_z, 0.1436878, 3/8)
        self.base_cylinder_leg_4 = BaseCylinderLeg(offset_x, offset_y, offset_z, 0.1436878, 4/8)
        self.base_cylinder_leg_5 = BaseCylinderLeg(offset_x, offset_y, offset_z, 0.1436878, 5/8)
        self.base_cylinder_leg_6 = BaseCylinderLeg(offset_x, offset_y, offset_z, 0.1436878, 6/8)
        self.base_cylinder_leg_7 = BaseCylinderLeg(offset_x, offset_y, offset_z, 0.1436878, 7/8)

        self.base_cylinder = cylinder(
            pos     = vector(0.0+offset_x, self.base_cylinder_leg_0.cyl.size.x+offset_y, offset_z),
            axis    = vector(0.0, 1.0, 0.0),
            size    = vector(0.0254, 0.3302, 0.3302),
            color   = main_colour,
            opacity = self.opacity,
            shininess = 0
        )

        self.isolator_cylinder_mid = cylinder(
            pos     = vector(0.0+offset_x, self.base_cylinder_leg_0.cyl.size.x+self.base_cylinder.size.x+offset_y, offset_z),
            axis    = vector(0.0, 1.0, 0.0),
            size    = vector(0.242, 0.153, 0.153),
            color   = vector(0.95,0.95,0.95),
            shininess = 0
        )

        self.isolator_cylinder_top = cylinder(
            pos     = vector(0.0+offset_x, self.base_cylinder_leg_0.cyl.size.x+self.base_cylinder.size.x+0.242+offset_y, offset_z),
            axis    = vector(0.0, 1.0, 0.0),
            size    = vector(0.020, 0.2, 0.2),
            color   = vector(0.95,0.95,0.95),
            shininess = 0
        )

        # self.stage_A_RV240CCHL = RV240CCHL(offset_x, 0.0254+0.0762, offset_z)
        self.stage_A_RV240CCHL = RV240CCHL(offset_x, offset_y+self.base_cylinder.size.x+self.base_cylinder_leg_0.cyl.size.x, offset_z)

        self.main_cylinder_diameter_outer = 0.09     #[m]
        self.main_cylinder_diameter_inner = 0.1143   #[m]
        self.bridge_cylinder_diameter     = 0.20828  #[m]
        self.bridge_cylinder_height       = 0.156718 #[m]
        self.counterweight_distance_from_centre_of_rotation_as_fraction_of_counterweight_cylinder_length = (1-0.172)
        self.height_above_table = self.stage_A_RV240CCHL.height_above_table + self.stage_A_RV240CCHL.RV240CCHL_base_height + self.stage_A_RV240CCHL.RV240CCHL_upper_height + self.bridge_cylinder_height/2

        self.horizontal_tube_sensor_side_outer = cylinder(
            pos     = vector(0.0+offset_x, self.height_above_table+offset_y, offset_z),
            axis    = vector(1.0, 0.0, 0.0),
            size    = vector(1.2698476, self.main_cylinder_diameter_outer, self.main_cylinder_diameter_outer),
            color   = main_colour,
            opacity = self.opacity,
            shininess = 0
        )
        self.horizontal_tube_sensor_side_inner = cylinder(
            pos     = vector(0.0+offset_x, self.height_above_table+offset_y, offset_z),
            axis    = vector(1.0, 0.0, 0.0),
            size    = vector(0.262763, self.main_cylinder_diameter_inner, self.main_cylinder_diameter_inner),
            color   = main_colour,
            opacity = self.opacity,
            shininess = 0
        )

        self.horizontal_tube_counterweight_side_outer = cylinder(
            pos     = vector(0.0+offset_x, self.height_above_table+offset_y, offset_z),
            axis    = vector(1.0, 0.0, 0.0),
            size    = vector(0.5637276, self.main_cylinder_diameter_outer, self.main_cylinder_diameter_outer),
            color   = main_colour,
            opacity = self.opacity,
            shininess = 0
        )
        self.horizontal_tube_counterweight_side_outer.rotate(0.5*τ, vector(0,1,0))

        self.horizontal_tube_counterweight_side_inner = cylinder(
            pos     = vector(0.0+offset_x, self.height_above_table+offset_y, offset_z),
            axis    = vector(1.0, 0.0, 0.0),
            size    = vector(0.262763, self.main_cylinder_diameter_inner, self.main_cylinder_diameter_inner),
            color   = main_colour,
            opacity = self.opacity,
            shininess = 0
        )
        self.horizontal_tube_counterweight_side_inner.rotate(0.5*τ, vector(0,1,0))

        self.vertical_tube = cylinder(
            pos     = vector(1.0+offset_x, self.height_above_table+offset_y, offset_z),
            axis    = vector(0.0, 1.0, 0.0),
            size    = vector(0.657352, self.main_cylinder_diameter_outer, self.main_cylinder_diameter_outer),
            color   = main_colour,
            opacity = self.opacity,
            shininess = 0
        )
        self.sensor_arm_component_D = box(
            pos     = vector(0.0+offset_x, 1.0+offset_y, offset_z),
            axis    = vector(0.0, 1.0, 0.0),
            size    = vector(0.009525, 0.178, 0.178),
            color   = main_colour
        )
        self.counterweight = box(
            pos     = vector(0.0+offset_x, offset_y, offset_z),
            axis    = vector(1.0, 0.0, 0.0),
            size    = vector(0.085, 0.255, 0.236),
            color   = main_colour,
            opacity = self.opacity,
            shininess = 0
        )
        self.bridge_cylinder = cylinder(
            pos     = vector(0.0+offset_x, self.height_above_table-self.bridge_cylinder_height/2+offset_y, offset_z),
            axis    = vector(0.0, 1.0, 0.0),
            size    = vector(self.bridge_cylinder_height, self.bridge_cylinder_diameter, self.bridge_cylinder_diameter),
            color   = main_colour,
            opacity = self.opacity,
            shininess = 0
        )

        self.sphere_on_corner_for_aesthetics = sphere(
            pos     = vector(1.0+offset_x, self.height_above_table+offset_y, offset_z),
            radius  = self.main_cylinder_diameter_outer/2,
            color   = main_colour,
            opacity = self.opacity,
            shininess = 0
        )
        
    
    def rotate_relative_radians(self, angular_increment_radians):
        """ Rotates sensor arm by some angular increment measured in radians.
        """
        self.horizontal_tube_sensor_side_inner.rotate(angular_increment_radians, vector(0,1,0))
        self.horizontal_tube_sensor_side_outer.rotate(angular_increment_radians, vector(0,1,0))
        self.horizontal_tube_counterweight_side_inner.rotate(angular_increment_radians, vector(0,1,0))
        self.horizontal_tube_counterweight_side_outer.rotate(angular_increment_radians, vector(0,1,0))

        self.vertical_tube.pos.x = self.horizontal_tube_sensor_side_outer.pos.x+self.horizontal_tube_sensor_side_outer.axis.x
        self.vertical_tube.pos.z = self.horizontal_tube_sensor_side_outer.pos.z+self.horizontal_tube_sensor_side_outer.axis.z

        self.sphere_on_corner_for_aesthetics.pos = self.vertical_tube.pos
        
        self.sensor_arm_component_D.rotate(angular_increment_radians, vector(0,1,0))
        self.sensor_arm_component_D.pos.x = self.vertical_tube.pos.x
        self.sensor_arm_component_D.pos.z = self.vertical_tube.pos.z
        self.sensor_arm_component_D.pos.y = self.vertical_tube.pos.y+self.vertical_tube.axis.y

        self.counterweight.rotate(angular_increment_radians, vector(0,1,0))
        self.counterweight.pos = self.horizontal_tube_counterweight_side_outer.axis*self.counterweight_distance_from_centre_of_rotation_as_fraction_of_counterweight_cylinder_length+self.horizontal_tube_counterweight_side_outer.pos

    def rotate_relative_degrees(self, angular_increment_degrees):
        """ Rotates sensor arm by some angular increment measured in degrees.
        """
        self.rotate_relative_radians(radians(angular_increment_degrees))
class Axes:
    def __init__(self, offset_x, offset_y, offset_z):
        self.axis_diameter = 0.005
        self.origin = vector(offset_x, offset_y, offset_z)
        self.axis_length = 1
        self.axis_size = vector(self.axis_length, self.axis_diameter, self.axis_diameter)
        self.x_axis = self.axis = cylinder(
            pos     = self.origin,
            axis    = vector(1, 0, 0),
            size    = self.axis_size,
            color   = vector(0.5, 1, 0.5)
        )
        self.y_axis = self.axis = cylinder(
            pos     = self.origin,
            axis    = vector(0, 1, 0),
            size    = self.axis_size,
            color   = vector(1, 0.5, 0.5)
        )
        self.z_axis = self.axis = cylinder(
            pos     = self.origin,
            axis    = vector(0, 0, 1),
            size    = self.axis_size,
            color   = vector(0.5, 0.5, 1)
        )
class Scatterometer:
    def __init__(self):
        self.offset_x = -0.42
        self.offset_y = 0
        self.offset_z = -0.39

        self.cumulative_angle_in_radians = 0
        self.refresh_rate = 100 # Hertz
        self.rotational_velocity = 0.025 # [rotation/second]
        self.axes = Axes(self.offset_x,self.offset_y,self.offset_z)
        self.sensor_arm = SensorArm(self.offset_x, self.offset_y, self.offset_z)
        self.lab_table_thickness = 0.1
        self.lab_table = box(
            pos     = vector(0.0, -self.lab_table_thickness/2, 0.0),
            axis    = vector(0.0, 1.0, 0.0),
            size    = vector(self.lab_table_thickness, 1.22, 1.82),
            color   = vector(0.6, 0.7, 0.8)
        )
        self.interval_arc_in_degrees = 0

    def calculateAngularIncrement(self):
        return self.rotational_velocity/self.refresh_rate*τ

    def move(self, angular_increment = "omitted"):
        rate(self.refresh_rate)
        if angular_increment == "omitted":
            angular_increment = self.calculateAngularIncrement()
        self.sensor_arm.rotate_relative_radians(angular_increment)
        self.cumulative_angle_in_radians += angular_increment
        return self.cumulative_angle_in_radians
    
    def rotateAbsolute(self, destination_angle):
        self.move(destination_angle-self.cumulative_angle_in_radians)
class GUI:
    def __init__(self, scene):
        self.scene = scene
        self.spin_polarity_dict = {
            1: "counter clockwise",
            0:         "undefined",
            -1:         "clockwise"
        }
        self.spin_polarity = 1
        self.slider_length = 1150
        self.sz = Scatterometer()
        self.running = False
        self.run_button = button(text="Run", pos=scene.title_anchor, bind=self.run)
        self.slider_shift_left = 15
        self.slider_shift_down = 5
        self.slider_inter_spacing_vertical = 35
        self.common_anchor = scene.caption_anchor #scene.title_anchor #
        self.sl_start      = slider(min=0, max=360, value=0, length=self.slider_length, bind=self.setStartAngle, step = 1, top=self.slider_shift_down, pos=self.common_anchor, left = self.slider_shift_left)
        self.sl_stop       = slider(min=0, max=360, value=45, length=self.slider_length, bind=self.setStopAngle,  step = 1, top=self.slider_shift_down+self.slider_inter_spacing_vertical, left = -(self.slider_length+2))
        self.sl_intervals  = slider(min=0, max=360, value=3, length=self.slider_length, bind=self.setIntervalCount,  step = 1, top=self.slider_shift_down+self.slider_inter_spacing_vertical*2, left = -(self.slider_length+2))
        self.wt_start = wtext(text="")
        self.wt_stop  = wtext(text="")
        self.wt_step  = wtext(text="")
        self.wt_log   = wtext(text="Measurement Log: \n")
        self.setStartAngle(self.sl_start)
        self.setStopAngle(self.sl_stop)
        self.setIntervalCount(self.sl_intervals)
        self.sz.move(0)
        self.scene.camera.pos  = vector(-1.61957, 1.2596, 1.33886)
        self.scene.camera.axis = -scene.camera.pos
        
        self.updateSpinPolarity()
        self.sample_intervals_in_degrees = []

    def run(self, b):
        self.running = not self.running
        if self.running:
            b.text = "Pause"
        else:
            b.text = "Run"

    def updateSpinPolarity(self):
        δ = self.sl_stop.value - self.sl_start.value
        Δ = abs(δ)
        Δ = 1 if Δ==0 else Δ
        self.spin_polarity = int(round(δ/Δ))
        print(self.spin_polarity)

    def setStartAngle(self, s):
        self.wt_start.text = '   Start Angle: {:1.2f}'.format(self.sl_start.value)+" [degrees]\n\n"
        self.updateSampleIntervals()
        self.sz.rotateAbsolute(radians(self.sl_start.value))
        self.updateSpinPolarity()

    def setStopAngle(self, s):
        self.wt_stop.text   = '   Stop  Angle: {:1.2f}'.format(self.sl_stop.value)+" [degrees]\n\n"
        self.updateSampleIntervals()
        self.updateSpinPolarity()

    def setIntervalCount(self, s):
        self.updateSampleIntervals()

    def updateSampleIntervals(self):
        precision = 100
        if self.sl_intervals.value != 0:
            self.sz.interval_arc_in_degrees = (self.sl_stop.value-self.sl_start.value)/self.sl_intervals.value
        else:
            self.sl_intervals.value = 1
            self.sz.interval_arc_in_degrees = (self.sl_stop.value-self.sl_start.value)/self.sl_intervals.value
        self.wt_step.text   = '   Regular measurement interval count: {:1.2f}'.format(self.sl_intervals.value)+" [measurements] measured every "+'{:1.2f}'.format(self.sz.interval_arc_in_degrees)+" [degrees]\n\n"
        self.sample_intervals_in_degrees = [x/precision for x in range(int(self.sl_start.value*precision),int(self.sl_stop.value*precision),int(self.sz.interval_arc_in_degrees*precision))]
        self.sample_intervals_in_degrees.append(self.sl_stop.value)
        print(self.sample_intervals_in_degrees)

class XPS:
    def __init__(self):
        IP_Address = "192.168.0.100",
        debugMode  = True
    def GroupInitialize(self):
        pass
    def GroupHomeSearch(self):
        pass
    def GroupMoveAbsolute(self):
        pass
    def GroupMoveRelative(self):
        pass
    def GroupMotionDisable(self):
        pass
    def GroupMotionEnable(self):
        pass
    def GroupMoveAbort(self):
        pass
    def GroupKill(self):
        pass
    def GroupSpinParametersSet(self):
        pass
    def GroupSpinModeStop(self):
        pass
    def SpinSlaveModeEnable(self):
        pass
    def SpinSlaveModeDisable(self):
        pass
    def GroupAnalogTrackingModeEnable(self):
        pass
    def GroupAnalogTrackingModeDisable(self):
        pass
    def GroupInitializeWithEncoderCalibration(self):
        pass
    def GroupReferencingStart(self):
        pass
    def GroupReferencingStop(self):
        pass

scene = canvas(
    title = "Scatterometer Equatorial Scan Control\n\n",
    x = 0,
    y = 0,
    width   = 1280,
    height  = 640,
    caption = "\n"
)

stop_tolerance_in_degrees = 0.1 
stop_tolerance_in_radians = radians(stop_tolerance_in_degrees)

gx = GUI(scene)
gx.next_measurement_point = gx.sz.interval_arc_in_degrees
gx.updateSampleIntervals()


print(gx.sample_intervals_in_degrees)
target_in_radians = radians(gx.sample_intervals_in_degrees.pop(0))
last_angle_in_radians = gx.sl_start.value

while True:
    if gx.running:
        print(gx.spin_polarity, round(degrees(target_in_radians)), round(degrees(last_angle_in_radians)), gx.sample_intervals_in_degrees)

        if(
            last_angle_in_radians > target_in_radians-stop_tolerance_in_radians
            and
            last_angle_in_radians < target_in_radians+stop_tolerance_in_radians
        ):
            print(gx.sample_intervals_in_degrees)
            if(len(gx.sample_intervals_in_degrees)>0):
                target_in_radians = radians(gx.sample_intervals_in_degrees.pop(0))
                sleep(1)
                gx.wt_log.text += ("reading taken at "+str(round(degrees(last_angle_in_radians),2))+"\n")
            else:
                gx.wt_log.text += ("reading taken at "+str(round(degrees(last_angle_in_radians),2))+"\n")
                gx.wt_log.text += ("Reading sequence complete")
                gx.running = False
                gx.run_button.text = "Run"

        last_angle_in_radians = gx.sz.move(gx.spin_polarity*gx.sz.calculateAngularIncrement())
        last_angle_in_radians = last_angle_in_radians + τ if last_angle_in_radians < 0 else last_angle_in_radians # Loop if cycles below zero
        last_angle_in_radians = last_angle_in_radians - τ if last_angle_in_radians > τ else last_angle_in_radians # Loop if cycles above 360

        # if gx.spin_polarity > 0:
            # if last_angle_in_radians >= radians(gx.next_measurement_point):
            #     sleep(1) # Simulated delay for sampling/reading
            #     gx.wt_log.text += ("reading taken at "+str(round(degrees(last_angle_in_radians),2))+"\n")
            #     gx.next_measurement_point += gx.sz.interval_arc_in_degrees
            #     if gx.next_measurement_point < 0:
            #         gx.next_measurement_point+360
            # if last_angle_in_radians >= radians(gx.sl_stop.value):
            #     gx.wt_log.text += ("Reading sequence complete")
            #     gx.running = False
            #     gx.run_button.text = "Run"
        # else:
        