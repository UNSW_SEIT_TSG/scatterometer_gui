## Mathematics Conventions
- Angles should be expressed as radians internally unless the code deals with rendering the value to the user.
- Floats are preferred over doubles.

## C# Style Guide used
In general I am following this guide: http://docs.godotengine.org/en/3.0/getting_started/scripting/c_sharp/c_sharp_style_guide.html.  The following is a brief summary.

### Class
```csharp
public class ClassName : Godot.Node
{
    GD.Print("Hello");
}
```

### Method
```csharp
public void MethodName(string camelCaseArgumentName)
{
    int camelCaseLocalVariable = 0;
    GD.Print(camelCaseArgumentName);
}`
```
### Property
```csharp
private bool _propertyName_ = false;
public bool PropertyName
{
    get
    {
        return _propertyName
    }
    set
    {
        _propertyName = value
    }
}
```

### Variables
```csharp
private _privateVariableName;
public PublicVariableName;
```
