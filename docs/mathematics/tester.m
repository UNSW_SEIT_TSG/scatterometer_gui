%% Section 1: determine scattering plane in sample frame
% user inputs alpha and beta as angles of scattering plane in x-z and y-z planes of sample
% gamma is angular increment between points chosen initially on the x-y plane

alphadeg=15; % degrees as an example
betadeg=0; % degrees as an example
gammadeg=10;% degrees as an example

pp=4;%incoming light 

alpha=alphadeg*pi/180;
beta=betadeg*pi/180;
gamma=gammadeg*pi/180;

% test points on unit circle in x-y plane:
rot1 = eulerZXZRotation( 0,beta,0 );% around x axis
rot2 = eulerZYZRotation(0,-alpha,0 );% around y axis
n=round(180/gammadeg);
x=[1,0,0]';
z=[0 ,0,0;0,0,1]';
for i=1:n+1
    a=(i-1)*gamma;
    rot0 = eulerZXZRotation( 0,0,a );% around z axis
    xyz([1,2,3],i)=rot0*x;
end

xyz(:,n+2)=xyz(:,1);

xyz1=rot1*xyz;
xyz2=rot2*xyz1;
rot=rot2*rot1;% rotation matrix that puts scattering points into sample frame

xyz3=rot*xyz;% check xyz3 == xyz2

rotaxis=rot*z;%[z,-z];% the axis of rotation perpedicular to the scattering plane (sample frame)

% plotting
p0=plot3(0,0,0,'ok','markersize',10,'markerfacecolor',[0,0,0]);
hold on;
% original points:
p1=plot3(xyz(1,:),xyz(2,:),xyz(3,:),'k.:');
zlim([-1,1]);
grid on
leg=legend('origin','points in x-y plane');

p2=plot3(xyz3(1,:),xyz3(2,:),xyz3(3,:),'ko:');
leg.String{end}='points in scattering plane of sample frame';
px=patch(xyz3(1,:)',xyz3(2,:)',xyz3(3,:)','k','FaceAlpha',.3);
px.Annotation.LegendInformation.IconDisplayStyle='off';

p3=plot3(rotaxis(1,:),rotaxis(2,:),rotaxis(3,:),'k','linewidth',1);
leg.String{end}='scattering plane axis (sample frame)';

hold off;
xlabel('X_{Lab}');
ylabel('Y_{Lab}');
zlabel('Z_{Lab}');

hold on;
sampleAxes=zeros(6,3);
sampleAxes(2,1)=1;sampleAxes(4,2)=1;sampleAxes(6,3)=1;
PSA=plot3(sampleAxes(:,1),sampleAxes(:,2),sampleAxes(:,3),'b','linewidth',2);
leg.String{end}='sample-frame axes';

xSample=text(0,0,0,'x_{sample}');
xSample.Position=sampleAxes(2,:);
ySample=text(0,0,0,'y_{sample}');
ySample.Position=sampleAxes(4,:);
zSample=text(0,0,0,'z_{sample}');
zSample.Position=sampleAxes(6,:);

view(130,30);
grid on;
sampleRect=zeros(4,3);
sampleRect(1,:)=[.5,0,.5];sampleRect(2,:)=[-.5,0,.5];
sampleRect(3,:)=[-.5,0,-.5];sampleRect(4,:)=[.5,0,-.5];
%patch(sampleRect(:,1),sampleRect(:,2),sampleRect(:,3),'b');
ptch=patch(sampleRect(:,1),sampleRect(:,2),sampleRect(:,3),'b','FaceAlpha',.3);
leg.String{end}='sample surface';
ptch.FaceColor=[.7,.7,1];
ptch.FaceAlpha=1;
ptch.EdgeColor=.3*[1,1,1];
%ptch.Annotation.LegendInformation.IconDisplayStyle='off';
%
hold off;
ax=gca;
ax.CameraViewAngle=5;
ax.DataAspectRatio=[1,1,1];
ax.Position(1)=0.3;
leg.Position=[0.485   0.802   0.5  0.18];
%% Section 2: go from xyz to r, theta, phi
rxy3=sqrt(xyz3(1,:).^2+xyz3(2,:).^2);
rxyz3=sqrt(sum(xyz3.^2));

rtp=[ rxyz3; atan2(rxy3,xyz3(3,:)); atan2(xyz3(2,:),xyz3(1,:))];
rotaxisRxy=sqrt(rotaxis(1,:).^2+rotaxis(2,:).^2);
rotaxisRxyz=sqrt(rotaxis(1,:).^2+rotaxis(2,:).^2+rotaxis(3,:).^2);
rotaxisRTP=[ rotaxisRxyz; atan2(rotaxisRxy,rotaxis(3,:)); atan2(rotaxis(2,:),rotaxis(1,:))];

%% Section 3: move scattering plane axis to the z axis, move other points the same
theta=rotaxisRTP(2,2);
phi=rotaxisRTP(3,2);
rot1 = eulerZXZRotation( 0,0,-phi);% around z axis (Group 7)
rot2 = eulerZYZRotation(0,-theta,0 );% around y axis (Group 5)

ra3=rot1*rotaxis;
ra4=rot2*rot1*rotaxis;
xyz4=rot2*rot1*xyz3;
%plot points:
hold on;
pra=plot3(ra4(1,:),ra4(2,:),ra4(3,:),'k--','linewidth',1);
pra.Annotation.LegendInformation.IconDisplayStyle='off';
p4=plot3(xyz4(1,:),xyz4(2,:),xyz4(3,:),'ko:');
p2.Marker=p1.Marker;
p2.MarkerSize=p1.MarkerSize;
leg.String{end}='points after two rotations';
px.Vertices=xyz4';
hold off;

% plot the sample surface at intermediate point and after full rotation
sampleRect3=rot1*sampleRect';% intermediate point
sampleRect3=sampleRect3';
sampleRect4=rot2*rot1*sampleRect';
sampleRect4=sampleRect4';
sampleAxes3=rot1*sampleAxes';% intermediate point
sampleAxes3=sampleAxes3';
sampleAxes4=rot2*rot1*sampleAxes';
sampleAxes4=sampleAxes4';

hold on;
PSA.Color=[0.9,0.9,1];% light blue 
% PSA3=plot3(sampleAxes3(:,1),sampleAxes3(:,2),sampleAxes3(:,3),...
%     'color',[0.75,0.75,1],'linewidth',2);
% PSA3.Annotation.LegendInformation.IconDisplayStyle='off';
PSA4=plot3(sampleAxes4(:,1),sampleAxes4(:,2),sampleAxes4(:,3),...
    'color',[0.5,0.5,1],'linewidth',2);
PSA4.Annotation.LegendInformation.IconDisplayStyle='off';
% ptch3=patch(sampleRect3(:,1),sampleRect3(:,2),sampleRect3(:,3),'b','FaceAlpha',.15);
% ptch3.LineStyle=':';
% ptch3.Annotation.LegendInformation.IconDisplayStyle='off';
ptch4=patch(sampleRect4(:,1),sampleRect4(:,2),sampleRect4(:,3),'b');
ptch4.FaceColor= ptch.FaceColor;
ptch4.FaceAlpha= ptch.FaceAlpha;
ptch4.LineStyle= ptch.LineStyle;
ptch4.Annotation.LegendInformation.IconDisplayStyle='off';
ptch.FaceAlpha=0.1;ptch.LineStyle=':';

xSample.Position=sampleAxes4(2,:);
ySample.Position=sampleAxes4(4,:);
zSample.Position=sampleAxes4(6,:);
hold off;
%% Section 4: move one particular point (pp) to the YLab-axis to be the incoming light direction
phipp=atan2(xyz4(2,pp),xyz4(1,pp));% angle to x-axis
rotz = eulerZXZRotation( 0,0,-phipp+pi/2);% around z axis (Group 2) to y axis
xyz5=rotz*xyz4;
sampleRect5=rotz*sampleRect4';
sampleRect5=sampleRect5';

sampleAxes5=rotz*sampleAxes4';
sampleAxes5=sampleAxes5';
%
hold on;
ptch5=patch(sampleRect5(:,1),sampleRect5(:,2),sampleRect5(:,3),'b','FaceAlpha',.35);
ptch5.FaceColor= ptch4.FaceColor;
ptch5.FaceAlpha= ptch4.FaceAlpha;
ptch5.LineStyle= ptch4.LineStyle;
ptch4.FaceAlpha=0.1;ptch4.LineStyle=':';
ptch5.Annotation.LegendInformation.IconDisplayStyle='off';
%
PSA4.Color=[0.75,0.75,1];% light blue 
PSA5=plot3(sampleAxes5(:,1),sampleAxes5(:,2),sampleAxes5(:,3),'color',[1,0.5,0.5],'linewidth',2);
PSA5.Annotation.LegendInformation.IconDisplayStyle='off';

xSample.Position=sampleAxes5(2,:);
ySample.Position=sampleAxes5(4,:);
zSample.Position=sampleAxes5(6,:);
%
p5=plot3(xyz5(1,:),xyz5(2,:),xyz5(3,:),'r.:','markersize',20);
leg.String{end}='designated incoming point moved to y-axis';

plot3([0 0],[0 1],[0 0],'g-','linewidth',5)
leg.String{end}='incoming light ray';
hold off;
px.Vertices=xyz5';
%% the final rotation matrix to get scattering points into lab x-y plane 
%    from the user-designated sample-frame points determined in section 1
rotfinal=rotz*rot2*rot1; 

% this matrix is related to the invers of the original matrix multiplied by
% the number of gamma (z-axis) rotations corresponding to the chosen
% incoming vector.
% to move between the points, rotate detector around the z-axis in
% increments of gamma:
rotZ=[cos(gamma) -sin(gamma) 0; sin(gamma) cos(gamma) 0; 0 0 1];
m=round((n+1)/2)-pp;
rr=rotfinal;
for i=1:m
    rr=rotZ'*rr;
end
round(1000*rot)==round(1000*rr')
%% 