function [ rot ] = eulerZYZRotation( alpha,beta,gamma )
%eulerZYZRotation: given three angles, return the ZYZ matrix that will rotate
%a coordinate system around the Z axis by alpha, around the new Y-axis by
%beta, then around the new Z axis by gamma.(angles given in radians)
%   this can be used to rotate a vector, the result being the new location
%   in the old coordinate system. To use it as a tranform where the 
%   coordinate sytems rotates, use the transpose or the angles to rotate the
%   vector in the opposite sense (sign and order): (-gamma, -beta, -alpha), 
%   and the result is the same vector expressed in the new coordinate system.

c1=cos(alpha);
c2=cos(beta);
c3=cos(gamma);
s1=sin(alpha);
s2=sin(beta);
s3=sin(gamma);
rot=[c1*c2*c3-s1*s3, -c1*c2*s3-c3*s1, c1*s2; ...
    c1*s3+c3*c2*s1,  c1*c3-s1*c2*s3,  s1*s2; ...
    -c3*s2,  s3*s2,  c2];
end