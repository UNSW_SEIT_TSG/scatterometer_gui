[x, y] = meshgrid(-5:0.5:5);  
Zv = @(x,y) 1 - x - y;
mesh(x,y,Zv(x,y));

hold on

[x, z] = meshgrid(-5:0.5:5);
Yv = @(x) 2*x;
mesh(x,Yv(x),z);

hold off