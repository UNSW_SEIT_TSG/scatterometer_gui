"""Module for handling euler rotations.
Functions euler_z_y_z_rotation() and euler_z_x_z_rotation()
translated to python from Joe Kurtz' matlab code."""
from math import cos, sin
import numpy as np
def euler_z_y_z_rotation(α, β, γ):
    """euler_z_y_z_rotation
    given three angles [radians]:
        α,
        β,
        γ,
    return:
        ZYZ matrix that will rotate a coordinate system around:
            the Z axis by α,
            the new Y-axis by β, then
            the new Z axis by γ.
    
    This can be used to rotate a vector, the result being the new location
    in the old coordinate system. To use it as a tranform where the
    coordinate sytems rotates, use the transpose or the angles to rotate the
    vector in the opposite sense (sign and order): (-γ, -β, -α),
    and the result is the same vector expressed in the new coordinate system."""
    c_α = cos(α)
    c_β = cos(β)
    c_γ = cos(γ)
    s_α = sin(α)
    s_β = sin(β)
    s_γ = sin(γ)
    return np.array([
        [c_α*c_β*c_γ - s_α*s_γ, -c_α*c_β*s_γ - s_α*c_γ, c_α*s_β],
        [s_α*c_β*c_γ + c_α*s_γ, -s_α*c_β*s_γ + c_α*c_γ, s_α*s_β],
        [   -s_β*c_γ,                s_β*s_γ,               c_β]
    ])


def euler_z_x_z_rotation(α:float, β:float, γ:float):
    """euler_z_x_z_rotation
    given three angles [radians]:
        α,
        β,
        γ,
    return:
        ZXZ matrix that will rotate a coordinate system around:
            the Z axis by α,
            the new X-axis by β, then
            the new Z axis by γ.
    
    This can be used to rotate a vector, the result being the new location
    in the old coordinate system. To use it as a tranform where the
    coordinate sytems rotates, use the angles to rotate the
    vector in the opposite sense (sign and order): (-γ, -β, -α),
    and the result is the same vector expressed in the new coordinate system."""
    c_α = cos(α)
    c_β = cos(β)
    c_γ = cos(γ)
    s_α = sin(α)
    s_β = sin(β)
    s_γ = sin(γ)
    return np.array([
        [c_α*c_γ-c_β*s_α*s_γ, -c_α*s_γ-c_γ*c_β*s_α,  s_α*s_β],
        [c_β*c_α*s_γ+c_γ*s_α,  c_α*c_β*c_γ-s_α*s_γ, -c_α*s_β],
        [s_γ*s_β, c_γ*s_β, c_β]
    ])
