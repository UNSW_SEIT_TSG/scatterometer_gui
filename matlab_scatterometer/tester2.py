"""Port of matlab code to calculate motor angles from scatter plane and
sample plane orientations.
"""
import numpy as np

from collections import namedtuple
from math import pi as π


from euler_rotation import euler_z_x_z_rotation, euler_z_y_z_rotation
from fself import fself


class Angle:
    def __init__(self, *, degrees=0, radians=0):
        """Creates an Angle object."""
        if degrees != 0 and radians != 0:
            raise ValueError("cannot nominate both degrees and radians")
        elif degrees != 0 and radians == 0:
            self._degrees = degrees
            self._radians = self.radians_from_degrees(degrees)
        elif degrees == 0 and radians != 0:
            self._radians = radians
            self._degrees = self.degrees_from_radians(radians)
        else:
            self._degrees = 0
            self._radians = 0
    def getDegrees(self):
        return self._degrees
    def getRadians(self):
        return self._radians
    def setDegrees(self, value):
        self._degrees = value
        self._radians = self.radians_from_degrees(value)
    def setRadians(self, value):
        self._radians = value
        self._degrees = self.degrees_from_radians(value)
    def radians_from_degrees(self, degrees):
        """Converts an angle in degrees to an angle in radians."""
        return degrees*π/180.0
    def degrees_from_radians(self, radians):
        """Converts an angle in radians to an angle in degrees."""
        return radians*180.0/π
    def __str__(self):
        return f'{self._degrees:.3f}° = {self._radians/π:.3f}π[rad] = {self._radians:.3f} [rad]'
    def __sub__(self, other):
        return Angle(degrees=self._degrees-other.degrees)
    def __add__(self, other):
        return Angle(degrees=self._degrees+other.degrees)
    def __mul__(self, other):
        if isinstance(other, Angle):
            raise NotImplementedError(fself().__doc__)
        else:
            return Angle(degrees=self._degrees*other)
    def __truediv__(self, other):
        if isinstance(other, Angle):
            quotient = (self._degrees*1.0)/(other.degrees*1.0)
            if quotient == quotient - quotient % 1:
                return round(quotient)
            else:
                return quotient
        else:
            return Angle(degrees=(self._degrees*1.0)/(other*1.0))
    degrees = property(fget=getDegrees, fset=setDegrees, doc="Degrees property")
    radians = property(fget=getRadians, fset=setRadians, doc="Radians property")

class RotaryStage:
    def __init__(self, θ_min: Angle, θ_max: Angle, θ_datum: Angle, polarity: int=1):
        self.θ_min = θ_min # Cannot rotate below this angle
        self.θ_max = θ_max # Cannot rotate above this angle
        self.θ_datum = θ_datum # Zero point wrt sample-frame y-axis.
        self.polarity = polarity # 1: Right-handed; -1: Left-handed rotation.
    def __str__(self):
        return ''.join([
            f'θ_min: {self.θ_min} θ_max: {self.θ_max} θ_datum: {self.θ_datum}',
            f'polarity: {self.polarity}'
        ])

def cos(θ: Angle):
    return kos(θ.radians)

def sin(θ: Angle):
    return zin(θ.radians)

def eulerZXZRotation():
    raise NotImplementedError(fself().__doc__)

def pprint(title, value):
    print(title)
    print(value)
    print()

def section_0(γ: Angle, γ_start: Angle, γ_final: Angle):
    """Section 0: Inputs and Limits
    Inputs:
        γ: measurement increment angle in x-y plane
        γ_start: angle of inbound light ray, also initial measurement angle.
        γ_final: final measurement angle
    """
    Result = namedtuple(
        'Result',
        [
            'reading_count',
            'γ_final',
            'γ_start_rad',
            'γ_rad',
            'measurement_point_index'
        ]
    )
    # print("γ_final:"+str(γ_final))
    # print("γ_start:"+str(γ_start))
    span = γ_final-γ_start
    # print(span)
    reading_count = span/γ
    # print("reading_count:"+str(reading_count))
    γ_final = γ_start+γ*reading_count
    # print("γ_final:"+str(γ_final))
    γ_start_rad = γ_start.radians
    # print("γ_start_rad:"+str(γ_start_rad))
    γ_rad = γ.radians
    # print("γ_rad:"+str(γ_rad))
    measurement_point_index = 1 # γ_start_rad ray == 1st measurement point
    return Result(
        reading_count=reading_count,
        γ_final=γ_final,
        γ_start_rad=γ_start_rad,
        γ_rad=γ_rad,
        measurement_point_index=measurement_point_index
    )

def section_1(α: Angle, β: Angle, reading_count: int, γ: Angle, γ_start: Angle, measurement_point_index: int):
    """Section 1: determine scattering plane in sample frame
    Inputs:
        α: angle formed by intersection of scattering plane with x-z plane
        β: angle formed by intersection of scattering plane with y-z plane
    """

    # Rotations to orient sample to scattering plane:
    rotation_1 = euler_z_x_z_rotation(0, β.radians, 0) # rotation around x axis
    rotation_2 = euler_z_y_z_rotation(0, -α.radians, 0) # rotation around y axis

    # the two vectors representing the intersections of the scattering plane
    # with the x-z and y-z planes:
    x = np.array([
        [0, 0, 0],
        [1, 0, 0]
    ]).T # oritin pt, endpoint
    y = np.array([
        [0, 0, 0],
        [0, 1, 0]
    ]).T # oritin pt, endpoint

    yz = np.dot(rotation_1, y) # rotate x-axis +β around x-axis
    xz = np.dot(rotation_2, x) # rotate x-axis -α around y-axis

    # the cross product of the two vectors is 
    rotaxis = np.cross(xz.T, yz.T).T #the axis normal to the scattering plane
    l = np.sqrt(sum(np.power(rotaxis[:,1],2)))
    rotaxis = rotaxis / l # normalize to unit length

    # decompose the rotaxis to spherical coords: r,θ,φ
    
    rotaxisRxy = np.sqrt(
        np.power(rotaxis[0, :], 2)+
        np.power(rotaxis[1, :], 2)
    )

    rotaxisRxyz = np.sqrt(
        np.power(rotaxis[0, :], 2)+
        np.power(rotaxis[1, :], 2)+
        np.power(rotaxis[2, :], 2)
    )

    rotaxisRTP = np.array([
        rotaxisRxyz,
        np.arctan2(rotaxisRxy, rotaxis[2, :]),
        np.arctan2(rotaxis[1, :], rotaxis[0, :])
    ])
    

    #
    # the rotations of the z-axis to move it to rotaxis: 
    θ = rotaxisRTP[1, 1]
    φ = rotaxisRTP[2, 1]

    # Calculate points for plotting:
    # 1) the measurement points in scattering plane using Rodrigues' rotation formula
    v0 = xz[:, 1] # starting point
    v1 = np.cross(rotaxis[:, 1], v0)
    
    # pprint("rotaxis[:, 1]:", rotaxis[:, 1])
    # pprint("np.dot(rotaxis[:, 1],v0:", np.dot(rotaxis[:, 1],v0))

    v2 = np.multiply(
        rotaxis[:, 1],
        np.dot(rotaxis[:, 1],v0)
    )
    
    j = 0
    xyzSample = np.zeros(3)
    for i in range(reading_count):
        angle = γ*i+γ_start
        
        # measurement points in the sample's frame:
        t = v0*cos(angle)+v1*sin(angle)+v2*(1-cos(angle))
        # print(i, angle, t, t[1],  sep="\t")
        
        if t[1] >= 0: #  keep only points in front of sample
            xyzSample = np.vstack((xyzSample, t))
            j += 1
    xyzSample = xyzSample.T
    # print(xyzSample)
    measurement_point_index = measurement_point_index+1
    # print(measurement_point_index)

    # 2) the sample coordinate axes
    sampleAxes = np.zeros((6,3))
    sampleAxes[1, 0] = 1
    sampleAxes[3, 1] = 1
    sampleAxes[5, 2] = 1

    # 3) the sample rectangle points
    sampleRect = np.array([
        [1, 0, 1],
        [-1, 0, 1],
        [-1, 0, -1],
        [1, 0, -1]
    ])
    sampleRect = sampleRect/2
    return rotaxisRTP, rotaxis, xyzSample, sampleRect, sampleAxes, measurement_point_index
def getLimits(group):
    return np.array([
        group.θ_min.degrees - group.θ_datum.degrees,
        group.θ_max.degrees - group.θ_datum.degrees,
    ])

def gonogo(group, θ_target):
    limits = getLimits(group)
    if θ_target.degrees>limits[0] and θ_target.degrees<limits[1]:
        return 1
    elif θ_target.degrees>limits[1] and (θ_target.degrees-360)>limits[0]:
        θ_target.degrees = θ_target.degrees-360
        return 1
    elif θ_target.degrees<limits[0] and (θ_target.degrees+360)<limits[1]:
        θ_target.degrees = θ_target.degrees+360
        return 1
    else:
        return 0

def section_2(rotaxisRTP, group_7):
    """Section 2: determine group 7 rotation and test limits"""
    φ = rotaxisRTP[2,1]
    rotG7 = Angle(radians=group_7.polarity*(-φ))
    # print(rotG7)

    # test to see if φ is within Group 7 limits
    gonogo7 = gonogo(group_7, rotG7)

    rotation_1 = euler_z_x_z_rotation(0, 0, group_7.polarity*rotG7.radians) # around z axis 
    # pprint("rotation_1", rotation_1)
    # note: absolute angle to send to Group7 motor = rotG7.degrees+datumG7
    return rotation_1

def section_3(rotaxisRTP, group_5):
    """Section 3: determine group 5 rotation and test limits"""
    θ = rotaxisRTP[1,1]
    # pprint("θ", θ)

    rotG5 = Angle(radians=group_5.polarity*(-θ))
    # pprint("rotG5", rotG5)


    # test to see if θ is within Group 5 limits
    gonogo5 = gonogo(group_5, rotG5)
    
    rotation_2 = euler_z_y_z_rotation(0, group_5.polarity*rotG5.radians, 0) # around y axis
    # pprint("rotation_2", rotation_2) 
    # note: angle to send to Group5 motor = rotG2deg+datumG2 (absolute)
    return rotation_2

def section_4(rotation_1, rotaxis, rotation_2, xyzSample, sampleRect, sampleAxes):
    """Section 4: move sample points for plotting Group 7 and Group 5 rotations"""
    # move scattering plane rotaxis to the z axis,

    ra3 = rotation_1.dot(rotaxis) # group 7 rotation
    # pprint("ra3", ra3)

    ra4 = rotation_2.dot(rotation_1.dot(rotaxis)) # group 5 rotation
    # pprint("ra4", ra4)


    # the measurement points 
    xyz3 = rotation_1.dot(xyzSample)
    # pprint("xyz3", xyz3)

    xyz4 = rotation_2.dot(rotation_1.dot(xyzSample))
    # pprint("xyz4", xyz4)


    # the sample surface 
    sampleRect3 = (rotation_1.dot(sampleRect.T)).T
    # pprint("sampleRect3", sampleRect3)

    sampleRect4 = rotation_2.dot(rotation_1.dot(sampleRect.T)).T
    # pprint("sampleRect4", sampleRect4)

    # the sample coordinate axes 
    sampleAxes3 = rotation_1.dot(sampleAxes.T).T
    # pprint("sampleAxes3", sampleAxes3)

    sampleAxes4 = rotation_2.dot(rotation_1.dot(sampleAxes.T)).T
    # pprint("sampleAxes4", sampleAxes4)
    return xyz4


def section_5(xyz4, measurement_point_index, group_2):
    """Section 5: select one particular point (measurement_point_index) to move to the YLab-axis (Group 2)"""
    # to be the γ_start_rad light direction
    φ_2_z = Angle(radians=atan2(xyz4[1, measurement_point_index-1], xyz4[0, measurement_point_index-1]))
    # pprint("φ_2_z", φ_2_z)

    rotG2 = Angle(radians=group_2.polarity*(-φ_2_z.radians+π/2))
    # pprint("rotG2", rotG2)

    # test to see if φ_2 is within Group 2 limits
    gonogo2 = gonogo(group_2, rotG2)
    # pprint("gonogo2", gonogo2)
    # pprint("rotG2", rotG2)
    # note: angle to send to Group2 motor = rotG2deg+datumG2
        
    rotz = euler_z_x_z_rotation(0, 0, group_2.polarity*rotG2.radians) # RHR around z axis 
    # pprint("rotz", rotz)
    return rotz

def section_6(rotz, xyz4):
    """Section 6: move points for plotting after Group 2 rotation"""
    # the measurement points 
    xyz5 = rotz.dot(xyz4)
    pprint("xyz5", xyz5)

if __name__ == "__main__":
    groups = {
        'g_2':  RotaryStage(
                    θ_min=Angle(degrees=-174),
                    θ_max=Angle(degrees=174),
                    θ_datum=Angle(degrees=160),
                    polarity=-1
                ),
        'g_5':  RotaryStage(
                    θ_min=Angle(degrees=-115),
                    θ_max=Angle(degrees=159),
                    θ_datum=Angle(degrees=2.2),
                    polarity=-1
                ),
        'g_7':  RotaryStage(
                    θ_min=Angle(degrees=-170),
                    θ_max=Angle(degrees=170),
                    θ_datum=Angle(degrees=2.2),
                    polarity=-1
                )
    }
    γ = Angle(degrees=10)
    γ_start = Angle(degrees=25)
    γ_final = Angle(degrees=155)

    result = section_0(
        γ=γ,
        γ_start=γ_start,
        γ_final=γ_final
    )
    r_axis_rtp, rotaxis, xyzSample, sampleRect, sampleAxes, measurement_point_index = section_1(
        α=Angle(degrees=-20),
        β=Angle(degrees=45),
        reading_count=result.reading_count,
        γ=γ,
        γ_start=γ_start,
        measurement_point_index=result.measurement_point_index
    )
    rotation_1 = section_2(rotaxisRTP=r_axis_rtp, group_7=groups['g_7'])
    rotation_2 = section_3(rotaxisRTP=r_axis_rtp, group_5=groups['g_5'])
    xyz4 = section_4(
        rotation_1=rotation_1,
        rotaxis=rotaxis,
        rotation_2=rotation_2,
        xyzSample=xyzSample,
        sampleRect=sampleRect,
        sampleAxes=sampleAxes
    )
    rotz = section_5(xyz4=xyz4, measurement_point_index=measurement_point_index, group_2=groups['g_2'])
    section_6(rotz=rotz, xyz4=xyz4)
