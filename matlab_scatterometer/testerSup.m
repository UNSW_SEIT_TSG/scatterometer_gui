hold on;
linz=plot3([rotaxis(1,2) rotaxis(1,2)],[rotaxis(2,2) rotaxis(2,2)],[rotaxis(3,2) 0],'--','linewidth',1.);
linz.Annotation.LegendInformation.IconDisplayStyle='off';
linxy=plot3([0 rotaxis(1,2)],[0 rotaxis(2,2)],[0 0],'--','linewidth',1.4);
linxy.Annotation.LegendInformation.IconDisplayStyle='off';
linxy.Color=linz.Color;
hold off;
%%
hold on;
l=sqrt([rotaxis(1,2) rotaxis(2,2) 0]*[rotaxis(1,2) rotaxis(2,2) 0]')*.75;
arcc=horzcat(arcit(phi,20,[1,0,0]'*l,[0,0,1]'));%,[rotaxis(1,2) rotaxis(2,2) 0]');
pintc=plot3(arcc(1,:),arcc(2,:),arcc(3,:),'linewidth',1);
pintc.Color=linxy.Color;
leg.String{end}=horzcat('\color[rgb]{',num2str(pintc.Color),'}\phi = ',num2str(phi*180/pi,3));

hold off;
%%

hold on;
l=sqrt(rotaxis(:,2)'*rotaxis(:,2))*.75;
arcd=arcit(theta,20,[0,0,1]'*l,[0,01,0]');
ra = eulerZXZRotation(0,0,phi );
arcd=ra*arcd;
pintd=plot3(arcd(1,:),arcd(2,:),arcd(3,:),'linewidth',1);
leg.String{end}=horzcat('\color[rgb]{',num2str(pintd.Color),'}\theta = ',num2str(theta*180/pi,3));
leg.Position(2)=leg.Position(2)-.05;
hold off;
