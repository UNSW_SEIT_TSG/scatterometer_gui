"""Port of matlab code to calculate motor angles from scatter plane and
sample plane orientations.
"""
import numpy as np
from angle import Angle, cos, sin, atan2, π
from angle import euler_z_x_z_rotation as euler_z_x_z
from angle import euler_z_y_z_rotation as euler_z_y_z
from rotary_stage import RotaryStage

def xz_from_rot(rotation):
    """Returnz xz from rotation."""
    return np.dot(rotation, np.array([[0, 1], [0, 0], [0, 0]]))

def yz_from_rot(rotation):
    """Returnz yz from rotation."""
    return np.dot(rotation, np.array([[0, 0], [0, 1], [0, 0]]))

def rot_axis_Rxy_from_rot_axis(rot_axis):
    return np.sqrt(sum(np.power(rot_axis[:2,:], 2)))

def rot_axis_Rxyz_from_rot_axis(rot_axis):
    return np.sqrt(sum(np.power(rot_axis, 2)))

def getRotAxis(α, β):
    ο = Angle(deg=0)
    yz = yz_from_rot(euler_z_x_z(ο, β, ο))
    xz = xz_from_rot(euler_z_y_z(ο, -α, ο))

    rot_axis = np.cross(xz.T, yz.T).T
    rot_axis_normalised = rot_axis / np.sqrt(sum(np.power(rot_axis[:, 1], 2)))
    return rot_axis_normalised

def getV0(α):
    ο = Angle(deg=0)
    return xz_from_rot(euler_z_y_z(ο, -α, ο))[:, 1]

# def section_1_θ(ι):


def section_1(ι):
    """Section 1: determine scattering plane in sample frame
    Inputs:
        α: angle formed by intersection of scattering plane with x-z plane
        β: angle formed by intersection of scattering plane with y-z plane
    """

    rotaxis = getRotAxis(ι.α, ι.β)
    rotaxisRxy = rot_axis_Rxy_from_rot_axis(rotaxis)
    rotaxisRxyz = rot_axis_Rxyz_from_rot_axis(rotaxis)

    rotaxisRTP = np.array([
        rotaxisRxyz,
        np.arctan2(rotaxisRxy, rotaxis[2, :]),
        np.arctan2(rotaxis[1, :], rotaxis[0, :])
    ])

    xyzSample = np.zeros(3)
    v0 = getV0(ι.α)
    v1 = np.cross(rotaxis[:, 1], v0)
    v2 = np.multiply(rotaxis[:, 1], np.dot(rotaxis[:, 1], v0))
    for i in range(ι.reading_count):
        ω = ι.γ_step*i+ι.γ_start
        t = v0*cos(ω)+v1*sin(ω)+v2*(1-cos(ω))
        if t[1] >= 0:
            xyzSample = np.vstack((xyzSample, t))
    xyzSample = (xyzSample.T)[:, 1:]
    θ = Angle(rad=rotaxisRTP[1, 1])
    φ = Angle(rad=rotaxisRTP[2, 1])

    return θ, φ, xyzSample

def section_2_rotG7(φ, group_7):
    """Section 2: determine group 7 rotation and test limits"""
    return -φ*group_7.polarity

def section_3_rotG5(θ, group_5):
    """Section 3: determine group 5 rotation and test limits"""
    return -θ*group_5.polarity

def section_2_rotation_1(rotG7, group_7):
    """Section 2: determine group 7 rotation and test limits"""
    return euler_z_x_z(Angle(rad=0), Angle(rad=0), rotG7*group_7.polarity)

def section_3_rotation_2(rotG5, group_5):
    """Section 3: determine group 5 rotation and test limits"""
    return euler_z_y_z(Angle(rad=0), rotG5*group_5.polarity, Angle(rad=0))

def section_4(rotation_1, rotation_2, xyzSample):
    """Section 4: move sample points for plotting Group 7 and Group 5 rotations"""
    return rotation_2.dot(rotation_1.dot(xyzSample))

def section_5(xyz4, measurement_index, group_2):
    """Section 5: select point (measurement_index) to move to the YLab-axis (Group 2)"""
    φ_2_z = atan2(xyz4[1, measurement_index-1], xyz4[0, measurement_index-1])
    left = group_2.polarity
    right = (-φ_2_z.rad+π/2)
    temp = left*right
    rotG2 = Angle(rad=temp)
    return rotG2

def calcTargetsFromInput(inp):
    """Main"""
    θ, φ, xyzSample = section_1(inp)

    g7_targ = section_2_rotG7(φ=φ, group_7=groups['g_7'])
    rotation_1 = section_2_rotation_1(rotG7=g7_targ, group_7=groups['g_7'])

    g5_targ = section_3_rotG5(θ=θ, group_5=groups['g_5'])
    rotation_2 = section_3_rotation_2(rotG5=g5_targ, group_5=groups['g_5'])

    xyz4 = section_4(rotation_1=rotation_1, rotation_2=rotation_2, xyzSample=xyzSample)
    g2_targ = section_5(xyz4=xyz4, measurement_index=inp.measurement_index, group_2=groups['g_2'])

    groups['g_2'].θ_target = g2_targ
    groups['g_5'].θ_target = g5_targ
    groups['g_7'].θ_target = g7_targ

    return groups

def printResult(groups):
    print(
        '\n'.join([
            f'g2_target: {groups["g_2"].θ_target}',
            f'g5_target: {groups["g_5"].θ_target}',
            f'g7_target: {groups["g_7"].θ_target}'
        ])
    )

    if groups["g_2"].go_no_go != 1:
        print("g_2 No go! <<<<<<")
    if groups["g_5"].go_no_go != 1:
        print("g_5 No go! <<<<<<")
    if groups["g_7"].go_no_go != 1:
        print("g_7 No go! <<<<<<")

    print()

class Input:
    """Collection of Input data as an object."""
    def __init__(self, α, β, γ_start, γ_final, reading_count):
        self.α = α
        self.β = β
        self.γ_start = γ_start
        self.γ_final = γ_final
        self.reading_count = reading_count
        self.span = γ_final - γ_start
        self.γ_step = Angle(deg=self.span.deg/reading_count)
        self.measurement_index = 1

if __name__ == "__main__":
    groups = {
        'g_2':  RotaryStage(
            θ_min=Angle(deg=-174),
            θ_max=Angle(deg=174),
            θ_datum=Angle(deg=160),
            polarity=-1
        ),
        'g_5':  RotaryStage(
            θ_min=Angle(deg=-115),
            θ_max=Angle(deg=159),
            θ_datum=Angle(deg=2.2),
            polarity=-1
        ),
        'g_7':  RotaryStage(
            θ_min=Angle(deg=-170),
            θ_max=Angle(deg=170),
            θ_datum=Angle(deg=2.2),
            polarity=-1
        )
    }

    ι = Input(
        α=Angle(deg=-20),
        β=Angle(deg=45),
        γ_start=Angle(deg=25),
        γ_final=Angle(deg=155),
        reading_count=13
    )

    for x in range(ι.reading_count):
        ι.measurement_index = x
        print(f'Reading: {x}')
        calcTargetsFromInput(ι)
        printResult(groups)

