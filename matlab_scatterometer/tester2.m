%% Section 0: inputs and limits
% user inputs alpha and beta as angles of scattering plane in x-z and y-z planes of sample
% gamma is angular increment between points chosen initially on the x-y plane
% cd '\\seit0237\root\Joe''s My Documents - laptop working directory\scatterometer';
close all;


% degrees as an example
alphadeg=-20;

% degrees as an example
betadeg=45;

% increment of measurement angle
gammadeg=10;

% starting measurement angle = incoming light angle
incomingdeg=25;

% ending measurement angle
endgammadeg=155;

% the scatterometer has limited angular rotation due to either mechanical
% or optical restrictions.

% limits on Group2 rotation (degrees) (mechanical)
limG2deg=[-174,174];

% the zero point with respect to sample-frame y-axis (sample normal)
datumG2=160;

% Group2 rotates left-handed
signG2=-1;


% limits on Group5 rotation (degrees) (mechanical)
limG5deg=[-115,159];

% the zero point with respect to sample-frame y-axis (sample normal)
datumG5=2.2;

% Group5 rotates left-handed
signG5=-1;


% limits on Group7 rotation (degrees) (mechanical or software?)
limG7deg=[-170 170];

% the zero point with respect to sample-frame y-axis (sample normal)
datumG7=2.2;

% Group5 rotates left-handed
signG7=-1;

alpha=alphadeg*pi/180;
beta=betadeg*pi/180;
ngamma=round((endgammadeg-incomingdeg)/gammadeg);
endgamma=(incomingdeg+ngamma*gammadeg)*pi/180;
incoming=incomingdeg*pi/180;
gamma=gammadeg*pi/180;

% incoming ray == 1st measurement point
pp=1;

%% Section 1: determine scattering plane in sample frame

% two rotations required to get sample point to scattering plane:

% around x axis
rot1 = eulerZXZRotation( 0,beta,0 );

% around y axis
rot2 = eulerZYZRotation(0,-alpha,0 );

% the two vectors representing the interections of the scattering plane
% with the x-z and y-z planes:

% oritin pt, endpoint
x=[0,0,0; 1,0,0]';

% oritin pt, endpoint
y=[0,0,0; 0,1,0]';


% rotate x-axis -alpha around y-axis
xz=rot2*x;

% rotate x-axis +beta around x-axis
yz=rot1*y;

% the cross product of the two vectors is 

%the axis normal to the scattering plane
rotaxis=cross(xz,yz);
l=sqrt(sum(rotaxis.^2,1));

% normalize to unit length
rotaxis=rotaxis./l(2);

% decompose the rotaxis to spherical coords: r,theta,phi
rotaxisRxy=sqrt(rotaxis(1,:).^2+rotaxis(2,:).^2);
rotaxisRxyz=sqrt(rotaxis(1,:).^2+rotaxis(2,:).^2+rotaxis(3,:).^2);
rotaxisRTP=[ rotaxisRxyz; atan2(rotaxisRxy,rotaxis(3,:)); atan2(rotaxis(2,:),rotaxis(1,:))];
%
% the rotations of the z-axis to move it to rotaxis: 
theta=rotaxisRTP(2,2);
phi=rotaxisRTP(3,2);

% Calculate points for plotting:
% 1) the measurement points in scattering plane using Rodrigues' rotation formula

% starting point
v0=xz(:,2);
v1=cross(rotaxis(:,2),v0);
v2=rotaxis(:,2).*(dot(rotaxis(:,2),v0));

j=0;
for i=1:ngamma
    angle=gamma*(i-1)+incoming;
    % measurement points in the sample's frame:
    t=v0*cos(angle)+v1*sin(angle)+v2*(1-cos(angle));
    
    if (t(2)>=0) %  keep only points in front of sample
        j=j+1;
        xyzSample(:,j)=t;
        % break;
    end
end
xyzSample=xyzSample(:,1:j);

% origin points added for plotting
xyzSample=horzcat([0;0;0],xyzSample,[0;0;0]);
pp=pp+1;

% 2) the sample coordinate axes
sampleAxes=zeros(6,3);
sampleAxes(2,1)=1;sampleAxes(4,2)=1;sampleAxes(6,3)=1;

% 3) the sample rectangle points
sampleRect=zeros(4,3);
sampleRect(1,:)=[.5,0,.5];sampleRect(2,:)=[-.5,0,.5];
sampleRect(3,:)=[-.5,0,-.5];sampleRect(4,:)=[.5,0,-.5];

%% Section 2: determine group 7 rotation and test limits
phi=rotaxisRTP(3,2);
rotG7=signG7*(-phi);
rotG7deg=rotG7*180/pi;

% test to see if phi is within Group 7 limits
limits=limG7deg-datumG7;

% no go
gonogo7=0;
if rotG7deg>limits(1) && rotG7deg<limits(2)
    gonogo7=1;
    % disp(1);
elseif rotG7deg>limits(2) && (rotG7deg-360)>limits(1)
    rotG7deg=rotG7deg-360;
    gonogo7=1;
    % disp(2);
elseif rotG7deg<limits(1) && (rotG7deg+360)<limits(2)
    rotG7deg=rotG7deg+360;
    gonogo7=1;
    % disp(3);
end
rotG7=rotG7deg*pi/180;

% around z axis 
rot1 = eulerZXZRotation( 0,0,signG7*rotG7);
% note: absolute angle to send to Group7 motor = rotG7deg+datumG7

%% Section 3: determine group 5 rotation and test limits
theta=rotaxisRTP(2,2);
rotG5=signG5*(-theta);
rotG5deg=rotG5*180/pi;

% test to see if theta is within Group 5 limits
limits=limG5deg-datumG5;

% no go
gonogo5=0;
if rotG5deg>limits(1) && rotG5deg<limits(2)
    gonogo5=1;
    % disp(1);
elseif rotG5deg>limits(2) && (rotG5deg-360)>limits(1)
    rotG5deg=rotG5deg-360;
    gonogo5=1;
    % disp(2);
elseif rotG5deg<limits(1) && (rotG5deg+360)<limits(2)
    rotG5deg=rotG5deg+360;
    gonogo5=1;
    % disp(3);
end
rotG5=rotG5deg*pi/180;

% around y axis 
rot2 = eulerZYZRotation(0, signG5*rotG5, 0);
% note: angle to send to Group5 motor = rotG2deg+datumG2 (absolute)


%% Section 4: move sample points for plotting Group 7 and Group 5 rotations
% move scattering plane rotaxis to the z axis,

% group 7 rotation
ra3=rot1*rotaxis;

% group 5 rotation
ra4=rot2*rot1*rotaxis;

% the measurement points 
xyz3=rot1*xyzSample;
xyz4=rot2*rot1*xyzSample;

% the sample surface 

% 
sampleRect3=rot1*sampleRect';
sampleRect3=sampleRect3';
sampleRect4=rot2*rot1*sampleRect';
sampleRect4=sampleRect4';

% the sample coordinate axes 
sampleAxes3=rot1*sampleAxes';
sampleAxes3=sampleAxes3';
sampleAxes4=rot2*rot1*sampleAxes';
sampleAxes4=sampleAxes4';

%% Section 5: select one particular point (pp) to move to the YLab-axis (Group 2) 
% to be the incoming light direction
phi2z=atan2(xyz4(2,pp),xyz4(1,pp));
rotG2=signG2*(-phi2z+pi/2);
rotG2deg=rotG2*180/pi;
% test to see if phi2 is within Group 2 limits
limits=limG2deg-datumG2;

% no go
gonogo2=0;
if rotG2deg>limits(1) && rotG2deg<limits(2)
    gonogo2=1;
    % disp(1);
elseif rotG2deg>limits(2) && (rotG2deg-360)>limits(1)
    rotG2deg=rotG2deg-360;
    gonogo2=1;
    % disp(2);
elseif rotG2deg<limits(1) && (rotG2deg+360)<limits(2)
    rotG2deg=rotG2deg+360;
    gonogo2=1;
    % disp(3);
end
rotG2=rotG2deg*pi/180;
% note: angle to send to Group2 motor = rotG2deg+datumG2


% RHR around z axis 
rotz = eulerZXZRotation( 0,0,signG2*rotG2)

%% Section 6: move points for plotting after Group 2 rotation
% the measurement points 
xyz5=rotz*xyz4

% the sample surface 
sampleRect5=rotz*sampleRect4';
sampleRect5=sampleRect5';
% the sample coordinate axes 
sampleAxes5=rotz*sampleAxes4';
sampleAxes5=sampleAxes5';

%% plotting section 1 result
figure(5);
p0=plot3(0,0,0,'ok','markersize',10,'markerfacecolor',[0,0,0]);
leg=legend('origin');

% hold on;
% % original points:
% p1=plot3(xyz(1,:),xyz(2,:),xyz(3,:),'k.:');
% xlim([-1,1]*1.2);
% ylim([-1,1]*1.2);
% zlim([-1,1]*1.2);
% grid on;
% leg=legend('origin','points in x-y plane');

hold on;
p2=plot3(xyzSample(1,:),xyzSample(2,:),xyzSample(3,:),'ko:');
leg.String{end}='points in scattering plane of sample frame';
px=patch(xyzSample(1,:)',xyzSample(2,:)',xyzSample(3,:)','k','FaceAlpha',.3);
px.Annotation.LegendInformation.IconDisplayStyle='off';

pra=plot3(rotaxis(1,:),rotaxis(2,:),rotaxis(3,:),'ko--','linewidth',1,'MarkerSize',3,'markerfacecolor',[0,0,0]);
leg.String{end}='scattering plane axis (sample frame)';

arca=horzcat(arcit(-alpha,10,x(:,2)*.75),xz);
arcb=horzcat(arcit(beta,10,y(:,2)*.75),yz);
pinta=plot3(arca(1,:),arca(2,:),arca(3,:),'r','linewidth',1);
leg.String{end}=horzcat('\color[rgb]{.5,0,0}\alpha = ',num2str(alphadeg));
pintb=plot3(arcb(1,:),arcb(2,:),arcb(3,:),'g','linewidth',1);
leg.String{end}=horzcat('\color[rgb]{0,.5,0}\beta = ',num2str(betadeg));
hold off;
xlabel('X_{Lab}');
ylabel('Y_{Lab}');
zlabel('Z_{Lab}');

hold on;
PSA=plot3(sampleAxes(:,1),sampleAxes(:,2),sampleAxes(:,3),'b','linewidth',2);
leg.String{end}='sample-frame axes';

xSample=text(0,0,0,'x_{sample}','color','b');
xSample.Position=sampleAxes(2,:);
ySample=text(0,0,0,'y_{sample}','color','b');
ySample.Position=sampleAxes(4,:);
zSample=text(0,0,0,'z_{sample}','color','b');
zSample.Position=sampleAxes(6,:);

view(130,30);
grid on;

ptch=patch(sampleRect(:,1),sampleRect(:,2),sampleRect(:,3),'b','FaceAlpha',.3);
leg.String{end}='sample surface';
ptch.FaceColor=[.7,.7,1];
ptch.FaceAlpha=1;
ptch.EdgeColor=.3*[1,1,1];
%ptch.Annotation.LegendInformation.IconDisplayStyle='off';
%
hold off;
ax=gca;
ax.CameraViewAngle=5;
ax.DataAspectRatio=[1,1,1];
ax.Position(1)=0.3;
leg.Position=[0.485   0.802   0.5  0.18];
fig=ax.Parent;
fig.InnerPosition(2)=100;
fig.InnerPosition(3)=600;
fig.InnerPosition(4)=500;
% testerSup % additional code for drawing theta and phi
%% plot first rotation (group 7) results, diminish old positions:
f2=copyobj(fig,groot);
% untesterSup
figure(5);
labAxes=zeros(6,3);
labAxes(2,1)=1.2;labAxes(4,2)=1.2;labAxes(6,3)=1.2;
hold on;
PLA=plot3(labAxes(:,1),labAxes(:,2),labAxes(:,3),'k-','linewidth',1);
leg.String{end}='lab-frame axes';
hold off;

pra.XData=ra3(1,:);pra.YData=ra3(2,:);pra.ZData=ra3(3,:);
hold on;
p3=plot3(xyz3(1,:),xyz3(2,:),xyz3(3,:),'ko:');
leg.String{end}=horzcat('points after Group 7 rotation -\phi = ',num2str(-phi*180/pi,3));
hold off;

pinta.LineStyle=':';
pinta.Color=min([1,1,1],pinta.Color+.7);
pintb.LineStyle=':';
pintb.Color=min([1,1,1],pintb.Color+.7);

p2.Marker='.';
p2.MarkerSize=1;
px.Vertices=xyz3';

hold on;

% light blue 
% PSA.Color=[0.9,0.9,1];
PSA.XData=sampleAxes3(:,1);
PSA.YData=sampleAxes3(:,2);
PSA.ZData=sampleAxes3(:,3);

ptch3=patch(sampleRect3(:,1),sampleRect3(:,2),sampleRect3(:,3),'b');
ptch3.FaceColor= ptch.FaceColor;
ptch3.FaceAlpha= ptch.FaceAlpha;
ptch3.LineStyle= ptch.LineStyle;
ptch3.Annotation.LegendInformation.IconDisplayStyle='off';
ptch.FaceAlpha=0.1;ptch.LineStyle=':';

xSample.Position=sampleAxes3(2,:);
ySample.Position=sampleAxes3(4,:);
zSample.Position=sampleAxes3(6,:);
hold off;
leg.Position(2)=leg.Position(2)-.05;
%% plot 2nd rotation (group 5) results, diminish old positions:
f2=copyobj(fig,groot);
figure(5);
pra.XData=ra4(1,:);pra.YData=ra4(2,:);pra.ZData=ra4(3,:);
hold on;
p4=plot3(xyz4(1,:),xyz4(2,:),xyz4(3,:),'ko:');
leg.String{end}=horzcat('points after Group 5 rotation -\theta = ',num2str(-theta*180/pi,3));
hold off;

p3.Marker=p2.Marker;
p3.MarkerSize=p2.MarkerSize;
px.Vertices=xyz4';

hold on;

% light blue 
% PSA.Color=[0.9,0.9,1];
PSA.XData=sampleAxes4(:,1);
PSA.YData=sampleAxes4(:,2);
PSA.ZData=sampleAxes4(:,3);

ptch4=patch(sampleRect4(:,1),sampleRect4(:,2),sampleRect4(:,3),'b');
ptch4.FaceColor= ptch3.FaceColor;
ptch4.FaceAlpha= ptch3.FaceAlpha;
ptch4.LineStyle= ptch3.LineStyle;
ptch4.Annotation.LegendInformation.IconDisplayStyle='off';
ptch3.FaceAlpha=0.1;ptch3.LineStyle=':';

xSample.Position=sampleAxes4(2,:);
ySample.Position=sampleAxes4(4,:);
zSample.Position=sampleAxes4(6,:);
hold off;
%% plot results after Group 2 rotation; diminish or delete some old results:
f3=copyobj(fig,groot);
figure(5);

delete(pra);
hold on;
ptch5=patch(sampleRect5(:,1),sampleRect5(:,2),sampleRect5(:,3),'b','FaceAlpha',.35);
ptch5.FaceColor= ptch4.FaceColor;
ptch5.FaceAlpha= ptch4.FaceAlpha;
ptch5.LineStyle= ptch4.LineStyle;
ptch5.Annotation.LegendInformation.IconDisplayStyle='on';
leg.String{end}='sample surface';

%ptch4.FaceAlpha=0.1;ptch4.LineStyle=':';
delete([ptch,ptch3,ptch4]);
%
PSA.XData=sampleAxes5(:,1);
PSA.YData=sampleAxes5(:,2);
PSA.ZData=sampleAxes5(:,3);

% light blue 
% PSA4.Color=[0.75,0.75,1];
% PSA5=plot3(sampleAxes5(:,1),sampleAxes5(:,2),sampleAxes5(:,3),'color',[1,0.5,0.5],'linewidth',2);
% PSA5.Annotation.LegendInformation.IconDisplayStyle='off';

xSample.Position=sampleAxes5(2,:);
ySample.Position=sampleAxes5(4,:);
zSample.Position=sampleAxes5(6,:);
%
p5=plot3(xyz5(1,:),xyz5(2,:),xyz5(3,:),'r.:','markersize',20);
leg.String{end}='designated sampling points';

if gonogo2==0
    plot3([0 0],[0 1],[0 0],'r-','linewidth',5);
else
    plot3([0 0],[0 1],[0 0],'g-','linewidth',5);
end
leg.String{end}=horzcat('incoming light ray, Group 2 = ',num2str(rotG2*180/pi,3),'^o');
hold off;
px.Vertices=xyz5';
f4=copyobj(fig,groot);
% delete(fig);
% clear fig;
figure(5);
delete([p2,p3,p4]);

close all;
%% 