"""Models a single rotary stage."""
from angle import Angle

class RotaryStage:
    """Models a single rotary stage."""
    def __init__(self, θ_min: Angle, θ_max: Angle, θ_datum: Angle, polarity: int = 1):
        self.θ_min = θ_min # Cannot rotate below this angle
        self.θ_max = θ_max # Cannot rotate above this angle
        self.θ_datum = θ_datum # Zero point wrt sample-frame y-axis.
        self.polarity = polarity # 1: Right-handed; -1: Left-handed rotation.
        self._θ_target = None
        self.go_no_go = None

    @property
    def θ_target(self):
        """Property wrapper for target angle."""
        return self._θ_target

    @θ_target.setter
    def θ_target(self, value):
        self._θ_target = value
        self.goNoGo()

    def __str__(self):
        return '\n'.join([
            f'θ_min: {self.θ_min}',
            f'θ_max: {self.θ_max}',
            f'θ_datum: {self.θ_datum}',
            f'polarity: {self.polarity}',
            f'θ_target: {self.θ_target}',
            f'go_no_go: {self.go_no_go}'
        ])

    def goNoGo(self):
        """Tests whether the stage can rotate to a target angle."""
        # group, θ_target
        lower = self.θ_min - self.θ_datum
        upper = self.θ_max - self.θ_datum
        turn = Angle(deg=360)

        if self._θ_target > lower and self._θ_target < upper:
            self.go_no_go = 1
        elif self._θ_target > upper and (self._θ_target-turn) > lower:
            self._θ_target = self._θ_target-turn
            self.go_no_go = 1
        elif self._θ_target < lower and (self._θ_target+turn) < upper:
            self._θ_target = self._θ_target+turn
            self.go_no_go = 1
        else:
            self.go_no_go = 0
