""""""
from math import cos as kos
from math import sin as zin
from math import atan2 as adan2
from math import pi as π
from fself import fself
from euler_rotation import euler_z_x_z_rotation as zxz
from euler_rotation import euler_z_y_z_rotation as zyz

class Angle:
    def __init__(self, *, deg=0, rad=0):
        """Creates an Angle object."""
        if deg != 0 and rad != 0:
            raise ValueError("cannot nominate both deg and rad")
        elif deg != 0 and rad == 0:
            self._deg = deg
            self._rad = self.radians_from_degrees(deg)
        elif deg == 0 and rad != 0:
            self._rad = rad
            self._deg = self.degrees_from_radians(rad)
        else:
            self._deg = 0
            self._rad = 0
    def getDegrees(self):
        return self._deg
    def getRadians(self):
        return self._rad
    def setDegrees(self, value):
        self._deg = value
        self._rad = self.radians_from_degrees(value)
    def setRadians(self, value):
        self._rad = value
        self._deg = self.degrees_from_radians(value)
    def radians_from_degrees(self, deg):
        """Converts an angle in deg to an angle in rad."""
        return deg*π/180.0
    def degrees_from_radians(self, rad):
        """Converts an angle in rad to an angle in deg."""
        return rad*180.0/π
    def __str__(self):
        return f'{self._deg:.3f}° = {self._rad/π:.3f}π[rad] = {self._rad:.3f} [rad]'
    def __sub__(self, other):
        return Angle(deg=self._deg-other.deg)
    def __add__(self, other):
        if isinstance(other, Angle):
            return Angle(deg=self._deg+other.deg)
        else:
            print(self)
            print(other)
            raise NotImplementedError(fself().__doc__)
    def __mul__(self, other):
        if isinstance(other, Angle):
            raise NotImplementedError(fself().__doc__)
        elif isinstance(other, int):
            return Angle(rad=self.rad*float(other))
        else:
            return Angle(deg=self._deg*other)
    def __neg__(self):
        return Angle(rad=self.rad*-1)
    def __lt__(self, other):
        return self.deg < other.deg
    def __gt__(self, other):
        return self.deg > other.deg
    def __truediv__(self, other):
        if isinstance(other, Angle):
            quotient = (self._deg*1.0)/(other.deg*1.0)
            if quotient == quotient - quotient % 1:
                return round(quotient)
            else:
                return quotient
        else:
            return Angle(deg=(self._deg*1.0)/(other*1.0))
    deg = property(fget=getDegrees, fset=setDegrees, doc="Deg property")
    rad = property(fget=getRadians, fset=setRadians, doc="Rad property")

def cos(θ: Angle):
    return kos(θ.rad)

def sin(θ: Angle):
    return zin(θ.rad)

def atan2(numerator, denominator):
    return Angle(rad=adan2(numerator, denominator))

def euler_z_y_z_rotation(α: Angle, β: Angle, γ: Angle):
    """euler_z_y_z_rotation
    given three angles:
        α,
        β,
        γ,
    return:
        ZYZ matrix that will rotate a coordinate system around:
            the Z axis by α,
            the new Y-axis by β, then
            the new Z axis by γ."""
    # print("α", α)
    # print("β", β)
    # print("γ", γ)
    return zyz(α.rad, β.rad, γ.rad)


def euler_z_x_z_rotation(α: Angle, β: Angle, γ: Angle):
    """euler_z_x_z_rotation
    given three angles:
        α,
        β,
        γ,
    return:
        ZXZ matrix that will rotate a coordinate system around:
            the Z axis by α,
            the new X-axis by β, then
            the new Z axis by γ."""
    return zxz(α.rad, β.rad, γ.rad)
