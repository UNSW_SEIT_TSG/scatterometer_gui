function [arc] = arcit(a0,n,axa,rotaxis)
% rotate point axa [x,y,z] in-plane around axis rotaxis and return arc points
% note as of 1/11/18: rotaxis must be x, y, or z axis
for x=1:3 % point must be on x,y,or z axis
    if axa(x)~=0
        break;
    end
end
if nargin==3
    if x==1
        rotaxis=[0 1 0];% default rotation point on x axis
    elseif x==2
        rotaxis=[1 0 0];% default rotation axis for point on y axis
    elseif x==3
        rotaxis=[1 0 0];% default rotation axis for point on z axis
    end
else % test for legal rotaxix
    if x==1
        if rotaxis(2)==0 && rotaxis(3)==0
            disp('illegal rot axis')
        end
    elseif x==2
        if rotaxis(1)==0 && rotaxis(3)==0
            disp('illegal rot axis')
        end
    else
        if rotaxis(1)==0 && rotaxis(2)==0
            disp('illegal rot axis')
        end
    end        
end     

arc=[];
for i=1:n+1 
    angle=(i-1)*(a0/n);
    if x==1
        if rotaxis(2)>0
            ra = eulerZYZRotation(0,angle,0 );% in xz plane 
        else
            ra = eulerZYZRotation(0,0,angle );% in xy plane 
        end
    elseif x==2
        if rotaxis(1)>0
            ra = eulerZXZRotation(0,angle,0 );% in yz plane 
        else
            ra = eulerZXZRotation(0,0,angle );% in xy plane 
        end
    else % x==3
        if rotaxis(1)>0
            ra = eulerZXZRotation(0,angle,0);% in yz plane
        else
            ra = eulerZYZRotation(0,angle,0 );% in xz plane 
        end            
    end
    arc=horzcat(arc,ra*axa);
end
end