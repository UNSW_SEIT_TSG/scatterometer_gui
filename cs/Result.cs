using Godot;
using System;
public class Result
{
    // This class is for holding a result from an experiment.
    // Over the course of an experiment many result objects will be created and
    // output to a text file.

    /* Lab Frame Definition
        x: perpendicular to y and z with positive being away from wall with door.
        y: along axis of light source beam away from light source
        z: vertical from floor/table

       Note:
        All angles are in radians, unless the notation is obviously different.
        Adopted value storage convention is to use floats in place of double.
    */
    private Angle _angleDeg;
    private Angle _scatterPlaneRotationAboutSampleYInSampleXZPlaneRad;
    private Angle _inclinationAngleBetweenSampleYAndScatterPlaneRad;
    private string _intensityReadingMeanOriginalString;
    private string _intensityReadingStdDevOriginalString;
    private double _intensityReadingMeanValue;
    private double _intensityReadingStdDevValue;
    private Vector3 _lightUnitVectorXYZInLabFrame = new Vector3(0,-1,0); // Vector is away from source.
    private Vector3 _samplePlaneNormalUnitVectorXYZInLabFrame;
    private Vector3 _scatterPlaneNormalUnitVectorXYZInLabFrame= new Vector3(0,1,0);
    private Vector3 _sensorUnitVectorXYZInLabFrame; // Vector is into sensor
    public Result(
        Vector3 sensorUnitVectorXYZInLabFrame,
        Vector3 samplePlaneNormalUnitVectorXYZInLabFrame,
        string intensityReadingMean,
        string intensityReadingStdDev)
    {
        // Result object constructor.
        _intensityReadingMeanOriginalString = intensityReadingMean;
        _intensityReadingStdDevOriginalString = intensityReadingStdDev;
        _samplePlaneNormalUnitVectorXYZInLabFrame = samplePlaneNormalUnitVectorXYZInLabFrame;
        _sensorUnitVectorXYZInLabFrame = sensorUnitVectorXYZInLabFrame;
    }
    public Result(
        Angle angleDeg, 
        Angle scatterPlaneRotationAboutSampleYInSampleXZPlaneRad, 
        Angle inclinationAngleBetweenSampleYAndScatterPlaneRad,
        string intensityReadingMean,
        string intensityReadingStdDev)
    {
        // Result object constructor.
        _angleDeg = angleDeg;

        _scatterPlaneRotationAboutSampleYInSampleXZPlaneRad =
            scatterPlaneRotationAboutSampleYInSampleXZPlaneRad;

        _inclinationAngleBetweenSampleYAndScatterPlaneRad =
            inclinationAngleBetweenSampleYAndScatterPlaneRad;

        _intensityReadingMeanOriginalString = intensityReadingMean;

        _intensityReadingStdDevOriginalString = intensityReadingStdDev;
    }
    public double IntensityReadingMean
    {
        get
        {
            return Double.Parse(
                _intensityReadingMeanOriginalString, 
                System.Globalization.NumberStyles.Float
            );
        }
    }
    public double IntensityReadingStdDev
    {
        get
        {
            return Double.Parse(
                _intensityReadingStdDevOriginalString, 
                System.Globalization.NumberStyles.Float
            );
        }
    }
    public Vector3 LightUnitVectorXYZInLabFrame
    {
        get
        {
            return _lightUnitVectorXYZInLabFrame;
        }
    }
    public Vector3 SamplePlaneNormalUnitVectorXYZInLabFrame
    {
        get
        {
            return _samplePlaneNormalUnitVectorXYZInLabFrame;
        }
    }
    public Vector3 ScatterPlaneNormalUnitVectorXYZInLabFrame
    {
        get
        {
            return _scatterPlaneNormalUnitVectorXYZInLabFrame;
        }
    }
    public Vector3 SensorUnitVectorXYZInLabFrame
    {
        get
        {
            return _sensorUnitVectorXYZInLabFrame;
        }
    }
    public Angle AngleDeg
    {
        get
        {
            return _angleDeg;
        }
    }
    public Angle ScatterPlaneRotationAboutSampleYInSampleXZPlaneRad
    {
        get
        {
            return _scatterPlaneRotationAboutSampleYInSampleXZPlaneRad;
        }
    }
    public Angle InclinationAngleBetweenSampleYAndScatterPlaneRad
    {
        get
        {
            return _inclinationAngleBetweenSampleYAndScatterPlaneRad;
        }
    }
    public override string ToString()
    {
        return "Result:\n"+
            "  angle: "+AngleDeg.ToString()+"\n"+
            "  scatterPlaneRotationAboutSampleYInSampleXZPlane: "+ScatterPlaneRotationAboutSampleYInSampleXZPlaneRad.ToString()+"\n"+
            "  inclinationAngleBetweenSampleYAndScatterPlane: "+InclinationAngleBetweenSampleYAndScatterPlaneRad.ToString()+"\n"+
            "  intensityReadingMean: "+_intensityReadingMeanOriginalString.ToString()+"\n"+
            "  intensityReadingStdDev: "+_intensityReadingStdDevOriginalString.ToString()+"\n"
        ;
    }
}