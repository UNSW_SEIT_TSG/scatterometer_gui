using Godot;
using System;
using System.Net.Sockets;
using System.Text;
public class TCPClient : Node
{
    public static void StartClient(Connection connection)
    {
        try
        {
            connection.socket.Connect(connection.remoteEndPoint);
            connection.connected = true;
        }
        catch (ArgumentNullException ane)
        {
            connection.connected = false;
            GD.Print(ane.ToString());
        }
        catch (SocketException se)
        {
            connection.connected = false;
            GD.Print(se.ToString());
        }
        catch (Exception e)
        {
            connection.connected = false;
            GD.Print(e.ToString());
        }
    }
    public static void Send(Connection connection, String message)
    {
        if (connection.connected){
            try
            {
                byte[] msg  = Encoding.ASCII.GetBytes(message);
                int bytesSent = connection.socket.Send(msg);
            }
            catch (ArgumentNullException ane)
            {
                GD.Print("ArgumentNullException : {0}", ane.ToString());
                connection.connected = false;
            }
            catch (SocketException se)
            {
                GD.Print("SocketException : {0}", se.ToString());
                connection.connected = false;
            }
            catch (Exception e)
            {
                GD.Print("Unexpected exception : {0}", e.ToString());
                connection.connected = false;
            }
        }
    }
    public static bool ShutdownAndClose(Connection connection)
    {
        if (connection.connected)
        {
            try
            {
                connection.socket.Shutdown(SocketShutdown.Both);
                connection.socket.Close();
                connection.socket.Dispose();
                connection.connected = false;
                return true;
            }
            catch (ArgumentNullException ane)
            {
                GD.Print("ArgumentNullException : {0}", ane.ToString());
                return false;
            }
            catch (SocketException se)
            {
                GD.Print("SocketException : {0}", se.ToString());
                return false;
            }
            catch (Exception e)
            {
                GD.Print("Exception : {0}", e.ToString());
                return false;
            }
        }
        else
        {
            return false;
        }
    }
    public static string receive(Connection connection)
    {
        string result = "";
        if (connection.connected)
        {
            try
            {
                int bytesRec = connection.socket.Receive(connection.bytes);
                result = Encoding.ASCII.GetString(connection.bytes,0,bytesRec);
            }
            catch (ArgumentNullException ane)
            {
                GD.Print("ArgumentNullException : {0}", ane.ToString());
                connection.connected = false;
            }
            catch (SocketException se)
            {
                GD.Print("SocketException : {0}", se.ToString());
                connection.connected = false;
            }
            catch (Exception e)
            {
                GD.Print("Exception : {0}", e.ToString());
                connection.connected = false;
            }
        }
        else
        {
            result = "Error: 'Not connected'";
        }
        return result;
    }
}