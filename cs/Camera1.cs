using Godot;
using System;

public class Camera1 : Camera
{
    // Member variables here, example:
    // private int a = 2;
    // private string b = "textvar";

    public override void _Ready()
    {
        // Called every time the node is added to the scene.
        // Initialization here
        // Camera c = (Camera)GetNode()
        GD.Print(this.ToString());
        float a = 1;
        this.LookAtFromPosition(new Vector3(a,a,a), new Vector3(0,0,0), new Vector3(0,1,0));
    }

//    public override void _Process(float delta)
//    {
//        // Called every frame. Delta is time since last frame.
//        // Update game logic here.
//        
//    }
}
