using Godot;
using System;
using System.Collections.Generic;
public class ScatterPlaneModel : Godot.Spatial
{
    
    float π;
    MeshInstance scatterPlaneNormal;
    bool doneOnce;
    private Glx _glx;
    private Util _util;
    private Experiment _exp;
    private Angle _inclinationOffset;
    private Scalar cloudRadius;
    private bool _middleMouseButtonPressed;
    private Vector2 _cameraRotationAboutYAxisInXZPlaneAndInclinationInRadians;
    private Scalar _cameraRadius; // Distance from camera to pivot point
    private Scalar _cameraUpperLimitRadius;
    Random r;
    public override void _Ready()
    {
        _glx = (Glx)GetNode("/root/Glx");
        _exp = _glx.exp;
        
        doneOnce = false;
        cloudRadius = new Scalar(5.65685424949f);
        _middleMouseButtonPressed = false;
        _cameraRotationAboutYAxisInXZPlaneAndInclinationInRadians = new Vector2(0.551724f, 0.785398f);
        _cameraRadius = new Scalar(11.3375f);  // Distance of camera for proper positioning of scene in viewport

        r = new Random();
        _util = new Util();
        π = _util.π;

        _inclinationOffset = new Angle();
        _inclinationOffset.Radians = π/2;
        _cameraUpperLimitRadius = new Scalar(0);
        _cameraUpperLimitRadius = _util.Sqrt(_util.Pow(7,2)*3);
        
        UpdateCameraPosition(new Vector2(0,0));
        scatterPlaneNormal = GetNode("../ScatterPlaneNormal") as MeshInstance;
    }
    private Scalar S(Angle θ)
    {
        return _util.Sin(θ);
    }
    private Scalar C(Angle θ)
    {
        return _util.Cos(θ);
    }
    private void ClearReadingNodesFromHemisphere()
    {
        if (GetNode("MeshInstances/").GetChildCount() > 0)
        {
            foreach (MeshInstance child in GetNode("MeshInstances/").GetChildren())
            {
                child.QueueFree();
            }
        }
    }
    public void PlacePointOnHemisphere(Node nx, Vector3 unit, Scalar radius, string todoDoingDone)
    {
        PackedScene sphereRes = (PackedScene)ResourceLoader.Load("res://scenes/Sphere.tscn");
        MeshInstance sphereInstance = (MeshInstance)sphereRes.Instance();
        nx.AddChild(sphereInstance, true);
        sphereInstance.Set("translation", radius*unit);
        SpatialMaterial surfaceMaterial = (SpatialMaterial)sphereInstance.GetSurfaceMaterial(0).Duplicate(false);
        
        if (todoDoingDone.Equals("todo"))
        {
            surfaceMaterial.AlbedoColor = _glx.palette.readingPointTodo;
        }
        else if (todoDoingDone.Equals("done"))
        {
            surfaceMaterial.AlbedoColor = _glx.palette.readingPointDone;
        }
        else if (todoDoingDone.Equals("doing"))
        {
            surfaceMaterial.AlbedoColor = _glx.palette.readingPointDoing;
        }
        else
        {
            surfaceMaterial.AlbedoColor = _glx.palette.readingPointError;
        }
        sphereInstance.SetSurfaceMaterial(0, surfaceMaterial);
    }
    public Vector3 PlotPoint(Angle arcRad, Angle inclineRad, Angle rotYRad, string todoDoingDone)
    {
        Vector3 prior = new Vector3(
            (S(arcRad) * C(inclineRad)).Value,
            (S(arcRad) * S(inclineRad)).Value,
            C(arcRad).Value
        );
        Vector3 point = prior.Rotated(new Vector3(0,1,0), rotYRad.Radians);
        PlacePointOnHemisphere(GetNode("MeshInstances/"), point, cloudRadius, todoDoingDone);
        return point;
    }
    public void PlotArc(List<Angle> seqRad, Angle inclineRad, Angle rotYRad, Angle shiftRad)
    {
        List<Vector3> XYZs = new List<Vector3>(seqRad.Count);
        for (int q = 0; q < seqRad.Count; q++)
        {

            Vector3 pointOnUnitSphere = PlotPoint(
                arcRad:        seqRad[q]+shiftRad,
                inclineRad:    inclineRad,
                rotYRad:    rotYRad,
                todoDoingDone: _exp.TodoDoingDone(q)
            );
            XYZs.Add(pointOnUnitSphere);
        }
        _exp.UpdateXYZs(XYZs);
    }
    public Vector2 θAndφFromXYZVector(Vector3 v)
    {
        Scalar r = new Scalar(v.Length());
        Angle θ = _util.Acos(v.z/r);
        Angle φ = _util.Atan2(v.y,v.x);
        return new Vector2(θ.Radians, φ.Radians);
    }
    public override void _Process(float delta)
    {
        String scatterPlaneNodeAddress = "../../../../../../../../..";
        String prefix_θφ = scatterPlaneNodeAddress+"/VBox/HBoxThetaPhiG5G8/SpinBox";
        //String prefix_αβ = scatterPlaneNodeAddress+"/VBox/HBoxAlphaBetaRotInc/SpinBox";

        if(_exp.ValueChanged)
        {
            GD.Print("!126 Scatter Plane Model _exp.ValueChanged"+ _exp.ValueChanged.ToString());
            ClearReadingNodesFromHemisphere();
            Angle ξ = new Angle();
            ξ.Radians = π/2;
            Angle shiftRadAngle = new Angle();
            shiftRadAngle.Degrees = 90;
            PlotArc(
                seqRad:_exp.readingSeqArc,
                inclineRad:_exp.ArcInclination+_inclinationOffset,
                rotYRad:_exp.ArcRotationAboutYAxisInXZPlane+ξ,
                shiftRad: shiftRadAngle
            );
            _exp.ValueChanged = false;
            
            Camera cam = GetNode("../Camera") as Camera;
            
/*             Node n = GetNode("../../");
            GD.Print(n.GetName().ToString());
            n = GetNode(scatterPlaneNodeAddress+"/VBox/HBoxAlphaBetaRotInc");
            GD.Print(n.GetName().ToString());
            foreach (Node child in n.GetChildren())
            {
                GD.Print(child.GetName().ToString());
            }
 */
            Vector2 θφ = θAndφFromXYZVector(_glx.TransformedNormal);
            scatterPlaneNormal.SetTransform(_glx.CalculateTransform());
            
            SpinBox θSpinbox = (SpinBox)GetNode(prefix_θφ+"Theta");
            _exp.θ.Radians = θφ.x;
            θSpinbox.SetValue(_exp.θ.Degrees);
            
            
            SpinBox φSpinbox = (SpinBox)GetNode(prefix_θφ+"Phi");
            _exp.φ.Radians = θφ.y;
            φSpinbox.SetValue(_exp.φ.Degrees);

            // TODO: update G5 and G7 spin boxes
            Vector3 SPNormal=_glx.TransformedNormal;
            Angle g7angle=new Angle();
            g7angle.Radians=(float)(-Math.Atan2(SPNormal.x,SPNormal.z));
            // assume that the G5/G7 motor inputs conform to RHR angles
            Vector3 afterG7normal=SPNormal.Rotated(new Vector3(0,1,0),g7angle.Radians);
            Angle g5angle=new Angle();
            g5angle.Radians=(float) Math.Atan2(afterG7normal.y,afterG7normal.z);

            SpinBox G7Spinbox = (SpinBox)GetNode(prefix_θφ+"Group7");
            G7Spinbox.SetValue(g7angle.Degrees);
            SpinBox G5Spinbox = (SpinBox)GetNode(prefix_θφ+"Group5");
            G5Spinbox.SetValue(g5angle.Degrees);
            
            
            GD.Print("scatter plane model: normal in sample frame:"+_glx.TransformedNormal.ToString());
        }
    }

    private void UpdateCameraPosition(Vector2 delta)
    {
        Camera cam = (Camera)GetNode("../Camera");
        
        // Adjust camera using planar delta vector from mouse input
        _cameraRotationAboutYAxisInXZPlaneAndInclinationInRadians += delta;

        // Loop rotation
        if(_cameraRotationAboutYAxisInXZPlaneAndInclinationInRadians.x > π/2)
        {
            _cameraRotationAboutYAxisInXZPlaneAndInclinationInRadians.x = π/2;
        }
        if(_cameraRotationAboutYAxisInXZPlaneAndInclinationInRadians.x < -π/2)
        {
            _cameraRotationAboutYAxisInXZPlaneAndInclinationInRadians.x = -π/2;
        }
        Angle ξ = new Angle();
        ξ.Radians = _cameraRotationAboutYAxisInXZPlaneAndInclinationInRadians.x;

        Angle ω = new Angle();
        ω.Radians = _cameraRotationAboutYAxisInXZPlaneAndInclinationInRadians.y;

        // Pick point in x y z terms using 2 angles and radius.
        Vector3 xyz = _util.XYZOnSphereViaInclinAndRotAboutY(ξ, ω, _cameraRadius);
        
        // Move camera to point xyz
        cam.SetTranslation(new Vector3(xyz.x, xyz.z, xyz.y));

        // Point the camera at the origin with the z axis being the vertical direction when viewed by the camera.
        cam.LookAt(new Vector3(0,0,0), new Vector3(0,0,1));
    }
    public override void _Input(InputEvent @event)
    {
        if (@event is InputEventMouse inputEventMouse)
        {
            if(inputEventMouse is InputEventMouseButton inputEventMouseButton)
            {
                if (inputEventMouseButton.GetButtonIndex() == (int)ButtonList.Middle)
                {
                    _middleMouseButtonPressed = inputEventMouseButton.IsPressed();
                }
                if ((
                        inputEventMouseButton.GetButtonIndex() == (int)ButtonList.WheelDown ||
                        inputEventMouseButton.GetButtonIndex() == (int)ButtonList.WheelUp
                    ) && 
                    GetViewport().GetMousePosition().x > 0 &&
                    GetViewport().GetMousePosition().y > 0 && 
                    GetViewport().GetMousePosition().y < GetViewport().GetSize().y
                )
                {
                    if (inputEventMouseButton.GetButtonIndex() == (int)ButtonList.WheelUp)
                    {
                        _cameraRadius += 0.1f;
                        if(_cameraRadius > _cameraUpperLimitRadius)
                        {
                            _cameraRadius = _cameraUpperLimitRadius;
                        }
                    }else{
                        Scalar cameraLowerLimitRadius = new Scalar(0.5f);
                        _cameraRadius -= 0.1f;
                        if(_cameraRadius < cameraLowerLimitRadius)
                        {
                            _cameraRadius = cameraLowerLimitRadius;
                        }
                    }
                    UpdateCameraPosition(new Vector2(0,0));
                }
            }
            if(inputEventMouse is InputEventMouseMotion inputEventMouseMotion)
            {
                if(_middleMouseButtonPressed)
                {
                    Vector2 motion = inputEventMouseMotion.GetRelative();
                    UpdateCameraPosition(new Vector2(motion.y,-motion.x)*0.001f);
                }
            }
        }
    }
}