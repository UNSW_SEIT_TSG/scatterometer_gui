using Godot;
using System;

public class ScatterPlane : Control
{
    private bool _autoMoveAndReadInProgress;
    private Glx _glx;
    private Util _util;
    private Experiment _exp;
    private bool _incRotValueChanged;
    private bool _alphaBetaValueChanged;
    MeshInstance scatterPlaneNormal;
    Camera cam2;
    Button buttonReturnToMain;
    Button buttonConnectDisconnect;
    Button buttonReturnToDatum;
    Button buttonAlignScatterPlane;
    Button buttonSingleReading;
    Button buttonMoveToNext;
    Button buttonInitAndHomeAll;
    Button buttonLockUnlock;
    Button buttonAutoMoveAndRead;
    SpinBox spinBoxStartAngle;
    SpinBox spinBoxFinalAngle;
    SpinBox spinBoxReadingCount;
    SpinBox spinBoxArcRotationAboutYAxisInXZPlaneAngle;
    SpinBox spinBoxInclinationAngle;
    SpinBox spinBoxTheta;
    SpinBox spinBoxPhi;
    SpinBox spinBoxAlpha;
    SpinBox spinBoxBeta;
    bool sceneJustOpened;
    public override void _Ready()
    {
        _autoMoveAndReadInProgress = false;
        sceneJustOpened = true;
        _glx = (Glx)GetNode("/root/Glx");
        _util = new Util();
        _exp = _glx.exp;
        cam2 = (Camera)GetNode("VBox/HBox2/VBox/VBox/VBox/ViewportContainer2/Viewport/Experiment/Camera2");
        scatterPlaneNormal = (MeshInstance)GetNode("VBox/HBox2/VBox2/VBox2/ViewportContainer/Viewport/Experiment/ScatterPlaneNormal");
        buttonReturnToMain = (Button)GetNode("VBox/HBox/ButtonReturnToMain");
        buttonConnectDisconnect = (Button)GetNode("VBox/HBox/ButtonConnectDisconnect");
        buttonReturnToDatum = (Button)GetNode("VBox/HBox/ButtonReturnToDatum");
        buttonAlignScatterPlane = (Button)GetNode("VBox/HBox/ButtonAlignScatterPlane");
        buttonSingleReading = (Button)GetNode("VBox/HBox/ButtonSingleReading");
        buttonMoveToNext = (Button)GetNode("VBox/HBox/ButtonMoveToNext");
        buttonLockUnlock = (Button)GetNode("VBox/HBox/ButtonLockUnlock");
        buttonAutoMoveAndRead = (Button)GetNode("VBox/HBox/ButtonAutoMoveAndRead");
        buttonInitAndHomeAll = (Button)GetNode("VBox/HBox/ButtonInitAndHomeAll");
        
        spinBoxStartAngle = (SpinBox)GetNode("VBox/HBox2/VBox/VBox/VBox/PanelContainer2/HBox/SpinBoxStartAngle");
        spinBoxFinalAngle = (SpinBox)GetNode("VBox/HBox2/VBox/VBox/VBox/PanelContainer2/HBox/SpinBoxFinalAngle");
        spinBoxReadingCount = (SpinBox)GetNode("VBox/HBox2/VBox/VBox/VBox/PanelContainer2/HBox/SpinBoxReadingCount");

       
        spinBoxArcRotationAboutYAxisInXZPlaneAngle = (SpinBox)GetNode("VBox/HBoxAlphaBetaRotInc/SpinBoxArcRotationAboutYAxisInXZPlaneAngle");
        spinBoxInclinationAngle = (SpinBox)GetNode("VBox/HBoxAlphaBetaRotInc/SpinBoxInclinationAngle");
        spinBoxTheta = (SpinBox)GetNode("VBox/HBoxThetaPhiG5G8/SpinBoxTheta");
        spinBoxPhi = (SpinBox)GetNode("VBox/HBoxThetaPhiG5G8/SpinBoxPhi");
        spinBoxAlpha = (SpinBox)GetNode("VBox/HBoxAlphaBetaRotInc/SpinBoxAlpha");
        spinBoxBeta = (SpinBox)GetNode("VBox/HBoxAlphaBetaRotInc/SpinBoxBeta");

        spinBoxStartAngle.SetValue(_exp.ArcStart.Degrees);
        spinBoxFinalAngle.SetValue(_exp.ArcFinal.Degrees);
        spinBoxReadingCount.SetValue(_exp.ReadingCount);

        // jjk: to ensure proper loading of spin boxes:
        spinBoxAlpha.SetEditable(false);
        spinBoxBeta.SetEditable(false);
        spinBoxArcRotationAboutYAxisInXZPlaneAngle.SetEditable(true);
        spinBoxInclinationAngle.SetEditable(true);
        _alphaBetaValueChanged=false;
        _incRotValueChanged=true;
        spinBoxInclinationAngle.SetValue(_exp.ArcInclination.Degrees);
        //spinBoxArcRotationAboutYAxisInXZPlaneAngle must be last one called (jjk)
        // so spinBoxArcRotationAboutYAxisInXZPlaneAngle will update alpha and beta and gui
        _incRotValueChanged=false;
        spinBoxArcRotationAboutYAxisInXZPlaneAngle.SetValue(_exp.ArcRotationAboutYAxisInXZPlane.Degrees);
        // spinBoxTheta;
        // spinBoxPhi;

        GD.Print("_glx.αβg5: "+_glx.αβg5.ToString());
        GD.Print("_glx.αβg7: "+_glx.αβg7.ToString());
        GD.Print("_glx.α:    "+_glx.α.ToString());
        GD.Print("_glx.β:    "+_glx.β.ToString());
    }
    public void _on_ButtonReturnToMain_pressed()
    {
        _glx.GoToScene("res://scenes/MainMenu.tscn");
    }
    private void _on_SpinBoxStartAngle_value_changed(float value)
    {
        Angle θ = new Angle();
        θ.Degrees = value;
        _exp.ArcStart = θ;
    }
    private void _on_SpinBoxFinalAngle_value_changed(float value)
    {
        Angle θ = new Angle();
        θ.Degrees = value;
        _exp.ArcFinal = θ;
    }
    private void _on_SpinBoxReadingCount_value_changed(int value)
    {
        _exp.ReadingCount = value;
    }
    private float[] alphaBetaFromInclinationRotation(float inclinV,float rotV)
    { // input angles in degrees
        Vector3 xaxis = new Vector3(1,0,0);
        Vector3 yaxis = new Vector3(0,1,0);
        Vector3 zaxis = new Vector3(0,0,1);
        
        Angle θInclination = new Angle();
        θInclination.Degrees= inclinV ;
        
        Angle rotAboutYinXZ = new Angle();
        rotAboutYinXZ.Degrees = rotV;

        Vector3 axis = xaxis.Rotated(yaxis, rotAboutYinXZ.Radians);
        Vector3 scatterPlaneNormal = zaxis.Rotated(yaxis, rotAboutYinXZ.Radians);
        GD.Print(axis.ToString());
        scatterPlaneNormal = scatterPlaneNormal.Rotated(axis, θInclination.Radians);
        GD.Print(scatterPlaneNormal.ToString());
        
        Vector3 alpha=scatterPlaneNormal.Cross(new Vector3(0,-1,0));
        Vector3 beta=scatterPlaneNormal.Cross(new Vector3(1,0,0));
        float[] alphaBeta = new float[2];
        alphaBeta[0] = (float) Math.Atan2(alpha.z,alpha.x)*180/((float)Math.PI);//(alpha.AngleTo(new Vector3(1,0,0)));
        alphaBeta[1] = (float) Math.Atan2(beta.z,beta.y)*180/((float)Math.PI);
        return alphaBeta;
    }
    private float[] inclinationRotFromAlphaBeta(float alphaV,float betaV)
    {   // input angles in degrees
        Vector3 xaxis=new Vector3(1,0,0);
        Vector3 yaxis=new Vector3(0,1,0);
        
        Vector3 beta=yaxis.Rotated(xaxis,(float)(betaV*Math.PI/180));
        Vector3 alpha=xaxis.Rotated(-yaxis,(float)(alphaV*Math.PI/180));// alpha defined wrt -yaxis using RHR
        Vector3 scatterPlaneNormal=alpha.Cross(beta);// a bit of seemingly redundant code
        Angle normalToYaxisAngle = new Angle();
        normalToYaxisAngle.Radians=scatterPlaneNormal.AngleTo(yaxis);
        Angle incAngle=new Angle();
        incAngle.Degrees=normalToYaxisAngle.Degrees-90;
        GD.Print("new inclination angle = "+incAngle.Degrees.ToString());
        
        float[] incRot = new float[2];
        incRot[0]=incAngle.Degrees;
        incRot[1]=(float) Math.Atan2(alpha.z,alpha.x)*180/((float)Math.PI);
    
        return incRot;
    }
    private void _on_SpinBoxArcRotationAboutYAxisInXZPlaneAngle_value_changed(float value)
    {
        // Angle θ = new Angle();
        // θ.Degrees = value;
        // GD.Print("ScatterPlane ArcRotationAboutYAxis angle changed:"+θ.ToString());
        // _exp.ArcRotationAboutYAxisInXZPlane = θ;
        // _exp.ValueChanged = true;//jjk: moved from Experiment class
        if (spinBoxArcRotationAboutYAxisInXZPlaneAngle.IsEditable() && !_incRotValueChanged){
            // this box changed by user
            GD.Print("inc or rot changed by edit: "+ value.ToString());
            _exp.ArcRotationAboutYAxisInXZPlane.Degrees = value;
            float incV=spinBoxInclinationAngle.GetValue();
            // do a calculation for the other parameters 
            float[] alphaBeta=alphaBetaFromInclinationRotation( incV, value);
            _incRotValueChanged=false;
            spinBoxAlpha.SetValue(alphaBeta[0]);
            spinBoxBeta.SetValue(alphaBeta[1]);
            }
        else{// this box changed by other spinbox (no calc necessary)
            GD.Print("ScatterPlane Rot changed :"+value.ToString());
            _exp.ArcRotationAboutYAxisInXZPlane.Degrees=value;
            _incRotValueChanged=false;
            }
        _exp.ValueChanged=true;// triggers GUI update

    }
    private void _on_SpinBoxInclinationAngle_value_changed(float value)
    {
        // Angle θ = new Angle();
        // θ.Degrees = value;
        // GD.Print("ScatterPlane inclination angle changed:"+θ.ToString());
        // _exp.ArcInclination = θ;
        // _exp.ValueChanged = true;//jjk: moved from Experiment 
        if (spinBoxInclinationAngle.IsEditable() && !_incRotValueChanged){
            // this box changed by user
            GD.Print("inc or rot changed by edit: "+ value.ToString());
            _exp.ArcInclination.Degrees = value;
            float rotV=spinBoxArcRotationAboutYAxisInXZPlaneAngle.GetValue();
            // do a calculation for the other parameters 
            float[] alphaBeta=alphaBetaFromInclinationRotation( value, rotV);
            _incRotValueChanged=false;
            spinBoxAlpha.SetValue(alphaBeta[0]);
            spinBoxBeta.SetValue(alphaBeta[1]);
            _exp.ValueChanged=true; // update gui
            }
        else{// this box changed by other spinbox (no calc necessary)
            GD.Print("ScatterPlane Inc changed :"+value.ToString());
            _exp.ArcInclination.Degrees=value;
            _incRotValueChanged=false;
            }
    }
    private void _on_SpinBoxAlpha_value_changed(float value){
        GD.Print("spinBoxAlpha: _alphaBetaValueChanged = " + _alphaBetaValueChanged.ToString());
        if (spinBoxAlpha.IsEditable() && !_alphaBetaValueChanged){
            // this box changed by user
            GD.Print("beta or alpha changed by edit: "+ value.ToString());
            _exp.Alpha.Degrees = value;
            float beta=spinBoxBeta.GetValue();
            // do a calculation for the other parameters 
            float[] incRot=inclinationRotFromAlphaBeta( value, beta);
            _alphaBetaValueChanged=false;

            GD.Print("new inclination angle = "+incRot[0].ToString());
            spinBoxInclinationAngle.SetValue(incRot[0]);
            spinBoxArcRotationAboutYAxisInXZPlaneAngle.SetValue(incRot[1]);
            }
        else{// this box changed by other spinbox (no calc necessary)
            GD.Print("ScatterPlane alpha changed :"+value.ToString());
            _exp.Alpha.Degrees=value;
            _alphaBetaValueChanged=false;
            }
        }
    private void _on_SpinBoxBeta_value_changed(float value){
        GD.Print("spinBoxBeta: _alphaBetaValueChanged = " + _alphaBetaValueChanged.ToString());
        if (spinBoxBeta.IsEditable() && !_alphaBetaValueChanged){ 
            // this box changed by user
            GD.Print("beta or alpha changed by edit: "+ value.ToString());
            _exp.Beta.Degrees = value;
            float alpha = spinBoxAlpha.GetValue();
            // do a calculation for the other parameters 
            float[] incRot=inclinationRotFromAlphaBeta( alpha, value);
            _alphaBetaValueChanged=false;

            GD.Print("new inclination angle = "+incRot[0].ToString());
            spinBoxInclinationAngle.SetValue(incRot[0]);
            spinBoxArcRotationAboutYAxisInXZPlaneAngle.Value=(incRot[1]);
            // rot angle doesn't change for default scatterometer configuration (4), so:
            _exp.ValueChanged=true; //for gui update
            }
        else{ // this box changed by other spinbox (no calc necessary)
            GD.Print("ScatterPlane beta changed:"+value.ToString());
            _exp.Beta.Degrees = value;
            _alphaBetaValueChanged=false;
            }
        }
    public override void _Process(float delta)
    {
        if(_exp.ValueChanged)
        {
            cam2.SetTranslation(_glx.TransformedNormal*10);
            GD.Print("ScatterPlane process _exp.ValueChanged: "+_exp.ValueChanged.ToString());
        }
        if (_glx.TransformedNormal.Length() > 0.2){
            cam2.LookAtFromPosition(
                _glx.TransformedNormal,
                new Vector3(0,0,0),
                new Vector3(0,1,0)
            );
        }
        if(sceneJustOpened)
        {
            sceneJustOpened = false;
            _exp.ValueChanged = true;
        }
        buttonAlignScatterPlane.SetVisible(_glx.relay.Connected);
        
        if(_glx.relay.Connected)
        {
            buttonConnectDisconnect.Text = "Disconnect";
        }
        else
        {
            buttonConnectDisconnect.Text = "Connect";
        }
        
        buttonInitAndHomeAll.SetVisible(_glx.relay.Connected);
        buttonMoveToNext.SetVisible(_glx.relay.Connected);
        buttonAutoMoveAndRead.SetVisible(_glx.relay.Connected);
        buttonLockUnlock.SetVisible(_glx.relay.Connected);
        
        buttonReturnToDatum.SetVisible(_glx.relay.Connected);
        buttonSingleReading.SetVisible(_glx.relay.Connected);
        
        if(_glx.relay.Connected)
        {
            RotaryStage group2 = _glx.stack.rotaryStages["group2"];
            int status = group2.StatusCode;
            bool ready = status >= 10 && status <= 18;
            buttonMoveToNext.SetDisabled(!ready||!_exp.Locked);
            buttonAutoMoveAndRead.SetDisabled(!ready||!_exp.Locked);

            bool readyToTakeReading;
            if (_exp.ReadingCurrentIndex < 0)
            {
                readyToTakeReading = false;
            }
            else{
                readyToTakeReading = _exp.ReadyToTakeReading;
            }
            

            buttonSingleReading.SetDisabled(!readyToTakeReading||!_exp.Locked);

            if (_exp.ReadingCurrentIndex<0)
            {
                buttonMoveToNext.Text = "Move to Start";
            }
            else if(_exp.ReadingCurrentIndex == _exp.ReadingCount-1)
            {
                buttonMoveToNext.SetDisabled(true);
            }
            else if(_exp.ReadingCurrentIndex == _exp.ReadingCount-2)
            {
                buttonMoveToNext.Text = "Move to Final";
            }
            else
            {
                buttonMoveToNext.Text = "Move to Next";
            }    
        }
        else
        {
            if(_exp.Locked){
                _exp.Locked = false;
            }
        }

        if(_exp.Locked)
        {
            buttonLockUnlock.Text = "Unlock";
        }
        else
        {
            buttonLockUnlock.Text = "Lock";
        }

        spinBoxStartAngle.SetEditable(!_exp.Locked);
        spinBoxFinalAngle.SetEditable(!_exp.Locked);
        spinBoxReadingCount.SetEditable(!_exp.Locked);
        // spinBoxArcRotationAboutYAxisInXZPlaneAngle.SetEditable(!_exp.Locked);
        // spinBoxInclinationAngle.SetEditable(!_exp.Locked);

        if (_autoMoveAndReadInProgress)
        {
            buttonAutoMoveAndRead.Text = "Stop Auto Move & Read";
            if(_glx.AutoCommandCount==0)
            {
                _autoMoveAndReadInProgress = false;
                _exp.SaveResultsToFile();
            }
        }
        else
        {
            buttonAutoMoveAndRead.Text = "Start Auto Move & Read";
        }
        
    }
    private void _on_ButtonAutoMoveAndRead_pressed()
    {
        if(_autoMoveAndReadInProgress)
        {
            _glx.stripAutoFromQueue();
        }
        else
        {
            while(_exp.ReadingCurrentIndex < _exp.ReadingCount-1)
            {
                _exp.MoveToNext("auto");
                _glx.relay.to.Enqueue("opm::GetOpticalPowerMeterReading()::auto");
                _glx.AutoCommandCount += 1;
            }
        }
        
        // Toggle auto move and read flag.
        _autoMoveAndReadInProgress = !_autoMoveAndReadInProgress;
    }
    private void _on_ButtonConnectDisconnect_pressed()
    {
        if(_glx.relay.Connected)
        {
            _glx.Disconnect();
        }
        else
        {
            _glx.Connect();
        }
    }
    private void _on_ButtonMoveToNext_pressed()
    {
        _exp.MoveToNext("manual");
        buttonSingleReading.SetDisabled(true);
    }
    private void _on_ButtonReturnToDatum_pressed()
    {
        foreach (System.Collections.Generic.KeyValuePair<string, RotaryStage> kvp in _glx.stack.rotaryStages)
        {
            kvp.Value.Move(new Angle(), "ReturnToDatum");
        }
        foreach (System.Collections.Generic.KeyValuePair<string, LinearStage> kvp in _glx.stack.linearStages)
        {
            kvp.Value.Move(new Scalar(0), "ReturnToDatum");
        }
    }
    private void _on_ButtonAlignScatterPlane_pressed()
    {
        Vector3 sampleNormal = _glx.GetSampleNormalInLabFrame();
        Vector3 sampleX = _glx.GetSampleXInLabFrame();
        
        Angle θ_2 = _glx.stack.rotaryStages["group2"].PositionXPSC8;
        Angle θ_5 = _glx.stack.rotaryStages["group5"].PositionXPSC8;
        Angle θ_7 = _glx.stack.rotaryStages["group7"].PositionXPSC8;
        Rotations θ = new Rotations(θ_2, θ_5, θ_7);
        GD.Print("265:"+θ.ToString());

        VecToMot v2m = new VecToMot();
        θ = v2m.MapVectorToMotorAngles(new MyVector3(sampleNormal), new MyVector3(sampleX), θ);
        GD.Print("Motor positions for g257 to align scatter plane: "+θ.ToString());
        
        if(_glx.relay.Connected)
        {
            _glx.stack.rotaryStages["group2"].Move(θ.θ_2, "AlignScatterPlane");
            _glx.stack.rotaryStages["group5"].Move(θ.θ_5, "AlignScatterPlane");
            _glx.stack.rotaryStages["group7"].Move(θ.θ_7, "AlignScatterPlane");
        }else{
            GD.Print("relay server not connected.");
        }
    }
    private void _on_ButtonSingleReading_pressed()
    {
        _glx.relay.to.Enqueue("opm::GetOpticalPowerMeterReading()::ScatterPlane.cs");
    }
    private void _on_ButtonInitAndHomeAll_pressed()
    {
        _glx.stack.InitialiseAll();
        _glx.stack.HomeSearchAll();
    }
    private void _on_ButtonReturnShowResults_pressed()
    {
        _exp.PrintResults();
        _exp.SaveResultsToFile();
    }
    private void _on_ButtonLockUnlock_pressed()
    {
        _exp.Locked = !_exp.Locked;
        _exp.ReadingCurrentIndex = -1;
    }
}
