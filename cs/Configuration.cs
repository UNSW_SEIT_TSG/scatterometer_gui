using Godot;
using System;

public class Configuration : Control
{
    private Glx _glx;
    SpinBox spinbox0;
    SpinBox spinbox1;
    SpinBox spinbox2;
    SpinBox spinbox3;
    SpinBox spinboxPortSend;
    SpinBox spinboxPortReceive;
    Label labelRelayIPValuePreview;
    Label labelRelayPortSendIPValuePreview;
    Label labelRelayPortReceiveIPValuePreview;
    public override void _Ready()
    {
        _glx = (Glx)GetNode("/root/Glx");
        string prefix = "VBox/GridContainer/";
        _glx.PrintNodeAndChildren(GetNode(prefix+"LabelRelayIPValuePreview"));
        spinbox0 = (SpinBox)GetNode(prefix+"HBoxRelayIPValueEdit/SpinBox0");
        spinbox1 = (SpinBox)GetNode(prefix+"HBoxRelayIPValueEdit/SpinBox1");
        spinbox2 = (SpinBox)GetNode(prefix+"HBoxRelayIPValueEdit/SpinBox2");
        spinbox3 = (SpinBox)GetNode(prefix+"HBoxRelayIPValueEdit/SpinBox3");
        spinboxPortSend = (SpinBox)GetNode(prefix+"HBoxRelayPortSendValueEdit/SpinBoxRelayPortSend");
        spinboxPortReceive = (SpinBox)GetNode(prefix+"HBoxRelayPortReceiveValueEdit/SpinBoxRelayPortReceive");
        labelRelayIPValuePreview = (Label)GetNode(prefix+"LabelRelayIPValuePreview");
        labelRelayPortSendIPValuePreview = (Label)GetNode(prefix+"LabelRelayPortSendValuePreview");
        labelRelayPortReceiveIPValuePreview = (Label)GetNode(prefix+"LabelRelayPortReceiveValuePreview");

        spinbox0.Value = _glx.relay.IPByte0;
        spinbox1.Value = _glx.relay.IPByte1;
        spinbox2.Value = _glx.relay.IPByte2;
        spinbox3.Value = _glx.relay.IPByte3;
        spinboxPortSend.Value = _glx.relay.PortSend;
        spinboxPortReceive.Value = _glx.relay.PortReceive;
    }
    public void _on_ButtonReturnToMain_pressed()
    {
        SaveConfigurationToFile();
        _glx.GoToScene("res://scenes/MainMenu.tscn");
    }
    private void updateRelayIPPreview()
    {
        Label label = (Label)GetNode("VBox/GridContainer/LabelRelayIPValuePreview");
        label.SetText(
            spinbox0.Value.ToString()+"."+
            spinbox1.Value.ToString()+"."+
            spinbox2.Value.ToString()+"."+
            spinbox3.Value.ToString()
        );
    }
    public void _on_SpinBox0_value_changed(int value)
    {
        updateRelayIPPreview();
    }
    public void _on_SpinBox1_value_changed(int value)
    {
        updateRelayIPPreview();
    }
    public void _on_SpinBox2_value_changed(int value)
    {
        updateRelayIPPreview();
    }
    public void _on_SpinBox3_value_changed(int value)
    {
        updateRelayIPPreview();
    }
    public void _on_SpinBoxRelayPortSend_value_changed(int value)
    {
        labelRelayPortSendIPValuePreview.SetText(value.ToString());
    }
    public void _on_SpinBoxRelayPortReceive_value_changed(int value)
    {
        labelRelayPortReceiveIPValuePreview.SetText(value.ToString());
    }
    public void _on_ButtonSave_pressed()
    {
    }
    public void _on_ButtonLoad_pressed()
    {
        _glx.LoadConfigurationFromFile();
    }
    private void SaveConfigurationToFile()
    {
        string configurationsAsSingleString = 
            "relayIP                     = "+labelRelayIPValuePreview.Text+";\n"+
            "relayPortSend               = "+labelRelayPortSendIPValuePreview.Text+";\n"+
            "relayPortReceive            = "+labelRelayPortReceiveIPValuePreview.Text+";\n"+
            ""
        ;
        System.IO.File.WriteAllText("config.cfg", configurationsAsSingleString);
    }
}
