using Godot;
using System;

public class Stack : Control
{
    private Glx _glx;
    Camera camera;
    public override void _Ready()
    {
        _glx = (Glx)GetNode("/root/Glx");
        camera = (Camera)GetNode("VBox/VBoxContainer/ViewportContainer/Viewport/Camera");
        camera.LookAt(new Vector3(0,1.1f,0), new Vector3(0,1,0));
    }
    private void _on_ButtonReturnToMain_pressed()
    {
        _glx.GoToScene("res://scenes/MainMenu.tscn");
    }
    private void _on_ButtonCameraRandomlyLocate_pressed()
    {
        Random random = new Random();
        Vector3 pointInUnitCube = new Vector3(1,1,1);
        bool continueTheLoop = true;
        while (continueTheLoop)
        {
            Scalar x = new Scalar(random.NextDouble()-0.5f);
            Scalar y = new Scalar(random.NextDouble()/2);
            Scalar z = new Scalar(random.NextDouble()-0.5f);
            pointInUnitCube = new Vector3(x.Value,y.Value,z.Value);
            if (pointInUnitCube.Length() <= 0.5f)
            {
                continueTheLoop = false;
            }
        }
        Vector3 pointOnUnitSphere = pointInUnitCube.Normalized()*2;
        Scalar radius = new Scalar(2.5f);
        Vector3 point = pointOnUnitSphere*radius.Value;
        camera.SetTranslation(point);
        camera.LookAt(new Vector3(0,1.1f,0), new Vector3(0,1,0));
    }
}
