using Godot;
using System;
public class LinearStage
{
    private Glx _glx;
    private Scalar _datum;
    private Scalar _maximum;
    private Scalar _minimum;
    private Scalar _offset;
    private Scalar _rawPosition;
    private int _statusCode;
    private string _statusString;
    private string _groupName;
    private bool _waitingForUpdate;
    public Scalar Datum
    {
        get
        {
            return _datum;
        }
        set
        {
            _datum = value;
        }
    }
    public Scalar Maximum
    {
        get
        {
            return _maximum;
        }
        set
        {
            _maximum = value;
        }
    }
    public Scalar Minimum
    {
        get
        {
            return _minimum;
        }
        set
        {
            _minimum = value;
        }
    }
    public Scalar Offset
    {
        get
        {
            return _offset;
        }
        set
        {
            _offset = value;
        }
    }
    public int StatusCode
    {
        get
        {
            return _statusCode;
        }
        set
        {
            _statusCode = value;
        }
    }
    public string StatusString
    {
        get
        {
            return StatusCodeToStatusString(StatusCode);
        }
    }
    public Scalar PositionXPSC8
    {
        get
        {
            return _rawPosition;
        }
        set
        {
            _rawPosition = value;
        }
    }
    public Scalar PositionGUI
    {
        get
        {
            return PositionXPSC8+Offset;
        }
    }
    public bool WaitingForUpdate
    {
        get
        {
            return _waitingForUpdate;
        }
        set
        {
            _waitingForUpdate = value;
        }
    }
    public string GroupName
    {
        get
        {
            return _groupName;
        }
        set
        {
            _groupName = value;
        }
    }
    public LinearStage(Glx glx, string groupName)
    {
        _offset = new Scalar(0);
        _minimum = new Scalar(0);
        _maximum = new Scalar(0);
        
        PositionXPSC8 = new Scalar(0);
        WaitingForUpdate = false;
        StatusCode = -1;
        _glx = glx;
        _groupName = groupName.ToUpper();
    }
    private string StatusCodeToStatusString(int statusCode)
    {
        string statusString = "";
        switch (statusCode)
        {
            case 0:
                statusString = "Not initialized state";
                break;
            case 1:
                statusString = "Not initialized state due to an emergency brake : see positioner error";
                break;
            case 2:
                statusString = "Not initialized state due to an emergency stop : see positioner error";
                break;
            case 3:
                statusString = "Not initialized state due to a following error during homing";
                break;
            case 4:
                statusString = "Not initialized state due to a following error";
                break;
            case 5:
                statusString = "Not initialized state due to an homing timeout";
                break;
            case 6:
                statusString = "Not initialized state due to a motion done timeout during homing";
                break;
            case 7:
                statusString = "Not initialized state due to a GroupKill or KillAll command";
                break;
            case 8:
                statusString = "Not initialized state due to an end of run after homing";
                break;
            case 9:
                statusString = "Not initialized state due to an error during encoder calibration";
                break;
            case 10:
                statusString = "Ready state due to an AbortMove command";
                break;
            case 11:
                statusString = "Ready state from homing";
                break;
            case 12:
                statusString = "Ready state from motion";
                break;
            case 13:
                statusString = "Ready State due to a MotionEnable command";
                break;
            case 14:
                statusString = "Ready state from slave";
                break;
            case 15:
                statusString = "Ready state from jogging";
                break;
            case 16:
                statusString = "Ready state from analog tracking";
                break;
            case 17:
                statusString = "Ready state from trajectory";
                break;
            case 18:
                statusString = "Ready state from spinning";
                break;
            case 19:
                statusString = "undefined status";
                break;
            case 20:
                statusString = "Disabled state";
                break;
            case 21:
                statusString = "Disabled state due to a following error on ready state";
                break;
            case 22:
                statusString = "Disabled state due to a following error during motion";
                break;
            case 23:
                statusString = "Disabled state due to a motion done timeout during moving";
                break;
            case 24:
                statusString = "Disable state due to a following error on slave state";
                break;
            case 25:
                statusString = "Disable state due to a following error on jogging state";
                break;
            case 26:
                statusString = "Disabled state due to a following error during trajectory";
                break;
            case 27:
                statusString = "Disabled state due to a motion done timeout during trajectory";
                break;
            case 28:
                statusString = "Disabled state due to a following error during analog tracking";
                break;
            case 29:
                statusString = "Disabled state due to a master/slave error during motion";
                break;
            case 30:
                statusString = "Disable state due to a master/slave error on slave state";
                break;
            case 31:
                statusString = "Disable state due to a master/slave error on jogging state";
                break;
            case 32:
                statusString = "Disabled state due to a master/slave error during trajectory";
                break;
            case 33:
                statusString = "Disabled state due to a master/slave error during analog tracking";
                break;
            case 34:
                statusString = "Disabled state due to a master/slave error on ready state";
                break;
            case 35:
                statusString = "Disable state due to a following error on spinning state";
                break;
            case 36:
                statusString = "Disable state due to a master/slave error on spinning state";
                break;
            case 37:
                statusString = "Disable state due to a following error on auto-tuning state";
                break;
            case 38:
                statusString = "Disable state due to a master/slave error on auto-tuning state";
                break;
            case 39:
                statusString = "Disable state due to an emergency stop on auto-tuning state";
                break;
            case 40:
                statusString = "Emergency braking";
                break;
            case 41:
                statusString = "Motor initialization state";
                break;
            case 42:
                statusString = "Not referenced state";
                break;
            case 43:
                statusString = "Homing state";
                break;
            case 44:
                statusString = "Moving state";
                break;
            case 45:
                statusString = "Trajectory state";
                break;
            case 46:
                statusString = "Slave state due to a SlaveEnable command";
                break;
            case 47:
                statusString = "Jogging state due to a JogEnable command";
                break;
            case 48:
                statusString = "Analog tracking state";
                break;
            case 49:
                statusString = "Encoder calibrating state";
                break;
            case 50:
                statusString = "Not initialized state due to a mechanical zero inconsistency during homing";
                break;
            case 51:
                statusString = "Spinning state due to a SpinParametersSet command";
                break;
            default:
                statusString = "undefined status";
                break;
        }
        return statusString;
    }
    public void Initialise()
    {
        _glx.relay.to.Enqueue("xpsc8::GroupInitialize("+_groupName+")::Stage.cs");
    }
    public void HomeSearch()
    {
        _glx.relay.to.Enqueue("xpsc8::GroupHomeSearch("+_groupName+")::Stage.cs");
    }
    public void StatusGet()
    {
        _glx.relay.to.Enqueue("xpsc8::GroupStatusGet("+_groupName+",int  *)::Stage.cs");
    }
    public void PositionCurrentGet()
    {
        _glx.relay.to.Enqueue("xpsc8::GroupPositionCurrentGet("+_groupName+",double  *)::Stage.cs");
    }
    public void Move(Scalar position)
    {
        Move(position, "Stage.cs");
    }
    public void Move(Scalar position, string tag)
    {
        // This will issue a GroupMoveAbsolute command where the input position
        // for this function being with respect to the datum defined on the
        // MotorControl scene.

        string rawPositionString = (position-_offset).ToString();
        
        // For debugging:
            // GD.Print("_offset:"+_offset.ToString());
            // GD.Print("target:"+position.ToString());
            // GD.Print("position_for_xpsc8:"+rawPositionString);
        
        _glx.relay.to.Enqueue("xpsc8::GroupMoveAbsolute(" + GroupName + "," + rawPositionString + ")::"+tag);
        if(tag=="auto")
        {
            _glx.AutoCommandCount += 1;
        }
        StatusGet();
        PositionCurrentGet();
    }
}