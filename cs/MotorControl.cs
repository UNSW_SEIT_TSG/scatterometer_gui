using Godot;
using System;
public class MotorControl : Control
{
    bool updateNeeded;
    private Glx _glx;
    SpinBox spinBoxGroup1Offset;
    SpinBox spinBoxGroup2Offset;
    SpinBox spinBoxGroup3Offset;
    SpinBox spinBoxGroup4Offset;
    SpinBox spinBoxGroup5Offset;
    SpinBox spinBoxGroup6Offset;
    SpinBox spinBoxGroup7Offset;
    SpinBox spinBoxGroup1Minimum;
    SpinBox spinBoxGroup2Minimum;
    SpinBox spinBoxGroup3Minimum;
    SpinBox spinBoxGroup4Minimum;
    SpinBox spinBoxGroup5Minimum;
    SpinBox spinBoxGroup6Minimum;
    SpinBox spinBoxGroup7Minimum;
    SpinBox spinBoxGroup1Maximum;
    SpinBox spinBoxGroup2Maximum;
    SpinBox spinBoxGroup3Maximum;
    SpinBox spinBoxGroup4Maximum;
    SpinBox spinBoxGroup5Maximum;
    SpinBox spinBoxGroup6Maximum;
    SpinBox spinBoxGroup7Maximum;
    SpinBox spinBoxGroup1Current;
    SpinBox spinBoxGroup2Current;
    SpinBox spinBoxGroup3Current;
    SpinBox spinBoxGroup4Current;
    SpinBox spinBoxGroup5Current;
    SpinBox spinBoxGroup6Current;
    SpinBox spinBoxGroup7Current;
    SpinBox spinBoxGroup1Target;
    SpinBox spinBoxGroup2Target;
    SpinBox spinBoxGroup3Target;
    SpinBox spinBoxGroup4Target;
    SpinBox spinBoxGroup5Target;
    SpinBox spinBoxGroup6Target;
    SpinBox spinBoxGroup7Target;
    Button buttonGroup1MoveToTarget;
    Button buttonGroup2MoveToTarget;
    Button buttonGroup3MoveToTarget;
    Button buttonGroup4MoveToTarget;
    Button buttonGroup5MoveToTarget;
    Button buttonGroup6MoveToTarget;
    Button buttonGroup7MoveToTarget;
    Button buttonConnect;
    Button buttonDisconnect;
    Button buttonTemp;
    Button buttonUpdate;
    Button buttonUpdateMinMax;
    Label labelGroup1Status;
    Label labelGroup2Status;
    Label labelGroup3Status;
    Label labelGroup4Status;
    Label labelGroup5Status;
    Label labelGroup6Status;
    Label labelGroup7Status;

    public override void _Ready()
    {
        updateNeeded = true;
        spinBoxGroup1Offset = (SpinBox)GetNode("VBox/HBox2/VBox0/GridContainer/SpinBoxGroup1Offset");
        spinBoxGroup2Offset = (SpinBox)GetNode("VBox/HBox2/VBox0/GridContainer/SpinBoxGroup2Offset");
        spinBoxGroup3Offset = (SpinBox)GetNode("VBox/HBox2/VBox0/GridContainer/SpinBoxGroup3Offset");
        spinBoxGroup4Offset = (SpinBox)GetNode("VBox/HBox2/VBox0/GridContainer/SpinBoxGroup4Offset");
        spinBoxGroup5Offset = (SpinBox)GetNode("VBox/HBox2/VBox0/GridContainer/SpinBoxGroup5Offset");
        spinBoxGroup6Offset = (SpinBox)GetNode("VBox/HBox2/VBox0/GridContainer/SpinBoxGroup6Offset");
        spinBoxGroup7Offset = (SpinBox)GetNode("VBox/HBox2/VBox0/GridContainer/SpinBoxGroup7Offset");

        spinBoxGroup1Minimum = (SpinBox)GetNode("VBox/HBox2/VBox0/GridContainer/SpinBoxGroup1Minimum");
        spinBoxGroup2Minimum = (SpinBox)GetNode("VBox/HBox2/VBox0/GridContainer/SpinBoxGroup2Minimum");
        spinBoxGroup3Minimum = (SpinBox)GetNode("VBox/HBox2/VBox0/GridContainer/SpinBoxGroup3Minimum");
        spinBoxGroup4Minimum = (SpinBox)GetNode("VBox/HBox2/VBox0/GridContainer/SpinBoxGroup4Minimum");
        spinBoxGroup5Minimum = (SpinBox)GetNode("VBox/HBox2/VBox0/GridContainer/SpinBoxGroup5Minimum");
        spinBoxGroup6Minimum = (SpinBox)GetNode("VBox/HBox2/VBox0/GridContainer/SpinBoxGroup6Minimum");
        spinBoxGroup7Minimum = (SpinBox)GetNode("VBox/HBox2/VBox0/GridContainer/SpinBoxGroup7Minimum");

        spinBoxGroup1Maximum = (SpinBox)GetNode("VBox/HBox2/VBox0/GridContainer/SpinBoxGroup1Maximum");
        spinBoxGroup2Maximum = (SpinBox)GetNode("VBox/HBox2/VBox0/GridContainer/SpinBoxGroup2Maximum");
        spinBoxGroup3Maximum = (SpinBox)GetNode("VBox/HBox2/VBox0/GridContainer/SpinBoxGroup3Maximum");
        spinBoxGroup4Maximum = (SpinBox)GetNode("VBox/HBox2/VBox0/GridContainer/SpinBoxGroup4Maximum");
        spinBoxGroup5Maximum = (SpinBox)GetNode("VBox/HBox2/VBox0/GridContainer/SpinBoxGroup5Maximum");
        spinBoxGroup6Maximum = (SpinBox)GetNode("VBox/HBox2/VBox0/GridContainer/SpinBoxGroup6Maximum");
        spinBoxGroup7Maximum = (SpinBox)GetNode("VBox/HBox2/VBox0/GridContainer/SpinBoxGroup7Maximum");

        spinBoxGroup1Current = (SpinBox)GetNode("VBox/HBox2/VBox0/GridContainer/SpinBoxGroup1Current");
        spinBoxGroup2Current = (SpinBox)GetNode("VBox/HBox2/VBox0/GridContainer/SpinBoxGroup2Current");
        spinBoxGroup3Current = (SpinBox)GetNode("VBox/HBox2/VBox0/GridContainer/SpinBoxGroup3Current");
        spinBoxGroup4Current = (SpinBox)GetNode("VBox/HBox2/VBox0/GridContainer/SpinBoxGroup4Current");
        spinBoxGroup5Current = (SpinBox)GetNode("VBox/HBox2/VBox0/GridContainer/SpinBoxGroup5Current");
        spinBoxGroup6Current = (SpinBox)GetNode("VBox/HBox2/VBox0/GridContainer/SpinBoxGroup6Current");
        spinBoxGroup7Current = (SpinBox)GetNode("VBox/HBox2/VBox0/GridContainer/SpinBoxGroup7Current");

        spinBoxGroup1Target = (SpinBox)GetNode("VBox/HBox2/VBox0/GridContainer/SpinBoxGroup1Target");
        spinBoxGroup2Target = (SpinBox)GetNode("VBox/HBox2/VBox0/GridContainer/SpinBoxGroup2Target");
        spinBoxGroup3Target = (SpinBox)GetNode("VBox/HBox2/VBox0/GridContainer/SpinBoxGroup3Target");
        spinBoxGroup4Target = (SpinBox)GetNode("VBox/HBox2/VBox0/GridContainer/SpinBoxGroup4Target");
        spinBoxGroup5Target = (SpinBox)GetNode("VBox/HBox2/VBox0/GridContainer/SpinBoxGroup5Target");
        spinBoxGroup6Target = (SpinBox)GetNode("VBox/HBox2/VBox0/GridContainer/SpinBoxGroup6Target");
        spinBoxGroup7Target = (SpinBox)GetNode("VBox/HBox2/VBox0/GridContainer/SpinBoxGroup7Target");

        labelGroup1Status = (Label)GetNode("VBox/HBox2/VBox0/StageStatusGridContainer2/LabelGroup1Status");
        labelGroup2Status = (Label)GetNode("VBox/HBox2/VBox0/StageStatusGridContainer2/LabelGroup2Status");
        labelGroup3Status = (Label)GetNode("VBox/HBox2/VBox0/StageStatusGridContainer2/LabelGroup3Status");
        labelGroup4Status = (Label)GetNode("VBox/HBox2/VBox0/StageStatusGridContainer2/LabelGroup4Status");
        labelGroup5Status = (Label)GetNode("VBox/HBox2/VBox0/StageStatusGridContainer2/LabelGroup5Status");
        labelGroup6Status = (Label)GetNode("VBox/HBox2/VBox0/StageStatusGridContainer2/LabelGroup6Status");
        labelGroup7Status = (Label)GetNode("VBox/HBox2/VBox0/StageStatusGridContainer2/LabelGroup7Status");

        buttonConnect = (Button)GetNode("VBox/HBox2/VBox0/ButtonConnect");
        buttonDisconnect = (Button)GetNode("VBox/HBox2/VBox0/ButtonDisconnect");
        buttonTemp = (Button)GetNode("VBox/HBox2/VBox0/ButtonTemp");
        buttonUpdate = (Button)GetNode("VBox/HBox2/VBox0/ButtonUpdate");
        buttonUpdateMinMax = (Button)GetNode("VBox/HBox2/VBox0/ButtonUpdateMinMax");
        _glx = (Glx)GetNode("/root/Glx");
    }
    public void _on_ButtonReturnToMain_pressed()
    {
        _glx.GoToScene("res://scenes/MainMenu.tscn");
    }
    private void _on_ButtonConnect_pressed()
    {
        _glx.Connect();
    }
    private void _on_ButtonUpdate_pressed()
    {
        _glx.RequestPositionAndStatusUpdates();
    }
    private void _on_ButtonDisconnect_pressed()
    {
        _glx.Disconnect();
    }
    public override void _Process(float delta)
    {
        if (updateNeeded && _glx.relay.Connected)
        {
            _on_ButtonUpdateMinMax_pressed();
            _glx.RequestPositionAndStatusUpdates();
            updateNeeded = false;
        }
        buttonDisconnect.SetVisible(_glx.relay.Connected);
        // buttonTemp.SetVisible(glx.relay.Connected);
        buttonConnect.SetVisible(!_glx.relay.Connected);
        buttonUpdate.SetVisible(_glx.relay.Connected);
        // buttonUpdateMinMax.SetVisible(glx.relay.Connected);

        if (_glx.MotorControlSceneValuesChanged)
        {
            RotaryStage g1 = _glx.stack.rotaryStages["group1"];
            RotaryStage g2 = _glx.stack.rotaryStages["group2"];
            LinearStage g3 = _glx.stack.linearStages["group3"];
            LinearStage g4 = _glx.stack.linearStages["group4"];
            RotaryStage g5 = _glx.stack.rotaryStages["group5"];
            LinearStage g6 = _glx.stack.linearStages["group6"];
            RotaryStage g7 = _glx.stack.rotaryStages["group7"];
  
            spinBoxGroup1Current.SetValue((g1.PositionXPSC8+g1.Offset).Degrees);
            spinBoxGroup2Current.SetValue((g2.PositionXPSC8+g2.Offset).Degrees);
            spinBoxGroup3Current.SetValue(g3.PositionXPSC8.Value+g3.Offset.Value);
            spinBoxGroup4Current.SetValue(g4.PositionXPSC8.Value+g4.Offset.Value);
            spinBoxGroup5Current.SetValue(g5.PositionXPSC8.Degrees+g5.Offset.Degrees);
            spinBoxGroup6Current.SetValue(g6.PositionXPSC8.Value+g6.Offset.Value);
            spinBoxGroup7Current.SetValue(g7.PositionXPSC8.Degrees+g7.Offset.Degrees);

            labelGroup1Status.SetText(g1.StatusString);
            labelGroup2Status.SetText(g2.StatusString);
            labelGroup3Status.SetText(g3.StatusString);
            labelGroup4Status.SetText(g4.StatusString);
            labelGroup5Status.SetText(g5.StatusString);
            labelGroup6Status.SetText(g6.StatusString);
            labelGroup7Status.SetText(g7.StatusString);
          
            spinBoxGroup1Minimum.SetValue(g1.Minimum.Degrees+g1.Offset.Degrees);
            spinBoxGroup2Minimum.SetValue(g2.Minimum.Degrees+g2.Offset.Degrees);
            spinBoxGroup3Minimum.SetValue(g3.Minimum.Value+g3.Offset.Value);
            spinBoxGroup4Minimum.SetValue(g4.Minimum.Value+g4.Offset.Value);
            spinBoxGroup5Minimum.SetValue(g5.Minimum.Degrees+g5.Offset.Degrees);
            spinBoxGroup6Minimum.SetValue(g6.Minimum.Value+g6.Offset.Value);
            spinBoxGroup7Minimum.SetValue(g7.Minimum.Degrees+g7.Offset.Degrees);

            spinBoxGroup1Maximum.SetValue(g1.Maximum.Degrees+g1.Offset.Degrees);
            spinBoxGroup2Maximum.SetValue(g2.Maximum.Degrees+g2.Offset.Degrees);
            spinBoxGroup3Maximum.SetValue(g3.Maximum.Value+g3.Offset.Value);
            spinBoxGroup4Maximum.SetValue(g4.Maximum.Value+g4.Offset.Value);
            spinBoxGroup5Maximum.SetValue(g5.Maximum.Degrees+g5.Offset.Degrees);
            spinBoxGroup6Maximum.SetValue(g6.Maximum.Value+g6.Offset.Value);
            spinBoxGroup7Maximum.SetValue(g7.Maximum.Degrees+g7.Offset.Degrees);

            spinBoxGroup1Target.SetMin(g1.Minimum.Degrees+g1.Offset.Degrees);
            spinBoxGroup2Target.SetMin(g2.Minimum.Degrees+g2.Offset.Degrees);
            spinBoxGroup3Target.SetMin(g3.Minimum.Value+g3.Offset.Value);
            spinBoxGroup4Target.SetMin(g4.Minimum.Value+g4.Offset.Value);
            spinBoxGroup5Target.SetMin(g5.Minimum.Degrees+g5.Offset.Degrees);
            spinBoxGroup6Target.SetMin(g6.Minimum.Value+g6.Offset.Value);
            spinBoxGroup7Target.SetMin(g7.Minimum.Degrees+g7.Offset.Degrees);

            spinBoxGroup1Target.SetMax(g1.Maximum.Degrees+g1.Offset.Degrees);
            spinBoxGroup2Target.SetMax(g2.Maximum.Degrees+g2.Offset.Degrees);
            spinBoxGroup3Target.SetMax(g3.Maximum.Value+g3.Offset.Value);
            spinBoxGroup4Target.SetMax(g4.Maximum.Value+g4.Offset.Value);
            spinBoxGroup5Target.SetMax(g5.Maximum.Degrees+g5.Offset.Degrees);
            spinBoxGroup6Target.SetMax(g6.Maximum.Value+g6.Offset.Value);
            spinBoxGroup7Target.SetMax(g7.Maximum.Degrees+g7.Offset.Degrees);

            spinBoxGroup1Offset.SetValue(g1.Offset.Degrees);
            spinBoxGroup2Offset.SetValue(g2.Offset.Degrees);
            spinBoxGroup3Offset.SetValue(g3.Offset.Value);
            spinBoxGroup4Offset.SetValue(g4.Offset.Value);
            spinBoxGroup5Offset.SetValue(g5.Offset.Degrees);
            spinBoxGroup6Offset.SetValue(g6.Offset.Value);
            spinBoxGroup7Offset.SetValue(g7.Offset.Degrees);

            _glx.MotorControlSceneValuesChanged = false;
        }
    }
    private void UpdateOffset(RotaryStage group)
    {
        group.Offset = group.PositionXPSC8*-1;
        _glx.MotorControlSceneValuesChanged = true;
    }
    private void UpdateOffset(LinearStage group)
    {
        group.Offset = group.PositionXPSC8*-1;
        _glx.MotorControlSceneValuesChanged = true;
    }
    private void ResetOffset(RotaryStage group)
    {
        group.Offset = new Angle();
        _glx.MotorControlSceneValuesChanged = true;
    }
    private void ResetOffset(LinearStage group)
    {
        group.Offset = new Scalar(0);
        _glx.MotorControlSceneValuesChanged = true;
    }
    private void _on_ButtonGroup1SetDatum_pressed()
    {
        UpdateOffset(_glx.stack.rotaryStages["group1"]);
    }
    private void _on_ButtonGroup2SetDatum_pressed()
    {
        UpdateOffset(_glx.stack.rotaryStages["group2"]);
    }
    private void _on_ButtonGroup3SetDatum_pressed()
    {
        UpdateOffset(_glx.stack.linearStages["group3"]);
    }
    private void _on_ButtonGroup4SetDatum_pressed()
    {
        UpdateOffset(_glx.stack.linearStages["group4"]);
    }
    private void _on_ButtonGroup5SetDatum_pressed()
    {
        UpdateOffset(_glx.stack.rotaryStages["group5"]);
    }
    private void _on_ButtonGroup6SetDatum_pressed()
    {
        UpdateOffset(_glx.stack.linearStages["group6"]);
    }
    private void _on_ButtonGroup7SetDatum_pressed()
    {
        UpdateOffset(_glx.stack.rotaryStages["group7"]);
    }
    private void _on_ButtonGroup1MoveToTarget_pressed()
    {
        Angle ξ = new Angle();
        ξ.Degrees = spinBoxGroup1Target.Value;
        _glx.stack.rotaryStages["group1"].Move(ξ, "MotorControl");
    }
    private void _on_ButtonGroup2MoveToTarget_pressed()
    {
        Angle ξ = new Angle();
        ξ.Degrees = spinBoxGroup2Target.Value;
        _glx.stack.rotaryStages["group2"].Move(ξ, "MotorControl");
    }
    private void _on_ButtonGroup3MoveToTarget_pressed()
    {
        _glx.stack.linearStages["group3"].Move(new Scalar(spinBoxGroup3Target.Value), "MotorControl");
    }
    private void _on_ButtonGroup4MoveToTarget_pressed()
    {
        _glx.stack.linearStages["group4"].Move(new Scalar(spinBoxGroup4Target.Value), "MotorControl");
    }
    private void _on_ButtonGroup5MoveToTarget_pressed()
    {
        Angle ξ = new Angle();
        ξ.Degrees = spinBoxGroup5Target.Value;
        _glx.stack.rotaryStages["group5"].Move(ξ, "MotorControl");
    }
    private void _on_ButtonGroup6MoveToTarget_pressed()
    {
        _glx.stack.linearStages["group6"].Move(new Scalar(spinBoxGroup6Target.Value), "MotorControl");
    }
    private void _on_ButtonGroup7MoveToTarget_pressed()
    {
        Angle ξ = new Angle();
        ξ.Degrees = spinBoxGroup7Target.Value;
        _glx.stack.rotaryStages["group7"].Move(ξ, "MotorControl");
    }
    private void _on_ButtonGroup1ResetDatum_pressed()
    {
        ResetOffset(_glx.stack.rotaryStages["group1"]);
    }
    private void _on_ButtonGroup2ResetDatum_pressed()
    {
        ResetOffset(_glx.stack.rotaryStages["group2"]);
    }
    private void _on_ButtonGroup3ResetDatum_pressed()
    {
        ResetOffset(_glx.stack.linearStages["group3"]);
    }
    private void _on_ButtonGroup4ResetDatum_pressed()
    {
        ResetOffset(_glx.stack.linearStages["group4"]);
    }
    private void _on_ButtonGroup5ResetDatum_pressed()
    {
        ResetOffset(_glx.stack.rotaryStages["group5"]);
    }
    private void _on_ButtonGroup6ResetDatum_pressed()
    {
        ResetOffset(_glx.stack.linearStages["group6"]);
    }
    private void _on_ButtonGroup7ResetDatum_pressed()
    {
        ResetOffset(_glx.stack.rotaryStages["group7"]);
    }
    private void _on_ButtonTemp_pressed()
    {
        GD.Print("_on_ButtonTemp_pressed");
    }
    private void _on_ButtonUpdateMinMax_pressed()
    {
        for (int groupNumber = 1; groupNumber <= 7; groupNumber++)
        {
            _glx.relay.to.Enqueue("relay::GroupMinimumGet(GROUP"+groupNumber.ToString()+")"+"::MotorControl.cs");
            _glx.relay.to.Enqueue("relay::GroupMaximumGet(GROUP"+groupNumber.ToString()+")"+"::MotorControl.cs");
        }
    }
    private void _on_ButtonGroup1Initialise_pressed()
    {
        _glx.relay.to.Enqueue("xpsc8::GroupInitialize(GROUP1)"+"::MotorControl.cs");
        _glx.RequestPositionAndStatusUpdates();
    }
    private void _on_ButtonGroup2Initialise_pressed()
    {
        _glx.relay.to.Enqueue("xpsc8::GroupInitialize(GROUP2)"+"::MotorControl.cs");
        _glx.RequestPositionAndStatusUpdates();
    }
    private void _on_ButtonGroup3Initialise_pressed()
    {
        _glx.relay.to.Enqueue("xpsc8::GroupInitialize(GROUP3)"+"::MotorControl.cs");
        _glx.RequestPositionAndStatusUpdates();
    }
    private void _on_ButtonGroup4Initialise_pressed()
    {
        _glx.relay.to.Enqueue("xpsc8::GroupInitialize(GROUP4)"+"::MotorControl.cs");
        _glx.RequestPositionAndStatusUpdates();
    }
    private void _on_ButtonGroup5Initialise_pressed()
    {
        _glx.relay.to.Enqueue("xpsc8::GroupInitialize(GROUP5)"+"::MotorControl.cs");
        _glx.RequestPositionAndStatusUpdates();
    }
    private void _on_ButtonGroup6Initialise_pressed()
    {
        _glx.relay.to.Enqueue("xpsc8::GroupInitialize(GROUP6)"+"::MotorControl.cs");
        _glx.RequestPositionAndStatusUpdates();
    }
    private void _on_ButtonGroup7Initialise_pressed()
    {
        _glx.relay.to.Enqueue("xpsc8::GroupInitialize(GROUP7)"+"::MotorControl.cs");
        _glx.RequestPositionAndStatusUpdates();
    }
    private void _on_ButtonGroup1Home_pressed()
    {
        _glx.relay.to.Enqueue("xpsc8::GroupHomeSearch(GROUP1)"+"::MotorControl.cs");
        _glx.RequestPositionAndStatusUpdates();
    }
    private void _on_ButtonGroup2Home_pressed()
    {
        _glx.relay.to.Enqueue("xpsc8::GroupHomeSearch(GROUP2)"+"::MotorControl.cs");
        _glx.RequestPositionAndStatusUpdates();
    }
    private void _on_ButtonGroup3Home_pressed()
    {
        _glx.relay.to.Enqueue("xpsc8::GroupHomeSearch(GROUP3)"+"::MotorControl.cs");
        _glx.RequestPositionAndStatusUpdates();
    }
    private void _on_ButtonGroup4Home_pressed()
    {
        _glx.relay.to.Enqueue("xpsc8::GroupHomeSearch(GROUP4)"+"::MotorControl.cs");
        _glx.RequestPositionAndStatusUpdates();
    }
    private void _on_ButtonGroup5Home_pressed()
    {
        _glx.relay.to.Enqueue("xpsc8::GroupHomeSearch(GROUP5)"+"::MotorControl.cs");
        _glx.RequestPositionAndStatusUpdates();
    }
    private void _on_ButtonGroup6Home_pressed()
    {
        _glx.relay.to.Enqueue("xpsc8::GroupHomeSearch(GROUP6)"+"::MotorControl.cs");
        _glx.RequestPositionAndStatusUpdates();
    }
    private void _on_ButtonGroup7Home_pressed()
    {
        _glx.relay.to.Enqueue("xpsc8::GroupHomeSearch(GROUP7)"+"::MotorControl.cs");
        _glx.RequestPositionAndStatusUpdates();
    }
}
