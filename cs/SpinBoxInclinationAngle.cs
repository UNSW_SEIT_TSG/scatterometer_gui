using Godot;
using System;

public class SpinBoxInclinationAngle : SpinBox
{
    // Declare member variables here. Examples:
    // private int a = 2;
    // private string b = "text";

    // Called when the node enters the scene tree for the first time.
    public override void _Ready()
    {
        
    }

//  // Called every frame. 'delta' is the elapsed time since the previous frame.
//  public override void _Process(float delta)
//  {
//      
//  }
    private void _on_CheckButton_toggled(bool value){
        //GD.Print("Inc Angle Boom");
        this.Editable = !value;
        if(this.Editable) {
            this.Step=0.5f;
            }
        else{ 
            this.Step=0.001f;
            }
    }
}
