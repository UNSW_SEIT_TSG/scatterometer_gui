using Godot;
using System;

public class MainMenu : Control
{
    private Glx _glx;
    public override void _Ready()
    {
        _glx = (Glx)GetNode("/root/Glx");
    }
    private void _on_ButtonSwitchToConsole_pressed()
    {
        _glx.GoToScene("res://scenes/Console.tscn");
    }
    private void _on_ButtonSwitchToMotorControl_pressed()
    {
        _glx.GoToScene("res://scenes/MotorControl.tscn");
    }
    private void _on_ButtonSwitchToScatterPlane_pressed()
    {
        _glx.GoToScene("res://scenes/ScatterPlane.tscn");
    }
    private void _on_ButtonSwitchToConfig_pressed()
    {
        _glx.GoToScene("res://scenes/Configuration.tscn");
    }
    private void _on_ButtonSwitchToStack_pressed()
    {
        _glx.GoToScene("res://scenes/Stack.tscn");
    }
    private void _on_ButtonSwitchToAlphaBetaCalc_pressed()
    {
        _glx.GoToScene("res://scenes/AlphaBetaCalc.tscn");
    }
}
