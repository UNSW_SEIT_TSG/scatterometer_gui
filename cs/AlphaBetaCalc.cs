using Godot;
using System;

public class AlphaBetaCalc : Node
{
    // Member variables here, example:
    // private int a = 2;
    // private string b = "textvar";
    [Signal]
    public delegate void HomeSignal();
    private Glx _glx;
    public override void _Ready()
    {
        // Called every time the node is added to the scene.
        // Initialization here
        _glx = (Glx)GetNode("/root/Glx");
        EmitSignal(nameof(HomeSignal));
    }

//    public override void _Process(float delta)
//    {
//        // Called every frame. Delta is time since last frame.
//        // Update game logic here.
//        
//    }
    private void _on_SpinBoxBeta_value_changed(float value){
        GD.Print("Beta: "+value.ToString());
    }
    private void _on_SpinBoxAlpha_value_changed(float value){
        GD.Print("Alpha: "+value.ToString());
    }
    public void _on_ButtonReturnToMain_pressed()
    {
        
        string common = "./Node2D/VBoxContainer/HBoxTop/";
        SpinBox SpBxAlpha = (SpinBox)GetNode(common+"SpinBoxAlpha");
        SpinBox SpBxBeta = (SpinBox)GetNode(common+"SpinBoxBeta");

        common = "./Node2D/VBoxContainer/HBoxContainer/";
        SpinBox SpBxG2 = (SpinBox)GetNode(common+"SpinBoxGroup2");
        SpinBox SpBxG5 = (SpinBox)GetNode(common+"SpinBoxGroup5");
        SpinBox SpBxG7 = (SpinBox)GetNode(common+"SpinBoxGroup7");
        
        _glx.α = SpBxAlpha.GetValue();
        _glx.β = SpBxBeta.GetValue();
        _glx.αβg5 = SpBxG5.GetValue();
        _glx.αβg7 = SpBxG7.GetValue();

        _glx.GoToScene("res://scenes/MainMenu.tscn");
    }
}
