using Godot;
using System;

public class Console : Control
{
    Button btnAlignScatterPlane;
    Button btnConnect;
    Button btnControllerStatusGet;
    Button btnControllerStatusStringGet;
    Button btnDisconnect;
    Button btnErrorStringGet;
    Button btnFirmwareVersionGet;
    Button btnGroupMotionEnable;
    Button btnGroupMoveAbort;
    Button btnGroupMoveAbsolute;
    Button btnGroupMoveCustom;
    Button btnGroupMoveRelative;
    Button btnGroupPositionCurrentGet;
    Button btnHelp;
    Button btnHomeAll;
    Button btnInitialiseAll;
    Button btnMoveToNext;
    Button btnRebootXPSC8;
    Button btnReturnToDatum;
    Button btnReturnToMain;
    Button btnSend;
    Button btnTakeReading;
    HBoxContainer hBoxControllerStatusStringGet;
    HBoxContainer hBoxErrorStringGet;
    HBoxContainer hBoxGroupNameAndTarget;
    HBoxContainer hBoxGroupNames;
    HBoxContainer hBoxGroupStatusStringGet;
    HBoxContainer hBoxHardwareDateAndTimeSet;
    HBoxContainer hBoxHeartbeatPeriod;
    HBoxContainer hBoxLogin;
    OptionButton optBtnBaseCommand;
    OptionButton optBtnDay;
    OptionButton optBtnGroupName;
    OptionButton optBtnGroupName2;
    OptionButton optBtnHour;
    OptionButton optBtnMinute;
    OptionButton optBtnMonth;
    OptionButton optBtnSecond;
    OptionButton optBtnWeekday;
    OptionButton optBtnYear;
    SpinBox heartbeatPeriodSpinBox;
    SpinBox spinBoxControllerStatusStringGet;
    SpinBox spinBoxErrorStatusCode;
    SpinBox groupStatusStringGetSpinBoxGroupStatusCode;
    SpinBox spinBoxTarget;
    TextEdit loginTextBoxName;
    TextEdit loginTextBoxPassword;
    TextEdit textEditCommsLog;
    private Glx _glx;
    private Util _util;
    public override void _Ready()
    {
        _util = new Util();
        _glx = (Glx)GetNode("/root/Glx");
        string prefix = "VBox/";
        btnReturnToMain = (Button)GetNode(prefix+"HBox0/ButtonReturnToMain");
        
        prefix += "HBox1/VBox/HBox";
        btnHomeAll = (Button)GetNode(prefix+"/ButtonHomeAll");
        btnInitialiseAll = (Button)GetNode(prefix+"/ButtonInitialiseAll");
        btnAlignScatterPlane = (Button)GetNode(prefix+"2/ButtonAlignScatterPlane");
        btnMoveToNext = (Button)GetNode(prefix+"2/ButtonMoveToNext");
        btnRebootXPSC8 = (Button)GetNode(prefix+"2/ButtonRebootXPSC8");
        btnReturnToDatum = (Button)GetNode(prefix+"2/ButtonReturnToDatum");
        btnTakeReading = (Button)GetNode(prefix+"2/ButtonTakeReading");
        btnControllerStatusGet = (Button)GetNode(prefix+"3/ButtonControllerStatusGet");
        btnControllerStatusStringGet = (Button)GetNode(prefix+"3/ButtonControllerStatusStringGet");
        btnErrorStringGet = (Button)GetNode(prefix+"3/ButtonErrorStringGet");
        btnFirmwareVersionGet = (Button)GetNode(prefix+"3/ButtonFirmwareVersionGet");
        btnHelp = (Button)GetNode(prefix+"3/ButtonHelp");
        btnGroupMotionEnable = (Button)GetNode(prefix+"4/ButtonGroupMotionEnable");
        btnGroupMoveAbort = (Button)GetNode(prefix+"4/ButtonGroupMoveAbort");
        btnGroupMoveAbsolute = (Button)GetNode(prefix+"4/ButtonGroupMoveAbsolute");
        btnGroupMoveCustom = (Button)GetNode(prefix+"4/ButtonGroupMoveCustom");
        btnGroupMoveRelative = (Button)GetNode(prefix+"4/ButtonGroupMoveRelative");
        btnGroupPositionCurrentGet = (Button)GetNode(prefix+"4/ButtonGroupPositionCurrentGet");
        optBtnDay = (OptionButton)GetNode(prefix+"5/OptionButtonDay");
        optBtnHour = (OptionButton)GetNode(prefix+"5/OptionButtonHour");
        optBtnMinute = (OptionButton)GetNode(prefix+"5/OptionButtonMinute");
        optBtnMonth = (OptionButton)GetNode(prefix+"5/OptionButtonMonth");
        optBtnSecond = (OptionButton)GetNode(prefix+"5/OptionButtonSecond");
        optBtnWeekday = (OptionButton)GetNode(prefix+"5/OptionButtonWeekday");
        optBtnYear = (OptionButton)GetNode(prefix+"5/OptionButtonYear");
        optBtnBaseCommand = (OptionButton)GetNode(prefix+"6/OptionButtonBaseCommand");
        hBoxControllerStatusStringGet = (HBoxContainer)GetNode(prefix+"ControllerStatusStringGet");
        spinBoxControllerStatusStringGet = (SpinBox)GetNode(prefix+"ControllerStatusStringGet/SpinBoxControllerStatusStringGet");
        hBoxErrorStringGet = (HBoxContainer)GetNode(prefix+"ErrorStringGet");
        spinBoxErrorStatusCode = (SpinBox)GetNode(prefix+"ErrorStringGet/SpinBoxErrorStatusCode");
        hBoxGroupNameAndTarget = (HBoxContainer)GetNode(prefix+"GroupNameAndTarget");
        optBtnGroupName2 = (OptionButton)GetNode(prefix+"GroupNameAndTarget/optButtonGroupName");
        spinBoxTarget = (SpinBox)GetNode(prefix+"GroupNameAndTarget/SpinBoxTarget");
        hBoxGroupNames = (HBoxContainer)GetNode(prefix+"GroupNames");
        optBtnGroupName = (OptionButton)GetNode(prefix+"GroupNames/optButtonGroupName");
        hBoxGroupStatusStringGet = (HBoxContainer)GetNode(prefix+"GroupStatusStringGet");
        groupStatusStringGetSpinBoxGroupStatusCode = (SpinBox)GetNode(prefix+"GroupStatusStringGet/SpinBoxGroupStatusCode");
        hBoxHeartbeatPeriod = (HBoxContainer)GetNode(prefix+"HeartbeatPeriod");
        heartbeatPeriodSpinBox = (SpinBox)GetNode(prefix+"HeartbeatPeriod/SpinBox");
        hBoxLogin = (HBoxContainer)GetNode(prefix+"Login");
        loginTextBoxName = (TextEdit)GetNode(prefix+"Login/TextBoxName");
        loginTextBoxPassword = (TextEdit)GetNode(prefix+"Login/TextBoxPassword");
        
        textEditCommsLog = (TextEdit)GetNode("VBox/HBox2/TextEditCommsLog");
        
        prefix = "VBox/HBox3/Button";
        btnConnect = (Button)GetNode(prefix+"Connect");
        btnDisconnect = (Button)GetNode(prefix+"Disconnect");
        btnSend = (Button)GetNode(prefix+"Send");
        
        
        hBoxHardwareDateAndTimeSet = (HBoxContainer)GetNode("VBox/HBox1/VBox/HBox5");

        PopulateCommandList();
        PopulateGroupNames();
        InitialiseHardwareDateAndTimeSetDropDowns();
    }
    private void PopulateCommandList()
    {
        optBtnBaseCommand.Clear();
        optBtnBaseCommand.AddItem("HardwareDateAndTimeSet", 0);
        optBtnBaseCommand.AddItem("GetOpticalPowerMeterReading", 1);
        optBtnBaseCommand.AddItem("SetHeartbeatPeriod", 2);
        optBtnBaseCommand.AddItem("Login", 3);
        optBtnBaseCommand.AddItem("GroupStatusStringGet", 4);
        optBtnBaseCommand.AddItem("GroupHomeSearch", 5);
        optBtnBaseCommand.AddItem("GroupInitWithEncoderCalib", 6);
        optBtnBaseCommand.AddItem("GroupKill", 7);
        optBtnBaseCommand.AddItem("GroupMotionDisable", 8);
        optBtnBaseCommand.AddItem("GroupMotionEnable", 9);
        optBtnBaseCommand.AddItem("GroupMoveAbort", 10);
        optBtnBaseCommand.AddItem("GroupPositionCurrentGet", 11);
        optBtnBaseCommand.AddItem("GroupStatusGet", 12);
        optBtnBaseCommand.AddItem("GroupVelocityCurrentGet", 13);
        optBtnBaseCommand.AddItem("ErrorStringGet", 14);
        optBtnBaseCommand.AddItem("GroupHomeSearchAndRelativeMove", 15);
        optBtnBaseCommand.AddItem("GroupInitialise", 16);
        optBtnBaseCommand.AddItem("GroupMoveCustom", 17);
        optBtnBaseCommand.AddItem("GroupMoveAbsolute", 18);
        optBtnBaseCommand.AddItem("GroupMoveRelative", 19);
        optBtnBaseCommand.AddItem("ControllerStatusStringGet", 20);
        optBtnBaseCommand.AddItem("ControllerStatusGet", 21);
        optBtnBaseCommand.AddItem("FirmwareVersionGet", 22);
        optBtnBaseCommand.AddItem("HardwareDateAndTimeGet", 23);
        optBtnBaseCommand.AddItem("KillAll", 24);
        optBtnBaseCommand.AddItem("Reboot", 25);
        optBtnBaseCommand.AddItem("DatumReset", 26);
        optBtnBaseCommand.AddItem("HomeSearchAll", 27);
        // optionButtonBaseCommand.AddItem("InitializeAll", 28);
        optBtnBaseCommand.AddItem("InitialiseAll", 29);
        optBtnBaseCommand.AddItem("RandomDance", 30);
        optBtnBaseCommand.AddItem("ReconnectToXpsc8", 31);
        optBtnBaseCommand.AddItem("StopHeartbeat", 32);
        optBtnBaseCommand.AddItem("StartHeartbeat", 33);
    }
    private void PopulateGroupNames()
    {
        string prefix = "group_name=GROUP";
        int groupNameCount = 7;
        optBtnGroupName.Clear();
        optBtnGroupName2.Clear();
        for (int q = 0; q < groupNameCount; q++)
        {
            optBtnGroupName.AddItem(prefix+(q+1).ToString(), q+1);
            optBtnGroupName2.AddItem(prefix+(q+1).ToString(), q+1);
        }
    }
    private void PopulateYears()
    {
        optBtnYear.Clear();
        for (int q = 2018; q < 2118; q++)
        {
            optBtnYear.AddItem("year="+q.ToString(), q);
        }
    }
    private void PopulateWeekdays()
    {
        string[] weekdays = {"Sun","Mon","Tue","Wed","Thu","Fri","Sat"};
        optBtnWeekday.Clear();
        for (int i = 0; i < 7; i++)
        {
            optBtnWeekday.AddItem("weekday="+weekdays[i], i);
        }
    }
    private void PopulateMonths()
    {
        string[] months = {
            "Jan","Feb","Mar","Apr","May","Jun",
            "Jul","Aug","Sep","Oct","Nov","Dec"
        };
        optBtnMonth.Clear();
        for (int i = 0; i < 12; i++)
        {
            optBtnMonth.AddItem("month="+months[i], i);
        }
    }
    private void PopulateHoursOrMinutes(OptionButton optionButton, string prefix)
    {
        optionButton.Clear();
        for (int q = 0; q < 60; q++)
        {
            optionButton.AddItem(prefix+(q).ToString(), (q));
        }
    }
    private void PopulateSeconds()
    {
        PopulateHoursOrMinutes(optBtnSecond, "second=");
    }
    private void PopulateMinutes()
    {
        PopulateHoursOrMinutes(optBtnMinute, "minute=");
    }
    private void PopulateHours()
    {
        optBtnHour.Clear();
        for (int q = 0; q < 24; q++)
        {
            optBtnHour.AddItem("hour="+(q).ToString(), (q));
        }
    }
    private void InitialiseHardwareDateAndTimeSetDropDowns()
    {
        PopulateWeekdays();
        PopulateMonths();
        PopulateYears();
        PopulateDays();
        PopulateHours();
        PopulateMinutes();
        PopulateSeconds();
    }
    public void _on_ButtonReturnToMain_pressed()
    {
        _glx.GoToScene("res://scenes/MainMenu.tscn");
    }
    public void _on_OptionButtonMonth_item_selected(int month_identifier)
    {
        PopulateDays();
    }
    private void PopulateDays(){
        optBtnDay.Clear();
        
        int selected_month = optBtnMonth.GetSelectedId();
        int selected_year = optBtnYear.GetSelectedId();
        for (int q = 0; q < _util.DaysInMonth(selected_month, selected_year); q++)
        {
            optBtnDay.AddItem("day="+(q+1).ToString(), q+1);
        }
    }
    private void SetAllInvisible()
    {
        hBoxHardwareDateAndTimeSet.SetVisible(false);
        hBoxHeartbeatPeriod.SetVisible(false);
        hBoxLogin.SetVisible(false);
        hBoxGroupStatusStringGet.SetVisible(false);
        hBoxGroupNames.SetVisible(false);
        hBoxErrorStringGet.SetVisible(false);
        hBoxGroupNameAndTarget.SetVisible(false);
        hBoxControllerStatusStringGet.SetVisible(false);
    }
    private void _on_OptionButtonBaseCommand_item_selected(int identifier)
    {
        SetAllInvisible();
        switch (identifier)
        {
            case 0:
                hBoxHardwareDateAndTimeSet.SetVisible(true);
                break;
            case 1:
                // GetOpticalPowerMeterReading
                break;
            case 2:
                hBoxHeartbeatPeriod.SetVisible(true);
                break;
            case 3:
                hBoxLogin.SetVisible(true);
                break;
            case 4:
                hBoxGroupStatusStringGet.SetVisible(true);
                break;
            case 5:
                SetGroupNameModeVisibility();
                break;
            case 6:
                SetGroupNameModeVisibility();
                break;
            case 7:
                SetGroupNameModeVisibility();
                break;
            case 8:
                SetGroupNameModeVisibility();
                break;
            case 9:
                SetGroupNameModeVisibility();
                break;
            case 10:
                SetGroupNameModeVisibility();
                break;
            case 11:
                SetGroupNameModeVisibility();
                break;
            case 12:
                SetGroupNameModeVisibility();
                break;
            case 13:
                SetGroupNameModeVisibility();
                break;
            case 14:
                hBoxErrorStringGet.SetVisible(true);
                break;
            case 15:
                SetGroupNameAndTargetModeVisibility();
                break;
            case 16:
                // GroupInitialise
                SetGroupNameModeVisibility();
                break;
            case 17:
                SetGroupNameAndTargetModeVisibility();
                break;
            case 18:
                SetGroupNameAndTargetModeVisibility();
                break;
            case 19:
                SetGroupNameAndTargetModeVisibility();
                break;
            case 20:
                hBoxControllerStatusStringGet.SetVisible(true);
                break;
            case 21:
                // ControllerStatusGet(); note there are no parameters.
                break;
            case 22:
                // FirmwareVersionGet(); note there are no parameters.
                break;
            case 23:
                // HardwareDateAndTimeGet(); note there are no parameters.
                break;
            case 24:
                // KillAll(); note there are no parameters.
                break;
            case 25:
                // reboot(); note there are no parameters.
                break;
            case 26:
                // DatumReset(); note there are no parameters.
                break;
            case 27:
                // HomeSearchAll(); note there are no parameters.
                break;
            case 28:
                // InitializeAll(); note there are no parameters.
                break;
            case 29:
                // InitialiseAll(); note there are no parameters.
                break;
            case 30:
                // RandomDance(); note there are no parameters.
                break;
            case 31:
                // ReconnectToXpsc8(); note there are no parameters.
                break;
            case 32:
                // StopHeartbeat(); note there are no parameters.
                break;
            case 33:
                // StartHeartbeat(); note there are no parameters.
                break;
            default:
                GD.Print("Default");
                break;
        }
    }
    private void SetGroupNameModeVisibility()
    {
        Label labelGroupNameCommand = (Label)GetNode("VBox/HBox1/VBox/HBoxGroupNames/LabelGroupNameCommand");
        labelGroupNameCommand.SetText(optBtnBaseCommand.GetText());
        SetAllInvisible();
        hBoxGroupNames.SetVisible(true);
    }
    private void SetGroupNameAndTargetModeVisibility()
    {
        Label labelGroupNameCommand = (Label)GetNode("VBox/HBox1/VBox/HBoxGroupNameAndTarget/LabelGroupNameCommand");
        labelGroupNameCommand.SetText(optBtnBaseCommand.GetText());
        SetAllInvisible();
        hBoxGroupNameAndTarget.SetVisible(true);
    }
    private void _on_ButtonConnect_pressed()
    {
        _glx.Connect();
    }
    private void _on_ButtonDisconnect_pressed()
    {
        _glx.Disconnect();
    }
    private void _on_ButtonSend_pressed()
    {
        string suffixTag = "Console.cs";
        string commandString = "";
        GD.Print(
            optBtnBaseCommand.Text," ",
            optBtnBaseCommand.Selected.ToString(),
            optBtnBaseCommand.Selected == 2
        );
        if (optBtnBaseCommand.Selected == 1)
        {
            commandString += "opm::";
        }
        else if (
            optBtnBaseCommand.Selected == 2 ||
            optBtnBaseCommand.Selected == 32 ||
            optBtnBaseCommand.Selected == 33
        )
        {
            commandString += "relay::";
            if(optBtnBaseCommand.Selected == 2)
            {
                _glx.ReconfigurePulseCheck(new Second((float)(heartbeatPeriodSpinBox.Value*2)));
            }
        }
        else if (
            optBtnBaseCommand.Selected == 26 ||
            optBtnBaseCommand.Selected == 27 ||
            optBtnBaseCommand.Selected == 28 ||
            optBtnBaseCommand.Selected == 30
        )
        {
            commandString += "distributor::";
        }
        else
        {
            commandString += "xpsc8::";
        }
        
        commandString += optBtnBaseCommand.Text;
        commandString += "(";
        switch (optBtnBaseCommand.Selected)
        {
            case 0:
                // HardwareDateAndTimeSet
                commandString +=
                    _glx.ExtractValueFromKeyValueString(optBtnWeekday.Text)+","+
                    _glx.ExtractValueFromKeyValueString(optBtnMonth.Text)+","+
                    _glx.ExtractValueFromKeyValueString(optBtnYear.Text)+","+
                    _glx.ExtractValueFromKeyValueString(optBtnDay.Text)+","+
                    _glx.ExtractValueFromKeyValueString(optBtnHour.Text)+","+
                    _glx.ExtractValueFromKeyValueString(optBtnMinute.Text)+","+
                    _glx.ExtractValueFromKeyValueString(optBtnSecond.Text)
                ;
                break;
            case 1:
                // GetOpticalPowerMeterReading
                // No arguments necessary
                break;
            case 2:
                // SetHeartbeatPeriod
                commandString += heartbeatPeriodSpinBox.Value.ToString();
                break;
            case 3:
                // login
                commandString +=
                    loginTextBoxName.Text+","+
                    loginTextBoxPassword.Text
                ;
                break;
            case 4:
                // GroupStatusStringGet
                commandString = "xpsc8::GroupStatusStringGet("+groupStatusStringGetSpinBoxGroupStatusCode.Value.ToString()+",char *";
                break;
            case 5:
                // GroupHomeSearch
                commandString += _glx.ExtractValueFromKeyValueString(optBtnGroupName.Text);
                
                break;
            case 6:
                // GroupInitWithEncoderCalib
                commandString =
                    "GroupInitializeWithEncoderCalibration("+
                    _glx.ExtractValueFromKeyValueString(optBtnGroupName.Text)+
                    ")"
                ;
                break;
            case 7:
                // GroupKill
                commandString += _glx.ExtractValueFromKeyValueString(optBtnGroupName.Text);
                break;
            case 8:
                // GroupMotionDisable
                commandString += _glx.ExtractValueFromKeyValueString(optBtnGroupName.Text);
                break;
            case 9:
                // GroupMotionEnable
                commandString += _glx.ExtractValueFromKeyValueString(optBtnGroupName.Text);
                break;
            case 10:
                // GroupMoveAbort
                commandString += _glx.ExtractValueFromKeyValueString(optBtnGroupName.Text);
                break;
            case 11:
                // GroupPositionCurrentGet
                commandString += _glx.ExtractValueFromKeyValueString(optBtnGroupName.Text)+",double  *";
                break;
            case 12:
                // GroupStatusGet
                commandString += _glx.ExtractValueFromKeyValueString(optBtnGroupName.Text)+",int  *";
                break;
            case 13:
                // GroupVelocityCurrentGet
                commandString += _glx.ExtractValueFromKeyValueString(optBtnGroupName.Text)+",double  *";
                break;
            case 14:
                // ErrorStringGet
                commandString += spinBoxErrorStatusCode.Value.ToString()+",char *";
                break;
            case 15:
                // GroupHomeSearchAndRelativeMove
                commandString +=
                    _glx.ExtractValueFromKeyValueString(optBtnGroupName2.Text)+","+
                    spinBoxTarget.Value.ToString()
                ;
                break;
            case 16:
                // GroupInitialise
                commandString = "xpsc8::GroupInitialize("+_glx.ExtractValueFromKeyValueString(optBtnGroupName.Text)+")";
                break;
            case 17:
                // GroupMoveCustom
                commandString +=
                    _glx.ExtractValueFromKeyValueString(optBtnGroupName2.Text)+","+
                    spinBoxTarget.Value.ToString()
                ;
                break;
            case 18:
                // GroupMoveAbsolute
                commandString +=
                    _glx.ExtractValueFromKeyValueString(optBtnGroupName2.Text)+","+
                    spinBoxTarget.Value.ToString()
                ;
                break;
            case 19:
                // GroupMoveRelative
                commandString +=
                    _glx.ExtractValueFromKeyValueString(optBtnGroupName2.Text)+","+
                    spinBoxTarget.Value.ToString()
                ;
                break;
            case 20:
                // ControllerStatusStringGet
                commandString +=
                    spinBoxControllerStatusStringGet.Value.ToString()+
                    ",char *"
                ;
                break;
            case 21:
                // ControllerStatusGet
                // No Arguments.
                commandString += "int  *";
                break;
            case 22:
                // FirmwareVersionGet
                // No Arguments.
                commandString += "char *";
                break;
            case 23:
                // HardwareDateAndTimeGet
                commandString += "char *";
                break;
            case 24:
                // KillAll
                // No Arguments.
                break;
            case 25:
                // reboot
                // No Arguments.
                break;
            case 26:
                // DatumReset
                // No Arguments.
                break;
            case 27:
                // HomeSearchAll
                // No Arguments.
                break;
            case 29:
                // InitialiseAll
                // No Arguments.
                break;
            case 30:
                // RandomDance
                // No Arguments.
                break;
            case 31:
                // ReconnectToXpsc8
                // No Arguments.
                break;
            case 32:
                // StopHeartbeat
                break;
            case 33:
                // StartHeartbeat
                break;
            default:
                // default
                break;
        }
        commandString += ")";
        commandString += "::"+suffixTag;
        _glx.ConsoleMessageFlag = true;
        _glx.relay.to.Enqueue(commandString);
    }
    public override void _Process(float delta)
    {
        btnDisconnect.SetVisible(_glx.relay.Connected);
        btnSend.SetVisible(_glx.relay.Connected);
        btnConnect.SetVisible(!_glx.relay.Connected);

        if (_glx.ConsoleMessageFlag == true)
        {
            textEditCommsLog.Text = "";
            int length = _glx.commsLog.Count;
            int current_line = 0;
            int max_lines = 60;
            for (int i = length-1; i >= 0; i--)
            {
                string line = _glx.commsLog[i];
                textEditCommsLog.Text += line+"\n";
                if (current_line>max_lines)
                {
                    break;
                }
            }
            _glx.ConsoleMessageFlag = false;
        }
    }
}
