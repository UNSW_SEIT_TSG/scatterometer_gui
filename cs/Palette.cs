using Godot;
using System;
public class Palette
{
    public Color black;
    public Color sampleX;
    public Color sampleY;
    public Color sampleZ;
    public Color startAngleVector;
    public Color finalAngleVector;
    public Color readingArc;
    public Color readingPointTodo;
    public Color readingPointDoing;
    public Color readingPointDone;
    public Color readingPointError;
    public Color rotationAboutYAxisInXZPlaneZeroturnVector;
    public Color rotationAboutYAxisInXZPlaneHeadingVector;
    public Color rotationAboutYAxisInXZPlaneHalfturnVector;
    public Color inclinationVector;
    public Color inclinationArc;
    public Palette()
    {
        black = new Color(0,0,0);
        finalAngleVector = MakeColour(132, 126, 234);
        inclinationArc = MakeColour(124, 161, 48);
        inclinationVector = MakeColour(201, 166, 60);
        readingArc = MakeColour(226, 212, 56);
        readingPointDoing = MakeColour(228, 96, 220);
        readingPointDone = MakeColour(129, 218, 71);
        readingPointError = MakeColour(1,0, 0);
        readingPointTodo = MakeColour(233, 178, 45);
        rotationAboutYAxisInXZPlaneHalfturnVector = MakeColour(191, 225, 109);
        rotationAboutYAxisInXZPlaneHeadingVector  = MakeColour(221, 132, 46);
        rotationAboutYAxisInXZPlaneZeroturnVector = MakeColour(73, 174, 85);
        sampleX = MakeColour(232, 83, 136);
        sampleY = MakeColour(100, 220, 82);
        sampleZ = MakeColour(214, 94, 211);
        startAngleVector = MakeColour(178, 228, 51);
    }
    private Color MakeColour(float r, float g, float b)
    {
        float _σ = 256.0f;
        return new Color(r/_σ, g/_σ, b/_σ);
    }
}