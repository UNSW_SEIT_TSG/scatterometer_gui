using Godot;
using System;

public class SpinBoxGroup5 : SpinBox
{
    // Member variables here, example:
    // private int a = 2;
    // private string b = "textvar";
    private float beta_target;
    private float beta_tolerance = 0.01f;
    private float last_err;
    private float err;
    private Random rnd;
    private float step_factor;
    private Glx _glx;

    public override void _Ready()
    {
        // Called every time the node is added to the scene.
        // Initialization here
        // _glx = (Glx)GetNode("/root/Glx");
        
        
        beta_target = 90.0f;
        rnd = new Random();
        float err = (float)rnd.NextDouble();
        step_factor = 0.5f;

        // this.SetValue(_glx.αβg5);
    }

//    public override void _Process(float delta)
//    {
//        // Called every frame. Delta is time since last frame.
//        // Update game logic here.
//        
//    }
    private void searchG5(float beta){
        bool disableSearch = true;
        if(!disableSearch){
            // GD.Print(">>>>>>>>>>>>>>>>");
            last_err = err;
            err = beta_target - beta;
            if (Math.Round(Math.Abs(last_err)*100) == Math.Round(Math.Abs(err)*100))
            {
                // GD.Print("last_err == err\t"+err.ToString());
                step_factor = (float)rnd.NextDouble();
            }else{
                // GD.Print("last_err != err\t"+last_err.ToString()+" != "+err.ToString());
            }
            
            if (Math.Abs(err) > beta_tolerance)
            {
                // GD.Print("Search not yet over.");
                // float result = this.GetValue()-err*(float)rnd.NextDouble()*.45f;
                float result = this.GetValue()-err*step_factor;
                this.SetValue(result);
                
            }else{
                GD.Print("End search as error is within tolerance.");
                step_factor = 0.5f;
            }

            if(Math.Abs(this.GetValue()) >= 360 || Math.Abs(this.GetValue()) == 180){
                this.SetValue(rnd.Next()*360);
                GD.Print(":(");
            }
            // GD.Print("");
        }
    }
    private void _on_MeshInstanceSampleYZScatterIntersection_MyMeshInstanceSampleYZScatterIntersection(float beta){
        GD.Print("Model generated beta:"+ beta.ToString());
        // searchG5(beta);
    }
    private void _on_SpinBoxBeta_MySpinBoxBetaSignal(float target_beta){
        GD.Print("_on_SpinBoxBeta_MySpinBoxBetaSignal: "+target_beta.ToString());
        this.beta_target = target_beta;
    }
    // private void _on_SpinBoxBeta_value_changed(float target_beta){
    //     GD.Print("_on_SpinBoxBeta_value_changed: "+target_beta.ToString());
    //     this.beta_target = target_beta;
    //     Node home = this.GetParent().GetParent().GetParent().GetParent();
    //     SpinBox g5 = (SpinBox)home.GetNode("Node2D/VBoxContainer/HBoxContainer/SpinBoxGroup5");
    //     searchG5(g5.GetValue());
    // }

    private float jjk_alpha_beta_to_g5(float alpha, float beta){
        // Do something to get g5
        return alpha + beta;
    }

    private void _on_SpinBoxBeta_value_changed_or_SpinBoxAlpha_value_changed(float junk){
        string common = "../../HBoxTop/";
        SpinBox SpBxAlpha = (SpinBox)GetNode(common+"SpinBoxAlpha");
        SpinBox SpBxBeta = (SpinBox)GetNode(common+"SpinBoxBeta");
        
        float alpha = SpBxAlpha.GetValue();
        float beta = SpBxBeta.GetValue();
        float result = jjk_alpha_beta_to_g5(alpha, beta);

        this.SetValue(result);
    }
}
