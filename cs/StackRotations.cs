using System;
using System.Text;

class StackRotations
{
    public MyVector3 group2;
    public MyVector3 group5;
    public MyVector3 group7;
    public MyVector3 sampleNormal;
    public MyVector3 sampleX;
    private Angle _group2Min;
    private Angle _group5Min;
    private Angle _group7Min;
    private Angle _group2Max;
    private Angle _group5Max;
    private Angle _group7Max;
    public StackRotations()
    {
        _group2Min = new Angle();
        _group5Min = new Angle();
        _group7Min = new Angle();
        
        _group2Max = new Angle();
        _group5Max = new Angle();
        _group7Max = new Angle();

        group2 = new MyVector3(0,-1,0);

        Angle ξ = new Angle();
        ξ.Degrees = 19;
        group5 = new MyVector3(0,0,1).Rotated(group2, ξ);

        Angle ω = new Angle();
        ω.Degrees = -2;
        group7 = new MyVector3(-1, 0, 0).Rotated(group5, ω).Rotated(group2, ξ);

        Angle ρ = new Angle();
        ρ.Degrees = -4;
        sampleNormal = new MyVector3(0,0,-1).Rotated(group7, ρ).Rotated(group5, ω).Rotated(group2, ξ);
        sampleX = new MyVector3(-1,0,0).Rotated(group7, ρ).Rotated(group5, ω).Rotated(group2, ξ);

        _group2Min.Degrees =   70; // -170
        _group5Min.Degrees = -118;
        _group7Min.Degrees = -170;
        _group2Max.Degrees =  100; // 170
        _group5Max.Degrees =  160;
        _group7Max.Degrees =  170;
    }
    public override string ToString()
    {
        MyVector3 g2 = group2;
        MyVector3 g5 = group5;
        MyVector3 g7 = group7;
        MyVector3 sm = sampleNormal;
        return ""+
            "group_2: "+group2+
            "group_5: "+group5+
            "group_7: "+group7+
            "sample: "+sampleNormal+
            "sampleX:"+sampleX
        ;
    }
    public Angle Group2Min
    {
        get
        {
            return _group2Min;
        }
    }
    public Angle Group5Min
    {
        get
        {
            return _group5Min;
        }
    }
    public Angle Group7Min
    {
        get
        {
            return _group7Min;
        }
    }
    public Angle Group2Max
    {
        get
        {
            return _group2Max;
        }
    }
    public Angle Group5Max
    {
        get
        {
            return _group5Max;
        }
    }
    public Angle Group7Max
    {
        get
        {
            return _group7Max;
        }
    }
}