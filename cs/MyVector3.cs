using System;
using System.Text;
using Godot;
public class MyVector3
{
    private StandardBasis _e;
    private Scalar _x;
    private Scalar _y;
    private Scalar _z;
    private Scalar _magnitude;
    private Angle _α;
    private Angle _β;
    private Angle _γ;
    private Util _ut;
    public MyVector3(Vector3 godotVec)
    {
        _ut = new Util();
        _e = new StandardBasis();
        _x = new Scalar(godotVec.x);
        _y = new Scalar(godotVec.y);
        _z = new Scalar(godotVec.z);
        CalculateDirectionalCosines();
        CalculateMagnitude();
    }
    public MyVector3(int x, int y, int z)
    {
        _ut = new Util();
        _e = new StandardBasis();
        _x = new Scalar(x);
        _y = new Scalar(y);
        _z = new Scalar(z);
        CalculateDirectionalCosines();
        CalculateMagnitude();
    }
    public MyVector3(float x, float y, float z)
    {
        _ut = new Util();
        _e = new StandardBasis();
        _x = new Scalar(x);
        _y = new Scalar(y);
        _z = new Scalar(z);
        CalculateDirectionalCosines();
        CalculateMagnitude();
    }
    public MyVector3(double x, double y, double z)
    {
        _ut = new Util();
        _e = new StandardBasis();
        _x = new Scalar(x);
        _y = new Scalar(y);
        _z = new Scalar(z);
        CalculateDirectionalCosines();
        CalculateMagnitude();
    }
    public MyVector3(Scalar x, Scalar y, Scalar z)
    {
        _ut = new Util();
        _e = new StandardBasis();
        _x = x;
        _y = y;
        _z = z;
        CalculateDirectionalCosines();
        CalculateMagnitude();
    }
    public override string ToString(){return "<"+x.ToString()+","+y.ToString()+","+z.ToString()+">";}
    public string ToString(string format){return "<"+x.ToString(format)+","+y.ToString(format)+","+z.ToString(format)+">";}
    public MyVector3(Angle α, Angle β, Angle γ, Scalar magnitude)
    {
        _ut = new Util();
        _e = new StandardBasis();
        _magnitude = magnitude;
        _α = α;
        _β = β;
        _γ = γ;
        CalculateXYZFromDirectionalCosinesAndMagnitude();
    }
    private void CalculateXYZFromDirectionalCosinesAndMagnitude()
    {
        // Compute x, y, z from α, β, γ
        _x = _ut.Cos(_α)*_magnitude;
        _y = _ut.Cos(_β)*_magnitude;
        _z = _ut.Cos(_γ)*_magnitude;
    }
    private void CalculateDirectionalCosines()
    {
        CalculateDirectionalCosine_α();
        CalculateDirectionalCosine_β();
        CalculateDirectionalCosine_γ();
    }
    public Angle ComputeAngleBetweenVectors(MyVector3 u, MyVector3 v)
    {
        // Returns the angle between two Vector3 objects
        return _ut.Acos(ComputeDotProductBetweenVectors(u, v)/(u.Magnitude*v.Magnitude));
    }
    public Angle ComputeAngleBetweenVectors(UnitVector3 v)
    {
        // Returns the angle between a Vector3 and a UnitVector3 object
        return _ut.Acos(ComputeDotProductBetweenVectors(this, v)/this.Magnitude);
    }
    private void CalculateDirectionalCosine_α()
    {
        _α = ComputeAngleBetweenVectors(_e.i);
    }
    private void CalculateDirectionalCosine_β()
    {
        _β = ComputeAngleBetweenVectors(_e.j);
    }
    private void CalculateDirectionalCosine_γ()
    {
        _γ = ComputeAngleBetweenVectors(_e.k);
    }
    private void CalculateMagnitude()
    {
        _magnitude = _ut.EuclideanDistance3(_x, _y, _z);
    }
    public Scalar Magnitude
    {
        get
        {
            CalculateMagnitude();
            return _magnitude;
        }
        set
        {
            MyVector3 tempVec = new MyVector3(_α, _β, _γ, value);
            _x = tempVec.x;
            _y = tempVec.y;
            _z = tempVec.z;
            CalculateMagnitude();
            CalculateDirectionalCosines();
        }
    }
    public Scalar x
    {
        get
        {
            return _x;
        }
        set
        {
            _x = value;
            CalculateMagnitude();
            CalculateDirectionalCosines();
        }
    }
    public Scalar y
    {
        get
        {
            return _y;
        }
        set
        {
            _y = value;
            CalculateMagnitude();
            CalculateDirectionalCosines();
        }
    }
    public Scalar z
    {
        get
        {
            return _z;
        }
        set
        {
            _z = value;
            CalculateMagnitude();
            CalculateDirectionalCosines();
        }
    }
    public MyVector3 scaled(Scalar scalar)
    {
        return new MyVector3(_x, _y, _z)*scalar;
    }
    public static MyVector3 operator + (MyVector3 left, MyVector3 right)
    {
        return new MyVector3(left.x + right.x, left.y + right.y, left.z + right.z);
    }
    public static MyVector3 operator - (MyVector3 left, MyVector3 right)
    {
        return new MyVector3(left.x - right.x, left.y - right.y, left.z - right.z);
    }
    public static MyVector3 operator * (MyVector3 left, Scalar scalar)
    {
        return new MyVector3(left.x * scalar, left.y * scalar, left.z * scalar);
    }
    public Scalar ComputeDotProductBetweenVectors(MyVector3 u, MyVector3 v)
    {
        // Returns the dot product of two MyVector3 objects.
        return u.x*v.x + u.y*v.y + u.z*v.z;
    }
    public Scalar ComputeDotProductBetweenVectors(MyVector3 u, UnitVector3 v)
    {
        // Returns the dot product of a MyVector3 with a UnitVector3.
        return u.x*v.x + u.y*v.y + u.z*v.z;
    }
    public Scalar ComputeDotProductBetweenVectors(MyVector3 other)
    {
        return ComputeDotProductBetweenVectors(this, other);
    }
    public MyVector3 Rotated(MyVector3 axis, Angle dθ)
    {
        // Returns new vector rotated by some angle
        Scalar originalMagnitude = _magnitude;

        Scalar uX = axis.x;
        Scalar uY = axis.y;
        Scalar uZ = axis.z;

        Scalar adj = _ut.Cos(dθ);
        Scalar opp = _ut.Sin(dθ);
        
        Scalar
            r0c0, r0c1, r0c2,
            r1c0, r1c1, r1c2,
            r2c0, r2c1, r2c2
        ;

        // rotationMatrix
            // Zeroth row
            r0c0 = adj+uX*uX*(1-adj);
            r0c1 = uX*uY*(1-adj)-uZ*opp;
            r0c2 = uX*uZ*(1-adj)+uY*opp;
            
            // First row
            r1c0 = uY*uX*(1-adj)+uZ*opp;
            r1c1 = adj+uY*uY*(1-adj);
            r1c2 = uY*uZ*(1-adj)-uX*opp;
            
            // Second row
            r2c0 = uZ*uX*(1-adj)-uY*opp;
            r2c1 = uZ*uY*(1-adj)+uX*opp;
            r2c2 = adj+uZ*uZ*(1-adj);
        
        // rotationMatrix dot product with this vector
        MyVector3 result = new MyVector3(
            this.x*r0c0 + this.y*r0c1 + this.z*r0c2,
            this.x*r1c0 + this.y*r1c1 + this.z*r1c2,
            this.x*r2c0 + this.y*r2c1 + this.z*r2c2
        );
        result.Magnitude = originalMagnitude;
        return result;
    }
}
