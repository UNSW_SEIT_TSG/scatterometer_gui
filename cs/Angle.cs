using System;
using System.Text;
using Godot;
public class Angle
{
    float _degrees;
    float _radians;
    float π = (float)Math.PI;
    public Angle()
    {
        // Angle constructor.
        _degrees = 0;
        _radians = 0;
    }
    public float Degrees
    {
        // Degrees property of Angle object.
        get
        {
            return _degrees;
        }
        set
        {
            _degrees = value; // Update degrees with assigned value.
            _radians = RadiansFromDegrees(value); // Recalculate radians value.
        }
    }
    public float Radians
    {
        // Radians property of Angle object.
        get
        {
            return _radians;
        }
        set
        {
            _radians = value; // Update radians with assigned value.
            _degrees = DegreesFromRadians(value); // Recalculate degrees value.
        }
    }
    private float DegreesFromRadians(float radians)
    {
        // Returns degrees from a value in radians.
        return radians*180/π;
    }
    private float RadiansFromDegrees(float degrees)
    {
        // Returns radians from a value in degrees.
        return degrees*π/180;
    }
    public static Angle operator + (Angle left, Angle right)
    {
        // Defines how two Angles should be added together.
        Angle result = new Angle();  // Create a new Angle object.
        result.Degrees = left.Degrees + right.Degrees; // Sum left and right and assign to new Angle object.
        return result; // Return new Angle object.
    }
    public static Angle operator - (Angle left, Angle right)
    {
        // Defines how an Angle should be subtracted from another.
        Angle result = new Angle();  // Create a new Angle object.
        result.Degrees = left.Degrees - right.Degrees; // Subtract the right Angle from the left and assign to new Angle object.
        return result; // Return new Angle object.
    }
    public static Angle operator * (Angle left, int right)
    {
        // Defines multiplication of an Angle by a non-Angle integer, where integer value is second parameter.
        Angle result = new Angle();  // Create a new Angle object.
        result.Degrees = left.Degrees * right; // Multiply Angle by integer value and assign to new Angle object.
        return result; // Return new Angle object.
    }
    public static Angle operator * (int left, Angle right)
    {
        // Defines multiplication of an Angle by a non-Angle integer, where integer value is first parameter.
        Angle result = new Angle();  // Create a new Angle object.
        result.Degrees = right.Degrees * left; // Multiply integer value by Angle and assign to new Angle object.
        return result; // Return new Angle object.
    }
    public static Angle operator * (Angle left, double right)
    {
        // Defines multiplication of an Angle by a non-Angle double, where double value is second parameter.
        Angle result = new Angle();  // Create a new Angle object.
        result.Degrees = (float)(left.Degrees * right); // Multiply Angle by double value and assign to new Angle object.
        return result; // Return new Angle object.
    }
    public static Angle operator * (double left, Angle right)
    {
        // Defines multiplication of an Angle by a non-Angle double, where double value is first parameter.
        Angle result = new Angle();  // Create a new Angle object.
        result.Degrees = (float)(right.Degrees * left); // Multiply double by Angle value and assign to new Angle object.
        return result; // Return new Angle object.
    }
    public static Angle operator / (Angle left, int right)
    {
        // Defines division of an Angle by an integer.
        Angle result = new Angle();  // Create a new Angle object.
        result.Degrees = left.Degrees/(right*1.0f); // Upgrades scalar as integer to float and divides Angle by float scalar.
        return result; // Return new Angle object.
    }
    public static Angle operator - (Angle left)
    {
        // Defines negation of an angle
        Angle result = new Angle();  // Create a new Angle object.
        result.Degrees = left.Degrees*-1; // Take original Angle and multiply by negative 1, and assign to new Angle object.
        return result; // Return new Angle object.
    }
    public static bool operator > (Angle left, Angle right)
    {
        // Defines greater than comparison of two angles.
        return left.Degrees > right.Degrees;
    }
    public static bool operator < (Angle left, Angle right)
    {
        // Defines less than comparison of two angles.
        return left.Degrees < right.Degrees;
    }
    public override string ToString()
    {
        // Defines the string to be returned when an Angle object is to be expressed as a string.
        return Degrees.ToString("0.000")+"[°] = "+Radians.ToString("0.000")+" [rad]";
    }
}
