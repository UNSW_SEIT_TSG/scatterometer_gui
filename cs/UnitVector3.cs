using System;
using System.Text;

public class UnitVector3
{
    private Scalar _x;
    private Scalar _y;
    private Scalar _z;
    private Scalar _magnitude;
    public UnitVector3(int x, int y, int z)
    {
        UnitVector3 _u = new UnitVector3(
            new Scalar(x),
            new Scalar(y),
            new Scalar(z)
        );
        Util ut = new Util();
        _magnitude = ut.EuclideanDistance3(_u.x, _u.y, _u.z);
        if (_magnitude.Value == 0)
        {
            throw new DivideByZeroException("magnitude == 0");
        }
        _x = _u.x/_magnitude;
        _y = _u.y/_magnitude;
        _z = _u.z/_magnitude;
        _magnitude.Value = 1;
    }
    public UnitVector3(Scalar x, Scalar y, Scalar z)
    {
        Util ut = new Util();
        _magnitude = ut.EuclideanDistance3(x, y, z);
        if (_magnitude.Value == 0)
        {
            throw new DivideByZeroException("magnitude == 0");
        }
        _x = x/_magnitude;
        _y = y/_magnitude;
        _z = z/_magnitude;
        _magnitude.Value = 1;
    }
    public Scalar x
    {
        get
        {
            return _x;
        }
    }
    public Scalar y
    {
        get
        {
            return _y;
        }
    }
    public Scalar z
    {
        get
        {
            return _z;
        }
    }
    public Scalar Magnitude
    {
        get
        {
            return new Scalar(1);
        }
    }
}
