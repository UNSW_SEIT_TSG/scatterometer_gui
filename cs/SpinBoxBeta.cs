using Godot;
using System;

public class SpinBoxBeta : SpinBox
{
    // Member variables here, example:
    // private int a = 2;
    // private string b = "textvar";
    private Glx _glx;
    private float _value;

    [Signal]
    public delegate void MySpinBoxBetaSignal();
    public override void _Ready()
    {
        // Called every time the node is added to the scene.
        // Initialization here
        _glx = (Glx)GetNode("/root/Glx");
        this.SetValue(_glx.β);
    }
//    public override void _Process(float delta)
//    {
//        // Called every frame. Delta is time since last frame.
//        // Update game logic here.
//        
//    }
    private void _on_CheckButton_toggled(bool value){
        this.Editable = value;
        if(this.Editable) {
            this.Step=0.5f;
            }
        else{ 
            this.Step=0.001f;
            }
    }
}
