using Godot;
using System;

public class SpinBoxGroup7 : SpinBox
{
    // Member variables here, example:
    // private int a = 2;
    // private string b = "textvar";
    private Glx _glx;
    public override void _Ready()
    {
        // Called every time the node is added to the scene.
        // Initialization here
        // _glx = (Glx)GetNode("/root/Glx");
        // this.SetValue(_glx.αβg7);
    }

//    public override void _Process(float delta)
//    {
//        // Called every frame. Delta is time since last frame.
//        // Update game logic here.
//        
//    }
    private void _on_SpinBoxAlpha_MySpinBoxAlphaSignal(float value){
        this.SetValue(value);
    }
    private void _on_SpinBoxAlpha_value_changed(float value){
        GD.Print(value);
        try
        {
            this.SetValue(value);    
        }
        catch (System.Exception e)
        {
            GD.Print(e);
        }
        
    }
}
