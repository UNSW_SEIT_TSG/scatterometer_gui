using Godot;
using System;

public class LabelAOut : Label
{
    // Member variables here, example:
    // private int a = 2;
    // private string b = "textvar";

    public override void _Ready()
    {
        // Called every time the node is added to the scene.
        // Initialization here
        
    }

//    public override void _Process(float delta)
//    {
//        // Called every frame. Delta is time since last frame.
//        // Update game logic here.
//        
//    }

    private void _on_ButtonCalcA_pressed(){
        GD.Print("_on_ButtonCalcA_pressed() in LabelAOut.cs");
        this.SetText("!");
        Node home = this.GetParent().GetParent().GetParent().GetParent();
        // foreach (Node item in home.GetChildren()){
        //     GD.Print(item.GetName());
        // }

        // GD.Print(this.GetParent().GetParent().GetParent().GetParent().GetChildren());
        MeshInstance mi_sample_plane = (MeshInstance)home.GetNode("MeshInstanceSampleNormal");
        MeshInstance mi_scatter_plane = (MeshInstance)home.GetNode("MeshInstanceScatterPlane");
        Vector3 n_s = mi_sample_plane.GetTranslation();
        // Vector3 n_c = mi_scatter_plane
        GD.Print(mi_sample_plane.Name);
        GD.Print(mi_sample_plane.GetTranslation().ToString());
        Plane plane0 = new Plane(new Vector3(0,1,0), 0);
        Plane plane1 = new Plane(new Vector3(0,0,0.5f), 0);
        Vector3 intersection = plane0.Normal.Normalized().Cross(n_s.Normalized()).Normalized();
        GD.Print(intersection.ToString());
    }
}
