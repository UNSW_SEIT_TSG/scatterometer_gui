using Godot;
using System;
public class StackModel
{
    private Glx _glx;
    public System.Collections.Generic.Dictionary<string, RotaryStage> rotaryStages;
    public System.Collections.Generic.Dictionary<string, LinearStage> linearStages;
    public StackModel(Glx glx)
    {
        _glx = glx;
        rotaryStages = new System.Collections.Generic.Dictionary<string, RotaryStage>();
        linearStages = new System.Collections.Generic.Dictionary<string, LinearStage>();
    }
    public int RotaryStageCount
    {
        get
        {
            return rotaryStages.Count;
        }
    }
    public int LinearStageCount
    {
        get
        {
            return linearStages.Count;
        }
    }
    public int StageCount
    {
        get
        {
            return RotaryStageCount+LinearStageCount;
        }
    }
    public void AddRotaryStage(string groupName)
    {
        rotaryStages.Add(groupName, new RotaryStage(_glx, groupName));
    }
    public void AddLinearStage(string groupName)
    {
        linearStages.Add(groupName, new LinearStage(_glx, groupName));
    }
    public void InitialiseAll()
    {
        foreach (RotaryStage group in rotaryStages.Values)
        {
            group.Initialise();
            group.StatusGet();
        }
        foreach (LinearStage group in linearStages.Values)
        {
            group.Initialise();
            group.StatusGet();
        }
    }
    public void HomeSearchAll()
    {
        foreach (RotaryStage group in rotaryStages.Values)
        {
            group.HomeSearch();
            group.StatusGet();
        }
        foreach (LinearStage group in linearStages.Values)
        {
            group.HomeSearch();
            group.StatusGet();
        }
    }
}