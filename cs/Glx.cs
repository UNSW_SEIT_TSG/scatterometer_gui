using Godot;
using System;
using System.Diagnostics;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
public class Glx : Node
{
    private float _α;
    private float _β;
    private float _αβg5;
    private float _αβg7;
    public float α {
        get
        {
            return _α;
        }
        set
        {
            _α = value;
        }
    }
    public float β {
        get
        {
            return _β;
        }
        set
        {
            _β = value;
        }
    }
    public float αβg5 {
        get
        {
            return _αβg5;
        }
        set
        {
            _αβg5 = value;
        }
    }
    public float αβg7 {
        get
        {
            return _αβg7;
        }
        set
        {
            _αβg7 = value;
        }
    }
    private float π;
    private bool _consoleMessageFlag;
    private bool _motorControlSceneValuesChanged;
    private bool _waitingForCollection;
    private bool _waitingForResponse;
    private Second _timeOfLastReceivedMessage;
    private Scalar _lineShadowWidth;
    private Scalar _lineWidth;
    private Second _pollPeriod;
    private Second _pulseCheckPeriod;
    private int _autoCommandCount;
    private int _pollRotaryStagesActionCounter;
    private int _pollLinearStagesActionCounter;
    private static Godot.Timer pollScatterometer=new Godot.Timer();
    private static Godot.Timer pulseCheck=new Godot.Timer();
    private string _killConnectionCode;
    private string lastCommand;
    private string lastResponse;
    private string lastTag;
    private Util _util;
    private Vector3 _transformedNormalVector;
    public Experiment exp;
    public List<string> commsLog;
    public Node CurrentScene {get; set;}
    public Palette palette;
    public Relay relay;
    public StackModel stack;
    private Stopwatch lastMessageReceivedStopWatch;
    public Second PulseCheckPeriod
    {
        get
        {
            return _pulseCheckPeriod;
        }
        set
        {
            _pulseCheckPeriod = value;
            pulseCheck.SetWaitTime(_pulseCheckPeriod.Seconds);
        }
    }
    public bool WaitingForCollection
    {
        get
        {
            return _waitingForCollection;
        }
        set
        {
            _waitingForCollection = value;
        }
    }
    private bool WaitingForResponse
    {
        get
        {
            return _waitingForResponse;
        }
        set
        {
            _waitingForResponse = value;
        }
    }
    public bool ConsoleMessageFlag
    {
        get
        {
            return _consoleMessageFlag;
        }
        set
        {
            _consoleMessageFlag = value;
        }
    }
    public bool MotorControlSceneValuesChanged
    {
        get
        {
            return _motorControlSceneValuesChanged;
        }
        set
        {
            _motorControlSceneValuesChanged = value;
        }
    }
    public string ExtractValueFromKeyValueString(string keyValueString)
    {
        // Assumed something of the form: "weekday=Sun"
        // With return of "Sun"
        return keyValueString.Split("=")[1];
    }
    public override void _Ready()
    {
        α = 0;
        β = 0; // was 90 jjk
        αβg5 = 0;
        αβg7 = 0;

        _util = new Util();
        lastMessageReceivedStopWatch = new Stopwatch();
        _autoCommandCount = 0; // Initialise count; variable is used for automation of experiment.
        π = _util.π;
        exp = new Experiment(this);
        palette = new Palette();
        _lineShadowWidth = new Scalar(4);
        _lineWidth = new Scalar(2);
        _pollPeriod = new Second(3);
        _pulseCheckPeriod = new Second(2);
        lastCommand = "";
        lastResponse = "";
        MotorControlSceneValuesChanged = true;
        WaitingForResponse = false;
        
        stack = new StackModel(this);
        stack.AddRotaryStage("group1");
        stack.AddRotaryStage("group2");
        stack.AddLinearStage("group3");
        stack.AddLinearStage("group4");
        stack.AddRotaryStage("group5");
        stack.AddLinearStage("group6");
        stack.AddRotaryStage("group7");
        commsLog = new List<string>();
        _killConnectionCode = "!!!";
        
        Viewport root = GetTree().GetRoot();
        CurrentScene = root.GetChild(root.GetChildCount()-1);
        
        relay = new Relay(this);

        pollScatterometer.SetOneShot(false);
        pollScatterometer.Connect("timeout", GetNode("."), "PollAction");
        pollScatterometer.SetWaitTime(_pollPeriod.Seconds);
        AddChild(pollScatterometer);
        _pollRotaryStagesActionCounter = 0;
        _pollLinearStagesActionCounter = 0;
        pollScatterometer.Start();

        pulseCheck.SetOneShot(false);
        pulseCheck.Connect("timeout", GetNode("."), "CheckPulse");
        pulseCheck.SetWaitTime(_pulseCheckPeriod.Seconds);
        AddChild(pulseCheck);
        pulseCheck.Start();
    }
    public void ReconfigurePulseCheck(Second newPulseCheckPeriod)
    {
        PulseCheckPeriod = newPulseCheckPeriod;
        pulseCheck.Disconnect("timeout", GetNode("."), "CheckPulse");
        pulseCheck.Stop();
        pulseCheck.SetWaitTime(PulseCheckPeriod.Seconds);
        pulseCheck.Connect("timeout", GetNode("."), "CheckPulse");
        pulseCheck.Start();
        GD.Print("pulseCheck.TimeLeft:", pulseCheck.TimeLeft.ToString());
    }
    public void GoToScene(string path)
    {
        CallDeferred(nameof(DeferredGotoScene), path);
    }
    public void DeferredGotoScene(string path)
    {
        CurrentScene.Free();
        var nextScene = (PackedScene)GD.Load(path);
        CurrentScene = nextScene.Instance();
        GetTree().GetRoot().AddChild(CurrentScene);
        GetTree().SetCurrentScene(CurrentScene);
    }
    public Vector3 VectorOfLineFromTwoPlanesAssumedPassingThroughOrigin(Plane planeA, Plane planeB)
    {
        return planeA.Normal.Cross(planeB.Normal);;
    }
    public void PrintNodeAndChildren(Node nodo)
    {
        GD.Print(nodo.GetName()+":");
        foreach (Node child in nodo.GetChildren())
        {
            GD.Print(" "+child.GetName());
        }
    }
    public Vector3 CalculateNormal()
    {
        Vector3 originalAxis = new Vector3(1,0,0);
        Vector3 originalNormal = new Vector3(0,1,0);
        
        Angle ξ = new Angle();
        ξ.Radians = π/2;

        Angle θInclination = new Angle();
        θInclination= exp.ArcInclination + ξ;
        
        Angle rotAboutYinXZ = new Angle();
        rotAboutYinXZ = exp.ArcRotationAboutYAxisInXZPlane;

        Vector3 axis = originalAxis.Rotated(originalNormal, rotAboutYinXZ.Radians);
        return originalNormal.Rotated(axis, θInclination.Radians);
    }
    public Transform CalculateTransform()
    {
        Vector3 originalAxis = new Vector3(1,0,0);
        Vector3 originalNormal = new Vector3(0,1,0);
        
        Angle ξ = new Angle();
        Angle θInclination = new Angle();
        Angle rotAboutYinXZ = new Angle();
        Angle beta=new Angle(); //jjk
        Angle alpha=new Angle(); //jjk

        ξ.Radians = π/2;
        θInclination = exp.ArcInclination + ξ;
        rotAboutYinXZ = exp.ArcRotationAboutYAxisInXZPlane;
        alpha = rotAboutYinXZ;
        GD.Print("Glx calculate transform 1: exp.ArcRotationAboutYAxisInXZPlane ="+
                exp.ArcRotationAboutYAxisInXZPlane.ToString());
        // rotate the x axis about the y axis by rotAboutYinXZ
        Vector3 axis = originalAxis.Rotated(originalNormal, rotAboutYinXZ.Radians);
        var t = new Transform(Basis.Identity, Vector3.Zero);
        // rotate a x,y,z system about the new x axis by θInclination
        Transform tt =
            t.Rotated(
                axis,
                θInclination.Radians
            ).Rotated(  // rotate the new xyz system about the new normal by ArcRotationAboutYAxisInXZPlane
                TransformedNormal, // jjk: huh?
                exp.ArcRotationAboutYAxisInXZPlane.Radians
            );
        GD.Print("Glx calculate transform 2: exp.ArcRotationAboutYAxisInXZPlane ="+
                exp.ArcRotationAboutYAxisInXZPlane.ToString());
        return tt;
    }
    public Vector3 TransformedNormal
    {
        get
        {
            return CalculateNormal();
        }
    }
    public void DrawLineWithShadow(Node2D n2d, Vector2 to, Color color)
    {
        Vector2 from = new Vector2(0,0);
        DrawLineWithShadow(n2d:n2d, from:from, to:to, color:color);
    }
    public void DrawLineWithShadow(Node2D n2d, Vector2 from, Vector2 to, Color color)
    {
        n2d.DrawLine(from:from, to:to, color:palette.black, width:_lineShadowWidth.Value);
        n2d.DrawLine(from:from, to:to, color:color, width:_lineWidth.Value);
    }
    public void DrawPolylineWithShadow(Node2D n2d, Vector2[] points, Color c)
    {
        n2d.DrawPolyline(points, palette.black, _lineShadowWidth.Value);
        n2d.DrawPolyline(points, c, _lineWidth.Value);
    }
    public Connection relaySendConnection;
    public Connection relayReceiveConnection;
    public void DrawArcLine(
        Node2D context, Angle datumOffsetRad, bool CWPos,
        Vector2 centre, Angle startRad, Angle finalRad, Scalar radius,
        Color colA, Color colB, Color colC, int polylinePointCount=100,
        bool drawStartAngleLine=true, bool drawFinalAngleLine=true)
    {
        startRad = (startRad + datumOffsetRad)*_util.PolFromBool(CWPos);
        finalRad = (finalRad + datumOffsetRad)*_util.PolFromBool(CWPos);
        if(drawStartAngleLine){
            DrawLineWithShadow(
                n2d:context,
                from:centre,
                to:_util.XYFromθRO(startRad, radius, centre),
                color:colA
            );
        }
        if(drawFinalAngleLine)
        {
            DrawLineWithShadow(
                n2d:context,
                from:centre,
                to:_util.XYFromθRO(finalRad, radius, centre),
                color:colB
            );
        }
        Vector2[] points = new Vector2[polylinePointCount];
        int index = 0;
        
        Angle arcIntervalRad = new Angle();
        arcIntervalRad = (finalRad-startRad)/(polylinePointCount-1);
        
        Angle currentRad = new Angle();
        currentRad = startRad;

        while (index < polylinePointCount)
        {
            points[index] = _util.XYFromθRO(currentRad, radius, centre);
            index+=1;
            currentRad+=arcIntervalRad;
        }
        DrawPolylineWithShadow(context, points, colC);
    }
    public void DrawCircleWithShadow(Node2D n2d, Vector2 position, Scalar radius, Color color)
    {
        n2d.DrawCircle(position:position,radius:radius.Value+2,color:palette.black);
        n2d.DrawCircle(position:position,radius:radius.Value,color:color);
    }
    public void DrawEllipticalArc(
        Color colA, Color colB, Color colC,
        Node2D context,
        Angle datumOffsetRad, Angle finalRad, Angle startRad,
        Scalar radius, Scalar semiMajor, Scalar semiMinor,
        Vector2 centre,
        bool drawStartAngleLine=true, bool drawFinalAngleLine=true,
        int polylinePointCount=100)
    {

        Vector2[] points = new Vector2[polylinePointCount];
        int index = 0;

        Angle arcSpanRad = new Angle();
        arcSpanRad = finalRad-startRad;

        Angle arcIntervalRad = new Angle();
        arcIntervalRad = arcSpanRad/(polylinePointCount-1);

        Angle currentAngleRad = new Angle();
        currentAngleRad = startRad;

        Angle θ = new Angle();
        θ = datumOffsetRad;

        
        while (index < polylinePointCount)
        {
            Angle α = new Angle();
            α = currentAngleRad;

            points[index] = _util.PointOnEllipse(semiMajor, semiMinor, radius, α, θ);
            index+=1;
            currentAngleRad+=arcIntervalRad;
        }
        
        if(drawStartAngleLine)
        {
            Vector2 vec = _util.PointOnEllipse(semiMajor, semiMinor, radius, startRad, θ);
            DrawLineWithShadow(context, from: centre, to:vec, color:colA);
        }
        
        if(drawFinalAngleLine)
        {
            Vector2 vec = _util.PointOnEllipse(semiMajor, semiMinor, radius, finalRad, θ);
            DrawLineWithShadow(context, from: centre, to:vec, color:colB);
        }
        DrawPolylineWithShadow(context, points, colC);
    }
    public void PrintVector2List(Vector2[] list, string listName, int indentSize=0)
    {
        string spaces = "";
        for (int z = 0; z < indentSize; z++)
        {
            spaces += " ";
        }
        int index = 0;
        foreach (Vector2 item in list)
        {
            GD.Print(spaces+listName+"["+index.ToString()+"]:"+item);
            index += 1;
        }
    }
    public void Connect()
    {
        relaySendConnection = new Connection(
            internetProtocolAddressString: relay.IPAddressString,
            port: relay.PortSend
        );

        relayReceiveConnection = new Connection(
            internetProtocolAddressString: relay.IPAddressString,
            port: relay.PortReceive
        );
        try
        {
            TCPClient.StartClient(relaySendConnection);
            TCPClient.StartClient(relayReceiveConnection);
            relay.Connected = true;
            lastMessageReceivedStopWatch.Restart();
        }
        catch (System.Exception)
        {
            GD.Print("Connection attempt failed");
            relay.Connected = false;
        }
    }
    public void Disconnect()
    {
        TCPClient.Send(relaySendConnection, _killConnectionCode);
        TCPClient.ShutdownAndClose(relaySendConnection);
        TCPClient.ShutdownAndClose(relayReceiveConnection);
        relay.Connected = false;
    }
    private async void CollectInboundData()
    {
        await Task.Run(() => ObtainIncomingData());
    }
    private void ObtainIncomingData()
    {
        string received_data = TCPClient.receive(relayReceiveConnection);
        if(received_data.Length>0)
        {
            relay.from.Enqueue(received_data);
        }
        WaitingForCollection = false;
    }
    public override void _Process(float delta)
    {
        if(relay.Connected)
        {
            if(!WaitingForCollection) // Waiting on async process to complete
            {
                WaitingForCollection = true;
                CollectInboundData();
            }
            if(!WaitingForResponse) // Waiting because a message requiring a response has been sent.
            {
                // If there is a message to send then send it.
                if(relay.to.Count > 0)
                {
                    string destCommandTag = relay.to.Dequeue();
                    string[] lastDestCommandTagArray = 
                        destCommandTag.Split(
                            new string[] { "::" }, StringSplitOptions.None
                        )
                    ;

                    lastCommand = lastDestCommandTagArray[0]+"::"+lastDestCommandTagArray[1];
                    lastTag = lastDestCommandTagArray[2];

                    GD.Print(destCommandTag);
                    commsLog.Add(destCommandTag);
                    TCPClient.Send(relaySendConnection, destCommandTag);
                    ConsoleMessageFlag = true;
                    WaitingForResponse = true;
                }
            }
        }
        else
        {
            if(lastMessageReceivedStopWatch.IsRunning)
            {
                lastMessageReceivedStopWatch.Reset();
            }
        }
        if (relay.from.Count > 0)
        {
            lastResponse = relay.from.Dequeue();

            // Note that a message was received and reset Stopwatch.
            lastMessageReceivedStopWatch.Restart();

            if (lastResponse.Equals("$$--@$$")) // Filter heartbeat from feed
            {
                GD.Print("--@"); // Comment this out if the heartbeat is noisy.
            }
            else
            {
                GD.Print(lastResponse);
                commsLog.Add(lastResponse);
                ConsoleMessageFlag = true;
                WaitingForResponse = false;
                try
                {
                    ProcessResponse(lastCommand, lastResponse);
                    if(lastTag=="auto")
                    {
                        AutoCommandCount -= 1;
                    }
                }
                catch (System.Exception)
                {
                    GD.Print("Exception:");
                    GD.Print("  "+lastCommand);
                    GD.Print("  "+lastResponse);
                }
            }
        }
    }
    private void ProcessResponse(string destAndCommand, string response)
    {
        if (destAndCommand == "xpsc8::GroupPositionCurrentGet(GROUP1,double  *)")
        {
            string[] response_components = response.Replace("$","").Split(",");
            stack.rotaryStages["group1"].PositionXPSC8.Degrees = (float.Parse(response_components[1]));
            MotorControlSceneValuesChanged = true;
        }
        else if (destAndCommand == "xpsc8::GroupPositionCurrentGet(GROUP2,double  *)")
        {
            string[] response_components = response.Replace("$","").Split(",");
            stack.rotaryStages["group2"].PositionXPSC8.Degrees = (float.Parse(response_components[1]));
            MotorControlSceneValuesChanged = true;
        }
        else if (destAndCommand == "xpsc8::GroupPositionCurrentGet(GROUP3,double  *)")
        {
            string[] response_components = response.Replace("$","").Split(",");
            stack.linearStages["group3"].PositionXPSC8 = new Scalar(float.Parse(response_components[1]));
            MotorControlSceneValuesChanged = true;
        }
        else if (destAndCommand == "xpsc8::GroupPositionCurrentGet(GROUP4,double  *)")
        {
            string[] response_components = response.Replace("$","").Split(",");
            stack.linearStages["group4"].PositionXPSC8 = new Scalar(float.Parse(response_components[1]));
            MotorControlSceneValuesChanged = true;
        }
        else if (destAndCommand == "xpsc8::GroupPositionCurrentGet(GROUP5,double  *)")
        {
            string[] response_components = response.Replace("$","").Split(",");
            stack.rotaryStages["group5"].PositionXPSC8.Degrees = (float.Parse(response_components[1]));
            MotorControlSceneValuesChanged = true;
        }
        else if (destAndCommand == "xpsc8::GroupPositionCurrentGet(GROUP6,double  *)")
        {
            string[] response_components = response.Replace("$","").Split(",");
            stack.linearStages["group6"].PositionXPSC8 = new Scalar(float.Parse(response_components[1]));
            MotorControlSceneValuesChanged = true;
        }
        else if (destAndCommand == "xpsc8::GroupPositionCurrentGet(GROUP7,double  *)")
        {
            string[] response_components = response.Replace("$","").Split(",");
            stack.rotaryStages["group7"].PositionXPSC8.Degrees = (float.Parse(response_components[1]));
            MotorControlSceneValuesChanged = true;
        }
        else if (destAndCommand == "xpsc8::GroupStatusGet(GROUP1,int  *)")
        {
            string[] response_components = response.Replace("$","").Split(",");
            stack.rotaryStages["group1"].StatusCode = Int16.Parse(response_components[1]);
            MotorControlSceneValuesChanged = true;
        }
        else if (destAndCommand == "xpsc8::GroupStatusGet(GROUP2,int  *)")
        {
            string[] response_components = response.Replace("$","").Split(",");
            stack.rotaryStages["group2"].StatusCode = Int16.Parse(response_components[1]);
            MotorControlSceneValuesChanged = true;
        }
        else if (destAndCommand == "xpsc8::GroupStatusGet(GROUP3,int  *)")
        {
            string[] response_components = response.Replace("$","").Split(",");
            stack.linearStages["group3"].StatusCode = Int16.Parse(response_components[1]);
            MotorControlSceneValuesChanged = true;
        }
        else if (destAndCommand == "xpsc8::GroupStatusGet(GROUP4,int  *)")
        {
            string[] response_components = response.Replace("$","").Split(",");
            stack.linearStages["group4"].StatusCode = Int16.Parse(response_components[1]);
            MotorControlSceneValuesChanged = true;
        }
        else if (destAndCommand == "xpsc8::GroupStatusGet(GROUP5,int  *)")
        {
            string[] response_components = response.Replace("$","").Split(",");
            stack.rotaryStages["group5"].StatusCode = Int16.Parse(response_components[1]);
            MotorControlSceneValuesChanged = true;
        }
        else if (destAndCommand == "xpsc8::GroupStatusGet(GROUP6,int  *)")
        {
            string[] response_components = response.Replace("$","").Split(",");
            stack.linearStages["group6"].StatusCode = Int16.Parse(response_components[1]);
            MotorControlSceneValuesChanged = true;
        }
        else if (destAndCommand == "xpsc8::GroupStatusGet(GROUP7,int  *)")
        {
            string[] response_components = response.Replace("$","").Split(",");
            stack.rotaryStages["group7"].StatusCode = Int16.Parse(response_components[1]);
            MotorControlSceneValuesChanged = true;
        }
        else if (destAndCommand == "relay::GroupMinimumGet(GROUP1)")
        {
            stack.rotaryStages["group1"].Minimum.Degrees = (Int16.Parse(response.Replace("$","")));
            MotorControlSceneValuesChanged = true;
        }
        else if (destAndCommand == "relay::GroupMinimumGet(GROUP2)")
        {
            stack.rotaryStages["group2"].Minimum.Degrees = (Int16.Parse(response.Replace("$","")));
            MotorControlSceneValuesChanged = true;
        }
        else if (destAndCommand == "relay::GroupMinimumGet(GROUP3)")
        {
            stack.linearStages["group3"].Minimum = new Scalar(Int16.Parse(response.Replace("$","")));
            MotorControlSceneValuesChanged = true;
        }
        else if (destAndCommand == "relay::GroupMinimumGet(GROUP4)")
        {
            stack.linearStages["group4"].Minimum = new Scalar(Int16.Parse(response.Replace("$","")));
            MotorControlSceneValuesChanged = true;
        }
        else if (destAndCommand == "relay::GroupMinimumGet(GROUP5)")
        {
            stack.rotaryStages["group5"].Minimum.Degrees = (Int16.Parse(response.Replace("$","")));
            MotorControlSceneValuesChanged = true;
        }
        else if (destAndCommand == "relay::GroupMinimumGet(GROUP6)")
        {
            stack.linearStages["group6"].Minimum = new Scalar(Int16.Parse(response.Replace("$","")));
            MotorControlSceneValuesChanged = true;
        }
        else if (destAndCommand == "relay::GroupMinimumGet(GROUP7)")
        {
            stack.rotaryStages["group7"].Minimum.Degrees = (Int16.Parse(response.Replace("$","")));
            MotorControlSceneValuesChanged = true;
        }
        else if (destAndCommand == "relay::GroupMaximumGet(GROUP1)")
        {
            stack.rotaryStages["group1"].Maximum.Degrees = (Int16.Parse(response.Replace("$","")));
            MotorControlSceneValuesChanged = true;
        }
        else if (destAndCommand == "relay::GroupMaximumGet(GROUP2)")
        {
            stack.rotaryStages["group2"].Maximum.Degrees = (Int16.Parse(response.Replace("$","")));
            MotorControlSceneValuesChanged = true;
        }
        else if (destAndCommand == "relay::GroupMaximumGet(GROUP3)")
        {
            stack.linearStages["group3"].Maximum = new Scalar(Int16.Parse(response.Replace("$","")));
            MotorControlSceneValuesChanged = true;
        }
        else if (destAndCommand == "relay::GroupMaximumGet(GROUP4)")
        {
            stack.linearStages["group4"].Maximum = new Scalar(Int16.Parse(response.Replace("$","")));
            MotorControlSceneValuesChanged = true;
        }
        else if (destAndCommand == "relay::GroupMaximumGet(GROUP5)")
        {
            stack.rotaryStages["group5"].Maximum.Degrees = (Int16.Parse(response.Replace("$","")));
            MotorControlSceneValuesChanged = true;
        }
        else if (destAndCommand == "relay::GroupMaximumGet(GROUP6)")
        {
            stack.linearStages["group6"].Maximum = new Scalar(Int16.Parse(response.Replace("$","")));
            MotorControlSceneValuesChanged = true;
        }
        else if (destAndCommand == "relay::GroupMaximumGet(GROUP7)")
        {
            stack.rotaryStages["group7"].Maximum.Degrees = (Int16.Parse(response.Replace("$","")));
            MotorControlSceneValuesChanged = true;
        }
        else if (destAndCommand == "opm::GetOpticalPowerMeterReading()")
        {
            GD.Print("opm::GetOpticalPowerMeterReading() --> "+response);
            string[] responseArray =
                response
                .Replace("$", "")
                .Replace("(", "")
                .Replace(")", "")
                .Replace(" ", "")
                .Split(',')
            ;
            string mean = responseArray[0];
            string standardDeviation = responseArray[1];
            exp.TakeReading(mean,standardDeviation);
        }
        else
        {
            GD.Print("Unexpected command response pair: "+destAndCommand+", "+response+"");
        }
    }
    public void StatusCheck()
    {
        foreach (RotaryStage group in stack.rotaryStages.Values)
        {
            GD.Print(group.StatusCode);
            GD.Print(group.StatusString);
        }
        foreach (LinearStage group in stack.linearStages.Values)
        {
            GD.Print(group.StatusCode);
            GD.Print(group.StatusString);
        }
    }
    private void CheckPulse()
    {
        if(relay.Connected)
        {
            if(lastMessageReceivedStopWatch.ElapsedMilliseconds > _pulseCheckPeriod.Milliseconds)
            {
                GD.Print("Pulse lost");
                Disconnect();
            }
            else
            {
                GD.Print("Pulse ok");
            }
        }
    }
    private void PollAction()
    {
        if (relay.Connected)
        {
            if(relay.to.Count < 2)
            {
                List<RotaryStage> rotaryStagesList = stack.rotaryStages.Values.ToList();
                List<LinearStage> linearStagesList = stack.linearStages.Values.ToList();
                RotaryStage rotaryGroup = rotaryStagesList[_pollRotaryStagesActionCounter];
                LinearStage linearGroup = linearStagesList[_pollLinearStagesActionCounter];
                rotaryGroup.StatusGet();
                linearGroup.StatusGet();
                rotaryGroup.PositionCurrentGet();
                linearGroup.PositionCurrentGet();
                _pollRotaryStagesActionCounter += 1;
                _pollLinearStagesActionCounter += 1;
                _pollRotaryStagesActionCounter = _pollRotaryStagesActionCounter % rotaryStagesList.Count;
                _pollLinearStagesActionCounter = _pollLinearStagesActionCounter % linearStagesList.Count;
            }
        }
    }
    public void RequestPositionAndStatusUpdates()
    {
        foreach (RotaryStage group in stack.rotaryStages.Values)
        {
            group.StatusGet();
            group.PositionCurrentGet();
        }
        foreach (LinearStage group in stack.linearStages.Values)
        {
            group.StatusGet();
            group.PositionCurrentGet();
        }
    }
    public void LoadConfigurationFromFile()
    {
        relay = new Relay(this);
    }
    public void stripAutoFromQueue()
    {
        Queue<string> tempQueue = new Queue<string>();
        string destCommandTag;
        string[] destCmdTagArray;

        // Go through each item in the queue bound for the relay
        // If it was tagged 'auto' do not keep it.
        // Re-build the queue with the remaining messages.
        while (relay.to.Count > 0)
        {
            destCommandTag = relay.to.Dequeue();
            destCmdTagArray = destCommandTag.Split(
                new string[] { "::" }, StringSplitOptions.None
            );

            if(destCmdTagArray[2] != "auto")
            {
                tempQueue.Enqueue(
                    destCmdTagArray[0]+"::"+destCmdTagArray[1]+"::"+destCmdTagArray[2]
                );
            }
        }

        while (tempQueue.Count > 0)
        {
            relay.to.Enqueue(tempQueue.Dequeue());
        }
    }
    public int AutoCommandCount
    {
        // AutoCommandCount property of Glx.
        // This property represents the progress of the experiment when
        // measurement and motion is automated.
        get
        {
            return _autoCommandCount;
        }
        set
        {
            _autoCommandCount = value;
        }
    }
    public Vector3 GetSampleNormalInLabFrame()
    {
        // Express initial sample frame vectors in lab frame terms
        // Vector3 xSampleLabFrame = new Vector3(-1,0,0);
        Vector3 ySampleLabFrame = new Vector3(0,0,1);
        // Vector3 zSampleLabFrame = new Vector3(0,1,0);

        // Return positive sample y axis in lab frame
        return TransformSampleFrameVectorToLabFrame(ySampleLabFrame);
    }
    public Vector3 GetSampleXInLabFrame()
    {
        Vector3 xSampleLabFrame = new Vector3(-1,0,0);
        return TransformSampleFrameVectorToLabFrame(xSampleLabFrame);
    }
    public Vector3 TransformSampleFrameVectorToLabFrame(Vector3 sampleFrameVector)
    {
        // Lab Axes
        Vector3 labX = new Vector3(1,0,0);
        Vector3 labY = new Vector3(0,1,0);
        Vector3 labZ = new Vector3(0,0,1);

        Vector3 result = sampleFrameVector;

        // Rotate sample frame around positive lab z axis
        Angle λ = exp.ArcRotationAboutYAxisInXZPlane;
        result = result.Rotated(labZ, λ.Radians);

        // Rotate sample frame around positive lab x axis
        Angle μ = exp.ArcInclination;
        result = result.Rotated(labX, μ.Radians);

        // Rotate sample frame around positive lab y axis to position inbound light beam
        Angle offset = new Angle();
        offset.Radians = π/2;
        Angle ξ = exp.ArcStart - offset;
        result = result.Rotated(-labY, ξ.Radians);
        
        // Return positive sample y axis in lab frame
        return result;
    }
}
