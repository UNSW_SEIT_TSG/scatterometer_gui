using Godot;
using System;
using System.Net;
using System.Net.Sockets;
using System.Threading;
using System.Text;

public class Connection{
    public bool connected = false;
    public byte[] bytes = new byte[1024];
    public IPAddress  ipAddress;
    public IPEndPoint remoteEP;
    public Socket sckt;
    public Connection(String ip_address_string, Int32 port){
        // Connect to a remote device
        try{
            ip_address_string = ip_address_string.Trim();
            this.ipAddress = System.Net.IPAddress.Parse(ip_address_string);
            this.remoteEP  = new IPEndPoint(this.ipAddress,port);
            this.sckt = new Socket(ipAddress.AddressFamily, SocketType.Stream, ProtocolType.Tcp);
        }catch (Exception e){
            GD.Print(e.ToString());
        }
    }
}