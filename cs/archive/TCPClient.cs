using Godot;
using System;
using System.Net;
using System.Net.Sockets;
using System.Threading;
using System.Text;

public class TCPClient : Node{
    public static void start_client(Connection connection){
        // Connect the socket to the remote endpoint. Catch any errors
        try {
            GD.Print(connection.remoteEP.ToString());
            connection.sckt.Connect(connection.remoteEP);
            GD.Print("Socket connected to {0}", connection.sckt.RemoteEndPoint.ToString());
            connection.connected = true;
        } catch (ArgumentNullException ane) {
            GD.Print("ArgumentNullException : {0}",ane.ToString());
            connection.connected = false;
        } catch (SocketException se) {
            GD.Print("SocketException : {0}",se.ToString());
            connection.connected = false;
        } catch (Exception e) {
            GD.Print("Unexpected exception : {0}", e.ToString());
            connection.connected = false;
        }
    }
    public static string send_receive(Connection connection, String message){
        string result = "";
        if(connection.connected){
            try {
                // Encode the data string into a byte array
                byte[] msg = Encoding.ASCII.GetBytes(message);
                // Send the data through the socket
                int bytesSent = connection.sckt.Send(msg);
                // Receive the response from the remote device
                int bytesRec = connection.sckt.Receive(connection.bytes);
                result = Encoding.ASCII.GetString(connection.bytes,0,bytesRec);
            } catch (ArgumentNullException ane) {
                GD.Print("ArgumentNullException : {0}",ane.ToString());
                connection.connected = false;
            } catch (SocketException se) {
                GD.Print("SocketException : {0}",se.ToString());
                connection.connected = false;
            } catch (Exception e) {
                GD.Print("Unexpected exception : {0}", e.ToString());
                connection.connected = false;
            }
        }else{
            result = "Error: 'Not connected'";
        }
        return result;
    }
    public static void send(Connection connection, String message){
        if(connection.connected){
            try {
                // Encode the data string into a byte array
                byte[] msg = Encoding.ASCII.GetBytes(message);
                // Send the data through the socket
                int bytesSent = connection.sckt.Send(msg);
                // Receive the response from the remote device
            } catch (ArgumentNullException ane) {
                GD.Print("ArgumentNullException : {0}",ane.ToString());
                connection.connected = false;
            } catch (SocketException se) {
                GD.Print("SocketException : {0}",se.ToString());
                connection.connected = false;
            } catch (Exception e) {
                GD.Print("Unexpected exception : {0}", e.ToString());
                connection.connected = false;
            }
        }
    }
    public static string receive(Connection connection){
        string result = "";
        if(connection.connected){
            try {
                // Receive the response from the remote device
                int bytesRec = connection.sckt.Receive(connection.bytes);
                result = Encoding.ASCII.GetString(connection.bytes,0,bytesRec);
            } catch (ArgumentNullException ane) {
                GD.Print("ArgumentNullException : {0}",ane.ToString());
                connection.connected = false;
            } catch (SocketException se) {
                GD.Print("SocketException : {0}",se.ToString());
                connection.connected = false;
            } catch (Exception e) {
                GD.Print("Unexpected exception : {0}", e.ToString());
                connection.connected = false;
            }
        }else{
            result = "Error: 'Not connected'";
        }
        return result;
    }
    public static bool shutdown_and_close(Connection connection){
        if(connection.connected){
            try {
                // Release the socket
                connection.sckt.Shutdown(SocketShutdown.Both);
                connection.sckt.Close();
                connection.sckt.Dispose();
                connection.connected = false;
                return true;
            } catch (ArgumentNullException ane) {
                GD.Print("ArgumentNullException : {0}",ane.ToString());
                return false;
            } catch (SocketException se) {
                GD.Print("SocketException : {0}",se.ToString());
                return false;
            } catch (Exception e) {
                GD.Print("Unexpected exception : {0}", e.ToString());
                return false;
            }
        }else{
            return false;
        }
    }
}