using Godot;
using System;
using System.IO;
using System.Text;
using System.Net;
using System.Net.Sockets;
using System.Collections.Generic;
using System.Globalization;

public class Ruut : Node
{    
    private static int                          wait_time;
    private static bool                         wait_for_measurement = false;
    private static Font                         sc_font;
    private static Theme                        sc_theme = new Theme();
    private static Table                        results_table = new Table();
    private static SCLog                        sc_comms_log, sc_results_log;    
    private static SCComms                      sckz = new SCComms(); 
    private static SCUtilities 	                scuf = new SCUtilities();
	private static Dictionary<string, string>   defaultsDictionary = new Dictionary<string, string>();
	private static Socket                       _clientSocket = new Socket(AddressFamily.InterNetwork, SocketType.Stream, ProtocolType.Tcp);
	private static bool                         waitForMoveToNextAngleClick;	
	private static DateTime                     rebootStartTime;
	private static Timer                        reboot_wait_timer = new Timer();
	private static Timer                        estimate_update_timer = new Timer();
	private static Timer                        heartbeat_timer = new Timer();
    private static float                        current_position;
    private static Range                        SAHS;
    private static Range                        FAHS;
    private static Range                        IAHS;
    public override void _Ready(){
		sc_comms_log   = new SCLog(
			"comms",
			GetNode(
                "./Panel/VBoxContainerMain/VBoxContainer/HBoxContainer/VBoxContainerControls/VBoxContainer2/VBoxContainerLabelLogComms/VBoxContainerLabelLogComms/TextEdit3"
            ) as Godot.TextEdit,
            true
		);
		sc_results_log = new SCLog(
			"results",
			GetNode(
                // "./Panel/VBoxContainerMain/VBoxContainer/HBoxContainer/VBoxContainerControls/VBoxContainer2/VBoxContainerLogResults/TextEdit2"
                "./Panel/VBoxContainerMain/VBoxContainer/HBoxContainer/VBoxContainerLogResults/TextEdit2"
            ) as Godot.TextEdit,
            false
		);
        SAHS = (Range)GetNode("./Panel/VBoxContainerMain/VBoxContainer/HBoxContainer/VBoxContainerControls/VBoxContainer/VBoxContainerStartAngle/HSliderStartAngle");
        FAHS = (Range)GetNode("./Panel/VBoxContainerMain/VBoxContainer/HBoxContainer/VBoxContainerControls/VBoxContainer/VBoxContainerFinalAngle/HSliderFinalAngle");
        IAHS = (Range)GetNode("./Panel/VBoxContainerMain/VBoxContainer/HBoxContainer/VBoxContainerControls/VBoxContainer/VBoxContainerIntervalControl/HSliderIntervalCount");
		GD.Print(OS.GetScreenCount());
		OS.SetWindowPosition(
			(
				OS.GetScreenSize(
					OS.GetCurrentScreen()
				) - 
				OS.GetWindowSize()
			)/2
		);

		loadDefaultsFromFile();
		waitForMoveToNextAngleClick = bool.Parse(defaultsDictionary["waitForMoveToNextAngleClick"]);

		sc_comms_log.writePrefixAndMessageToLogWithTimestamp("Application started", "");
		calcIntervalArcFromStartFinalAnglesAndIntervalCount();
		GetNode("./Panel/VBoxContainerMain/VBoxContainer/HBoxContainer/VBoxContainerControls/VBoxContainer/VBoxContainerStartAngle/HSliderStartAngle")
            .Set("value",float.Parse(defaultsDictionary["defaultStartAngle"]))
        ;
		GetNode("./Panel/VBoxContainerMain/VBoxContainer/HBoxContainer/VBoxContainerControls/VBoxContainer/VBoxContainerFinalAngle/HSliderFinalAngle")
            .Set("value",float.Parse(defaultsDictionary["defaultFinalAngle"]))
        ;
		_on_HSliderIntervalCount_value_changed(Int32.Parse(defaultsDictionary["defaultIntervalCount"]));
	
		attemptConnection();

		// Setup reboot wait delay timer for later.
		reboot_wait_timer.SetOneShot(true);
		reboot_wait_timer.Connect("timeout", GetNode("."), "attemptConnection");
		wait_time = String.Equals(
                defaultsDictionary["localControllerIP"],
                defaultsDictionary["controllerIPRaw"]
            )
            ? 5
            : 52
        ;
        reboot_wait_timer.SetWaitTime(wait_time);
		AddChild(reboot_wait_timer);

		estimate_update_timer.SetOneShot(false);
		estimate_update_timer.Connect("timeout", GetNode("."), "makeEstimateUpdate");
		estimate_update_timer.SetWaitTime(1);
		AddChild(estimate_update_timer);

        // Setup Heartbeat Timer
        heartbeat_timer.SetOneShot(false);
        heartbeat_timer.Connect("timeout", GetNode("."), "heartbeatCheckConnection");
        heartbeat_timer.SetWaitTime(10);
        AddChild(heartbeat_timer);

        // _on_Button7_pressed();
    }

    // Layout Based Code
        // Title and Top Bar
            private void _on_ButtonMinimise_pressed(){
                OS.SetWindowMinimized(true);
            }
            private void _on_ButtonQuit_pressed(){
                GD.Print("private void _on_ButtonQuit_pressed()");
                GetTree().Quit();
            }

        // Offset Calibration
            // Slider
                private void    _on_HSliderOffsetAngle_value_changed(float value){
                    GD.Print("_on_HSliderOffsetAngle_value_changed.  Not yet implemented.");
                }
            // Angle Too Small, "Too <"
                private void    _on_ButtonOffsetSearchTooSmall_pressed(){
                    sc_comms_log.writePrefixAndMessageToLogWithTimestamp("GUI to Log", "button_calibration_too_small_pressed()");
                }
            // Reset Search
                private void    _on_ButtonOffsetSearchReset_pressed(){
                    sc_comms_log.writePrefixAndMessageToLogWithTimestamp("GUI to Log", "button_calibration_reset_pressed()");
                }
            // Angle Too Large, "Too >"
                private void    _on_ButtonOffsetSearchTooBig_pressed(){
                    sc_comms_log.writePrefixAndMessageToLogWithTimestamp("GUI to Log", "button_calibration_too_big_pressed()");
                }
            // Offset Angle Textedit display

        // Start Angle
            // Slider
                private void    _on_HSliderStartAngle_value_changed(float value){
                    setIntervalArcValue(calcIntervalArcFromStartFinalAnglesAndIntervalCount());

                    GetNode("Panel/VBoxContainerMain/VBoxContainer/HBoxContainer/VBoxContainerControls/VBoxContainer/VBoxContainerStartAngle/HBoxContainer/TextEdit")
                        .Set("text","Start Angle: " + value.ToString() + "°;")
                    ;
                }
            // Move Sensor Arm to Start Angle Position
                private void    _on_ButtonStartAngleMoveTo_pressed(){
                    updateCurrentPosition(group_move_absolute("GROUP1",SAHS.GetValue()));
                    sc_comms_log.writePrefixAndMessageToLogWithTimestamp("current position: ", current_position.ToString());
                }
            // Start Angle Textedit display

        // Final Angle
            // Slider
                private void    _on_HSliderFinalAngle_value_changed(float value){
                    setIntervalArcValue(calcIntervalArcFromStartFinalAnglesAndIntervalCount());
                    GetNode("Panel/VBoxContainerMain/VBoxContainer/HBoxContainer/VBoxContainerControls/VBoxContainer/VBoxContainerFinalAngle/HBoxContainerFinalAngle/TextEdit")
                        .Set("text","Final Angle: " + value.ToString() + "°;")
                    ;
                }
            // // Move Sensor Arm to Final Angle Position
                private void    _on_ButtonFinalAngleMoveTo_pressed(){
                    updateCurrentPosition(group_move_absolute("GROUP1",FAHS.GetValue()));
                    sc_comms_log.writePrefixAndMessageToLogWithTimestamp("current position: ", current_position.ToString());
                }
            // Final Angle Textedit display

        // Interval Control
            // Slider
                private void    _on_HSliderIntervalCount_value_changed(float value){
                    setIntervalArcValue(calcIntervalArcFromStartFinalAnglesAndIntervalCount());
                    GetNode("Panel/VBoxContainerMain/VBoxContainer/HBoxContainer/VBoxContainerControls/VBoxContainer/VBoxContainerIntervalControl/TextEdit")
                        .Set("text","Interval count: " + value.ToString() + ";\nReading count: "+(value+1).ToString()+";");
                    ;
                }
            // Move Sensor Arm to Final Angle Position
            // Final Angle Textedit display

        // Motion Sequence
            private void    _on_ButtonGenerateSequence_pressed(){
                sc_comms_log.writePrefixAndMessageToLogWithTimestamp("GUI to Log", "button_sequence_generate_pressed()");
                List<float> target_position_sequence = scuf.generateSequenceAsListOfFloats(
                    SAHS.GetValue(), 
                    FAHS.GetValue(), 
                    (int)IAHS.GetValue()
                );
                loadTargetPositionSequenceToResultsTable(target_position_sequence);
                updateGUITextBoxWithAnglesInIntervalList(target_position_sequence);
                results_table.startTime = DateTime.Now;
                var glx = (Global)GetNode("/root/Global");
                glx.readingAnglesSequenceMarker = -1;
                glx.update_hemisphere_scene = true;
            }
            private void loadTargetPositionSequenceToResultsTable(List<float> target_position_sequence){
                results_table.removeAllRecords();
                foreach (var target in target_position_sequence)
                {
                    Record empty_record = new Record(target, 0, 0, false);
                    results_table.addRecord(empty_record);
                    
                }
                results_table.printTable(sc_results_log,false);
            }
            private void    _on_ButtonExecuteNextInSequence_pressed(){
                var glx = (Global)GetNode("/root/Global");
                if(wait_for_measurement){
                    sc_comms_log.writePrefixAndMessageToLogWithTimestamp("GUI to Log", "Cannot proceed as observation has not been entered for this angle.");
                }else{
                    if(glx.readingAnglesSequenceMarker<glx.readingAnglesSequenceLength){
                        wait_for_measurement = true;
                        glx.readingAnglesSequenceMarker += 1;
                        updateCurrentPosition(group_move_absolute("GROUP1",glx.readingAnglesSequence[glx.readingAnglesSequenceMarker]));
                        

                        results_table.printTable(sc_results_log, false);
                        // sc_results_log.writeToLog(glx.readingAnglesSequence[glx.readingAnglesSequenceMarker].ToString());
                    }
                    if(glx.readingAnglesSequenceMarker==glx.readingAnglesSequenceLength){    
                        GD.Print("Sequence complete (A)");
                    }
                }

            }
            private void    _on_ButtonImportSequence_pressed(){
                sc_comms_log.writePrefixAndMessageToLogWithTimestamp("GUI to Log", "button_sequence_import_pressed()");
                string text = System.IO.File.ReadAllText(".//import.txt");

                List<float> target_position_sequence = scuf.loadAngleSequenceFromStringToList(text);
                results_table.startTime = DateTime.Now;
                loadTargetPositionSequenceToResultsTable(target_position_sequence);
                updateGUITextBoxWithAnglesInIntervalList(target_position_sequence);

                var glx = (Global)GetNode("/root/Global");
                glx.readingAnglesSequenceMarker = -1;
                glx.update_hemisphere_scene = true;
            }
        // Results Log
            private void    _on_ButtonAddReading_pressed(){
                var glx = (Global)GetNode("/root/Global");
                Godot.TextEdit text_edit = GetNode("./Panel/VBoxContainerMain/VBoxContainer/HBoxContainer/VBoxContainerLogResults/VBoxContainerLogResults/GridContainer/TextEdit2") as Godot.TextEdit;
                GD.Print(text_edit.GetText());
                results_table.enterObservation(glx.readingAnglesSequenceMarker, float.Parse(text_edit.GetText()));
                results_table.printTable(sc_results_log, false);
                wait_for_measurement = false;
                results_table.finalTime = DateTime.Now;
                text_edit.SetText("");
            }
            private void    _on_ButtonResultsLogClear_pressed(){
                sc_comms_log.writePrefixAndMessageToLogWithTimestamp("GUI to Log", "_on_ButtonResultsLogClear_pressed()");
            }
            private void    _on_ButtonSaveResults_pressed(){
                sc_comms_log.writePrefixAndMessageToLogWithTimestamp("GUI to Log", "_on_ButtonSaveResults_pressed()");
                results_table.printTable(sc_results_log, true);
            }
        // Comms Log
            private void _on_ButtonCommsLogClear_pressed(){
                sc_comms_log.writePrefixAndMessageToLogWithTimestamp("GUI to Log", "_on_ButtonCommsLogClear_pressed()");
            }

        // Bottom Toolbar
            private void    _on_ButtonControllerReboot_pressed(){
                GD.Print("_on_ButtonControllerReboot_pressed.   Implemented, but not tested.");
                Node root = GetNode(".");
                SendLoop(root, "Reboot()",false);
                restoreConnection();
            }
            private string  _on_ButtonControllerStatusGet_pressed(bool quiet){
                return SendLoop(GetNode("."), "ControllerStatusGet(int *)", quiet);
            }
            private void    _on_ButtonSensorArmInitialise_pressed(){
                GD.Print("_on_ButtonSensorArmInitialise_pressed.  Implemented, not yet tested.");
                SendLoop(GetNode("."), "GroupInitialize(GROUP1)", false);
            }
            private void    _on_ButtonSensorArmHome_pressed(){
                GD.Print("_on_ButtonSensorArmHome_pressed.  Implemented, not yet tested.");
                SendLoop(GetNode("."), "GroupHomeSearch(GROUP1)", false);
            }
            private void    _on_ButtonTestWriteToFile_pressed(){
                GD.Print("_on_ButtonTestWriteToFile_pressed()");
                string[] example_strings = {
                    "beep",
                    "boom",
                    "clang"
                };
                string file_path = ".\\logs\\" + "test.log";
                System.IO.File.WriteAllLines(@file_path, example_strings);
            }

            private void _on_ButtonAppendTextToExistingFile_pressed(){
                Random rng = new Random();
                // string file_path = ".\\logs\\" + "test.log";
                const string AllowedChars = "0123456789ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz#@$^*()";
                foreach (var r_string in RandomStrings(AllowedChars, 4, 4, 1, rng)){
                    GD.Print(r_string);
                    // appendTextToFile(file_path, r_string);
                }
            }

            public void _on_Button7_pressed(){
                Node root = GetNode("");
                Node spatial_node = root.GetNode("Panel/VBoxContainerMain/VBoxContainer/HBoxContainer/VBoxContainerLogResults2/ViewportContainer2/Viewport/Main/Spatial/");
                double radius = 5.65685424949;
                int point_count = 10;
                generate_points_on_hemisphere(spatial_node, radius, point_count);
            }

    // Non-layout based Code

    // Un-categorised Code
        public Vector3 random_point_on_surface_of_unit_sphere(Random q){
            // manhattan_unit_ball centred at 0,0,0
            // manhattan_unit_ball extents: [-1 to 1,-1 to 1,-1 to 1]
            while (true){
                // Select point in unit cube
                Vector3 point_in_manhattan_unit_ball = new Vector3(
                    (float)q.NextDouble()*2-1,
                    (float)q.NextDouble()*1-0, /* Upper hemisphere only */
                    // (float)q.NextDouble()*2-1, /* Full sphere only */
                    (float)q.NextDouble()*2-1
                );
                
                // point_in_manhattan_unit_ball.Length

                double r = point_in_manhattan_unit_ball.Length();
                if(r<=1){ /* if distance from centre to point is less than unit radius */
                    // acceptance case
                    // extend vector til magnitude is same as unit radius, i.e. it sits on the surface of the sphere.
                    // return point on sphere
                    return new Vector3(
                        (float)(point_in_manhattan_unit_ball.x/r),
                        (float)(point_in_manhattan_unit_ball.y/r),
                        (float)(point_in_manhattan_unit_ball.z/r)
                    );
                }
                // rejection case
                // re-select point in unit cube by continuing while loop.
            }
        }
        public void place_point_on_hemisphere(Node spatial_node, Vector3 input_point, double radius){
            PackedScene sphere_scene_resource = (PackedScene)ResourceLoader.Load("res://sphere.tscn");
            MeshInstance sphere_scene_instance = (MeshInstance)sphere_scene_resource.Instance();
            Vector3 scaled_point = new Vector3(
                (float)(input_point.x*radius),
                (float)(input_point.y*radius),
                (float)(input_point.z*radius)
            );

            spatial_node.AddChild(sphere_scene_instance, true);
            sphere_scene_instance
                .Set(
                    "translation", 
                    scaled_point
                )
            ;
        }

        public void generate_points_on_hemisphere(Node spatial_node, double radius, int point_count){
            PackedScene sphere_scene_resource = (PackedScene)ResourceLoader.Load("res://sphere.tscn");
            Random random_variable = new Random();
            MeshInstance sphere_scene_instance;
            for (int q = 0; q < point_count; q++)
            {
                sphere_scene_instance = (MeshInstance)sphere_scene_resource.Instance();
                Vector3 selected_point = random_point_on_surface_of_unit_sphere(random_variable);
                Vector3 scaled_point = new Vector3(
                    (float)(selected_point.x*radius),
                    (float)(selected_point.y*radius),
                    (float)(selected_point.z*radius)
                );
                // GD.Print(
                //     q.ToString()+": "+
                //     scaled_point.x.ToString()+","+
                //     scaled_point.y.ToString()+","+
                //     scaled_point.z.ToString()+","+
                //     scaled_point.Length().ToString()
                // );
                spatial_node.AddChild(sphere_scene_instance, true);
                sphere_scene_instance
                    .Set(
                        "translation", 
                        scaled_point
                    )
                ;
            }
        }
        public Vector3 point_on_spherical_surface_using_inclination_and_azimuth(double inclination, double azimuth, double radius=1){
            return new Vector3(
                (float)(radius*Math.Cos(azimuth)*Math.Cos(inclination)), /*(float)(radius * Math.Sin(theta) * Math.Cos(phi)),*/
                (float)(radius*                1*Math.Sin(inclination)), /*(float)(radius * Math.Sin(inclination))*//*(float)(radius * Math.Sin(theta) * Math.Sin(phi)),*/
                (float)(radius*Math.Sin(azimuth)*Math.Cos(inclination))
            );       
        }

        private Vector3 random_point_on_spherical_surface_using_inclination_and_azimuth(Random random, double radius){
            double single_turn_in_radians = 2.00*Math.PI;
            double turn = single_turn_in_radians;

            double u = random.NextDouble();
            double v = random.NextDouble();

            double
                inclination  = u*turn*0.5,
                azimuth      = Math.Acos(2*v-1)
            ;
            return point_on_spherical_surface_using_inclination_and_azimuth(inclination, azimuth);
        }

        private void generate_points_on_hemisphere_old(){
            PackedScene sphere_scene_resource = (PackedScene)ResourceLoader.Load("res://sphere.tscn");
            Random random = new Random();
            Node branch = GetNode("");
            MeshInstance sphere_scene_instance;
            double radius = 5.65685424949;

            for (int q = 0; q < 1000; q++)
            {
                sphere_scene_instance = (MeshInstance)sphere_scene_resource.Instance();
                Vector3 selected_point = random_point_on_spherical_surface_using_inclination_and_azimuth(random, radius);
                branch.AddChild(sphere_scene_instance, true);
                sphere_scene_instance.Set("translation", selected_point);
            }
        }
        private void _inspect_node(Node node){
                int childrenCount = node.GetChildCount();
                for (int z = 0; z < childrenCount; z++)
                {   
                    Node child = node.GetChild(z);
                    GD.Print(
                        z.ToString()+"\t"+
                        child.GetName()+"\t"+
                        child.GetChildCount()
                    );
                }
        }
        private static IEnumerable<string> RandomStrings(
            string allowedChars,
            int minLength,
            int maxLength,
            int count,
            Random rng
        ){
            char[] chars = new char[maxLength];
            int setLength = allowedChars.Length;

            while (count-- > 0)
            {
                int length = rng.Next(minLength, maxLength + 1);

                for (int i = 0; i < length; ++i)
                {
                    chars[i] = allowedChars[rng.Next(setLength)];
                }

                yield return new string(chars, 0, length);
            }
        }
        private void updateCurrentPosition(float current_position){
            var glx = (Global)GetNode("/root/Global");
            Godot.TextEdit currentPositionTextBox = GetNode("./Panel/VBoxContainerMain/VBoxContainer/HBoxContainer/VBoxContainerLogResults/VBoxContainerLogResults/GridContainer/TextEdit") as Godot.TextEdit;
            results_table.enterActualAngle(glx.readingAnglesSequenceMarker,current_position);
            currentPositionTextBox.SetText((current_position.ToString()));
        }
        private void updateGUITextBoxWithAnglesInIntervalList(List<float> float_list){
            var glx = (Global)GetNode("/root/Global");
            glx.readingAnglesSequence = float_list;
            
            string readingAngleSequenceAsString = scuf.convertListOfFloatsToCommaSeparatedString(
                glx.readingAnglesSequence
            );

            writeToIntervalList(GetNode("."),readingAngleSequenceAsString);
            sc_comms_log.writePrefixAndMessageToLogWithTimestamp(
                "GUI to Log: Generated reading angle sequence", 
                readingAngleSequenceAsString
            );
            glx.readingAnglesSequenceLength = float_list.Count;
        }
        private static string SendStringToXPSC8(string req){
            byte[] buffer = Encoding.ASCII.GetBytes(req);
            _clientSocket.Send(buffer);
            byte[] receivedBuf = new byte[1024];
            int rec = _clientSocket.Receive(receivedBuf);
            byte[] data = new byte[rec];
            Array.Copy(receivedBuf, data, rec);
            return Encoding.ASCII.GetString(data);
        }
        private int errorCodeFromMessageReceived(string message_received){
            string error_code_as_string = message_received.Substr(0, message_received.IndexOf(","));
            return Int32.Parse(error_code_as_string);
        }
        private string SendLoop(Node root, string req, bool quiet){
            string message_received = SendStringToXPSC8(req);

            if(!quiet){
                GD.Print(req);
                Ruut.writeGUIToXPSC8MessageToLog(root, req);
                Ruut.writeXPSC8ToGUIMessageToLog(root, message_received);
            }
            
            // Extract Error code from message_received
            int error_code_as_int = errorCodeFromMessageReceived(message_received);

            if (error_code_as_int != 0 && !quiet){
                // Request XPS-C8 to explain the Error Code
                string command_string = "ErrorStringGet("+error_code_as_int.ToString()+",char *)";
                Ruut.writeGUIToXPSC8MessageToLog(root, command_string);
                string error_code_explanation = SendStringToXPSC8(command_string);
                Ruut.writeXPSC8ToGUIMessageToLog(root, error_code_explanation);
            }
            return message_received;
        }

        private void writeToIntervalList(Node root, string message){
            TextEdit z = GetNode(
                "Panel/VBoxContainerMain/VBoxContainer/HBoxContainer/VBoxContainerControls/VBoxContainer/VBoxContainerMotionSequence/VBoxContainerMotionSequence/TextEdit"
                ) as TextEdit
            ;
            z.SetText("Angles at which the scatterometer will stop to allow for data collection:\n"+message);
        }	
        private static void writeGUIToXPSC8MessageToLog(Node root, string message){
            sc_comms_log.writePrefixAndMessageToLogWithTimestamp("GUI to XPS-C8", message);
        }
        private static void writeXPSC8ToGUIMessageToLog(Node root, string message){
            sc_comms_log.writePrefixAndMessageToLogWithTimestamp("XPS-C8 to GUI", message);
        }
        void AddOrUpdateDict(Dictionary<string, string> dict, string key, string value)
        {
            if (dict.ContainsKey(key))
            {
                dict[key] = value;
            }
            else
            {
                dict.Add(key, value);
            }
        }
        private void loadDefaultsFromFile(){
            string defaults_string = System.IO.File.ReadAllText("defaults.config");
            string[] listOfDefaultStrings = defaults_string.Split("\n");
            foreach (string defaultString in listOfDefaultStrings){
                string[] defaultKeyValuePairStrings = defaultString.Replace(";","").Replace(" ","").Split("=");
                string key = defaultKeyValuePairStrings[0];
                string value = defaultKeyValuePairStrings[1];

                if (key.Equals("controllerIP"))
                {
                    AddOrUpdateDict(defaultsDictionary,"controllerIPRaw", value);
                    value = sckz.cleanIPAddressString(value);
                }
                AddOrUpdateDict(defaultsDictionary,key, value);
            }
        }
        private void makeEstimateUpdate(){
            double result = DateTime.Now.Subtract(rebootStartTime).TotalSeconds;
            sc_comms_log.writePrefixAndMessageToLogWithTimestamp("Estimated time til reconnection", (wait_time-(int)result).ToString());
        }

        private void heartbeatCheckConnection(){
            try
            {
                if(attemptHandshake(true)){
                    GD.Print("Heartbeat OK.");
                }else{
                    GD.Print("Heartbeat problem.");
                    heartbeat_timer.Stop();
                    restoreConnection();
                }    
            }
            catch (SocketException)
            {
                heartbeat_timer.Stop();
                GD.Print("Heartbeat problem.");
                restoreConnection();
            }
            

        }

        private void restoreConnection(){
            updateControllerStatusIndicatorColour(false);
            reboot_wait_timer.Start();
            rebootStartTime = DateTime.Now;
            estimate_update_timer.Start();
            _clientSocket.Close();
            _clientSocket = new Socket(AddressFamily.InterNetwork, SocketType.Stream, ProtocolType.Tcp);
        }

        private void attemptConnection(){
            GD.Print("Attempting connection");
            bool connected = false;
            
            if(LoopConnect()){
                if(attemptHandshake(false)){
                    connected = true;
                    
                }else{
                    connected = false;
                    // _clientSocket.Close();
                }
            }

            if(connected){
                sc_comms_log.writePrefixAndMessageToLogWithTimestamp("XPS-C8 Controller connected on "+defaultsDictionary["controllerIP"]+":"+defaultsDictionary["controllerPort"], "");
                sc_comms_log.writePrefixAndMessageToLogWithTimestamp("Note: IP address and port can be changed by editing defaults.config.", "");
                estimate_update_timer.Stop();

                // Check if sensor arm is ready for commands
                GD.Print("Check if sensor arm is ready for commands");
                string ec = SendLoop(GetNode("."), "GroupMoveRelative(GROUP1,0)", false);
                if(Int32.Parse(ec.Substring(0,ec.IndexOf(",")))<0){
                    // Need to initialise and home
                    SendLoop(GetNode("."), "GroupInitialize(GROUP1)", false);
                    SendLoop(GetNode("."), "GroupHomeSearch(GROUP1)", false);
                }
                
                updateCurrentPosition(query_current_position("GROUP1"));
                heartbeat_timer.Start();
            }else{
                sc_comms_log.writePrefixAndMessageToLogWithTimestamp("Connection attempt failed", "");
                reboot_wait_timer.Start();
            }
            updateControllerStatusIndicatorColour(connected);
        }
        private bool attemptHandshake(bool quiet){
            return
                errorCodeFromMessageReceived(
                    _on_ButtonControllerStatusGet_pressed(quiet)
                ) == 0
            ;
        }
        private bool LoopConnect(){
            int attempts = 0;

            while (!_clientSocket.Connected)
            {
                try
                {
                    attempts += 1;
                    IPAddress controllerIP = System.Net.IPAddress.Parse(defaultsDictionary["controllerIP"]);
                    _clientSocket.Connect(controllerIP, Int32.Parse(defaultsDictionary["controllerPort"]));
                }
                catch (SocketException)
                {
                    Console.WriteLine("Connection attempts: "+attempts.ToString());
                    return false;
                }
            }
            Console.WriteLine("Connected");
            return true;
        }
        private void updateControllerStatusIndicatorColour(bool connected){
            ColorRect cr = (ColorRect)GetNode("Panel/VBoxContainerMain/Title Bar/HBoxContainerRight/ColorRect");
            Label connected_label = GetNode("Panel/VBoxContainerMain/Title Bar/HBoxContainerRight/Label") as Label;
            if(connected){
                cr.Set("color", "#56e042");
                connected_label.SetText("Connected");
            }else{
                cr.Set("color", "#e04242");
                connected_label.SetText("Not Connected");
            }
        }
        private float calcIntervalArcFromStartFinalAnglesAndIntervalCount(){
            return (FAHS.GetValue()-SAHS.GetValue())/IAHS.GetValue();
        }
        private void setIntervalArcValue(float intervalArcAngle){
            setIntervalArcText(intervalArcAngle);
        }
        private void setIntervalArcText(float intervalArcAngle){
            GetNode("Panel/VBoxContainerMain/VBoxContainer/HBoxContainer/VBoxContainerControls/VBoxContainer/VBoxContainerIntervalControl/TextEdit")
                .Set("text","Interval arc: " + intervalArcAngle.ToString("0.0000").Left(6) + "°;")
            ;
        }
        private float group_move_absolute(string group, float angle){
            string commandString;

            commandString ="GroupMoveAbsolute("+group+","+angle.ToString()+")";
            SendLoop(GetNode("."), commandString, false);
            
            commandString ="GroupPositionCurrentGet("+group+",double  *)";
            string[] resultStrings = SendLoop(GetNode("."), commandString, false).Split(",");
            
            return float.Parse(resultStrings[1]);
        }
        private float query_current_position(string group){
            string commandString;
            
            commandString ="GroupPositionCurrentGet("+group+",double  *)";
            string[] resultStrings = SendLoop(GetNode("."), commandString, false).Split(",");
            
            return float.Parse(resultStrings[1]);
        }
    //======================
}

public class SCLog{
	public Godot.TextEdit text_edit_node;
	private int logLineCount;
    string file_path;
    string log_name;

    public bool autoWriteToLog;
	public SCLog(string log_name, Godot.TextEdit text_edit_node, bool autoWriteToLog){
		GD.Print("SCLog instantiation");
		this.log_name = log_name;
		this.text_edit_node = text_edit_node;
        this.file_path = ".\\logs\\" + log_name +".log";
        this.autoWriteToLog = autoWriteToLog;
	}

	public void writeToLog(string message){
        if(autoWriteToLog){
            this.appendTextToFile(message);
        }
		string oldString =  this.text_edit_node.GetText();
		if (oldString.Length>2){
            oldString =  oldString.Left(oldString.Length-2);    
        }
        
		string newString;
		if (oldString.Length==0)
		{
			newString = message;
		}else{
			newString = oldString + "\n" +message;
		}
		
		this.text_edit_node.SetText(newString+"\n"+"\n");
		logLineCount += 1;
		this.text_edit_node.CursorSetLine(logLineCount+3, true, true);
	}

	public void writePrefixAndMessageToLogWithTimestamp(string prefix, string message){
		string timestamp = DateTime.Today.ToLongDateString()+" "+DateTime.Now.ToLongTimeString();
		writeToLog(prefix+" @ "+timestamp+"> "+message);
	}

    public void appendTextToFile(string textToWriteToFile){
        string file_path = this.file_path;
        using(
            System.IO.StreamWriter file = new System.IO.StreamWriter(@file_path, true)
        ){
            file.WriteLine(textToWriteToFile);
        }
    }

    public void clearLog(){
        this.text_edit_node.SetText("");
    }
}

public class SCComms{
	public SCComms(){
		GD.Print("ScatterometerComms instantiation");
	}
	public string cleanIPAddressString(string inputAddress){
        return
            Convert.ToInt16(inputAddress.Split('.')[0]).ToString() + "." +
            Convert.ToInt16(inputAddress.Split('.')[1]).ToString() + "." +
            Convert.ToInt16(inputAddress.Split('.')[2]).ToString() + "." +
            Convert.ToInt16(inputAddress.Split('.')[3]).ToString()
        ;
    }
}

public class SCUtilities{
	public SCUtilities(){
		GD.Print("SCUtilities instantiation");
	}

	public List<float> generateSequenceAsListOfFloats(float startAngle, float finalAngle, int intervalCount){
		int n = intervalCount+1;
		var list = new List<float>(n);
		float totalArc = finalAngle - startAngle;
		float intervalArc = totalArc/intervalCount;
		for (int i = 0; i < n; ++i)
			list.Add(startAngle+intervalArc*i);
		return list;
	}
	public List<float> loadAngleSequenceFromStringToList(string stringContainingCommaSeparatedAngleList){
		string[] stringArrayContainingAngles = stringContainingCommaSeparatedAngleList.Split(",");
		var list = new List<float>(stringArrayContainingAngles.Length);
		foreach (string angleAsString in stringArrayContainingAngles){
			float angleAsFloat = float.Parse(angleAsString);
			list.Add(angleAsFloat);
			GD.Print(angleAsFloat);
		}
		return list;
	}
	public string convertListOfFloatsToCommaSeparatedString(List<float> floatList){
		string result = "";
		bool latch = false;
		foreach (var element in floatList)
		{
			if(!latch){
				result = element.ToString();
				latch = true;
			}else{
				result = result +", "+ element.ToString();
			}
		}
		return result;
	}
}

public class Record{
    public float target_position;
    public float actual_position;
    public float reading;

    public bool reading_written;

    public Record(
        float target_position,
        float actual_position,
        float reading,
        bool  reading_written
    ){
        this.target_position    = target_position;
        this.actual_position    = actual_position;
        this.reading            = reading;
        this.reading_written    = reading_written;
    }

    public Record(
        float target_position
    ){
        this.target_position    = target_position;
        this.actual_position    = 0f;
        this.reading            = 0f;
        this.reading_written    = false;
    }
}

public class Table{
    
    List<Record> table = new List<Record>();
    public DateTime startTime;
    public DateTime finalTime;

    public Table(){
        GD.Print("Table instantiated.");
    }

    public void addRecord(Record record){
        this.table.Add(record);
    }

    public void enterActualAngle(int index, float actual_angle){
        if(index >= 0){
            table[index].actual_position = actual_angle;
        }
    }
    public void enterObservation(int index, float observed_value){
        if(index >= 0){
            table[index].reading = observed_value;
            table[index].reading_written = true;
        }
    }
    public void printTable(SCLog log, bool writeToFile){
        log.clearLog();

        if(writeToFile){
            string header_string = "   Target |    Actual |  Measured ";
            log.appendTextToFile("Experiment Started at: "+this.startTime.ToLongDateString()+" "+this.startTime.ToLongTimeString());
            log.appendTextToFile("Experiment Ended at: "+this.finalTime.ToLongDateString()+" "+this.finalTime.ToLongTimeString());
            log.appendTextToFile(header_string);
        }
        
        CultureInfo ci = new CultureInfo("en-au");
        string printable_record;
        foreach (var record in table)
        {
            
            if (record.reading_written){
                printable_record = 
                    record.target_position.ToString("F04", ci).PadLeft(9,' ')+" | "+
                    record.actual_position.ToString("F04", ci).PadLeft(9,' ')+" | "+
                    record.reading.ToString("F04", ci).PadLeft(9,' ')
                ;
            }else{
                if(record.actual_position != 0f){
                    printable_record = 
                        record.target_position.ToString("F04", ci).PadLeft(9,' ')+" | "+
                        record.actual_position.ToString("F04", ci).PadLeft(9,' ')+" | "+
                        "    NULL "
                    ;
                }else{
                    printable_record = 
                        record.target_position.ToString("F04", ci).PadLeft(9,' ')+" | "+
                        "    NULL "+" | "+
                        "    NULL "
                    ;
                }
                
            }
            
            GD.Print(printable_record);
            log.writeToLog(printable_record);
            if(writeToFile){
                log.appendTextToFile(printable_record);
            }
        }
        log.writeToLog("");

        if(writeToFile){
            log.appendTextToFile("End of results.\n");
        }
    }
    public void removeAllRecords(){
        table.Clear();
    }
}