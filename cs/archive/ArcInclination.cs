using Godot;
using System;

public class ArcInclination : Node2D
{
    // Member variables here, example:
    // private int a = 2;
    // private string b = "textvar";
    // Godot.Vector2 centre_of_viewport = new Godot.Vector2(958/2, 462/2);
    Godot.Vector2 centre_of_viewport = new Godot.Vector2(0, 0);
    float datum_offset_degrees = 0.0f;
    bool  clockwise_is_positive = false;
    Godot.Label sample_y_axis_label;
    Godot.Label arc_rotation_about_y_axis_in_x_z_plane_axis_label;

    float base_radius = 434.0f/1.8f;
    public override void _Ready()
    {
        // Called every time the node is added to the scene.
        // Initialization here
        Camera2D cam    = GetNode("Camera2D") as Camera2D;
        Sprite   sprite = GetNode("Sprite") as Sprite;
        // sprite.SetPosition(centre_of_viewport);
        sprite.SetVisible(false);
        
        float horizontal_shift = 0;// base_radius*0.45f;
        float vertical_shift = -base_radius*0.45f;
        cam.SetPosition(new Vector2(
            centre_of_viewport.x+horizontal_shift,
            centre_of_viewport.y+vertical_shift
        ));
        
        // float zoom = 1.8f;
        // cam.SetZoom(new Vector2(zoom, zoom));
        
        sample_y_axis_label = GetNode("Control/sample_y_axis_label") as Godot.Label;
        arc_rotation_about_y_axis_in_x_z_plane_axis_label = GetNode("Control/arc_rotation_about_y_axis_in_x_z_plane_axis_label") as Godot.Label;
    }
    public override void _Process(float delta)
    {
        // Called every frame. Delta is time since last frame.
        // Update game logic here.
        Update();
    }
    public void DrawLineWithShadow(ArcInclination context, Vector2 centre_of_viewport, Vector2 axis_end_point, Godot.Color border_colour, Godot.Color colour){
        float width = 2.0f;
        float shadow_width = width+2.0f;
        context.DrawLine(
            centre_of_viewport,
            axis_end_point,
            border_colour,
            shadow_width
        );
        context.DrawLine(
            centre_of_viewport,
            axis_end_point,
            colour,
            width
        );
    }
    public override void _Draw(){
        Glx glx = GetNode("/root/Global") as Glx;
        float inclination_angle_offset = 90.0f;

        Vector2 axis_end_point;
        // Sample y axis
        axis_end_point = new Vector2(centre_of_viewport.x,centre_of_viewport.y-(float)(base_radius*1.2));
        DrawLineWithShadow(this, centre_of_viewport, axis_end_point, glx.black, glx.sample_y_colour);
        
        // Sample x-z plane as axis
        // axis_end_point = new Vector2(centre_of_viewport.x+(float)(base_radius*0.9),centre_of_viewport.y);
        // DrawLineWithShadow(this, centre_of_viewport, axis_end_point, glx.black, glx.inclination_vector_colour);


        glx.draw_arc_line(
            context:this,
            datum_offset_degrees:datum_offset_degrees,
            clockwise_is_positive:clockwise_is_positive,
            centre:centre_of_viewport,
            start_angle_degrees:90,
            final_angle_degrees:glx.get_arc_inclination_angle_degrees()+inclination_angle_offset,
            radius:(float)(base_radius*0.9),
            width:2,
            color_a:glx.rotation_about_y_axis_in_x_z_plane_heading_vector_colour,
            color_b:glx.inclination_vector_colour,
            color_c:glx.inclination_arc_colour,
            draw_start_angle_line:false,
            draw_final_angle_line:true
        );
        
        sample_y_axis_label.SetPosition(new Vector2(axis_end_point.x+5, axis_end_point.y));
        // arc_rotation_about_y_axis_in_x_z_plane_axis_label.SetPosition(new Vector2(axis_end_point.x+(float)(base_radius*0.9)-80, centre_of_viewport.y+4));
        
        DrawCircle(
            position:centre_of_viewport,
            radius:4,
            color:glx.black
        );
        DrawCircle(
            position:centre_of_viewport,
            radius:2,
            color:glx.rotation_about_y_axis_in_x_z_plane_halfturn_vector_colour
        );
    }
}
