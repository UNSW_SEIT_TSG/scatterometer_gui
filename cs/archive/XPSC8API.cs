using Godot;
using System;
using System.IO;
using System.Text;
using System.Net;
using System.Net.Sockets;
using System.Collections.Generic;
using System.Globalization;
public class XPSC8API{
    public Node q;
    private Socket _clientSocket;
    private Log comms_log, results_log;
    public XPSC8API(
        Node q, 
        Socket _clientSocket,
        Log comms_log,
        Log results_log
    ){
        this.q             = q;
        this._clientSocket = _clientSocket;
        this.comms_log     = comms_log;
        this.results_log   = results_log;
    }
    public string SendLoop(string req, bool quiet){
        string message_received=SendStringToXPSC8(req);

        if(!quiet){
            GD.Print(req);
            this.comms_log.writePrefixAndMessageToLogWithTimestamp("GUI to XPS-C8", req);
            this.comms_log.writePrefixAndMessageToLogWithTimestamp("XPS-C8 to GUI", message_received);
        }
        
        // Extract Error code from message_received
        int error_code_as_int=errorCodeFromMessageReceived(message_received);

        if (error_code_as_int != 0 && !quiet){
            // Request to XPS-C8 to explain the Error Code
            string command_string="ErrorStringGet("+error_code_as_int.ToString()+",char *)";
            this.comms_log.writePrefixAndMessageToLogWithTimestamp("GUI to XPS-C8", command_string);
            string error_code_explanation=SendStringToXPSC8(command_string);
            this.comms_log.writePrefixAndMessageToLogWithTimestamp("XPS-C8 to GUI", error_code_explanation);
        }
        return message_received;
    }
    public string SendStringToXPSC8(string req){
        byte[] buffer=Encoding.ASCII.GetBytes(req);
        this._clientSocket.Send(buffer);
        byte[] receivedBuf=new byte[1024];
        int rec=this._clientSocket.Receive(receivedBuf);
        byte[] data=new byte[rec];
        System.Array.Copy(receivedBuf, data, rec);
        return Encoding.ASCII.GetString(data);
    }
    public int errorCodeFromMessageReceived(string message_received){
        string error_code_as_string=message_received.Substr(0, message_received.IndexOf(","));
        return Int32.Parse(error_code_as_string);
    }
    public float query_current_position(string group){
        string   commandString ="GroupPositionCurrentGet("+group+",double  *)";
        string[] resultStrings=SendLoop(commandString, false).Split(",");
        return   float.Parse(resultStrings[1]);
    }
    public float group_move_absolute(string group, float angle){
        string commandString ="GroupMoveAbsolute("+group+","+angle.ToString()+")";
        SendLoop(commandString, false);
        
        commandString ="GroupPositionCurrentGet("+group+",double  *)";
        string[] resultStrings=SendLoop(commandString, false).Split(",");
        
        return float.Parse(resultStrings[1]);
    }
    private bool attemptHandshake(bool quiet){
        return
            errorCodeFromMessageReceived(
                SendLoop("ControllerStatusGet(int *)", quiet)
            ) == 0
        ;
    }
}
