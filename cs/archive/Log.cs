using Godot;
using System;
public class Log{
    public Godot.TextEdit text_edit_node;
    private int logLineCount;
    string file_path;
    string log_name;
    public bool autoWriteToLog;
    public Log(string log_name, Godot.TextEdit text_edit_node, bool autoWriteToLog){
        GD.Print("Log instantiation");
        this.log_name = log_name;
        this.text_edit_node = text_edit_node;
        this.file_path = ".\\logs\\" + log_name +".log";
        this.autoWriteToLog = autoWriteToLog;
    }
    public void writeToLog(string message){
        if(autoWriteToLog){
            this.appendTextToFile(message);
        }
        string oldString =  this.text_edit_node.GetText();
        if (oldString.Length>2){
            oldString = oldString.Left(oldString.Length-2);
        }
        string newString = (oldString.Length==0) ? message : oldString+"\n"+message;
        this.text_edit_node.SetText(newString+"\n"+"\n");
        logLineCount += 1;
        this.text_edit_node.CursorSetLine(logLineCount+3, true, true);
    }
    public void writePrefixAndMessageToLogWithTimestamp(string prefix, string message){
        string timestamp = DateTime.Today.ToLongDateString()+" "+DateTime.Now.ToLongTimeString();
        writeToLog(prefix+" @ "+timestamp+"> "+message);
    }
    public void appendTextToFile(string textToWriteToFile){
        string file_path = this.file_path;
        using(
            System.IO.StreamWriter file = new System.IO.StreamWriter(@file_path, true)
        ){
            file.WriteLine(textToWriteToFile);
        }
    }
    public void clearLog(){
        this.text_edit_node.SetText("");
    }
}