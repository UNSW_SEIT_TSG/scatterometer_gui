using Godot;
using System;
using System.Collections.Generic;
public class Glx:Godot.Node{
    public  Queue<string> from_relay_server = new Queue<string>();
    public  List<float> reading_sequence_arc_angle_degrees;
    public  List<Vector3> reading_list_as_vector3s;
    private float       arc_start_angle_degrees=0.0f;
    private float       arc_final_angle_degrees=180.0f;
    private float       arc_span_degrees; // update_arc_span_degrees() called automatically when either the start or final angles change.
    private float       arc_interval_degrees; // update_arc_interval_degrees() called automatically when either the start angle, final angle, or reading_count change.
    private int         reading_count=30;
    private float       arc_inclination_angle_degrees=10.0f;
    private float       arc_rotation_about_y_axis_in_x_z_plane_angle_degrees=5.0f;
    private float       θ;
    private float       φ;
    private bool        value_changed;
    private bool        execution_enabled = false;
    private bool        relay_server_connected = false;
    private int         current_reading_index = -1;
    public bool         waiting_for_response = false;
    public Godot.Color black                           = new Godot.Color(0,0,0);
    public Godot.Color sample_x_colour                 = new Godot.Color(232.0f/256,83.0f/256,136.0f/256);
    public Godot.Color sample_y_colour                 = new Godot.Color(100.0f/256,220.0f/256,82.0f/256);
    public Godot.Color sample_z_colour                 = new Godot.Color(214.0f/256,94.0f/256,211.0f/256);
    public Godot.Color start_angle_vector_colour       = new Godot.Color(178.0f/256,228.0f/256,51.0f/256);
    public Godot.Color final_angle_vector_colour       = new Godot.Color(132.0f/256,126.0f/256,234.0f/256);
    public Godot.Color reading_arc_colour              = new Godot.Color(226.0f/256,212.0f/256,56.0f/256);
    public Godot.Color reading_point_todo_colour       = new Godot.Color(238.0f/256,77.0f/256,46.0f/256);
    public Godot.Color reading_point_doing_colour      = new Godot.Color(93.0f/256,232.0f/256,167.0f/256);
    public Godot.Color reading_point_done_colour       = new Godot.Color(224.0f/256,105.0f/256,86.0f/256);
    public Godot.Color rotation_about_y_axis_in_x_z_plane_zeroturn_vector_colour  = new Godot.Color(73.0f/256,174.0f/256,85.0f/256);
    public Godot.Color rotation_about_y_axis_in_x_z_plane_heading_vector_colour   = new Godot.Color(221.0f/256,132.0f/256,46.0f/256);
    public Godot.Color rotation_about_y_axis_in_x_z_plane_halfturn_vector_colour  = new Godot.Color(191.0f/256,225.0f/256,109.0f/256);
    public Godot.Color inclination_vector_colour       = new Godot.Color(201.0f/256,166.0f/256,60.0f/256);
    public Godot.Color inclination_arc_colour          = new Godot.Color(124.0f/256,161.0f/256,48.0f/256);
    public float getHeadingFromStartFinalCountIndex(int index){
        return ((arc_final_angle_degrees - arc_start_angle_degrees)/(reading_count-1))*index;
    }
    public void updateVector3ReadingSequenceFromAngularReadingSequence(){
        float reading_sequence_arc_angle_degrees_offset = 0;
        reading_list_as_vector3s = new List<Vector3>(reading_sequence_arc_angle_degrees.Count);

        for (int q = 0; q < reading_sequence_arc_angle_degrees.Count; q++){
            double arc_angle_in_radians = radians_from_degrees(
                reading_sequence_arc_angle_degrees[q]+reading_sequence_arc_angle_degrees_offset
            );
            double inclination_angle_in_radians = radians_from_degrees(arc_inclination_angle_degrees);
            double rotation_about_y_axis_in_x_z_plane_angle_in_radians = radians_from_degrees(arc_rotation_about_y_axis_in_x_z_plane_angle_degrees);

            Vector3 before_rotation_about_y_axis_in_x_z_Plane_rotation = new Vector3(
                (float)(Math.Sin(arc_angle_in_radians) * Math.Cos(inclination_angle_in_radians)),
                (float)(Math.Sin(arc_angle_in_radians) * Math.Sin(inclination_angle_in_radians)),
                (float)(Math.Cos(arc_angle_in_radians))
            );

            Vector3 point_on_unit_sphere = before_rotation_about_y_axis_in_x_z_Plane_rotation.Rotated(new Vector3(0,1,0), (float)(rotation_about_y_axis_in_x_z_plane_angle_in_radians));
            reading_list_as_vector3s.Add(point_on_unit_sphere);
        }
    }
    public List<Vector3> getFirstMedianLastListAsXYZ(){
        updateVector3ReadingSequenceFromAngularReadingSequence();
        List<Vector3> first_median_last = new List<Vector3>(3);
        GD.Print(reading_list_as_vector3s.Count);
        first_median_last.Add(reading_list_as_vector3s[0]);
        first_median_last.Add(reading_list_as_vector3s[(int)(reading_count/2)]);
        first_median_last.Add(reading_list_as_vector3s[reading_count-1]);
        return first_median_last;
    }

    public override void _Ready(){
        update_arc_span_degrees();
        update_sequence();  // Initialise sequence from defaults
        updateVector3ReadingSequenceFromAngularReadingSequence();
    }
    private string get_sequence_as_string(List<float> sequence){
        string result = "";
        foreach (float item in sequence){
            string string_increment = (item.ToString()+", ");
            result += string_increment;
        }
        return result;
    }
    public void update_xyzs(List<Vector3> xyzs){
        reading_list_as_vector3s = new List<Vector3>(reading_count);
        for (int i = 0; i < reading_count; i++){
            reading_list_as_vector3s.Add(xyzs[i]);
        }
    }
    public void listXYZsOfReadingSequence(){
        foreach (var item in reading_list_as_vector3s)
        {
            GD.Print(item);
        }
    }
    public float transform_angle(
        float input_angle_degrees, 
        float offset_angle_degrees, 
        bool clockwise_is_positive
    ){
        return (input_angle_degrees + offset_angle_degrees)*polarity_from_bool(clockwise_is_positive);
    }
    public override string ToString(){
        return "Glx properties:"+
        "  arc_start_angle_degrees:            "+arc_start_angle_degrees.ToString()+"\n"+
        "  arc_final_angle_degrees:            "+arc_final_angle_degrees.ToString()+"\n"+
        "  reading_count:                      "+reading_count.ToString()+"\n"+
        "  arc_inclination_angle_degrees:      "+arc_inclination_angle_degrees.ToString()+"\n"+
        "  arc_rotation_about_y_axis_in_x_z_plane_angle_degrees:          "+arc_rotation_about_y_axis_in_x_z_plane_angle_degrees.ToString()+"\n"+
        "  value_changed:                      "+value_changed.ToString()+"\n"+
        "  reading_sequence_arc_angle_degrees: "+get_sequence_as_string(reading_sequence_arc_angle_degrees)+"\n"+
        ""
        ;
    }
    public float get_γ(){update_arc_interval_degrees(); return arc_interval_degrees;}
    public float get_θ(){return θ;}
    public void set_θ(float new_θ){θ=new_θ;}
    public void set_θ(double new_θ){θ=(float)new_θ;}
    public float get_φ(){return φ;}
    public void set_φ(float new_φ){φ=new_φ;}
    public void set_φ(double new_φ){φ=(float)new_φ;}
    private void update_arc_span_degrees(){
        arc_span_degrees = arc_final_angle_degrees-arc_start_angle_degrees;
    }
    private float get_arc_span_degrees(){
        return arc_span_degrees;
    }
    private void update_arc_interval_degrees(){
        update_arc_span_degrees(); // Called to ensure arc_span_degrees is up to date prior to calculating arc_interval.
        arc_interval_degrees = arc_span_degrees/(float)(reading_count-1);
    }
    public void update_sequence(){
        // Generate angle intervals within selected scattering plane
        reading_sequence_arc_angle_degrees = new List<float>(reading_count);
        update_arc_interval_degrees();
        for (int q = 0; q < reading_count; q++){
            reading_sequence_arc_angle_degrees.Add(q*arc_interval_degrees+arc_start_angle_degrees);
        }

        // Transform sequence in scattering plane to 3D cartesian points on hemisphere surface
    }
    public float get_arc_rotation_about_y_axis_in_x_z_plane_angle_degrees(){
        return arc_rotation_about_y_axis_in_x_z_plane_angle_degrees;
    }
    public int get_current_reading_index(){
        return current_reading_index;
    }
    public int increment_current_reading_index(int step=1){
        return current_reading_index;
    }
    public void set_current_reading_index(int value){
        current_reading_index = value;
    }
    public void  set_arc_rotation_about_y_axis_in_x_z_plane_angle_degrees(float value){
        arc_rotation_about_y_axis_in_x_z_plane_angle_degrees = value;
        set_value_changed(true);
    }
    public float get_arc_final_angle_degrees(){
        return arc_final_angle_degrees;
    }
    public void  set_arc_final_angle_degrees(float value){
        arc_final_angle_degrees = value;
        update_arc_span_degrees();
        update_arc_interval_degrees();
        update_sequence();
        set_value_changed(true);
    }
    public float get_arc_inclination_angle_degrees(){
        return arc_inclination_angle_degrees;
    }
    public void  set_arc_inclination_angle_degrees(float value){
        arc_inclination_angle_degrees = value;
        set_value_changed(true);
    }
    public float get_arc_start_angle_degrees(){
        return arc_start_angle_degrees;
    }
    public void  set_arc_start_angle_degrees(float value){
        arc_start_angle_degrees = value;
        update_arc_span_degrees();
        update_arc_interval_degrees();
        update_sequence();
        set_value_changed(true);
    }
    public int   get_reading_count(){
        return reading_count;
    }
    public void  set_reading_count(float value){
        set_reading_count((int)Math.Round(value));
    }
    public void  set_reading_count(int value){
        reading_count = value;
        update_arc_interval_degrees();
        update_sequence();
        set_value_changed(true);
    }
    public bool get_relay_server_connected(){
        return relay_server_connected;
    }
    public void set_relay_server_connected(bool value){
        relay_server_connected = value;
        GD.Print(value);
    }
    public bool  get_value_changed(){
        return value_changed;
    }
    public void  toggle_value_changed(){
        value_changed = !value_changed;
    }
    public void  set_value_changed(bool value){
        value_changed = value;
    }
    public bool  get_execution_enabled(){
        return execution_enabled;
    }
    public void  toggle_execution_enabled(){
        execution_enabled = !execution_enabled;
    }
    public void  set_execution_enabled(bool value){
        execution_enabled = value;
    }
    public int polarity_from_bool(bool b){
        return b ? 1 : -1;
    }
    public double radians_from_degrees(double angle_in_degrees){
        return angle_in_degrees*Math.PI/180.0f;
    }
    public double degrees_from_radians(double angle_in_radians){
        return angle_in_radians*180.0f/Math.PI;
    }
    public Vector2 x_y_vector_from_angle_radius_and_centre(Vector2 centre, float radius, float angle_degrees){
        return new Vector2(
            (float)(centre.x+radius*Math.Cos(radians_from_degrees(angle_degrees))),
            (float)(centre.y+radius*Math.Sin(radians_from_degrees(angle_degrees)))
        );
    }
    public void draw_elliptical_arc(
        Node2D context,
        float datum_offset_degrees,
        bool clockwise_is_positive,
        Vector2 centre,
        float start_angle_degrees,
        float final_angle_degrees,
        float radius,
        Godot.Color color_a,
        Godot.Color color_b,
        Godot.Color color_c,
        int polyline_point_count=100,
        float width=1,
        bool draw_start_angle_line=true,
        bool draw_final_angle_line=true,
        float semi_major=0.5f,
        float semi_minor=1.0f){

        start_angle_degrees = (start_angle_degrees)*polarity_from_bool(clockwise_is_positive);
        final_angle_degrees = (final_angle_degrees)*polarity_from_bool(clockwise_is_positive);

        float shadow_width = width+2.0f;

        Vector2[] points = new Vector2[polyline_point_count];
        int z = 0;
        float arc_interval = (final_angle_degrees-start_angle_degrees)/(polyline_point_count-1);
        float current_angle_degrees = start_angle_degrees;

        float theta = (float)(radians_from_degrees(datum_offset_degrees*polarity_from_bool(clockwise_is_positive)));
        while (z < polyline_point_count){
            float alpha = (float)(radians_from_degrees(current_angle_degrees));
            
            points[z] = new Vector2(
                (float)(
                    radius*(semi_major*Math.Cos(alpha)*Math.Cos(theta) - semi_minor*Math.Sin(alpha)*Math.Sin(theta))
                ),
                (float)(
                    radius*(semi_major*Math.Cos(alpha)*Math.Sin(theta) + semi_minor*Math.Sin(alpha)*Math.Cos(theta))
                )
            );
            // points[z] = x_y_vector_from_angle_radius_and_centre(centre, radius, current_angle_degrees);
            z+=1;
            current_angle_degrees+=arc_interval;
        }
        
        if(draw_start_angle_line){
            float start_angle_radians = (float)(radians_from_degrees(start_angle_degrees));
            Godot.Vector2 vec = new Vector2(
                (float)(
                    radius*(semi_major*Math.Cos(start_angle_radians)*Math.Cos(theta) - semi_minor*Math.Sin(start_angle_radians)*Math.Sin(theta))
                ),
                (float)(
                    radius*(semi_major*Math.Cos(start_angle_radians)*Math.Sin(theta) + semi_minor*Math.Sin(start_angle_radians)*Math.Cos(theta))
                )
            );
            
            context.DrawLine(
                centre,
                vec,
                black,
                shadow_width
            );
            context.DrawLine(
                centre,
                vec,
                color_a,
                width
            );
        }
        
        if(draw_final_angle_line){
            float final_angle_radians = (float)(radians_from_degrees(final_angle_degrees));
            Godot.Vector2 vec = new Vector2(
                (float)(
                    radius*(semi_major*Math.Cos(final_angle_radians)*Math.Cos(theta) - semi_minor*Math.Sin(final_angle_radians)*Math.Sin(theta))
                ),
                (float)(
                    radius*(semi_major*Math.Cos(final_angle_radians)*Math.Sin(theta) + semi_minor*Math.Sin(final_angle_radians)*Math.Cos(theta))
                )
            );
            
            context.DrawLine(
                centre,
                vec,
                black,
                shadow_width
            );
            context.DrawLine(
                centre,
                vec,
                color_b,
                width
            );
        }

        context.DrawPolyline(
            points,
            black,
            shadow_width
        );

        context.DrawPolyline(
            points,
            color_c,
            width
        );
    }
    public void draw_arc_line(
        Node2D context,
        float datum_offset_degrees,
        bool clockwise_is_positive,
        Vector2 centre,
        float start_angle_degrees,
        float final_angle_degrees,
        float radius,
        Godot.Color color_a,
        Godot.Color color_b,
        Godot.Color color_c,
        int polyline_point_count=100,
        float width=1,
        bool draw_start_angle_line=true,
        bool draw_final_angle_line=true){
        start_angle_degrees = (start_angle_degrees + datum_offset_degrees)*polarity_from_bool(clockwise_is_positive);
        final_angle_degrees = (final_angle_degrees + datum_offset_degrees)*polarity_from_bool(clockwise_is_positive);

        float shadow_width = width+2.0f;

        if(draw_start_angle_line){
            context.DrawLine(
                centre,
                x_y_vector_from_angle_radius_and_centre(centre, radius, start_angle_degrees),
                black,
                shadow_width
            );
            context.DrawLine(
                centre,
                x_y_vector_from_angle_radius_and_centre(centre, radius, start_angle_degrees),
                color_a,
                width
            );
        }
        
        if(draw_final_angle_line){
            context.DrawLine(
                centre,
                x_y_vector_from_angle_radius_and_centre(centre, radius, final_angle_degrees),
                black,
                shadow_width
            );
            context.DrawLine(
                centre,
                x_y_vector_from_angle_radius_and_centre(centre, radius, final_angle_degrees),
                color_b,
                width
            );
        }

        Vector2[] points = new Vector2[polyline_point_count];
        int z = 0;
        float arc_interval = (final_angle_degrees-start_angle_degrees)/(polyline_point_count-1);
        float current_angle_degrees = start_angle_degrees;
        while (z < polyline_point_count){
            points[z] = x_y_vector_from_angle_radius_and_centre(centre, radius, current_angle_degrees);
            z+=1;
            current_angle_degrees+=arc_interval;
        }
        
        context.DrawPolyline(
            points,
            black,
            shadow_width
        );

        context.DrawPolyline(
            points,
            color_c,
            width
        );
    }
    public Vector3 point_on_spherical_surface_using_inclination_and_rotation_about_y_axis_in_x_z_plane(double inclination, double rotation_about_y_axis_in_x_z_plane, double radius=1){
        return new Vector3(
            (float)(radius*Math.Cos(rotation_about_y_axis_in_x_z_plane)*Math.Cos(inclination)),
            (float)(radius*                1*Math.Sin(inclination)),
            (float)(radius*Math.Sin(rotation_about_y_axis_in_x_z_plane)*Math.Cos(inclination))
        );
    }
    public int polarity(float number){
        return 
            (number==0)
            ? 0
            : (number>1)
                ?1
                :-1
        ;
    }
    public int polarity(int number){
        return 
            (number==0)
            ? 0
            : (number>1)
                ?1
                :-1
        ;
    }
    public Godot.Color colour_from_progress(int point_state){
        Godot.Color result_colour;
        switch (point_state){
            case -1:
                result_colour = reading_point_todo_colour;
            break;
            case 0:
                result_colour = reading_point_doing_colour;
            break;
            case 1:
                result_colour = reading_point_done_colour;
            break;
            default:
                result_colour = new Godot.Color(1,1,1);
            break;
        }
        return result_colour;
    }
    public void draw_box_with_border(
        Node2D context,
        Vector2 centre,
        Godot.Color border_colour,
        Godot.Color fill_colour,
        int width=60,
        int height=8,
        int border_thickness=1
    ){
        // Render Sample as box with border
        Vector2 top_left_corner = new Vector2(centre.x-width/2,centre.y-height/2);
        Vector2 dimensions = new Vector2(width,height);
        context.DrawRect(new Rect2(top_left_corner,dimensions),border_colour);
        
        top_left_corner = new Vector2(top_left_corner.x+border_thickness,top_left_corner.y+border_thickness);
        dimensions = new Vector2(width-border_thickness*2,height-border_thickness*2);
        context.DrawRect(new Rect2(top_left_corner,dimensions),fill_colour);
    }
    public Vector2 rotate_Vector2_by_angle_in_radians(Vector2 input_vector, double angle_in_radians){
        return new Vector2(
            (float)( input_vector.x*Math.Cos(angle_in_radians) - input_vector.y*Math.Sin(angle_in_radians)),
            (float)(-input_vector.x*Math.Sin(angle_in_radians) + input_vector.y*Math.Cos(angle_in_radians))
        );
    }
    public Vector2 rotate_Vector2_by_angle_in_degrees(Vector2 input_vector, double angle_in_degrees){
        return rotate_Vector2_by_angle_in_radians(input_vector, radians_from_degrees(angle_in_degrees));
    }
    public List<float> loadAngleSequenceFromStringToList(string stringContainingCommaSeparatedAngleList){
        // For loading angle sequences from a file (excludes file operations)
        string[] stringArrayContainingAngles = stringContainingCommaSeparatedAngleList.Split(",");
        var list = new List<float>(stringArrayContainingAngles.Length);
        foreach (string angleAsString in stringArrayContainingAngles){
            float angleAsFloat = float.Parse(angleAsString);
            list.Add(angleAsFloat);
            GD.Print(angleAsFloat);
        }
        return list;
    }
    public string convertListOfFloatsToCommaSeparatedString(List<float> floatList){
        // For preparing readings or other data for saving to a file
        string result = "";
        bool latch = false;
        foreach (var element in floatList)
        {
            if(!latch){
                result = element.ToString();
                latch = true;
            }else{
                result = result +", "+ element.ToString();
            }
        }
        return result;
    }
}