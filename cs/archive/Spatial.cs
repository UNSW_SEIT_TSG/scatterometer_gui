using Godot;
using System;
using System.Collections.Generic;
public class Spatial : Godot.Spatial
{
    // Member variables here, example:
    // private int a = 2;
    // private string b = "textvar";
    private bool middle_mouse_button_pressed = false;
    private Vector2 camera_rotation_about_y_axis_in_x_z_plane_and_inclination_in_radians = new Vector2(0.551724f, 0.785398f);
    private double  cloud_radius = 5.65685424949;
    double camera_radius = 11.3375;
    MeshInstance scatterPlaneNormal;
    
    float inclination_angle_offset = 90.0f;

    // Used for hemisphere
        private bool has_north_pole = false;
        private int points_added = 0;   
        
        // Used for physics based distribution of points on hemisphere
            private int iteration = 0; 
            private float stability_last = 0.0f; 
            private float stability_delta = 1.0f;

    Vector3 scatter_plane_basis_vector = new Vector3(0,1,0);

    Glx glx;
    public override void _Ready()
    {
        // Called every time the node is added to the scene.
        // Initialization here
        glx = GetNode("/root/Global") as Glx;
        update_camera_position(new Vector2(0,0));
        scatterPlaneNormal = GetNode("../ScatterPlaneNormal") as MeshInstance;
    }
    private void update_camera_position(Vector2 delta){
        Camera cam = GetNode("../Camera") as Camera;
        camera_rotation_about_y_axis_in_x_z_plane_and_inclination_in_radians += delta;
        if(camera_rotation_about_y_axis_in_x_z_plane_and_inclination_in_radians.x > Math.PI/2){
            camera_rotation_about_y_axis_in_x_z_plane_and_inclination_in_radians.x = (float)(Math.PI/2);
        }
        if(camera_rotation_about_y_axis_in_x_z_plane_and_inclination_in_radians.x < -Math.PI/2){
            camera_rotation_about_y_axis_in_x_z_plane_and_inclination_in_radians.x = -(float)(Math.PI/2);
        }
        Vector3 xyz = glx.point_on_spherical_surface_using_inclination_and_rotation_about_y_axis_in_x_z_plane(
                camera_rotation_about_y_axis_in_x_z_plane_and_inclination_in_radians.x,
                camera_rotation_about_y_axis_in_x_z_plane_and_inclination_in_radians.y,
                camera_radius
            );
        
        Vector3 revised_vector3 = new Vector3(xyz.x, xyz.z, xyz.y);
        cam.SetTranslation(
            revised_vector3
        );
        cam.LookAt(new Vector3(0,0,0), new Vector3(0,0,1));
    }
    private void clear_reading_nodes_from_hemisphere(){
        if (GetNode("MeshInstances/").GetChildCount() > 0){
            foreach (MeshInstance child in GetNode("MeshInstances/").GetChildren()){
                child.QueueFree();
            }
        }
    }
    public override void _Process(float delta)
    {
       // Called every frame. Delta is time since last frame.
       // Update game logic here.
        if (glx.get_value_changed()){
            clear_reading_nodes_from_hemisphere();
            plot_angle_sequence_as_arc_on_hemisphere(
                angle_sequence:glx.reading_sequence_arc_angle_degrees,
                inclination_angle_in_degrees:glx.get_arc_inclination_angle_degrees()+inclination_angle_offset,
                rotation_about_y_axis_in_x_z_plane_angle_in_degrees:glx.get_arc_rotation_about_y_axis_in_x_z_plane_angle_degrees()+90,
                angle_sequence_offset:0
            );
            glx.set_value_changed(false);
            Camera cam = GetNode("../Camera") as Camera;
            GD.Print(cam.GetName());
            // GD.Print(AxesScatterPlane.GetInstanceId());
           
            var t = new Transform(Basis.Identity, Vector3.Zero);
            // float theta = (float)glx.radians_from_degrees(15);
            float theta = (float)glx.radians_from_degrees(glx.get_arc_inclination_angle_degrees()+inclination_angle_offset);
            
            Vector3 original_normal_vector = new Vector3(0,1,0);
            Vector3 original_axis_for_theta_rotation = new Vector3(1,0,0);
            Vector3 axis_for_theta_rotation = original_axis_for_theta_rotation.Rotated(new Vector3(0,1,0), (float)glx.radians_from_degrees(glx.get_arc_rotation_about_y_axis_in_x_z_plane_angle_degrees()));
            
            Vector3 transformed_normal_vector = original_normal_vector.Rotated(axis_for_theta_rotation, theta);
            var t2 = t.Rotated(axis_for_theta_rotation, theta).Rotated(transformed_normal_vector, (float)glx.radians_from_degrees(glx.get_arc_rotation_about_y_axis_in_x_z_plane_angle_degrees()));
            // GD.Print(t2.origin);
            // GD.Print(t2.basis);
            GD.Print(transformed_normal_vector);
            Vector2 theta_phi = theta_and_phi_from_x_y_z_vector(transformed_normal_vector);
            double theta_degrees = glx.degrees_from_radians(theta_phi.x);
            double phi_degrees = glx.degrees_from_radians(theta_phi.y);
            GD.Print("θ: "+theta_degrees.ToString()+"; φ: "+phi_degrees.ToString());
            scatterPlaneNormal.SetTransform(t2);

            SpinBox theta_spinbox = (SpinBox)GetNode("/root/ArcSceneRoot/VBox/HBoxThetaPhiG5G8/SpinBoxTheta");
            theta_spinbox.SetValue((float)theta_degrees);
            glx.set_θ(theta_degrees);

            SpinBox phi_spinbox = (SpinBox)GetNode("/root/ArcSceneRoot/VBox/HBoxThetaPhiG5G8/SpinBoxPhi");
            phi_spinbox.SetValue((float)phi_degrees);
            glx.set_φ(phi_degrees);
        }       
    }
    public Vector2 theta_and_phi_from_x_y_z_vector(Vector3 v){
        float r = v.Length();
        float theta = (float)Math.Acos(v.z/r);
        float phi = (float)Math.Atan2(v.y,v.x);
        return new Vector2(theta, phi);
    }
    public Vector3 vectorOfLineFromTwoPlanesAssumedPassingThroughOrigin(Plane plane_a, Plane plane_b){
        return plane_a.Normal.Cross(plane_b.Normal);;
    }
    public void plot_angle_sequence_as_arc_on_hemisphere(
        List<float> angle_sequence,
        float inclination_angle_in_degrees,
        float rotation_about_y_axis_in_x_z_plane_angle_in_degrees,
        float angle_sequence_offset
    ){
        List<Vector3> xyzs = new List<Vector3>(angle_sequence.Count);

        for (int q = 0; q < angle_sequence.Count; q++){
            Vector3 point_on_unit_sphere = plot_arc_point_on_hemisphere(
                arc_angle_in_radians:        glx.radians_from_degrees(angle_sequence[q]+angle_sequence_offset),
                inclination_angle_in_radians:glx.radians_from_degrees(inclination_angle_in_degrees),
                rotation_about_y_axis_in_x_z_plane_angle_in_radians:    glx.radians_from_degrees(rotation_about_y_axis_in_x_z_plane_angle_in_degrees)
            );
            // GD.Print("point_on_unit_sphere.ToString(): "+point_on_unit_sphere.ToString());
            xyzs.Add(point_on_unit_sphere);
        }
        glx.update_xyzs(xyzs);
    }

    public Vector3 plot_arc_point_on_hemisphere(
        double arc_angle_in_radians,
        double inclination_angle_in_radians,
        double rotation_about_y_axis_in_x_z_plane_angle_in_radians
    ){
        Vector3 before_rotation_about_y_axis_in_x_z_plane_rotation = new Vector3(
            (float)(Math.Sin(arc_angle_in_radians) * Math.Cos(inclination_angle_in_radians)),
            (float)(Math.Sin(arc_angle_in_radians) * Math.Sin(inclination_angle_in_radians)),
            (float)(Math.Cos(arc_angle_in_radians))
        );

        Vector3 point_on_unit_sphere = before_rotation_about_y_axis_in_x_z_plane_rotation.Rotated(new Vector3(0,1,0), (float)(rotation_about_y_axis_in_x_z_plane_angle_in_radians));

        place_point_on_hemisphere(
            GetNode("MeshInstances/"),
            point_on_unit_sphere,
            cloud_radius
        );
        return point_on_unit_sphere;
    }

    public void place_axes(Node spatial_node){
        PackedScene sphere_scene_resource = (PackedScene)ResourceLoader.Load("res://scenes/axes.tscn");
        MeshInstance sphere_scene_instance = (MeshInstance)sphere_scene_resource.Instance();
        // Vector3 scaled_point = new Vector3(
        //     (float)(input_point.x*radius),
        //     (float)(input_point.y*radius),
        //     (float)(input_point.z*radius)
        // );

        spatial_node.AddChild(sphere_scene_instance, true);
        sphere_scene_instance.RotateX((float)glx.radians_from_degrees(45));
        // sphere_scene_instance
        //     .Set(
        //         "translation", 
        //         scaled_point
        //     )
        // ;
    }

    public void place_point_on_hemisphere(Node spatial_node, Vector3 input_point, double radius){
        PackedScene sphere_scene_resource = (PackedScene)ResourceLoader.Load("res://scenes/sphere.tscn");
        MeshInstance sphere_scene_instance = (MeshInstance)sphere_scene_resource.Instance();
        Vector3 scaled_point = new Vector3(
            (float)(input_point.x*radius),
            (float)(input_point.y*radius),
            (float)(input_point.z*radius)
        );

        spatial_node.AddChild(sphere_scene_instance, true);
        sphere_scene_instance
            .Set(
                "translation", 
                scaled_point
            )
        ;
    }

    public override void _Input(InputEvent @event){
        if (@event is InputEventKey eventKey){
            if (eventKey.Pressed && eventKey.Scancode == (int)KeyList.Escape){
                GetTree().Quit();
            } else if (eventKey.Pressed && eventKey.Scancode == (int)KeyList.F5){
                GetTree().ReloadCurrentScene();
            }
        }
        if (@event is InputEventMouse iem){
            if(iem is InputEventMouseButton iem_b){
                // if (iem_b.GetButtonIndex() == (int)ButtonList.Left){
                // }
                // if (iem_b.GetButtonIndex() == (int)ButtonList.MaskLeft){
                // }
                // if (iem_b.GetButtonIndex() == (int)ButtonList.MaskMiddle){
                // }
                // if (iem_b.GetButtonIndex() == (int)ButtonList.MaskRight){
                // }
                if (iem_b.GetButtonIndex() == (int)ButtonList.Middle){
                    middle_mouse_button_pressed = iem_b.IsPressed();
                }
                // if (iem_b.GetButtonIndex() == (int)ButtonList.Right){
                // }
                if (
                    (
                        iem_b.GetButtonIndex() == (int)ButtonList.WheelDown ||
                        iem_b.GetButtonIndex() == (int)ButtonList.WheelUp
                    ) && 
                    GetViewport().GetMousePosition().x > 0 &&
                    GetViewport().GetMousePosition().y > 0 && 
                    GetViewport().GetMousePosition().y < 933
                ){
                    if (iem_b.GetButtonIndex() == (int)ButtonList.WheelUp){
                        double camera_upper_limit_radius = Math.Sqrt(Math.Pow(7,2)*3);
                        camera_radius += 0.1;
                        if(camera_radius > camera_upper_limit_radius){
                            camera_radius = camera_upper_limit_radius;
                        }
                        
                    }else{
                        double camera_lower_limit_radius = 0.5;
                        camera_radius -= 0.1;
                        if(camera_radius < camera_lower_limit_radius){
                            camera_radius = camera_lower_limit_radius;
                        }
                    }
                    update_camera_position(new Vector2(0,0));
                }
                // if (iem_b.GetButtonIndex() == (int)ButtonList.WheelLeft){
                // }
                // if (iem_b.GetButtonIndex() == (int)ButtonList.WheelRight){
                // }
                if (iem_b.GetButtonIndex() == (int)ButtonList.WheelUp && GetViewport().GetMousePosition().x > 0){

                }
            }
            if(iem is InputEventMouseMotion iem_m){
                if(middle_mouse_button_pressed){
                    Vector2 relative_motion = iem_m.GetRelative();
                    update_camera_position(new Vector2(
                        relative_motion.y*0.001f,
                        -relative_motion.x*0.001f
                    ));
                }
            }
        }
    }

    public void add_points_to_hemisphere_by_random_selection_from_uniform_distribution_over_hemisphere_surface(
        int new_point_count = 2
    ){
        iteration = 0;
        stability_delta = 1;
        points_added += new_point_count;
        generate_points_on_hemisphere(
            GetNode("MeshInstances/"), 
            cloud_radius, 
            new_point_count
        );
    }

    public void generate_points_on_hemisphere(Node spatial_node, double radius, int point_count){
        PackedScene sphere_scene_resource = (PackedScene)ResourceLoader.Load("res://scenes/sphere.tscn");
        Random random_variable = new Random();
        MeshInstance sphere_scene_instance;
        for (int q = 0; q < point_count; q++)
        {
            sphere_scene_instance = (MeshInstance)sphere_scene_resource.Instance();
            Vector3 selected_point = random_point_on_surface_of_unit_sphere(random_variable);
            Vector3 scaled_point = new Vector3(
                (float)(selected_point.x*radius),
                (float)(selected_point.y*radius),
                (float)(selected_point.z*radius)
            );
            spatial_node.AddChild(sphere_scene_instance, true);
            sphere_scene_instance
                .Set(
                    "translation", 
                    scaled_point
                )
            ;
        }
    }

    public Vector3 random_point_on_surface_of_unit_sphere(Random q){
        // manhattan_unit_ball centred at 0,0,0
        // manhattan_unit_ball extents: [-1 to 1,-1 to 1,-1 to 1]
        while (true){
            // Select point in unit cube
            Vector3 point_in_manhattan_unit_ball = new Vector3(
                (float)q.NextDouble()*2-1,
                (float)q.NextDouble()*1-0, /* Upper hemisphere only */
                // (float)q.NextDouble()*2-1, /* Full sphere only */
                (float)q.NextDouble()*2-1
            );
            
            // point_in_manhattan_unit_ball.Length

            double r = point_in_manhattan_unit_ball.Length();
            if(r<=1){ /* if distance from centre to point is less than unit radius */
                // acceptance case
                // extend vector til magnitude is same as unit radius, i.e. it sits on the surface of the sphere.
                // return point on sphere
                return new Vector3(
                    (float)(point_in_manhattan_unit_ball.x/r),
                    (float)(point_in_manhattan_unit_ball.y/r),
                    (float)(point_in_manhattan_unit_ball.z/r)
                );
            }
            // rejection case
            // re-select point in unit cube by continuing while loop.
        }
    }

    public void adjust_spacing(Node root, double radius){
        int start_index;
        int point_count = root.GetChildCount();
        Vector3[] forces = new Vector3[point_count];
        
        MeshInstance child, child_a, child_b;
        Vector3 displacement_between_centres, child_a_centre, child_b_centre, force, new_translation;
        
        Vector3 south_pole = new Vector3(0,(float)(-radius),0);
        
        if (has_north_pole){
            Vector3 north_pole = new Vector3(0,(float)(radius),0);
            MeshInstance north_child = root.GetChild(0) as MeshInstance;
            north_child.SetTranslation(north_pole);
            start_index = 1;
        }else{
            start_index = 0;
        }
        

        // Calculate Forces
        for (int child_index_outer = start_index; child_index_outer < point_count; child_index_outer++){
            // Initialise force to zero.
            forces[child_index_outer] = new Vector3(0,0,0);
            child_a = root.GetChild(child_index_outer) as MeshInstance;
            double force_magnitude;

            for (int child_index_inner = 0; child_index_inner < point_count; child_index_inner++){
            if (child_index_outer != child_index_inner){
                    child_b = root.GetChild(child_index_inner) as MeshInstance;
                    child_a_centre = (Vector3) child_a.Get("translation");
                    child_b_centre = (Vector3) child_b.Get("translation");

                    // Calculate force between points A and B
                    displacement_between_centres = child_a_centre - child_b_centre;
                    force_magnitude = 1/Math.Pow(displacement_between_centres.Length(),2);

                    force = new Vector3(
                        (float)(displacement_between_centres.x*force_magnitude),
                        (float)(displacement_between_centres.y*force_magnitude),
                        (float)(displacement_between_centres.z*force_magnitude)
                    );

                    forces[child_index_outer] += force;
                }
                // Apply polarisation to convert to hemisphere
                    child_a_centre = (Vector3) child_a.Get("translation");
                    child_b_centre = south_pole;

                    // Calculate force between points A and B
                    displacement_between_centres = child_a_centre - child_b_centre;
                    
                    double hemisphere_ratio = 1;
                    // The hemisphere_ratio defines the strength of the force at the south_pole. 
                    // Values > 1 will push the perimeter toward the north pole,
                    // if the value is 1 it will be at the equator of the sphere, and 
                    // values < 1 will push the perimeter to the south pole.

                    force_magnitude = 
                        hemisphere_ratio
                        /
                        Math.Pow(displacement_between_centres.Length(),2)
                    ;

                    force = new Vector3(
                        (float)(displacement_between_centres.x*force_magnitude),
                        (float)(displacement_between_centres.y*force_magnitude),
                        (float)(displacement_between_centres.z*force_magnitude)
                    );

                    forces[child_index_outer] += force;
            }
        }

        // Apply forces & Reproject points on to sphere surface
        for (int child_index = 0; child_index < point_count; child_index++){
            child = root.GetChild(child_index) as MeshInstance;
            new_translation = child.GetTranslation()+forces[child_index];
            new_translation = new_translation.Normalized();
            new_translation = new Vector3(
                (float)(new_translation.x*radius),
                (float)(new_translation.y*radius),
                (float)(new_translation.z*radius)
            );
            child.SetTranslation(new_translation);
        }

        float stability = 0;

        foreach (Vector3 force_i in forces){
            stability += force_i.Length();
        }
        stability /= (point_count*point_count);
        stability_delta = Math.Abs(stability_last-stability);

        if (iteration % 100 == 0 || stability_delta == 0){
            GD.Print(
                "points:\t"         +point_count.ToString()+"\t"+
                "i:\t"              +iteration.ToString()+"\t"+
                "stability:\t"      +stability.ToString()+"\t"+
                "stability_delta:\t"+stability_delta.ToString()
            );
        }
        iteration += 1;
        stability_last = stability;
    }
}
