using Godot;
using System;
using System.Globalization;
using System.Net;
using System.Net.Sockets;
using System.Collections.Generic;
using System.Threading.Tasks;
using System.Diagnostics;
using System.IO;
public class ArcSceneRoot : Node
{
    SpinBox theta;
    private bool latch = false;
    Stopwatch stopWatch = new Stopwatch();
    private static int   reboot_wait_time;
    private static Timer reboot_wait_timer=new Timer();
    Glx glx;
    Button execute_sequence_button;
    Button disabled_execute_sequence_button;
    Button execution_safety_switch;
    Button disabled_execution_safety_switch;
    private static Godot.Dictionary<string, string>   defaultsDictionary=new Godot.Dictionary<string, string>();
    // Connection xpsc8_connection;
    Connection relay_send_connection;
    Connection relay_receive_connection;
    public override void _Ready(){
        // Called every time the node is added to the scene.
        // Initialization here
        glx = GetNode("/root/Global") as Glx;
        initialise_spinbox_values();
        execute_sequence_button = GetNode("VBox/Panel2/HBoxContainer/HBoxContainer/ButtonExecute") as Button;        
        disabled_execute_sequence_button = GetNode("VBox/Panel2/HBoxContainer/HBoxContainer/Button_Disabled") as Button;
        execution_safety_switch = GetNode("VBox/Panel2/HBoxContainer/HBoxContainer/execution_safety_switch") as Button;
        disabled_execution_safety_switch = GetNode("VBox/Panel2/HBoxContainer/HBoxContainer/disabled_execution_safety_switch") as Button;
        loadDefaultsFromFile();

        // Setup reboot wait delay timer for later.
        reboot_wait_timer.SetOneShot(true);
        reboot_wait_timer.Connect("timeout", GetNode("."), "request_relay_server_reconnect_to_xpsc8");
        reboot_wait_time=String.Equals(
                defaultsDictionary["localControllerIP"],
                defaultsDictionary["controllerIPRaw"]
            )
            ? 5
            : 52
        ;
        reboot_wait_timer.SetWaitTime(reboot_wait_time);
        AddChild(reboot_wait_timer);
        GD.Print(glx.get_relay_server_connected());
    }
    private void loadDefaultsFromFile(){
        string defaults_string=System.IO.File.ReadAllText("defaults.config");
        string[] listOfDefaultStrings=defaults_string.Split("\n");
        foreach (string defaultString in listOfDefaultStrings){
            string[] defaultKeyValuePairStrings=defaultString.Replace(";","").Replace(" ","").Split("=");
            string key=defaultKeyValuePairStrings[0];
            string value=defaultKeyValuePairStrings[1];

            if (key.Equals("controllerIP"))
            {
                AddOrUpdateDict(defaultsDictionary,"controllerIPRaw", value);
                value = 
                    Convert.ToInt16(value.Split('.')[0]).ToString() + "." +
                    Convert.ToInt16(value.Split('.')[1]).ToString() + "." +
                    Convert.ToInt16(value.Split('.')[2]).ToString() + "." +
                    Convert.ToInt16(value.Split('.')[3]).ToString()
                ;
            }
            AddOrUpdateDict(defaultsDictionary,key, value);
        }
    }
    private void AddOrUpdateDict(Godot.Dictionary<string, string> dict, string key, string value){
        if (dict.ContainsKey(key)){
            dict[key]=value;
        }else{
            dict.Add(key, value);
        }
    }
    private void initialise_spinbox_values(){
        SpinBox start_angle       = GetNode("VBox/HBoxContainer3/VBoxContainer/Panel4/HBoxTitleBar/SpinBoxStartAngle")   as SpinBox;
        SpinBox final_angle       = GetNode("VBox/HBoxContainer3/VBoxContainer/Panel4/HBoxTitleBar/SpinBoxFinalAngle")   as SpinBox;
        SpinBox reading_count     = GetNode("VBox/HBoxContainer3/VBoxContainer/Panel4/HBoxTitleBar/SpinBoxReadingCount") as SpinBox;
        SpinBox rotation_about_y_axis_in_x_z_plane_angle     = GetNode("VBox/HBoxContainer3/VBoxContainer/HBoxContainer/VBoxContainer/Panel5/HBoxTitleBar/SpinBoxArcRotationAboutYAxisInXZPlaneAngle") as SpinBox;
        SpinBox inclination_angle = GetNode("VBox/HBoxContainer3/VBoxContainer/HBoxContainer/VBoxContainer3/Panel5/HBoxTitleBar2/SpinBoxInclinationAngle") as SpinBox;

        start_angle.SetValue(glx.get_arc_start_angle_degrees());
        final_angle.SetValue(glx.get_arc_final_angle_degrees());
        reading_count.SetValue(glx.get_reading_count());
        rotation_about_y_axis_in_x_z_plane_angle.SetValue(glx.get_arc_rotation_about_y_axis_in_x_z_plane_angle_degrees());
        inclination_angle.SetValue(glx.get_arc_inclination_angle_degrees());
    }
    private void _on_execution_safety_switch_toggled(bool value){
        glx.set_execution_enabled(value);
    } 
    public override void _Process(float delta)
    {
        Button button_disconnect = GetNode("VBox/Panel2/HBoxContainer/HBoxContainer/ButtonDisconnect") as Button;
        button_disconnect.SetVisible(glx.get_relay_server_connected());
        Button button_connect = GetNode("VBox/Panel2/HBoxContainer/HBoxContainer/ButtonConnect") as Button;
        button_connect.SetVisible(!glx.get_relay_server_connected());

        if(glx.from_relay_server.Count>0){
            // GD.Print(glx.from_relay_server.Count, ":",stopWatch.ElapsedMilliseconds);
            stopWatch.Restart();
            String deq = glx.from_relay_server.Dequeue();
            if (!(deq.Left(2).Equals("~:") && deq.Right(deq.Length - 3).Equals(": ~"))){
                GD.Print(deq);
            }            
            latch = false;
        }

        if(glx.get_relay_server_connected()){
            if(!glx.waiting_for_response){
                glx.waiting_for_response = true;
                collectInboundData();
            }
        }
        
        // Check if server dropped
        if(
            stopWatch.ElapsedMilliseconds>3000 &
            glx.get_relay_server_connected() &
            !latch
        ){
            GD.Print("heartbeat lost");
            latch = true;
            disconnect();
        }

        
    }
    private void update_gamma(){
        SpinBox gamma = GetNode("VBox/HBoxContainer3/VBoxContainer2/Panel5/HBoxTitleBar/SpinBoxGamma") as SpinBox;
        gamma.SetValue(get_γ());
    }
    private void update_alpha(){
        SpinBox alpha = GetNode("VBox/HBoxContainer3/VBoxContainer2/Panel5/HBoxTitleBar/SpinBoxAlpha") as SpinBox;
        alpha.SetValue(get_α());
    }
    private void update_beta(){
        SpinBox beta = GetNode("VBox/HBoxContainer3/VBoxContainer2/Panel5/HBoxTitleBar/SpinBoxBeta") as SpinBox;
        beta.SetValue(get_β());
    }
    private void _on_SpinBoxStartAngle_value_changed(float value){
        glx.set_arc_start_angle_degrees(value);
        update_gamma();
    }
    private void _on_SpinBoxFinalAngle_value_changed(float value){
        glx.set_arc_final_angle_degrees(value);
        update_gamma();
    }
    private void _on_SpinBoxReadingCount_value_changed(float value){
        glx.set_reading_count(value);
        update_gamma();
    }
    private void _on_SpinBoxInclinationAngle_value_changed(float value){
        glx.set_arc_inclination_angle_degrees(value);
        glx.update_sequence();
        update_beta();
    }
    private void _on_SpinBoxArcRotationAboutYAxisInXZPlaneAngle_value_changed(float value){
        glx.set_arc_rotation_about_y_axis_in_x_z_plane_angle_degrees(value);
        glx.update_sequence();
        update_alpha();
        update_beta();
    }
    public override void _Input(InputEvent @event){
        if (@event is InputEventKey eventKey){
            if (eventKey.Pressed && eventKey.Scancode == (int)KeyList.Escape){
                graceful_exit();
            }
        }
    }
    public void _on_CheckButton_toggled(){
        CheckButton execution_safety_switch = GetNode("VBox/Panel2/HBoxContainer/CheckButton") as CheckButton;
    }
    private void update_connectivity_indicator(
        ColorRect color_rect,
        Label label,
        string connection_name,
        bool connected
    ){
        color_rect.Set(
            "color",
            (connected) ? "#56e042" : "#e04242"
        );
        label.SetText((connected) ? connection_name+" connected" : connection_name+" not connected");
    }
    private void _on_ButtonMoveToNext_pressed(){
        if(glx.get_relay_server_connected()){
            TCPClient.send(relay_send_connection,"group_move_relative(group_name=GROUP1,target="+glx.get_γ()+")");
        }else{
            GD.Print("relay server not connected.");
        }
    }
    private void _on_ButtonSingleReading_pressed(){
        if(glx.get_relay_server_connected()){
            TCPClient.send(relay_send_connection,"get_optical_power_meter_reading()");
        }else{
            GD.Print("relay server not connected.");
        }
    }
    private void _on_ButtonRunPythonScript_pressed(){
        GD.Print("_on_ButtonRunPythonScript_pressed");
        // GD.Print(run_cmd(cmd: "c:\\r\\scatterometer\\py\\spatial\\hello.py", args: "45 45 10 4"));
        GD.Print(run_cmd(cmd: "c:\\r\\scatterometer\\py\\spatial\\hello.py", arg_1: "45", arg_2: "45", arg_3: "10", arg_4: "4"));
    }
    private void _on_ButtonAlignScatterPlane_pressed(){
        GD.Print("_on_ButtonAlignScatterPlane_pressed");
        // float neg_nγ = glx.get_γ()*glx.get_reading_count();
        float neg_nγ = glx.get_arc_start_angle_degrees();
        float neg_θ = glx.get_θ();
        float neg_φ = glx.get_φ();
        // GD.Print(" Group 2 Move to -nγ:"+neg_nγ.ToString("0.00")+" below datum position");
        GD.Print(" Group 5 Move to -θ:"+neg_θ.ToString("0.00")+" below datum position");
        GD.Print(" Group 7 Move to -φ:"+neg_φ.ToString("0.00")+" below datum position");
        if(glx.get_relay_server_connected()){
            // TCPClient.send(relay_send_connection, "group_move_custom(group_name=GROUP2,target="+neg_nγ.ToString("0.00")+")");
            TCPClient.send(relay_send_connection, "group_move_custom(group_name=GROUP5,target="+neg_θ.ToString("0.00")+")");
            TCPClient.send(relay_send_connection, "group_move_custom(group_name=GROUP7,target="+neg_φ.ToString("0.00")+")");
        }else{
            GD.Print("relay server not connected.");
        }
        
    }

    public string run_cmd(string cmd, string arg_1, string arg_2, string arg_3, string arg_4)
    {
        ProcessStartInfo start = new ProcessStartInfo();
        start.FileName = "py";
        start.Arguments = string.Format("\"{0}\" \"{1}\" \"{2}\" \"{3}\" \"{4}\"", cmd, arg_1, arg_2, arg_3, arg_4);
        start.UseShellExecute = false;// Do not use OS shell
        start.CreateNoWindow = true; // We don't need new window
        start.RedirectStandardOutput = true;// Any output, generated by application will be redirected back
        start.RedirectStandardError = true; // Any error in standard output will be redirected back (for example exceptions)
        using (Process process = Process.Start(start))
        {
            using (StreamReader reader = process.StandardOutput)
            {
                string stderr = process.StandardError.ReadToEnd(); // Here are the exceptions from our Python script
                string result = reader.ReadToEnd(); // Here is the result of StdOut(for example: print "test")
                return result;
            }
        }
    }
    private float get_α(){
        Spatial spx = new Spatial();
        List<Vector3> first_median_last = glx.getFirstMedianLastListAsXYZ();
        Plane scatter_plane = new Plane(first_median_last[0], first_median_last[1], first_median_last[2]);

        
        //                         a  b  c  d in equation ax+by+cx+d = 0
        Plane xz_plane = new Plane(0, 1, 0, 0);
        Vector3 x_axis = new Vector3(1,0,0);
        Vector3 intersection_vector = spx.vectorOfLineFromTwoPlanesAssumedPassingThroughOrigin(scatter_plane, xz_plane);
        float alpha = (float)(glx.degrees_from_radians(intersection_vector.AngleTo(x_axis)));
        return alpha;
    }
    private float get_β(){
        Spatial spx = new Spatial();

        List<Vector3> first_median_last = glx.getFirstMedianLastListAsXYZ();
        Plane scatter_plane = new Plane(first_median_last[0], first_median_last[1], first_median_last[2]);

        //                         a  b  c  d in equation ax+by+cx+d = 0
        Plane yz_plane = new Plane(1, 0, 0, 0);
        Vector3 y_axis = new Vector3(0, 1, 0);
        Vector3 intersection_vector = spx.vectorOfLineFromTwoPlanesAssumedPassingThroughOrigin(scatter_plane, yz_plane);

        float beta_1 = (float)(glx.degrees_from_radians(intersection_vector.AngleTo(y_axis)));
        float beta_2 = (float)(glx.degrees_from_radians((intersection_vector*-1).AngleTo(y_axis)));

        GD.Print("intersection_vector: ", intersection_vector);
        return beta_2;
    }
    private float get_γ(){
        return (glx.get_arc_final_angle_degrees()-glx.get_arc_start_angle_degrees())/(glx.get_reading_count()-1);
    }

    private void _on_ButtonLogin_pressed(){
        TCPClient.send(relay_send_connection, "login()");
    }
    private void _on_ButtonReboot_pressed(){
        TCPClient.send(relay_send_connection, "reboot()");
        reboot_wait_timer.Start();
    }
    private void request_relay_server_reconnect_to_xpsc8(){
        TCPClient.send(relay_send_connection,"reconnect_to_xpsc8()");
    }
    private void _on_ButtonInitialiseAll_pressed(){
        TCPClient.send(relay_send_connection,"initialise_all()");
    }
    private void _on_ButtonHomeAll_pressed(){
        TCPClient.send(relay_send_connection,"home_search_all()");
    }
    private void _on_ButtonExecute_pressed(){
        // State codes (for details see StateMachine.cs file)
        HashSet<int> not_initialized_states = new HashSet<int>{0,1,2,3,4,5,6,7,8,9,50,63,66,67,71,72};
        HashSet<int> ready_states           = new HashSet<int>{10,11,12,13,14,15,16,17,18,70,77};
        HashSet<int> error_states           = new HashSet<int>{19,52,53,54,55,56,57,58,59,60,61,62,65};
        HashSet<int> disabled_states        = new HashSet<int>{20,21,22,23,24,25,26,27,28,29,30,31,32,33,34,35,36,37,38,39,74,75,76};

        // Temporary code for testing save function, only one stage is in use in
        // the below code
        // <-------

        // string group_name, response, command_string;

        // for (int i = 1; i <= 7; i++){
        //     group_name = "GROUP"+i.ToString();
        //     response = TCPClient.send_receive(xpsc8_connection, "GroupStatusGet("+group_name+",int *)");
        //     string[] response_strings=response.Split(",");
        //     int response_code = Int32.Parse(response_strings[1]);
        //     command_string = "GroupMoveAbsolute("+group_name+","+datum_positions[i-1]+")";
        //     if(ready_states.Contains(response_code)){
        //         GD.Print(command_string+":\t"+TCPClient.send_receive(xpsc8_connection, command_string));
        //     }else if(error_states.Contains(response_code)){
        //         GD.Print(
        //             TCPClient.send_receive(
        //                 xpsc8_connection,
        //                 "ErrorStringGet("+response_code.ToString()+",char *)")
        //         );
        //     }
        // }

        // Need to review this code once custom transformation is provided by Joe
        // bool transformation_provided_by_joe = false; 
        // if(transformation_provided_by_joe){
        //     // Apply ArcRotationAboutYAxisInXZPlane rotation via group move action
        //     string rotation_about_y_axis_in_x_z_plane_stage = "GROUP7";
        //     bool rotation_about_y_axis_in_x_z_plane_stage_clockwise_is_positive = true;
        //     float rotation_about_y_axis_in_x_z_plane_stage_angle = glx.transform_angle(45,datum_positions[6], rotation_about_y_axis_in_x_z_plane_stage_clockwise_is_positive);
        //     command_string = "GroupMoveAbsolute("+rotation_about_y_axis_in_x_z_plane_stage+","+rotation_about_y_axis_in_x_z_plane_stage_angle+")";
        //     GD.Print(command_string+":\t"+TCPClient.send_receive(xpsc8_connection, command_string));

        //     // Apply inclination rotation via group move action
        //     string inclination_stage = "GROUP5";
        //     bool inclination_stage_clockwise_is_positive = true;
        //     float inclination_stage_angle = glx.transform_angle(45,datum_positions[6], inclination_stage_clockwise_is_positive);
        //     command_string = "GroupMoveAbsolute("+inclination_stage+","+inclination_stage_angle+")";
        //     GD.Print(command_string+":\t"+TCPClient.send_receive(xpsc8_connection, command_string));
        // }
        
        /* Not yet implemented
        // Cycle each inbound light source angle via stack rotation
        foreach (float light_source_angle in glx.reading_sequence_arc_angle_degrees){
            // Cycle each outbound reflection angle via sensor arm move action
            float light_source_angle_motor = glx.transform_angle(light_source_angle,datum_positions[1],true);
            command_string = 
                "GroupMoveAbsolute("+
                    +light_source_angle_motor+","
                    +light_source_angle_motor+
                ")"
            ;
            GD.Print(command_string+":\t"+TCPClient.send_receive(xpsc8_connection, command_string));

            foreach (float reflection_angle in glx.reading_sequence_arc_angle_degrees){
                float reflection_angle_motor = glx.transform_angle(reflection_angle,datum_positions[0],true);
                command_string = 
                    "GroupMoveAbsolute("+
                        +reflection_angle_motor+","
                        +reflection_angle_motor+
                    ")"
                ;
                GD.Print(command_string+":\t"+TCPClient.send_receive(xpsc8_connection, command_string));
                // Take reading
                GD.Print(
                    light_source_angle.ToString()+" "+
                    reflection_angle.ToString()+" "+
                    take_reading()
                );
            }
        }
        */
    }
    private string take_reading(){
        TCPClient.send(relay_send_connection, "get_optical_power_meter_reading()");
        return TCPClient.receive(relay_receive_connection);
        // GD.Print(TCPClient.send_receive(xpsc8_connection, "Hello there "+DateTime.Now.ToString("yyyyMMddHHmmss")));
        // TCPClient.send(relay_send_connection, "Ahoy "+DateTime.Now.ToString("yyyyMMddHHmmss")+"\n");
    }
    private void _on_ButtonQuit_pressed(){
        graceful_exit();
    }
    private void _on_ButtonMinimise_pressed(){
        OS.SetWindowMinimized(true);
    }
    private void graceful_exit(){
        if(glx.get_relay_server_connected()){
            disconnect();
        }
        GetTree().Quit();
    }
    private void _on_ButtonTest_pressed(){
        GD.Print("_on_ButtonTest_pressed");
        TCPClient.send(relay_send_connection, "random_dance()");
    }
    private void _on_ButtonConnect_pressed(){
        GD.Print("_on_ButtonConnect_pressed");
        connect();
    }
    private void _on_ButtonDisconnect_pressed(){
        GD.Print("_on_ButtonDisconnect_pressed");
        disconnect();
    }
    private void _on_ButtonDatum_pressed(){
        GD.Print("_on_ButtonDatum_pressed");
        TCPClient.send(relay_send_connection,"datum_reset()");
    }
    private void _on_ButtonKillServerTest_pressed(){
        GD.Print("_on_ButtonKillServerTest_pressed()");
        TCPClient.send(relay_send_connection,"!!!");
    }
    private void disconnect(){
        TCPClient.send(relay_send_connection, "!!!");
        TCPClient.send(relay_send_connection, "");
        TCPClient.shutdown_and_close(relay_send_connection);
        TCPClient.shutdown_and_close(relay_receive_connection);
        GD.Print("Disconnected");
        glx.set_relay_server_connected(false);
    }
    private void connect(){
        relay_send_connection    = new Connection(
            defaultsDictionary["relayIP"],
            Int32.Parse(defaultsDictionary["relayPortSend"])
        );
        relay_receive_connection = new Connection(
            defaultsDictionary["relayIP"],
            Int32.Parse(defaultsDictionary["relayPortReceive"])
        );

        try{
            TCPClient.start_client(relay_send_connection);
            TCPClient.start_client(relay_receive_connection);
            glx.set_relay_server_connected(true);    
        }
        catch (Exception)
        {
            GD.Print("connection attempt failed");
            glx.set_relay_server_connected(false);    
        }
    }
    private void obtainIncomingData(){
        string received_data = TCPClient.receive(relay_receive_connection);
        if(received_data.Length>0){
            // GD.Print("received_data.Length: ", received_data.Length);
            glx.from_relay_server.Enqueue(received_data);
        }
        glx.waiting_for_response = false;
    }
    private async void collectInboundData(){
        await Task.Run(() => obtainIncomingData());
    }
}