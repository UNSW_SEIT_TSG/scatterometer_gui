using Godot;
using System;

public class ArcAlphaBetaIntervals : Node2D
{
    // Member variables here, example:
    // private int a = 2;
    // private string b = "textvar";
    // Godot.Vector2 centre_of_viewport = new Godot.Vector2(958/2, 462/2);
    Godot.Vector2 centre_of_viewport = new Godot.Vector2(0, 0);
    float datum_offset_degrees = 0.0f;
    bool  clockwise_is_positive = false;
    bool  latch = false;
    float radius = (float)(462*0.9);
    public override void _Ready()
    {
        // Called every time the node is added to the scene.
        // Initialization here
        Camera2D cam    = GetNode("Camera2D") as Camera2D;
        Sprite   sprite = GetNode("Sprite") as Sprite;
        // sprite.SetPosition(centre_of_viewport);
        sprite.SetVisible(false);
        cam.SetPosition(new Vector2(centre_of_viewport.x, (float)((462/2)*-0.9)));
    }
    public override void _Process(float delta)
    {
        // Called every frame. Delta is time since last frame.
        // Update game logic here.
        Update();
    }
    
    public override void _Draw(){
        Glx glx = GetNode("/root/Global") as Glx;
        
        float width = 2.0f;
        float shadow_width = width+2.0f;
        float inclination_angle_offset = 90.0f;
        float inclination_angle = glx.get_arc_inclination_angle_degrees()+inclination_angle_offset;

        DrawLine(
            from:centre_of_viewport,
            to:glx.x_y_vector_from_angle_radius_and_centre(
                centre:centre_of_viewport,
                radius:(float)(radius*Math.Cos(glx.radians_from_degrees(inclination_angle))),
                angle_degrees:-90
            ),
            color:glx.black,
            width:shadow_width
        );

        DrawLine(
            from:centre_of_viewport,
            to:glx.x_y_vector_from_angle_radius_and_centre(
                centre:centre_of_viewport,
                radius:(float)(radius*Math.Cos(glx.radians_from_degrees(inclination_angle))),
                angle_degrees:-90
            ),
            color:glx.rotation_about_y_axis_in_x_z_plane_heading_vector_colour,
            width:width
        );

        DrawLine(
            from:glx.x_y_vector_from_angle_radius_and_centre(
                centre:centre_of_viewport,
                radius:(float)(radius*Math.Cos(glx.radians_from_degrees(inclination_angle))),
                angle_degrees:-90
            ),
            to:new Vector2(centre_of_viewport.x, centre_of_viewport.y-radius),
            color:glx.black,
            width:shadow_width
        );

        DrawLine(
            from:glx.x_y_vector_from_angle_radius_and_centre(
                centre:centre_of_viewport,
                radius:(float)(radius*Math.Cos(glx.radians_from_degrees(inclination_angle))),
                angle_degrees:-90
            ),
            to:new Vector2(centre_of_viewport.x, centre_of_viewport.y-radius),
            color:glx.inclination_arc_colour,
            width:width
        );

        glx.draw_elliptical_arc(
            context:this,
            datum_offset_degrees:-90,
            clockwise_is_positive:clockwise_is_positive,
            centre:centre_of_viewport,
            start_angle_degrees:90,
            final_angle_degrees:270,
            radius:radius,
            width:width,
            draw_final_angle_line:true,
            draw_start_angle_line:true,
            color_a:glx.rotation_about_y_axis_in_x_z_plane_halfturn_vector_colour,
            color_b:glx.rotation_about_y_axis_in_x_z_plane_zeroturn_vector_colour,
            color_c:glx.black,
            semi_major:(float)(Math.Cos(glx.radians_from_degrees(inclination_angle)))
        );

        glx.draw_arc_line(
            context:this,
            datum_offset_degrees:datum_offset_degrees,
            clockwise_is_positive:clockwise_is_positive,
            centre:centre_of_viewport,
            start_angle_degrees:glx.get_arc_start_angle_degrees(),
            final_angle_degrees:glx.get_arc_final_angle_degrees(),
            radius:radius,
            width:width,
            color_a:glx.start_angle_vector_colour,
            color_b:glx.final_angle_vector_colour,
            color_c:glx.reading_arc_colour
        );
        draw_reading_locations();
        // if (!latch)
        // {
        //     draw_reading_locations();
        //     latch = true;    
        // }
        
    }
    private void draw_reading_locations(){
        Glx glx = GetNode("/root/Global") as Glx;
        int counter = 0;
        foreach (float reading_angle in glx.reading_sequence_arc_angle_degrees){
            float transformed_angle = 
                (reading_angle + datum_offset_degrees)*
                glx.polarity_from_bool(clockwise_is_positive)
            ;
            Vector2 point =
                glx.x_y_vector_from_angle_radius_and_centre(
                    new Vector2(0,0), 
                    radius, 
                    transformed_angle
                )
            ;
            DrawCircle(
                point,
                4,
                new Godot.Color(0,0,0)
            );
            DrawCircle(
                point,
                3,
                glx.colour_from_progress(glx.polarity(counter-glx.get_current_reading_index()))
            );
            counter += 1;
        }
    }
}

