using Godot;
using System;
public class Redundant{
    private Range                        SAHS;
    private Range                        FAHS;
    private Range                        IAHS;
    Node q;
    Glx glx;
    public Redundant(
        Node  q,
        Glx   glx,
        Range SAHS,
        Range FAHS,
        Range IAHS
    ){
        this.q = q;
        this.glx = glx;
        this.SAHS = SAHS;
        this.FAHS = FAHS;
        this.IAHS = IAHS;
    }
    private void setIntervalArcText(float intervalArcAngle){
        q.GetNode("Panel/VBoxContainerMain/VBoxContainer/HBoxContainer/VBoxContainerControls/VBoxContainer/VBoxContainerIntervalControl/TextEdit")
            .Set("text","Interval arc: " + intervalArcAngle.ToString("0.0000").Left(6) + "°;")
        ;
    }
    private void setIntervalArcValue(float intervalArcAngle){
        setIntervalArcText(intervalArcAngle);
    }
    private float calcIntervalArcFromStartFinalAnglesAndIntervalCount(){
        return (FAHS.GetValue()-SAHS.GetValue())/IAHS.GetValue();
    }
    private void attemptConnection(){
        // GD.Print("Attempting connection");
        // bool connected=false;
        
        // if(LoopConnect()){
        //     if(attemptHandshake(false)){
        //         connected=true;
                
        //     }else{
        //         connected=false;
        //         // _clientSocket.Close();
        //     }
        // }

        // if(connected){
        //     comms_log.writePrefixAndMessageToLogWithTimestamp("XPS-C8 Controller connected on "+defaultsDictionary["controllerIP"]+":"+defaultsDictionary["controllerPort"], "");
        //     comms_log.writePrefixAndMessageToLogWithTimestamp("Note: IP address and port can be changed by editing defaults.config.", "");
        //     estimate_update_timer.Stop();

        //     // Check if sensor arm is ready for commands
        //     GD.Print("Check if sensor arm is ready for commands");
        //     string ec=SendLoop(GetNode("."), "GroupMoveRelative(GROUP1,0)", false);
        //     if(Int32.Parse(ec.Substring(0,ec.IndexOf(",")))<0){
        //         // Need to initialise and home
        //         SendLoop(GetNode("."), "GroupInitialize(GROUP1)", false);
        //         SendLoop(GetNode("."), "GroupHomeSearch(GROUP1)", false);
        //     }
            
        //     updateCurrentPosition(query_current_position("GROUP1"));
        //     heartbeat_timer.Start();
        // }else{
        //     comms_log.writePrefixAndMessageToLogWithTimestamp("Connection attempt failed", "");
        //     reboot_wait_timer.Start();
        // }
        // updateControllerStatusIndicatorColour(connected);
    }

    /* public class TCPClient{
        public bool connected = false;
        public static Socket _clientSocket;
        public IPAddress controllerIP;
        public Int32 controllerPort;
        public TCPClient(){
            instantiate_socket();
        }
        public TCPClient(
            IPAddress controllerIP,
            Int32 controllerPort
        ){
            this.controllerIP   = controllerIP;
            this.controllerPort   = controllerPort;
            instantiate_socket();
        }
        public TCPClient(
            string controllerIPString,
            string controllerPortString
        ){
            this.controllerIP   = System.Net.IPAddress.Parse(controllerIPString);
            this.controllerPort = Int32.Parse(controllerPortString);
            instantiate_socket();
        }
        public TCPClient(
            Dictionary<string, string> defaultsDictionary
        ){
            this.controllerIP   = System.Net.IPAddress.Parse(defaultsDictionary["controllerIP"]);
            this.controllerPort = Int32.Parse(defaultsDictionary["controllerPort"]);
            instantiate_socket();
        }
        public void instantiate_socket(){
            _clientSocket = new Socket(AddressFamily.InterNetwork, SocketType.Stream, ProtocolType.Tcp);
        }
        private void restoreConnection(){
            // updateControllerStatusIndicatorColour(false);
            // reboot_wait_timer.Start();
            // rebootStartTime=DateTime.Now;
            // estimate_update_timer.Start();
            _clientSocket.Close();
            instantiate_socket();
        }
        private bool LoopConnect(){
            int attempts=0;
            while (!_clientSocket.Connected)
            {
                try
                {
                    attempts += 1;
                    _clientSocket.Connect(this.controllerIP, this.controllerPort);
                }
                catch (SocketException)
                {
                    Console.WriteLine("Connection attempts: "+attempts.ToString());
                    return false;
                }
            }
            Console.WriteLine("Connected");
            return true;
        }
        public bool connect(){
            bool result = false;
            try{
                _clientSocket.Connect(this.controllerIP, this.controllerPort);
                result = true;
            }catch (SocketException){
                GD.Print("SocketException");
            }
            connected = result;
            return result;
        }
    }*/
}