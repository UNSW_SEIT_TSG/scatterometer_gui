using Godot;
using System;
using System.Collections.Generic;

public class Spatial : Godot.Spatial{
    private bool middle_mouse_button_pressed = false;
    Ruut root_instance_for_accessing_root_methods = new Ruut();
    double camera_radius = 10.1;
    private double cloud_radius = 5.65685424949;
    private int points_added = 0; 
    private bool has_north_pole = false;
    private float stability_last = 0.0f;
    private float stability_delta = 1.0f;
    private int iteration = 0;
    private Vector2 azimuth_and_inclination = new Vector2(0.551724f, 0.785398f);

    private string[] point_distribution_modes = new string[]{
        "hemisphere_uniform_random_distribution",
        "hemisphere_uniform_physics_distribution",
        "single_arc"
    };
    private int point_distribution_mode = 2;

    public override void _Ready(){
        // Initialization here
        update_camera_position(new Vector2(0,0));
        GD.Print(
            rotate_Vector2_by_angle(new Vector2(0,1), rad_from_deg(45))
        );
        switch (point_distribution_mode){
            case 0:
                GD.Print("case 0: hemisphere_uniform_random_distribution");
            break;
            case 1:
                GD.Print("case 1: hemisphere_uniform_physics_distribution");
                add_points_to_hemisphere_by_random_selection_from_uniform_distribution_over_hemisphere_surface(2);
            break;
            case 2:
                GD.Print("case 2: single_arc");
                // generate_points_along_arc_on_hemisphere(0,180,10);
            break;
            default: GD.Print("Default"); break;
        }
    }
    public float rad_from_deg(float angle_in_degrees){
        return (float)((angle_in_degrees/360)*Math.PI*2);
    }
    public Vector2 rotate_Vector2_by_angle(Vector2 input_vector, double angle_in_radians){
        return new Vector2(
            (float)( input_vector.x*Math.Cos(angle_in_radians) - input_vector.y*Math.Sin(angle_in_radians)),
            (float)(-input_vector.x*Math.Sin(angle_in_radians) + input_vector.y*Math.Cos(angle_in_radians))
        );
    }
    public void clear_reading_nodes_from_hemisphere(){
        if (GetNode("MeshInstances/").GetChildCount() > 0){
            foreach (MeshInstance child in GetNode("MeshInstances/").GetChildren()){
                child.QueueFree();
            }
        }
    }
    public void generate_points_along_arc_on_hemisphere(float start_angle, float final_angle, int interval_count){
        
        List<float> angle_sequence = new List<float>(interval_count+1);
        float interval_arc = (final_angle-start_angle)/interval_count;
        for (int q = 0; q <= interval_count; q++){
            angle_sequence[q] = start_angle+q*interval_arc;
        }

        plot_angle_sequence_as_arc_on_hemisphere(angle_sequence, rad_from_deg(45), rad_from_deg(45), 0);
    }
    public void plot_angle_sequence_as_arc_on_hemisphere(
        List<float> angle_sequence,
        float inclination_angle_in_radians,
        float azimuth_angle_in_radians,
        float angle_sequence_offset
    ){
        for (int q = 0; q < angle_sequence.Count; q++){
            plot_arc_point_on_hemisphere(
                rad_from_deg(angle_sequence[q]+angle_sequence_offset),
                inclination_angle_in_radians,
                azimuth_angle_in_radians
            );
        }
    }
    public void plot_arc_point_on_hemisphere(double arc_angle_in_radians, double inclination_angle_in_radians, double azimuth_angle_in_radians){
        Vector3 before_azimuth_rotation = new Vector3(
            (float)(Math.Sin(arc_angle_in_radians) * Math.Cos(inclination_angle_in_radians)),
            (float)(Math.Sin(arc_angle_in_radians) * Math.Sin(inclination_angle_in_radians)),
            (float)(Math.Cos(arc_angle_in_radians))
        );

        root_instance_for_accessing_root_methods.place_point_on_hemisphere(
            GetNode("MeshInstances/"),
            before_azimuth_rotation.Rotated(new Vector3(0,1,0), (float)(azimuth_angle_in_radians)),
            cloud_radius
        );
    }
    public void add_points_to_hemisphere_by_random_selection_from_uniform_distribution_over_hemisphere_surface(int point_count = 2){
        iteration = 0;
        stability_delta = 1;
        points_added += point_count;
        root_instance_for_accessing_root_methods.generate_points_on_hemisphere(
            GetNode("MeshInstances/"), 
            cloud_radius, 
            point_count
        );
    }
    public void adjust_spacing(Node root, double radius){
        int start_index;
        int point_count = root.GetChildCount();
        Vector3[] forces = new Vector3[point_count];
        
        MeshInstance child, child_a, child_b;
        Vector3 displacement_between_centres, child_a_centre, child_b_centre, force, new_translation;
        
        Vector3 south_pole = new Vector3(0,(float)(-radius),0);
        
        if (has_north_pole){
            Vector3 north_pole = new Vector3(0,(float)(radius),0);
            MeshInstance north_child = root.GetChild(0) as MeshInstance;
            north_child.SetTranslation(north_pole);
            start_index = 1;
        }else{
            start_index = 0;
        }
        

        // Calculate Forces
        for (int child_index_outer = start_index; child_index_outer < point_count; child_index_outer++){
            // Initialise force to zero.
            forces[child_index_outer] = new Vector3(0,0,0);
            child_a = root.GetChild(child_index_outer) as MeshInstance;
            double force_magnitude;

            for (int child_index_inner = 0; child_index_inner < point_count; child_index_inner++){
            if (child_index_outer != child_index_inner){
                    child_b = root.GetChild(child_index_inner) as MeshInstance;
                    child_a_centre = (Vector3) child_a.Get("translation");
                    child_b_centre = (Vector3) child_b.Get("translation");

                    // Calculate force between points A and B
                    displacement_between_centres = child_a_centre - child_b_centre;
                    force_magnitude = 1/Math.Pow(displacement_between_centres.Length(),2);

                    force = new Vector3(
                        (float)(displacement_between_centres.x*force_magnitude),
                        (float)(displacement_between_centres.y*force_magnitude),
                        (float)(displacement_between_centres.z*force_magnitude)
                    );

                    forces[child_index_outer] += force;
                }
                // Apply polarisation to convert to hemisphere
                    child_a_centre = (Vector3) child_a.Get("translation");
                    child_b_centre = south_pole;

                    // Calculate force between points A and B
                    displacement_between_centres = child_a_centre - child_b_centre;
                    
                    double hemisphere_ratio = 1;
                    // The hemisphere_ratio defines the strength of the force at the south_pole. 
                    // Values > 1 will push the perimeter toward the north pole,
                    // if the value is 1 it will be at the equator of the sphere, and 
                    // values < 1 will push the perimeter to the south pole.

                    force_magnitude = 
                        hemisphere_ratio
                        /
                        Math.Pow(displacement_between_centres.Length(),2)
                    ;

                    force = new Vector3(
                        (float)(displacement_between_centres.x*force_magnitude),
                        (float)(displacement_between_centres.y*force_magnitude),
                        (float)(displacement_between_centres.z*force_magnitude)
                    );

                    forces[child_index_outer] += force;
            }
        }

        // Apply forces & Reproject points on to sphere surface
        for (int child_index = 0; child_index < point_count; child_index++){
            child = root.GetChild(child_index) as MeshInstance;
            new_translation = child.GetTranslation()+forces[child_index];
            new_translation = new_translation.Normalized();
            new_translation = new Vector3(
                (float)(new_translation.x*radius),
                (float)(new_translation.y*radius),
                (float)(new_translation.z*radius)
            );
            child.SetTranslation(new_translation);
        }

        float stability = 0;

        foreach (Vector3 force_i in forces){
            stability += force_i.Length();
        }
        stability /= (point_count*point_count);
        stability_delta = Math.Abs(stability_last-stability);

        if (iteration % 100 == 0 || stability_delta == 0){
            GD.Print(
                "points:\t"         +point_count.ToString()+"\t"+
                "i:\t"              +iteration.ToString()+"\t"+
                "stability:\t"      +stability.ToString()+"\t"+
                "stability_delta:\t"+stability_delta.ToString()
            );
        }
        iteration += 1;
        stability_last = stability;
    }
    public void update_camera_position(Vector2 delta){
        Camera cam = GetNode("../Camera") as Camera;
        azimuth_and_inclination += delta;
        if(azimuth_and_inclination.x > Math.PI/2){
            azimuth_and_inclination.x = (float)(Math.PI/2);
        }
        if(azimuth_and_inclination.x < 0){
            azimuth_and_inclination.x = 0;
        }
        cam.SetTranslation(
            root_instance_for_accessing_root_methods.point_on_spherical_surface_using_inclination_and_azimuth(
                azimuth_and_inclination.x,
                azimuth_and_inclination.y,
                camera_radius
            )
        );
        cam.LookAt(new Vector3(0,0,0), new Vector3(0,1,0));
    }
    public override void _Input(InputEvent @event){
        if (@event is InputEventKey eventKey){
            if (eventKey.Pressed && eventKey.Scancode == (int)KeyList.Escape){
                GetTree().Quit();
            } else if (eventKey.Pressed && eventKey.Scancode == (int)KeyList.F5){
                GetTree().ReloadCurrentScene();
            } else if (eventKey.Pressed && eventKey.Scancode == (int)KeyList.Shift){
                add_points_to_hemisphere_by_random_selection_from_uniform_distribution_over_hemisphere_surface(points_added);
            } else if (eventKey.Pressed && eventKey.Scancode == (int)KeyList.Minus){
                adjust_spacing(GetNode("MeshInstances/"), cloud_radius);
            } else if (eventKey.Pressed && eventKey.Scancode == (int)KeyList.N){
                has_north_pole = !has_north_pole;
                adjust_spacing(GetNode("MeshInstances/"), cloud_radius);
            } else if (eventKey.Pressed && eventKey.Scancode == (int)KeyList.Z){
                update_camera_position(new Vector2(0.001f,0));
            }
        }
        if (@event is InputEventMouse iem){
            if(iem is InputEventMouseButton iem_b){
                // if (iem_b.GetButtonIndex() == (int)ButtonList.Left){
                // }
                // if (iem_b.GetButtonIndex() == (int)ButtonList.MaskLeft){
                // }
                // if (iem_b.GetButtonIndex() == (int)ButtonList.MaskMiddle){
                // }
                // if (iem_b.GetButtonIndex() == (int)ButtonList.MaskRight){
                // }
                if (iem_b.GetButtonIndex() == (int)ButtonList.Middle){
                    middle_mouse_button_pressed = iem_b.IsPressed();
                }
                // if (iem_b.GetButtonIndex() == (int)ButtonList.Right){
                // }
                if (iem_b.GetButtonIndex() == (int)ButtonList.WheelDown){
                    double camera_upper_limit_radius = Math.Sqrt(Math.Pow(7,2)*3);
                    camera_radius += 0.1;
                    if(camera_radius > camera_upper_limit_radius){
                        camera_radius = camera_upper_limit_radius;
                    }
                    update_camera_position(new Vector2(0,0));
                }
                // if (iem_b.GetButtonIndex() == (int)ButtonList.WheelLeft){
                // }
                // if (iem_b.GetButtonIndex() == (int)ButtonList.WheelRight){
                // }
                if (iem_b.GetButtonIndex() == (int)ButtonList.WheelUp){
                    double camera_lower_limit_radius = 0.5;
                    camera_radius -= 0.1;
                    if(camera_radius < camera_lower_limit_radius){
                        camera_radius = camera_lower_limit_radius;
                    }
                    update_camera_position(new Vector2(0,0));
                }
            }
            if(iem is InputEventMouseMotion iem_m){
                if(middle_mouse_button_pressed){
                    Vector2 relative_motion = iem_m.GetRelative();
                    update_camera_position(new Vector2(
                        relative_motion.y*0.001f,
                        relative_motion.x*0.001f
                    ));
                }
            }
        }
    }
    public override void _Process(float delta){
        // Called every frame. Delta is time since last frame.
        // Update game logic here.
        var glx = (Global)GetNode("/root/Global");
        if (glx.update_hemisphere_scene){
            clear_reading_nodes_from_hemisphere();
            plot_angle_sequence_as_arc_on_hemisphere(
                glx.readingAnglesSequence,
                rad_from_deg(glx.arc_inclination_degrees),
                rad_from_deg(glx.arc_azimuth_degrees),
                90
            );
            glx.update_hemisphere_scene = false;
        }

        switch (point_distribution_mode){
            case 0: // hemisphere_uniform_random_distribution
            break;
            case 1: // hemisphere_uniform_physics_distribution
                if(stability_delta > 0){
                    adjust_spacing(GetNode("MeshInstances/"), cloud_radius);
                }else{
                    // if(points_added <= 4096){
                    //     add_points(1);
                    // }
                }
            break;
            case 2: // single_arc
            break;
            default: GD.Print("Error:m invalid point_distribution_mode value"); break;
        }
    }
}
