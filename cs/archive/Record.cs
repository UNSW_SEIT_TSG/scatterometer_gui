using Godot;
using System;
public class Record{
    public float target_position;
    public float actual_position;
    public float reading;
    public bool reading_written;
    public Record(
        float target_position,
        float actual_position,
        float reading,
        bool  reading_written
    ){
        this.target_position    = target_position;
        this.actual_position    = actual_position;
        this.reading            = reading;
        this.reading_written    = reading_written;
    }
    public Record(
        float target_position
    ){
        this.target_position    = target_position;
        this.actual_position    = 0f;
        this.reading            = 0f;
        this.reading_written    = false;
    }
}