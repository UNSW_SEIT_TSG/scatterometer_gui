using Godot;
using System;

public class StateMachine: Node{

    int[] not_initialized_states = {0,1,2,3,4,5,6,7,8,9,50,63,66,67,71,72};
    int[] ready_states = {10,11,12,13,14,15,16,17,18,70,77};
    int[] error_states = {19,52,53,54,55,56,57,58,59,60,61,62,65};
    int[] disabled_states = {20,21,22,23,24,25,26,27,28,29,30,31,32,33,34,35,36,37,38,39,74,75,76};

    string[] other_state_strings = {
        "Emergency braking",                                                                // 40
        "Motor initialization state",                                                       // 41
        "Not referenced state",                                                             // 42
        "Homing state",                                                                     // 43
        "Moving state",                                                                     // 44
        "Trajectory state",                                                                 // 45
        "Slave state due to a SlaveEnable command",                                         // 46
        "Jogging state due to a JogEnable command",                                         // 47
        "Analog tracking state",                                                            // 48
        "Encoder calibrating state",                                                        // 49
        "Spinning state due to a SpinParametersSet command",                                // 51
        "Referencing state",                                                                // 64
        "Auto-tuning state",                                                                // 68
        "Scaling calibration state",                                                        // 69
        "Excitation signal generation state",                                               // 73
    };
    
    string[] stage_states = {
        "Not initialized state",                                                            // 00
        "Not initialized state due to an emergency brake : see positioner error",           // 01
        "Not initialized state due to an emergency stop : see positioner error",            // 02
        "Not initialized state due to a following error during homing",                     // 03
        "Not initialized state due to a following error",                                   // 04
        "Not initialized state due to an homing timeout",                                   // 05
        "Not initialized state due to a motion done timeout during homing",                 // 06
        "Not initialized state due to a GroupKill or KillAll command",                      // 07
        "Not initialized state due to an end of run after homing",                          // 08
        "Not initialized state due to an error during encoder calibration",                 // 09
        "Ready state due to an AbortMove command",                                          // 10
        "Ready state from homing",                                                          // 11
        "Ready state from motion",                                                          // 12
        "Ready State due to a MotionEnable command",                                        // 13
        "Ready state from slave",                                                           // 14
        "Ready state from jogging",                                                         // 15
        "Ready state from analog tracking",                                                 // 16
        "Ready state from trajectory",                                                      // 17
        "Ready state from spinning",                                                        // 18
        "Error : undefined status",                                                         // 19
        "Disabled state",                                                                   // 20
        "Disabled state due to a following error on ready state",                           // 21
        "Disabled state due to a following error during motion",                            // 22
        "Disabled state due to a motion done timeout during moving",                        // 23
        "Disable state due to a following error on slave state",                            // 24
        "Disable state due to a following error on jogging state",                          // 25
        "Disabled state due to a following error during trajectory",                        // 26
        "Disabled state due to a motion done timeout during trajectory",                    // 27
        "Disabled state due to a following error during analog tracking",                   // 28
        "Disabled state due to a master/slave error during motion",                         // 29
        "Disable state due to a master/slave error on slave state",                         // 30
        "Disable state due to a master/slave error on jogging state",                       // 31
        "Disabled state due to a master/slave error during trajectory",                     // 32
        "Disabled state due to a master/slave error during analog tracking",                // 33
        "Disabled state due to a master/slave error on ready state",                        // 34
        "Disable state due to a following error on spinning state",                         // 35
        "Disable state due to a master/slave error on spinning state",                      // 36
        "Disable state due to a following error on auto-tuning state",                      // 37
        "Disable state due to a master/slave error on auto-tuning state",                   // 38
        "Disable state due to an emergency stop on auto-tuning state",                      // 39
        "Emergency braking",                                                                // 40
        "Motor initialization state",                                                       // 41
        "Not referenced state",                                                             // 42
        "Homing state",                                                                     // 43
        "Moving state",                                                                     // 44
        "Trajectory state",                                                                 // 45
        "Slave state due to a SlaveEnable command",                                         // 46
        "Jogging state due to a JogEnable command",                                         // 47
        "Analog tracking state",                                                            // 48
        "Encoder calibrating state",                                                        // 49
        "Not initialized state due to a mechanical zero inconsistency during homing",       // 50
        "Spinning state due to a SpinParametersSet command",                                // 51
        "Error : undefined status",                                                         // 52
        "Error : undefined status",                                                         // 53
        "Error : undefined status",                                                         // 54
        "Error : undefined status",                                                         // 55
        "Error : undefined status",                                                         // 56
        "Error : undefined status",                                                         // 57
        "Error : undefined status",                                                         // 58
        "Error : undefined status",                                                         // 59
        "Error : undefined status",                                                         // 60
        "Error : undefined status",                                                         // 61
        "Error : undefined status",                                                         // 62
        "Not initialized state due to a motor initialization error",                        // 63
        "Referencing state",                                                                // 64
        "Error : undefined status",                                                         // 65
        "Not initialized state due to a perpendicularity error homing",                     // 66
        "Not initialized state due to a master/slave error during homing",                  // 67
        "Auto-tuning state",                                                                // 68
        "Scaling calibration state",                                                        // 69
        "Ready state from auto-tuning",                                                     // 70
        "Not initialized state from scaling calibration",                                   // 71
        "Not initialized state due to a scaling calibration error",                         // 72
        "Excitation signal generation state",                                               // 73
        "Disable state due to a following error on excitation signal generation state",     // 74
        "Disable state due to a master/slave error on excitation signal generation state",  // 75
        "Disable state due to an emergency stop on excitation signal generation state",     // 76
        "Ready state from excitation signal generation",                                    // 77
    };   
}