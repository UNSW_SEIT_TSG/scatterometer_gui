using Godot;
using System;

public class Node2D : Godot.Node2D
{
    private static Range SAHS;
    private static Ruut rz;
    private static Godot.Panel panel;

    // Member variables here, example:
    // private int a = 2;
    // private string b = "textvar";

    public override void _Ready()
    {
        // Called every time the node is added to the scene.
        // Initialization here

        Node node = GetTree().GetRoot();
        rz = node.GetChild(node.GetChildCount()-1) as Ruut;
        // GD.Print(rz.ToString());
        // GD.Print(rz.GetName());

        panel = rz.GetNode("Panel") as Godot.Panel;
        // GD.Print(panel.GetName());
    }

   public override void _Process(float delta)
   {
       // Called every frame. Delta is time since last frame.
       // Update game logic here.
       Update();
   }

    public override void _Draw(){
        Godot.Vector2 centre_point = new Godot.Vector2(640/2, 500/2);
        DrawLine(new Godot.Vector2(centre_point.x, 0), centre_point, new Godot.Color(0.0f,1,0.0f), 1); // Draw beam from light source

        Godot.Range SAHS = panel.GetNode("./VBoxContainerMain/VBoxContainer/HBoxContainer/VBoxContainerControls/VBoxContainer/VBoxContainerStartAngle/HSliderStartAngle") as Godot.Range;
        Godot.Range FAHS = panel.GetNode("./VBoxContainerMain/VBoxContainer/HBoxContainer/VBoxContainerControls/VBoxContainer/VBoxContainerFinalAngle/HSliderFinalAngle") as Godot.Range; 

        draw_arc_from_sequence(
            new Godot.Color(0.5f,0.5f,0.5f)
        );

        // draw_arc(
        //     centre_point,
        //     SAHS.GetValue(),
        //     FAHS.GetValue(),
        //     new Godot.Color(0.5f,0.5f,0.5f),
        //     centre_point.y*0.8f
        // );

        
    }
    private void draw_arc_from_sequence(
        Godot.Color colour,
        float radius=100,
        int thickness=8,
        float phase_offset_degrees=-90.0f // Used to define a particular angle.
    ){
        float arc_start_angle_degrees=0.0f;
        float arc_final_angle_degrees=0.0f;
        float arc_length_degrees=0.0f;

        Godot.Vector2 arc_centre_screen_pixels = new Godot.Vector2(640/2, 500/2);
        var glx = (Global)GetNode("/root/Global");

        if(glx.readingAnglesSequenceLength>0){
            arc_start_angle_degrees = glx.readingAnglesSequence[0]+phase_offset_degrees;
            arc_final_angle_degrees = glx.readingAnglesSequence[glx.readingAnglesSequenceLength-1]+phase_offset_degrees;
            arc_length_degrees = arc_final_angle_degrees-arc_start_angle_degrees;

            int counter = 0;
            Godot.Color inner_circle_colour = new Godot.Color(0,0,0);

            foreach (float angle in glx.readingAnglesSequence)
            {
                Godot.Vector2 marker_centre = point_from_start_point_heading_radius(
                    arc_centre_screen_pixels,
                    radiansFromDegrees((angle+phase_offset_degrees)), 
                    radius
                );

                DrawCircle(marker_centre,3,new Godot.Color(0,0,0));
                if (counter < glx.readingAnglesSequenceMarker){
                    inner_circle_colour = new Godot.Color(0.25f,1,0.25f);
                }else if (glx.readingAnglesSequenceMarker == counter){
                    inner_circle_colour = new Godot.Color(0.25f,0.25f,1);
                }else{
                    inner_circle_colour = new Godot.Color(1,0.25f,0.25f);
                }
                DrawCircle(marker_centre,2,inner_circle_colour);

                counter += 1;
            }
        }
        
        // Render Sample as box with border
            int width  = 60;
            int height = 8;
            DrawRect(
                new Rect2(
                    new Vector2(arc_centre_screen_pixels.x-width/2,arc_centre_screen_pixels.y-height/2),  // sample_rectangle_position,
                    new Vector2(width,height)   // sample_rectangle_size
                ),
                new Godot.Color(0,0,0)
            );
            DrawRect(
                new Rect2(
                    new Vector2(arc_centre_screen_pixels.x-width/2+1,arc_centre_screen_pixels.y-height/2+1),  // sample_rectangle_position,
                    new Vector2(width-2,height-2)   // sample_rectangle_size
                ),
                new Godot.Color(1,1,1)
            );
            
            DrawCircle(arc_centre_screen_pixels, 2, new Godot.Color(0,0,0));
            DrawCircle(arc_centre_screen_pixels, 1, new Godot.Color(0.5f,1,0.5f));        
    }
    private float radiansFromDegrees(float angle_in_degrees){
        return angle_in_degrees * (float)(Math.PI)/180.0f;
    }
    private float float_polarity(float number){
        return (number>0)?1:-1;
    }
    private void draw_arc(
        Godot.Vector2 arc_centre_screen_pixels,
        float arc_start_angle_degrees,
        float arc_final_angle_degrees,
        Godot.Color colour,
        float radius=100,
        int thickness=8,
        float phase_offset_degrees=-90.0f // Used to define a particular angle.
    ){  
        float arc_length_degrees = arc_final_angle_degrees-arc_start_angle_degrees;
        arc_start_angle_degrees += phase_offset_degrees;
        arc_final_angle_degrees += phase_offset_degrees;
        

        float heading_current_degrees = arc_start_angle_degrees;
        float heading_step_degrees    = 5f*float_polarity(arc_length_degrees);
        bool  condition               = true;

        Godot.Vector2 next_point, prev_point, temp_vector;
        
        next_point = new Godot.Vector2(point_from_start_point_heading_radius(arc_centre_screen_pixels, radiansFromDegrees(heading_current_degrees), radius));
        temp_vector = new Godot.Vector2(next_point-arc_centre_screen_pixels);
        heading_current_degrees += heading_step_degrees;

        while (condition){
            prev_point = new Godot.Vector2(next_point);
            heading_current_degrees += heading_step_degrees;
            
            condition = (arc_length_degrees>0)
                ? heading_current_degrees < arc_final_angle_degrees
                : heading_current_degrees > arc_final_angle_degrees
            ;
            
            if(!condition){
                heading_current_degrees = arc_final_angle_degrees;
            }
            next_point = new Godot.Vector2(point_from_start_point_heading_radius(arc_centre_screen_pixels, radiansFromDegrees(heading_current_degrees), radius));
            
            DrawLine(prev_point, next_point, new Godot.Color(0,0,0), thickness);
            DrawLine(prev_point, next_point, colour, thickness-2);
        }
        
        int width  = 60;
        int height = 8;
        DrawRect(
            new Rect2(
                new Vector2(arc_centre_screen_pixels.x-width/2,arc_centre_screen_pixels.y-height/2),  // sample_rectangle_position,
                new Vector2(width,height)   // sample_rectangle_size
            ),
            new Godot.Color(0,0,0)
        );
        DrawRect(
            new Rect2(
                new Vector2(arc_centre_screen_pixels.x-width/2+1,arc_centre_screen_pixels.y-height/2+1),  // sample_rectangle_position,
                new Vector2(width-2,height-2)   // sample_rectangle_size
            ),
            new Godot.Color(1,1,1)
        );
        
        DrawCircle(arc_centre_screen_pixels, 2, new Godot.Color(0,0,0));
        DrawCircle(arc_centre_screen_pixels, 1, new Godot.Color(0.5f,1,0.5f));

        Godot.Range IAHS = panel.GetNode("./VBoxContainerMain/VBoxContainer/HBoxContainer/VBoxContainerControls/VBoxContainer/VBoxContainerIntervalControl/HSliderIntervalCount") as Godot.Range;
        float interval_count = IAHS.GetValue();
        float interval_arc_degrees = (arc_length_degrees)/interval_count;
        float reading_count = interval_count+1;

        for (int z = 0; z < reading_count; z++)
        {
            Godot.Vector2 marker_centre = point_from_start_point_heading_radius(
                arc_centre_screen_pixels,
                radiansFromDegrees(arc_start_angle_degrees+z*interval_arc_degrees), 
                radius
            );
            DrawCircle(marker_centre,3,new Godot.Color(0,0,0));
            DrawCircle(marker_centre,2,new Godot.Color(1,0.25f,0.25f));
        }
    }
    private Godot.Vector2 point_from_start_point_heading_radius(Vector2 start_point, float heading_radians, float radius){
        return new Godot.Vector2(
            (float) (start_point.x+radius*Math.Cos(heading_radians)), 
            (float) (start_point.y+radius*Math.Sin(heading_radians))
        );
    }
    private void drawRandomlyLocatedCircles(float leftBound, float rightBound, float lowerBound, float upperBound){
        Random stoch = new Random();
        Godot.Vector2 circle_centre;
        
        for (int z = 0; z < 1000; z++)
        {
            circle_centre = new Godot.Vector2(leftBound + stoch.Next() % (int)(rightBound-leftBound), lowerBound + stoch.Next() % (int)(upperBound-lowerBound));
            DrawCircle(circle_centre, 2, new Godot.Color(0.5f,0.5f,1));    
        }
    }
}