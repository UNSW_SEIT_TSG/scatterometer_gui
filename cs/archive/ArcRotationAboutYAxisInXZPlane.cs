using Godot;
using System;

public class ArcRotationAboutYAxisInXZPlane : Node2D
{
    // Member variables here, example:
    // private int a = 2;
    // private string b = "textvar";
    // Godot.Vector2 centre_of_viewport = new Godot.Vector2(958/2, 462/2);
    Godot.Vector2 centre_of_viewport = new Godot.Vector2(0, 0);
    float datum_offset_degrees = -90.0f;
    bool  clockwise_is_positive = false;
    Godot.Label sample_x_axis_label;
    Godot.Label sample_z_axis_label;
    private int polar_int_from_bool(bool b){
        return
            b
            ? 1
            : -1
        ;
    }
    public override void _Ready()
    {
        // Called every time the node is added to the scene.
        // Initialization here
        Camera2D cam    = GetNode("Camera2D") as Camera2D;
        Sprite   sprite = GetNode("Sprite") as Sprite;
        // sprite.SetPosition(centre_of_viewport);
        sprite.SetVisible(false);
        cam.SetPosition(centre_of_viewport);
        sample_x_axis_label = GetNode("Control/sample_x_axis_label") as Godot.Label;
        sample_z_axis_label = GetNode("Control/sample_z_axis_label") as Godot.Label;
    }
    public override void _Process(float delta)
    {
        // Called every frame. Delta is time since last frame.
        // Update game logic here.
        Update();
    }
    private double radians_from_degrees(double angle_in_degrees){
        return angle_in_degrees*Math.PI/180.0f;
    }
    private Vector2 x_y_vector_from_angle_radius_and_centre(Vector2 centre, float radius, float angle_degrees){
        return
            new Vector2(
                (float)(centre.x+radius*Math.Cos(radians_from_degrees(angle_degrees))),
                (float)(centre.y+radius*Math.Sin(radians_from_degrees(angle_degrees)))
            )
        ;
    }
    
    public override void _Draw(){
        Glx glx = GetNode("/root/Global") as Glx;
        
        sample_x_axis_label.SetPosition(new Vector2((float)(centre_of_viewport.x-462*0.99*0.5),centre_of_viewport.y-20));
        sample_z_axis_label.SetPosition(new Vector2(centre_of_viewport.x+5,(float)(centre_of_viewport.y-462*0.75*0.5)));

        float offset = -90.0f;
        float width = 2.0f;
        float shadow_width = width+2.0f;

        float square_edge = (float)(462*0.9/Math.Sqrt(2));

        float inclination_angle_offset = 90.0f;
        float inclination_degrees = glx.get_arc_inclination_angle_degrees()+inclination_angle_offset;

        // Draw square representing sample plane
        DrawRect(
            new Rect2(
                position:new Vector2(centre_of_viewport.x-square_edge/2, centre_of_viewport.y-square_edge/2),
                size:new Vector2(square_edge,square_edge)
            ),
            new Godot.Color(0.8f,0.8f,0.8f),
            filled:true
        );

        // x axis lines
            // x axis shadow
            this.DrawLine(
                centre_of_viewport,
                new Vector2((float)(centre_of_viewport.x-462*0.95*0.5),centre_of_viewport.y),
                glx.black,
                shadow_width
            );

            // x axis line
            this.DrawLine(
                centre_of_viewport,
                new Vector2((float)(centre_of_viewport.x-462*0.95*0.5),centre_of_viewport.y),
                glx.sample_x_colour,
                width
            );

        // z axis lines
            // z axis shadow
            this.DrawLine(
                centre_of_viewport,
                new Vector2(
                    centre_of_viewport.x,
                    (float)(centre_of_viewport.y-462*0.95*0.5)
                ),
                glx.black,
                shadow_width
            );

            // z axis line
            this.DrawLine(
                centre_of_viewport,
                new Vector2(
                    centre_of_viewport.x,
                    (float)(centre_of_viewport.y-462*0.95*0.5)
                ),
                glx.sample_z_colour,
                width
            );

        // arc on sample plane before inclination applied
        glx.draw_arc_line(
            context:this,
            datum_offset_degrees:datum_offset_degrees+glx.get_arc_rotation_about_y_axis_in_x_z_plane_angle_degrees(),
            clockwise_is_positive:clockwise_is_positive,
            centre:centre_of_viewport,
            start_angle_degrees:offset,
            final_angle_degrees:180+offset,
            radius:(float)(462*0.9*0.5),
            width:2,
            color_a:glx.rotation_about_y_axis_in_x_z_plane_halfturn_vector_colour,
            color_b:glx.rotation_about_y_axis_in_x_z_plane_zeroturn_vector_colour,
            color_c:glx.black
        );

        DrawLine(
            from:centre_of_viewport,
            to:glx.x_y_vector_from_angle_radius_and_centre(
                centre:centre_of_viewport,
                radius:(float)(462*0.9*0.5),
                angle_degrees:(datum_offset_degrees+glx.get_arc_rotation_about_y_axis_in_x_z_plane_angle_degrees())*-1.0f
            ),
            color:glx.black,
            width:shadow_width
        );

        DrawLine(
            from:centre_of_viewport,
            to:glx.x_y_vector_from_angle_radius_and_centre(
                centre:centre_of_viewport,
                radius:(float)(462*0.9*0.5*Math.Cos(glx.radians_from_degrees(inclination_degrees))),
                angle_degrees:(datum_offset_degrees+glx.get_arc_rotation_about_y_axis_in_x_z_plane_angle_degrees())*-1.0f
            ),
            color:glx.black,
            width:shadow_width
        );

        DrawLine(
            from:glx.x_y_vector_from_angle_radius_and_centre(
                centre:centre_of_viewport,
                radius:(float)(462*0.9*0.5*Math.Cos(glx.radians_from_degrees(inclination_degrees))),
                angle_degrees:(datum_offset_degrees+glx.get_arc_rotation_about_y_axis_in_x_z_plane_angle_degrees())*-1.0f
            ),
            to:glx.x_y_vector_from_angle_radius_and_centre(
                centre:centre_of_viewport,
                radius:(float)(462*0.9*0.5),
                angle_degrees:(datum_offset_degrees+glx.get_arc_rotation_about_y_axis_in_x_z_plane_angle_degrees())*-1.0f
            ),
            color:glx.inclination_arc_colour,
            width:width
        );

        DrawLine(
            from:centre_of_viewport,
            to:glx.x_y_vector_from_angle_radius_and_centre(
                centre:centre_of_viewport,
                radius:(float)(462*0.9*0.5*Math.Cos(glx.radians_from_degrees(inclination_degrees))),
                angle_degrees:(datum_offset_degrees+glx.get_arc_rotation_about_y_axis_in_x_z_plane_angle_degrees())*-1.0f
            ),
            color:glx.inclination_vector_colour,
            width:width
        );

        // glx.draw_arc_line(
        glx.draw_elliptical_arc(
            context:this,
            datum_offset_degrees:datum_offset_degrees+glx.get_arc_rotation_about_y_axis_in_x_z_plane_angle_degrees(),
            clockwise_is_positive:clockwise_is_positive,
            centre:centre_of_viewport,
            start_angle_degrees:glx.get_arc_start_angle_degrees()+offset,
            final_angle_degrees:glx.get_arc_final_angle_degrees()+offset,
            radius:(float)(462*0.9*0.5),
            width:2,
            draw_final_angle_line:true,
            draw_start_angle_line:true,
            color_a:glx.start_angle_vector_colour,
            color_b:glx.final_angle_vector_colour,
            color_c:glx.reading_arc_colour,
            semi_major:(float)(Math.Cos(glx.radians_from_degrees(inclination_degrees)))
        );

        DrawCircle(
            position:centre_of_viewport,
            radius:4,
            color:glx.black
        );
        DrawCircle(
            position:centre_of_viewport,
            radius:2,
            color:glx.sample_y_colour
        );
    }
}
