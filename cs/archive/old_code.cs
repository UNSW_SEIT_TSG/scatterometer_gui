using Godot;
using System;
using System.IO;
using System.Text;
using System.Net;
using System.Net.Sockets;
using System.Collections.Generic;
using System.Globalization;

public class Ruut : Node{    
    private static int                          wait_time;
    private static bool                         wait_for_measurement=false;
    private static Table                        results_table=new Table();
    private static Log                          comms_log, results_log;
    private static bool                         waitForMoveToNextAngleClick;    
    private static DateTime                     rebootStartTime;
    private static Timer                        reboot_wait_timer=new Timer();
    private static Timer                        estimate_update_timer=new Timer();
    private static Timer                        heartbeat_timer=new Timer();
    private static float                        current_position;
    private static Range                        SAHS;
    private static Range                        FAHS;
    private static Range                        IAHS;
    Glx glx;
    public override void _Ready(){
        glx=GetNode("/root/Global") as Glx;
        comms_log  =new Log(
            "comms",
            GetNode(
                "./Panel/VBoxContainerMain/VBoxContainer/HBoxContainer/VBoxContainerControls/VBoxContainer2/VBoxContainerLabelLogComms/VBoxContainerLabelLogComms/TextEdit3"
            ) as Godot.TextEdit,
            true
        );
        results_log=new Log(
            "results",
            GetNode(
                // "./Panel/VBoxContainerMain/VBoxContainer/HBoxContainer/VBoxContainerControls/VBoxContainer2/VBoxContainerLogResults/TextEdit2"
                "./Panel/VBoxContainerMain/VBoxContainer/HBoxContainer/VBoxContainerLogResults/TextEdit2"
            ) as Godot.TextEdit,
            false
        );
        SAHS=(Range)GetNode("./Panel/VBoxContainerMain/VBoxContainer/HBoxContainer/VBoxContainerControls/VBoxContainer/VBoxContainerStartAngle/HSliderStartAngle");
        FAHS=(Range)GetNode("./Panel/VBoxContainerMain/VBoxContainer/HBoxContainer/VBoxContainerControls/VBoxContainer/VBoxContainerFinalAngle/HSliderFinalAngle");
        IAHS=(Range)GetNode("./Panel/VBoxContainerMain/VBoxContainer/HBoxContainer/VBoxContainerControls/VBoxContainer/VBoxContainerIntervalControl/HSliderIntervalCount");
        GD.Print(OS.GetScreenCount());
        OS.SetWindowPosition(
            (
                OS.GetScreenSize(
                    OS.GetCurrentScreen()
                ) - 
                OS.GetWindowSize()
            )/2
        );

        loadDefaultsFromFile();
        waitForMoveToNextAngleClick=bool.Parse(defaultsDictionary["waitForMoveToNextAngleClick"]);

        comms_log.writePrefixAndMessageToLogWithTimestamp("Application started", "");
        calcIntervalArcFromStartFinalAnglesAndIntervalCount();
        GetNode("./Panel/VBoxContainerMain/VBoxContainer/HBoxContainer/VBoxContainerControls/VBoxContainer/VBoxContainerStartAngle/HSliderStartAngle")
            .Set("value",float.Parse(defaultsDictionary["defaultStartAngle"]))
        ;
        GetNode("./Panel/VBoxContainerMain/VBoxContainer/HBoxContainer/VBoxContainerControls/VBoxContainer/VBoxContainerFinalAngle/HSliderFinalAngle")
            .Set("value",float.Parse(defaultsDictionary["defaultFinalAngle"]))
        ;
        _on_HSliderIntervalCount_value_changed(Int32.Parse(defaultsDictionary["defaultIntervalCount"]));
    
        attemptConnection();

        // Setup reboot wait delay timer for later.
        reboot_wait_timer.SetOneShot(true);
        reboot_wait_timer.Connect("timeout", GetNode("."), "attemptConnection");
        wait_time=String.Equals(
                defaultsDictionary["localControllerIP"],
                defaultsDictionary["controllerIPRaw"]
            )
            ? 5
            : 52
        ;
        reboot_wait_timer.SetWaitTime(wait_time);
        AddChild(reboot_wait_timer);

        estimate_update_timer.SetOneShot(false);
        estimate_update_timer.Connect("timeout", GetNode("."), "makeEstimateUpdate");
        estimate_update_timer.SetWaitTime(1);
        AddChild(estimate_update_timer);

        // Setup Heartbeat Timer
        heartbeat_timer.SetOneShot(false);
        heartbeat_timer.Connect("timeout", GetNode("."), "heartbeatCheckConnection");
        heartbeat_timer.SetWaitTime(10);
        AddChild(heartbeat_timer);

        // _on_Button7_pressed();
    }

    // Layout Based Code
        // Title and Top Bar
            private void _on_ButtonMinimise_pressed(){
                Glx glx=GetNode("/root/Global") as Glx;
                OS.SetWindowMinimized(true);
            }

        // Offset Calibration
            // Slider
                private void    _on_HSliderOffsetAngle_value_changed(float value){
                    Glx glx=GetNode("/root/Global") as Glx;
                    GD.Print("_on_HSliderOffsetAngle_value_changed.  Not yet implemented.");
                }
            // Angle Too Small, "Too <"
                private void    _on_ButtonOffsetSearchTooSmall_pressed(){
                    Glx glx=GetNode("/root/Global") as Glx;
                    comms_log.writePrefixAndMessageToLogWithTimestamp("GUI to Log", "button_calibration_too_small_pressed()");
                }
            // Reset Search
                private void    _on_ButtonOffsetSearchReset_pressed(){
                    Glx glx=GetNode("/root/Global") as Glx;
                    comms_log.writePrefixAndMessageToLogWithTimestamp("GUI to Log", "button_calibration_reset_pressed()");
                }
            // Angle Too Large, "Too >"
                private void    _on_ButtonOffsetSearchTooBig_pressed(){
                    Glx glx=GetNode("/root/Global") as Glx;
                    comms_log.writePrefixAndMessageToLogWithTimestamp("GUI to Log", "button_calibration_too_big_pressed()");
                }
            // Offset Angle Textedit display

        // Start Angle
            // Slider
                private void    _on_HSliderStartAngle_value_changed(float value){
                    Glx glx=GetNode("/root/Global") as Glx;
                    setIntervalArcValue(calcIntervalArcFromStartFinalAnglesAndIntervalCount());

                    GetNode("Panel/VBoxContainerMain/VBoxContainer/HBoxContainer/VBoxContainerControls/VBoxContainer/VBoxContainerStartAngle/HBoxContainer/TextEdit")
                        .Set("text","Start Angle: " + value.ToString() + "°;")
                    ;
                }
            // Move Sensor Arm to Start Angle Position
                private void    _on_ButtonStartAngleMoveTo_pressed(){
                    Glx glx=GetNode("/root/Global") as Glx;
                    updateCurrentPosition(group_move_absolute("GROUP1",SAHS.GetValue()));
                    comms_log.writePrefixAndMessageToLogWithTimestamp("current position: ", current_position.ToString());
                }
            // Start Angle Textedit display

        // Final Angle
            // Slider
                private void    _on_HSliderFinalAngle_value_changed(float value){
                    Glx glx=GetNode("/root/Global") as Glx;
                    setIntervalArcValue(calcIntervalArcFromStartFinalAnglesAndIntervalCount());
                    GetNode("Panel/VBoxContainerMain/VBoxContainer/HBoxContainer/VBoxContainerControls/VBoxContainer/VBoxContainerFinalAngle/HBoxContainerFinalAngle/TextEdit")
                        .Set("text","Final Angle: " + value.ToString() + "°;")
                    ;
                }
            // // Move Sensor Arm to Final Angle Position
                private void    _on_ButtonFinalAngleMoveTo_pressed(){
                    Glx glx=GetNode("/root/Global") as Glx;
                    updateCurrentPosition(group_move_absolute("GROUP1",FAHS.GetValue()));
                    comms_log.writePrefixAndMessageToLogWithTimestamp("current position: ", current_position.ToString());
                }
            // Final Angle Textedit display

        // Interval Control
            // Slider
                private void    _on_HSliderIntervalCount_value_changed(float value){
                    Glx glx=GetNode("/root/Global") as Glx;
                    setIntervalArcValue(calcIntervalArcFromStartFinalAnglesAndIntervalCount());
                    GetNode("Panel/VBoxContainerMain/VBoxContainer/HBoxContainer/VBoxContainerControls/VBoxContainer/VBoxContainerIntervalControl/TextEdit")
                        .Set("text","Interval count: " + value.ToString() + ";\nReading count: "+(value+1).ToString()+";");
                    ;
                }
            // Move Sensor Arm to Final Angle Position
            // Final Angle Textedit display

        // Motion Sequence
            private void    _on_ButtonGenerateSequence_pressed(){
                Glx glx=GetNode("/root/Global") as Glx;
                
                comms_log.writePrefixAndMessageToLogWithTimestamp("GUI to Log", "button_sequence_generate_pressed()");
                
                loadTargetPositionSequenceToResultsTable();
                updateGUITextBoxWithAnglesInIntervalList(glx.reading_sequence_arc_angle_degrees);
                results_table.startTime=DateTime.Now;
                
                glx.set_current_reading_index(-1);
                glx.set_value_changed(true);
            }
            private void loadTargetPositionSequenceToResultsTable(){
                Glx glx=GetNode("/root/Global") as Glx;
                results_table.removeAllRecords();
                foreach (var target in glx.reading_sequence_arc_angle_degrees)
                {
                    Record empty_record=new Record(target, 0, 0, false);
                    results_table.addRecord(empty_record);
                    
                }
                results_table.printTable(results_log,false);
            }
            private void    _on_ButtonExecuteNextInSequence_pressed(){
                Glx glx=GetNode("/root/Global") as Glx;
                if(wait_for_measurement){
                    comms_log.writePrefixAndMessageToLogWithTimestamp("GUI to Log", "Cannot proceed as observation has not been entered for this angle.");
                }else{
                    if(glx.get_current_reading_index()<glx.get_reading_count()){
                        wait_for_measurement=true;
                        glx.increment_current_reading_index();
                        updateCurrentPosition(group_move_absolute("GROUP1",glx.reading_sequence_arc_angle_degrees[glx.get_current_reading_index()]));
                        results_table.printTable(results_log, false);
                        // results_log.writeToLog(glx.reading_sequence_arc_angle_degrees[glx.get_current_reading_index()].ToString());
                    }
                    if(glx.get_current_reading_index()==glx.get_reading_count()){
                        GD.Print("Sequence complete (A)");
                    }
                }

            }
            private void    _on_ButtonImportSequence_pressed(){
                Glx glx=GetNode("/root/Global") as Glx;
                comms_log.writePrefixAndMessageToLogWithTimestamp("GUI to Log", "button_sequence_import_pressed()");
                string text=System.IO.File.ReadAllText(".//import.txt");

                results_table.startTime=DateTime.Now;
                loadTargetPositionSequenceToResultsTable();
                updateGUITextBoxWithAnglesInIntervalList(glx.reading_sequence_arc_angle_degrees);

                glx.set_current_reading_index(-1);
                glx.set_value_changed(true);
            }
        // Results Log
            private void    _on_ButtonAddReading_pressed(){
                Glx glx=GetNode("/root/Global") as Glx;
                Godot.TextEdit text_edit=GetNode("./Panel/VBoxContainerMain/VBoxContainer/HBoxContainer/VBoxContainerLogResults/VBoxContainerLogResults/GridContainer/TextEdit2") as Godot.TextEdit;
                GD.Print(text_edit.GetText());
                results_table.enterObservation(glx.get_current_reading_index(), float.Parse(text_edit.GetText()));
                results_table.printTable(results_log, false);
                wait_for_measurement=false;
                results_table.finalTime=DateTime.Now;
                text_edit.SetText("");
            }
            private void    _on_ButtonResultsLogClear_pressed(){
                Glx glx=GetNode("/root/Global") as Glx;
                comms_log.writePrefixAndMessageToLogWithTimestamp("GUI to Log", "_on_ButtonResultsLogClear_pressed()");
            }
            private void    _on_ButtonSaveResults_pressed(){
                Glx glx=GetNode("/root/Global") as Glx;
                comms_log.writePrefixAndMessageToLogWithTimestamp("GUI to Log", "_on_ButtonSaveResults_pressed()");
                results_table.printTable(results_log, true);
            }
        // Comms Log
            private void _on_ButtonCommsLogClear_pressed(){
                Glx glx=GetNode("/root/Global") as Glx;
                comms_log.writePrefixAndMessageToLogWithTimestamp("GUI to Log", "_on_ButtonCommsLogClear_pressed()");
            }

        // Bottom Toolbar
            private void    _on_ButtonControllerReboot_pressed(){
                Glx glx=GetNode("/root/Global") as Glx;
                GD.Print("_on_ButtonControllerReboot_pressed.   Implemented, but not tested.");
                Node root=GetNode(".");
                SendLoop(root, "Reboot()",false);
                restoreConnection();
            }
            private string  _on_ButtonControllerStatusGet_pressed(bool quiet){
                return SendLoop(GetNode("."), "ControllerStatusGet(int *)", quiet);
            }
            private void    _on_ButtonSensorArmInitialise_pressed(){
                Glx glx=GetNode("/root/Global") as Glx;
                GD.Print("_on_ButtonSensorArmInitialise_pressed.  Implemented, not yet tested.");
                SendLoop(GetNode("."), "GroupInitialize(GROUP1)", false);
            }
            private void    _on_ButtonSensorArmHome_pressed(){
                Glx glx=GetNode("/root/Global") as Glx;
                GD.Print("_on_ButtonSensorArmHome_pressed.  Implemented, not yet tested.");
                SendLoop(GetNode("."), "GroupHomeSearch(GROUP1)", false);
            }
            private void    _on_ButtonTestWriteToFile_pressed(){
                Glx glx=GetNode("/root/Global") as Glx;
                GD.Print("_on_ButtonTestWriteToFile_pressed()");
                string[] example_strings={
                    "beep",
                    "boom",
                    "clang"
                };
                string file_path=".\\logs\\" + "test.log";
                System.IO.File.WriteAllLines(@file_path, example_strings);
            }

            private void _on_ButtonAppendTextToExistingFile_pressed(){
                Glx glx=GetNode("/root/Global") as Glx;
                Random rng=new Random();
                // string file_path=".\\logs\\" + "test.log";
                const string AllowedChars="0123456789ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz#@$^*()";
                foreach (var r_string in RandomStrings(AllowedChars, 4, 4, 1, rng)){
                    GD.Print(r_string);
                    // appendTextToFile(file_path, r_string);
                }
            }

    // Non-layout based Code

    // Un-categorised Code
        
        public Vector3 point_on_spherical_surface_using_inclination_and_azimuth(double inclination, double azimuth, double radius=1){
            return new Vector3(
                (float)(radius*Math.Cos(azimuth)*Math.Cos(inclination)), /*(float)(radius * Math.Sin(theta) * Math.Cos(phi)),*/
                (float)(radius*                1*Math.Sin(inclination)), /*(float)(radius * Math.Sin(inclination))*//*(float)(radius * Math.Sin(theta) * Math.Sin(phi)),*/
                (float)(radius*Math.Sin(azimuth)*Math.Cos(inclination))
            );
        }

        private Vector3 random_point_on_spherical_surface_using_inclination_and_azimuth(Random random, double radius){
            double single_turn_in_radians=2.00*Math.PI;
            double turn=single_turn_in_radians;
            double u=random.NextDouble();
            double v=random.NextDouble();

            double
                inclination =u*turn*0.5,
                azimuth     =Math.Acos(2*v-1)
            ;
            return point_on_spherical_surface_using_inclination_and_azimuth(inclination, azimuth);
        }
        private void _inspect_node(Node node){
            Glx glx=GetNode("/root/Global") as Glx;
                int childrenCount=node.GetChildCount();
                for (int z=0; z < childrenCount; z++)
                {   
                    Node child=node.GetChild(z);
                    GD.Print(
                        z.ToString()+"\t"+
                        child.GetName()+"\t"+
                        child.GetChildCount()
                    );
                }
        }
        private static IEnumerable<string> RandomStrings(
            string allowedChars,
            int minLength,
            int maxLength,
            int count,
            Random rng
        ){
            char[] chars=new char[maxLength];
            int setLength=allowedChars.Length;

            while (count-- > 0)
            {
                int length=rng.Next(minLength, maxLength + 1);

                for (int i=0; i < length; ++i)
                {
                    chars[i]=allowedChars[rng.Next(setLength)];
                }

                yield return new string(chars, 0, length);
            }
        }
        private void updateCurrentPosition(float current_position){
            Glx glx=GetNode("/root/Global") as Glx;
            Godot.TextEdit currentPositionTextBox=GetNode("./Panel/VBoxContainerMain/VBoxContainer/HBoxContainer/VBoxContainerLogResults/VBoxContainerLogResults/GridContainer/TextEdit") as Godot.TextEdit;
            results_table.enterActualAngle(glx.get_current_reading_index(),current_position);
            currentPositionTextBox.SetText((current_position.ToString()));
        }
        private void updateGUITextBoxWithAnglesInIntervalList(List<float> float_list){
            Glx glx=GetNode("/root/Global") as Glx;
            glx.reading_sequence_arc_angle_degrees=float_list;
            
            string readingAngleSequenceAsString=glx.convertListOfFloatsToCommaSeparatedString(
                glx.reading_sequence_arc_angle_degrees
            );

            writeToIntervalList(GetNode("."),readingAngleSequenceAsString);
            comms_log.writePrefixAndMessageToLogWithTimestamp(
                "GUI to Log: Generated reading angle sequence", 
                readingAngleSequenceAsString
            );
            glx.set_reading_count(float_list.Count);
        }
        private void writeToIntervalList(Node root, string message){
            Glx glx=GetNode("/root/Global") as Glx;
            TextEdit z=GetNode(
                "Panel/VBoxContainerMain/VBoxContainer/HBoxContainer/VBoxContainerControls/VBoxContainer/VBoxContainerMotionSequence/VBoxContainerMotionSequence/TextEdit"
                ) as TextEdit
            ;
            z.SetText("Angles at which the scatterometer will stop to allow for data collection:\n"+message);
        }   
        
        
        

        private void makeEstimateUpdate(){
            Glx glx=GetNode("/root/Global") as Glx;
            double result=DateTime.Now.Subtract(rebootStartTime).TotalSeconds;
            comms_log.writePrefixAndMessageToLogWithTimestamp("Estimated time til reconnection", (wait_time-(int)result).ToString());
        }
        private void heartbeatCheckConnection(){
            Glx glx=GetNode("/root/Global") as Glx;
            try
            {
                if(attemptHandshake(true)){
                    GD.Print("Heartbeat OK.");
                }else{
                    GD.Print("Heartbeat problem.");
                    heartbeat_timer.Stop();
                    restoreConnection();
                }    
            }
            catch (SocketException)
            {
                heartbeat_timer.Stop();
                GD.Print("Heartbeat problem.");
                restoreConnection();
            }
        }
}