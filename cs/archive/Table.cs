using Godot;
using System;
using System.Collections.Generic;
using System.Globalization;
public class Table{
    List<Record> table = new List<Record>();
    public DateTime startTime;
    public DateTime finalTime;
    public Table(){
        GD.Print("Table instantiated.");
    }
    public void addRecord(Record record){
        this.table.Add(record);
    }
    public void enterActualAngle(int index, float actual_angle){
        if(index >= 0){
            table[index].actual_position = actual_angle;
        }
    }
    public void enterObservation(int index, float observed_value){
        if(index >= 0){
            table[index].reading = observed_value;
            table[index].reading_written = true;
        }
    }
    public void printTable(Log log, bool writeToFile){
        log.clearLog();
        if(writeToFile){
            string header_string = "   Target |    Actual |  Measured ";
            log.appendTextToFile("Experiment Started at: "+this.startTime.ToLongDateString()+" "+this.startTime.ToLongTimeString());
            log.appendTextToFile("Experiment Ended at: "+this.finalTime.ToLongDateString()+" "+this.finalTime.ToLongTimeString());
            log.appendTextToFile(header_string);
        }
        CultureInfo ci = new CultureInfo("en-au");
        string printable_record;
        foreach (var record in table)
        {
            if (record.reading_written){
                printable_record = 
                    record.target_position.ToString("F04", ci).PadLeft(9,' ')+" | "+
                    record.actual_position.ToString("F04", ci).PadLeft(9,' ')+" | "+
                    record.reading.ToString("F04", ci).PadLeft(9,' ')
                ;
            }else{
                if(record.actual_position != 0f){
                    printable_record = 
                        record.target_position.ToString("F04", ci).PadLeft(9,' ')+" | "+
                        record.actual_position.ToString("F04", ci).PadLeft(9,' ')+" | "+
                        "    NULL "
                    ;
                }else{
                    printable_record = 
                        record.target_position.ToString("F04", ci).PadLeft(9,' ')+" | "+
                        "    NULL "+" | "+
                        "    NULL "
                    ;
                }
            }
            GD.Print(printable_record);
            log.writeToLog(printable_record);
            if(writeToFile){
                log.appendTextToFile(printable_record);
            }
        }
        log.writeToLog("");

        if(writeToFile){
            log.appendTextToFile("End of results.\n");
        }
    }
    public void removeAllRecords(){
        table.Clear();
    }
}