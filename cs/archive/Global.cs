using Godot;
using System;
using System.Collections.Generic;
public class Global:Godot.Node{
    public List<float> readingAnglesSequence;
    public int readingAnglesSequenceLength;
	public int readingAnglesSequenceMarker=-1;	
    public float arc_inclination_degrees=90;
    public float arc_azimuth_degrees=0;

    public bool update_hemisphere_scene=false;

    // public Node CurrentScene{get; set;}
    public override void _Ready(){
    }
}