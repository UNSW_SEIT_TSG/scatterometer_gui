using Godot;
using System;

public class MeshInstanceSampleYZScatterIntersection : MeshInstance
{
    [Signal]
    public delegate void MyMeshInstanceSampleYZScatterIntersection();
    private const double tau = 6.283185307179586476925286767;
    Vector3 location;
    public override void _Ready()
    {
        // Called every time the node is added to the scene.
        // Initialization here
        resetLocation();
        this.Translation = location;
    }

//    public override void _Process(float delta)
//    {
//        // Called every frame. Delta is time since last frame.
//        // Update game logic here.
//        
//    }
    private void _on_MeshInstanceSample_MySignal(Vector3 axisG2, Vector3 axisG5, Vector3 axisG7, float g2, float g5, float g7){
        updateLocation(axisG2, axisG5, axisG7, g2, g5, g7);
    }
    private float degreesToRadians(float degrees){
        return (float)(degrees*tau/360.0);
    }
    private float radiansToDegrees(float radians){
        return (float)(radians*360.0/tau);
    }
    private void resetLocation(){
        location = new Vector3(0,0.5f,0);
    }
    public void updateLocation(Vector3 axisG2, Vector3 axisG5, Vector3 axisG7, float g2, float g5, float g7){
        Node home = this.GetParent();
        MeshInstance mi_sample_normal = (MeshInstance)home.GetNode("MeshInstanceSampleNormal");
        MeshInstance mi_sample_right = (MeshInstance)home.GetNode("MeshInstanceSampleRight");
        MeshInstance mi_sample_top = (MeshInstance)home.GetNode("MeshInstanceSampleTop");

        MeshInstanceSampleNormal misn = (MeshInstanceSampleNormal)mi_sample_normal;
        MeshInstanceSampleRight misr = (MeshInstanceSampleRight)mi_sample_right;
        MeshInstanceSampleTop mist = (MeshInstanceSampleTop)mi_sample_top;

        misn.updateLocation(axisG2, axisG5, axisG7, g2, g5, g7);
        misr.updateLocation(axisG2, axisG5, axisG7, g2, g5, g7);
        mist.updateLocation(axisG2, axisG5, axisG7, g2, g5, g7);

        Vector3 n_s = mi_sample_normal.GetTranslation(); // Red
        Vector3 r_s = mi_sample_right.GetTranslation(); // Blue
        // Vector3 t_s = mi_sample_top.GetTranslation(); // Green
        
        
        Plane plane_yz = new Plane(r_s, 0);
        Plane scatter_plane = new Plane(new Vector3(0, 1, 0), 0);
        Vector3 intersection_yz = plane_yz.Normal.Cross(scatter_plane.Normal).Normalized();

        this.Translation = intersection_yz/2;
        float beta = radiansToDegrees(n_s.AngleTo(intersection_yz));
        // GD.Print("Beta:  "+beta.ToString());
        EmitSignal(nameof(MyMeshInstanceSampleYZScatterIntersection), beta);
    }
}
