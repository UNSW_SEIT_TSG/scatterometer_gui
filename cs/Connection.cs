using Godot;
using System;
using System.Net;
using System.Net.Sockets;
public class Connection
{
    public bool connected = false;
    public byte[] bytes = new byte[1024];
    public IPAddress internetProtocolAddress;
    public IPEndPoint remoteEndPoint;
    public Socket socket;
    public Connection(string internetProtocolAddressString, Int32 port)
    {
        try
        {
            internetProtocolAddressString = internetProtocolAddressString.Trim();
            this.internetProtocolAddress = System.Net.IPAddress.Parse(internetProtocolAddressString);
            this.remoteEndPoint = new IPEndPoint(this.internetProtocolAddress, port);
            this.socket = new Socket(internetProtocolAddress.AddressFamily, SocketType.Stream, ProtocolType.Tcp);
        }
        catch (Exception e)
        {
            GD.Print(e.ToString());
        }
    }
}