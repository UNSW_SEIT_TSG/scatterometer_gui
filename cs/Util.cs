using Godot;
using System;
public class Util
{
    public float π;
    public float τ;
    public Util()
    {
        π = (float)Math.PI;
        τ = π*2;
    }
    public int DaysInMonth(int monthIndex, int year)
    {
        int result;
        switch (monthIndex)
        {
            case 0: /* Jan */
                result = 31;
                break;
            case 1: /* Feb */
                if(year % 4 == 0)
                {
                    result = 29;
                }
                else
                {
                    result = 28;
                }
                break;
            case 2: /* Mar */
                result = 31;
                break;
            case 3: /* Apr */
                result = 30;
                break;
            case 4: /* May */
                result = 31;
                break;
            case 5: /* Jun */
                result = 30;
                break;
            case 6: /* Jul */
                result = 31;
                break;
            case 7: /* Aug */
                result = 31;
                break;
            case 8: /* Sep */
                result = 30;
                break;
            case 9: /* Oct */
                result = 31;
                break;
            case 10: /* Nov */
                result = 30;
                break;
            case 11: /* Dec */
                result = 31;
                break;
            default:
                result = 28;
                break;
        }   
        return result;
    }
    public Scalar Uniform(Scalar lower_bound, Scalar higher_bound)
    {
        Random ρ = new Random();
        Scalar span = higher_bound - lower_bound;
        return new Scalar(ρ.NextDouble()*span.Value + lower_bound.Value);
    }
    public int Min(int left, int right)
    {
        if (left <= right)
        {
            return left;
        }
        else
        {
            return right;
        }
    }
    public int GetLargestCommonFactor(int leftInt, int rightInt)
    {
        int largestCommonFactor = 1;
        int smallerInt = Min(leftInt, rightInt);
        
        float leftFloat = (float)leftInt;
        float rightFloat = (float)rightInt;
        float smallerFloat = (float)smallerInt;
        
        for (int z = smallerInt; z > 1; z--)
        {
            float lByZ = leftFloat/z;
            float rByZ = rightFloat/z;
            if (
                lByZ == lByZ - lByZ % 1
                &&
                rByZ == rByZ - rByZ % 1
            )
            {

             
                largestCommonFactor = z;
                break;
            }
        }
        return largestCommonFactor;
    }
    public Quat QuatFrom2Vector3s(Vector3 u, Vector3 v)
    {
        Scalar norm_u_norm_v = Sqrt(u.Dot(u) * v.Dot(v));
        Scalar real_part = norm_u_norm_v + u.Dot(v);
        Vector3 w;

        if (real_part.Value < 0.000001f * norm_u_norm_v.Value)
        {
            /* If u and v are exactly opposite, rotate 180 degrees
            * around an arbitrary orthogonal axis. Axis normalisation
            * can happen later, when we normalise the quaternion. */
            real_part.Value = 0.0f;
            w = Abs(u.x) > Abs(u.z)
                ? new Vector3(-u.y, u.x, 0)
                : new Vector3(0, -u.z, u.y);
        }
        else
        {
            /* Otherwise, build quaternion the standard way. */
            w = u.Cross(v);
        }

        Quat q = new Quat(real_part.Value, w.x, w.y, w.z);
        return q.Normalized();
    }
    public float Abs(float x)
    {
        return (float)(Math.Abs(x));
    }
    public int Abs(int x)
    {
        return (int)Math.Abs(x);
    }
    public Scalar Abs(Scalar x)
    {
        return new Scalar(Math.Abs(x.Value));
    }
    public Angle Acos(Scalar r)
    {
        Angle result = new Angle();
        result.Radians = (float)Math.Acos(r.Value);
        return result;
    }
    public Angle Atan2(float y, float x)
    {
        Angle result = new Angle();
        result.Radians = (float)Math.Atan2(y, x);
        return result;
    }
    public Angle Atan2(Scalar y, Scalar x)
    {
        Angle result = new Angle();
        result.Radians = (float)Math.Atan2(y.Value, x.Value);
        return result;
    }
    public Scalar Cos(Angle θ)
    {
        return new Scalar(Math.Cos(θ.Radians));
    }
    public bool EqualWithinTolerance(Angle λ, Angle ρ, Angle tolerance)
    {
        return λ.Degrees < (ρ.Degrees + tolerance.Degrees) && λ.Degrees > (ρ.Degrees - tolerance.Degrees);
    }
    public bool EqualWithinTolerance(Angle λ, Angle ρ, Experiment exp)
    {
        return EqualWithinTolerance(λ.Degrees, ρ.Degrees, exp.Tolerance.Degrees);
    }
    public bool EqualWithinTolerance(float left, float right, float tolerance)
    {
        return left < (right + tolerance) && left > (right - tolerance);
    }
    public bool EqualWithinTolerance(Scalar left, Scalar right, Experiment exp)
    {
        return EqualWithinTolerance(left.Value, right.Value, exp.ToleranceScalar.Value);
    }
    public bool EqualWithinTolerance(Scalar left, Scalar right, Scalar tolerance)
    {
        return left.Value < (right.Value + tolerance.Value) && left.Value > (right.Value - tolerance.Value);
    }
    public Scalar EuclideanDistance3(Scalar Δx, Scalar Δy, Scalar Δz)
    {
        return Sqrt(Sq(Δx)+Sq(Δy)+Sq(Δz));
    }
    public Vector2 PointOnEllipse(Scalar semiMajor, Scalar semiMinor, Scalar radius, Angle α, Angle θ)
    {
        return new Vector2(semiMajor.Value*Cos(α).Value*Cos(θ).Value - semiMinor.Value*Sin(α).Value*Sin(θ).Value,semiMajor.Value*Cos(α).Value*Sin(θ).Value + semiMinor.Value*Sin(α).Value*Cos(θ).Value)*radius.Value;
    }
    public int PolFromBool(bool b)
    {
        return b ? 1 : -1;
    }
    public Scalar Pow(int x, int y)
    {
        return new Scalar(Math.Pow(x,y));
    }
    public Scalar Pow(Scalar x, Scalar y)
    {
        return new Scalar(Math.Pow(x.Value,y.Value));
    }
    public Scalar Sin(Angle θ)
    {
        return new Scalar(Math.Sin(θ.Radians));
    }
    public Scalar Sq(Scalar x)
    {
        return x*x;
    }
    public Scalar Sqrt(double x)
    {
        return new Scalar(Math.Sqrt(x));
    }
    public Scalar Sqrt(Scalar x)
    {
        return new Scalar(Math.Sqrt(x.Value));
    }
    public Vector2  XYFromRadiusAndCentre(Vector2 centre, Scalar r, Angle θ)
    {
        return new Vector2((centre.x+r*Cos(θ)).Value,(centre.y+r*Sin(θ)).Value);
    }
    public Vector2  XYFromθRO(Angle θ, Scalar radius, Vector2 origin)
    {
        return origin + new Vector2(Cos(θ).Value,Sin(θ).Value)*radius.Value;
    }
    public Vector3  XYZOnSphereViaInclinAndRotAboutY(Angle iRad, Angle rRad, Scalar ρ)
    {
        return new Vector3((ρ.Value*Cos(rRad).Value*Cos(iRad).Value),(ρ.Value*1*Sin(iRad).Value),(ρ.Value*Sin(rRad).Value*Cos(iRad).Value));
    }
    public Angle Uniform(Angle lower_bound, Angle higher_bound)
    {
        Random ρ = new Random();
        Angle span =  new Angle();
        Angle result =  new Angle();

        span = higher_bound - lower_bound;
        result = (span*ρ.NextDouble() + lower_bound);
        return result;
    }
}