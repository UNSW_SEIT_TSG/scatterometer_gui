using Godot;
using System;
using System.Collections.Generic;

public class Relay
{
    private Glx _glx;
    private bool _connected;
    private UInt16 _IPByte0;
    private UInt16 _IPByte1;
    private UInt16 _IPByte2;
    private UInt16 _IPByte3;
    private UInt16 _portSend;
    private UInt16 _portReceive;
    public Queue<string> to;
    public Queue<string> from;
    public UInt16 PortSend
    {
        get
        {
            return _portSend;
        }
        set
        {
            _portSend = value;
        }
    }
    public UInt16 PortReceive
    {
        get
        {
            return _portReceive;
        }
        set
        {
            _portReceive = value;
        }
    }
    public UInt16 IPByte0
    {
        get
        {
            return _IPByte0;
        }
        set
        {
            _IPByte0 = value;
        }
    }
    public UInt16 IPByte1
    {
        get
        {
            return _IPByte1;
        }
        set
        {
            _IPByte1 = value;
        }
    }
    public UInt16 IPByte2
    {
        get
        {
            return _IPByte2;
        }
        set
        {
            _IPByte2 = value;
        }
    }
    public UInt16 IPByte3
    {
        get
        {
            return _IPByte3;
        }
        set
        {
            _IPByte3 = value;
        }
    }
    public bool Connected
    {
        get
        {
            return _connected;
        }
        set
        {
            _connected = value;
            if(_connected)
            {
                _glx.RequestPositionAndStatusUpdates();
            }
        }
    }
    public Relay(Glx glx)
    {
        _glx = glx;
        _connected = false;
    
        string configurationsString = System.IO.File.ReadAllText("config.cfg");
        string[] listOfDefaultStrings = configurationsString.Split("\n");
        foreach (string configurationString in listOfDefaultStrings)
        {
            string[] configurationKeyValuePairStrings = configurationString.Replace(";","").Replace(" ","").Split("=");
            string key=configurationKeyValuePairStrings[0];
            string value=configurationKeyValuePairStrings[1];
            if (key.Equals("relayIP"))
            {
                string[] bytesAsStrings = value.Split(".");
                IPByte0 = Convert.ToUInt16(bytesAsStrings[0]);
                IPByte1 = Convert.ToUInt16(bytesAsStrings[1]);
                IPByte2 = Convert.ToUInt16(bytesAsStrings[2]);
                IPByte3 = Convert.ToUInt16(bytesAsStrings[3]);
            }
            if (key.Equals("relayPortSend"))
            {
                PortSend = Convert.ToUInt16(value);
            }
            if (key.Equals("relayPortReceive"))
            {
                PortReceive = Convert.ToUInt16(value);
            }
        }
        to = new Queue<string>();
        from = new Queue<string>();
    }
    public string IPAddressString
    {
        get
        {
            return
                IPByte0.ToString()+"."+
                IPByte1.ToString()+"."+
                IPByte2.ToString()+"."+
                IPByte3.ToString()
            ;
        }
    }
}