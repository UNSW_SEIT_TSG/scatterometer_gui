using Godot;
using System;

public class MeshInstanceSampleTop : MeshInstance
{
    // Member variables here, example:
    // private int a = 2;
    // private string b = "textvar";
    private const double tau = 6.283185307179586476925286767;
    Vector3 location;
    public Vector3 initialLocation;
    public override void _Ready()
    {
        // Called every time the node is added to the scene.
        // Initialization here
        initialLocation = new Vector3(0,0,-0.5f);
        resetLocation();
        this.Translation = initialLocation;
    }

//    public override void _Process(float delta)
//    {
//        // Called every frame. Delta is time since last frame.
//        // Update game logic here.
//        
//    }
    private void _on_MeshInstanceSample_MySignal(Vector3 axisG2, Vector3 axisG5, Vector3 axisG7, float g2, float g5, float g7){
        updateLocation(axisG2, axisG5, axisG7, g2, g5, g7);
    }
    private float degreesToRadians(float degrees){
        return (float)(degrees*tau/360.0);
    }
    private void resetLocation(){
        location = initialLocation;
    }
    public void updateLocation(Vector3 axisG2, Vector3 axisG5, Vector3 axisG7, float g2, float g5, float g7){
        resetLocation();
        location = location.Rotated(axisG2, degreesToRadians(g2));
        location = location.Rotated(axisG5, degreesToRadians(g5));
        location = location.Rotated(axisG7, degreesToRadians(g7));
        this.Translation = location;
    }
}
