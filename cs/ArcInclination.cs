using Godot;
using System;

public class ArcInclination : Node2D
{
    // This class is for rendering the ArcInclination Angle in the GUI.
    
    // Reference for centre of viewport
    Vector2 vCentre = new Godot.Vector2(0,0);
    
    // Defines whether rendered angles are positive in clockwise direction.
    bool clockwiseIsPositive = false;
    
    // Defines view rotation to present inclination angle in desired orientation.
    Angle inclinationOffset;
    
    // Defines view rotation to present modelled angles in desired orientation.
    Angle datumOffset;
    
    // Reference to global singleton class to access common state and functions.
    private Glx _glx;
    
    // Reference to custom utility class.
    Util _util;
    public override void _Ready()
    {
        // This function runs when scene is loaded.
        _util = new Util(); // Instantiates custom utility function.
        _glx = (Glx)GetNode("/root/Glx"); // Assigns reference to global class.
        
        inclinationOffset = new Angle(); // Instantiates inclinationOffset.
        inclinationOffset.Radians = _util.π/2; // Assigns inclinationOffset value.

        datumOffset = new Angle(); // Instantiates datumOffset.
        
        // Assigns reference to Camera object of scene.
        Camera2D cam = (Camera2D)GetNode("Camera2D") as Camera2D;
        cam.SetPosition(vCentre + new Vector2(0, -45)); // Sets Camera position.
    }
   public override void _Process(float delta)
   {
       // This function runs every delta [ms] tick.
       Update(); // Updates content of scene.
   }
   public override void _Draw()
   {
        // This function draws the features to be rendered by the camera object.       
        
        // End point of line representing axis.
        Vector2 axisEndPoint = vCentre + new Vector2(0, -120);
        
        // Draw sample y axis as line with shadow using colour defined in Glx.
        _glx.DrawLineWithShadow(
            n2d:this, // Reference to canvas where the line will be drawn.
            to:axisEndPoint,
            color:_glx.palette.sampleY
        );

        // Draw arc representing ArcInclination Angle.
        _glx.DrawArcLine(
            context:this,
            datumOffsetRad:datumOffset,
            CWPos:clockwiseIsPositive,
            centre:vCentre,
            startRad:inclinationOffset,
            finalRad:_glx.exp.ArcInclination+inclinationOffset,
            radius:new Scalar(90),
            colA:_glx.palette.rotationAboutYAxisInXZPlaneHeadingVector,
            colB:_glx.palette.inclinationVector,
            colC:_glx.palette.inclinationArc,
            drawStartAngleLine:false,
            drawFinalAngleLine:true
        );

        // Set Position of axis label.
        Label labelSampleYAxis = (Label)GetNode("Control/LabelSampleYAxis");
        labelSampleYAxis.SetPosition(axisEndPoint + new Vector2(5, 0));
        
        // Draw small circle representing axis perpendicular to drawing plane.
        _glx.DrawCircleWithShadow(
            this,
            vCentre,
            new Scalar(2),
            _glx.palette.rotationAboutYAxisInXZPlaneHalfturnVector
        );
   }
}
