using Godot;
using System;
public class ArcRotationAboutYAxisInXZPlane : Node2D
{
    Godot.Vector2 vCentre = new Godot.Vector2(0, 0);
    bool  clockwiseIsPositive = false;
    private Glx _glx;
    Util _util;
    public override void _Ready()
    {
        _util = new Util();
        Camera2D cam = (Camera2D)GetNode("Camera2D");
        cam.SetPosition(vCentre);
        _glx = (Glx)GetNode("/root/Glx");
    }
    public override void _Process(float delta)
    {
        Update();
    }
    public override void _Draw()
    {
        Color sqCol = new Godot.Color(0.8f,0.8f,0.8f);
        Scalar length = new Scalar(200);
        Scalar sqEdge = length*0.9f/_util.Sqrt(2);
        Scalar radius1 = length*0.475f;
        Scalar radius2 = length*0.45f;
        
        Angle inclinationAngleOffset = new Angle();
        inclinationAngleOffset.Radians = _util.π/2;

        Angle inclinationRad = new Angle();
        inclinationRad = _glx.exp.ArcInclination+inclinationAngleOffset;
        
        Angle angleRad = new Angle();
        angleRad = -_glx.exp.ArcRotationAboutYAxisInXZPlane;

        Label sampleXAxisLabel = GetNode("Control/SampleXAxisLabel") as Label;
        Label sampleZAxisLabel = GetNode("Control/SampleZAxisLabel") as Label;

        Vector2 sampleXAxisLabelOffset = new Vector2(length.Value*0.63f, 0);
        Vector2 sampleZAxisLabelOffset = new Vector2(0, length.Value*0.4f);
        Vector2 sqx = new Vector2(sqEdge.Value, sqEdge.Value);
        Rect2 square = new Rect2(position: sqx*-0.5f, size: sqx);
        Vector2 xAxisEnd = new Vector2(-radius1.Value, 0);
        Vector2 zAxisEnd = new Vector2(0, -radius1.Value);

        sampleXAxisLabel.SetPosition(vCentre - sampleXAxisLabelOffset);
        sampleZAxisLabel.SetPosition(vCentre - sampleZAxisLabelOffset);

        DrawRect(square, sqCol, filled:true); // Sample plane
        _glx.DrawLineWithShadow(this, xAxisEnd, _glx.palette.sampleX); // x axis
        _glx.DrawLineWithShadow(this, zAxisEnd, _glx.palette.sampleZ); // z axis

        // arc on sample plane before inclination applied

        _glx.DrawEllipticalArc(
            context:this,
            datumOffsetRad:angleRad,
            centre:vCentre,
            startRad:_glx.exp.ArcStart,
            finalRad:_glx.exp.ArcFinal,
            radius:radius2,
            drawFinalAngleLine:false,
            drawStartAngleLine:false,
            colA:_glx.palette.rotationAboutYAxisInXZPlaneHalfturnVector,
            colB:_glx.palette.rotationAboutYAxisInXZPlaneZeroturnVector,
            colC:_glx.palette.black,
            semiMinor:new Scalar(1),
            semiMajor:new Scalar(1)
        );

        Angle ξ = new Angle();
        ξ.Radians = _util.π/2;
        Vector2 v0 = _util.XYFromRadiusAndCentre(
            vCentre,
            radius2*_util.Cos(inclinationRad),
            angleRad+ξ
        );
        Vector2 v1 = _util.XYFromRadiusAndCentre(
            vCentre,
            radius2,
            angleRad+ξ
        );
        _glx.DrawLineWithShadow(this, v0, v1, _glx.palette.inclinationArc);
        _glx.DrawLineWithShadow(this, vCentre, v0, _glx.palette.inclinationVector);

        _glx.DrawEllipticalArc(
            context:this,
            datumOffsetRad:angleRad,
            centre:vCentre,
            startRad:_glx.exp.ArcStart,
            finalRad:_glx.exp.ArcFinal,
            radius:radius2,
            drawFinalAngleLine:true,
            drawStartAngleLine:true,
            colA:_glx.palette.finalAngleVector,
            colB:_glx.palette.startAngleVector,
            colC:_glx.palette.readingArc,
            semiMinor:(_util.Cos(inclinationRad)),
            semiMajor:new Scalar(1)
        );

        _glx.DrawCircleWithShadow(this, vCentre, new Scalar(2), _glx.palette.sampleY);
    }
}