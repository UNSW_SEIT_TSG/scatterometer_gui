﻿using Godot;
using System;
using System.Text;

class VecToMot
{
    private static void Test()
    {
        MyVector3 vec_a = new MyVector3(1, 0, 0);
        MyVector3 axis = new MyVector3(1, -1, 1);

        GD.Print(vec_a.ToString());
        MyVector3 vec_c;
        Angle θ = new Angle();
        for (int step = 0; step < 4; step++)
        {
            θ.Degrees = step*120;
            vec_c = vec_a.Rotated(axis, θ);
            GD.Print(vec_c.ToString()+"\t"+θ.ToString());
        }
    }
    public static StackRotations ApplyRotations(StackRotations z, Rotations θ)
    {
        // Rotation about Group 7 Axis
        z.sampleNormal = z.sampleNormal.Rotated(z.group7, θ.θ_7);
        z.sampleX = z.sampleX.Rotated(z.group7, θ.θ_7);

        // Rotation about Group 5 Axis
        z.sampleX = z.sampleX.Rotated(z.group5, θ.θ_5);
        z.sampleNormal = z.sampleNormal.Rotated(z.group5, θ.θ_5);
        z.group7 = z.group7.Rotated(z.group5, θ.θ_5);

        // Rotation about Group 2 Axis
        z.sampleX = z.sampleX.Rotated(z.group2, θ.θ_2);
        z.sampleNormal = z.sampleNormal.Rotated(z.group2, θ.θ_2);
        z.group7 = z.group7.Rotated(z.group2, θ.θ_2);
        z.group5 = z.group5.Rotated(z.group2, θ.θ_2);

        return z;
    }
    public static MyVector3 ShotNormal(Rotations θ){return ApplyRotations(new StackRotations(), θ).sampleNormal;}
    public static MyVector3 ShotX(Rotations θ){return ApplyRotations(new StackRotations(), θ).sampleX;}
    public static Scalar error(MyVector3 targetNormal, MyVector3 targetX, Rotations θ)
    {
        // Need to consider both sample normal vector and rotation of the sample
        // around the normal.

        return (0
                +(targetNormal - ShotNormal(θ)).Magnitude
                +(targetX - ShotX(θ)).Magnitude
            )
        ;
    }
    public static Rotations[] GenerateChoices(Rotations θ, Angle δDeg)
    {
        return new Rotations[]{
            new Rotations(θ.θ_2+δDeg, θ.θ_5, θ.θ_7),
            new Rotations(θ.θ_2-δDeg, θ.θ_5, θ.θ_7),
            new Rotations(θ.θ_2, θ.θ_5+δDeg, θ.θ_7),
            new Rotations(θ.θ_2, θ.θ_5-δDeg, θ.θ_7),
            new Rotations(θ.θ_2, θ.θ_5, θ.θ_7+δDeg),
            new Rotations(θ.θ_2, θ.θ_5, θ.θ_7-δDeg)
        };
    }
    public Rotations MapVectorToMotorAngles(MyVector3 targetNormal, MyVector3 targetX, Rotations θ){
        Util ut = new Util();
        targetNormal.Magnitude = new Scalar(1); // Normalise vector.
        targetX.Magnitude = new Scalar(1); // Normalise vector.

        int iterations = -1;
        int rollCount = 1;
        
        Angle originalStep = new Angle();
        originalStep.Degrees = 12;

        int safety_limit = 50000;
        
        Angle step = new Angle();
        step.Degrees = originalStep.Degrees;

        Angle stepMin = new Angle();
        stepMin.Degrees = 0.00000001f;
        
        StackRotations σ = new StackRotations();

        Scalar ε_last = error(targetNormal, targetX, θ);
        Scalar ε_min = new Scalar(float.PositiveInfinity);
        Scalar ε;
        
        Rotations choice_min = new Rotations(new Angle(), new Angle(), new Angle());

        while (true)
        {
            iterations += 1;
            ε_last = ε_min;
            ε_min = new Scalar(float.PositiveInfinity);

            Rotations[] choices = GenerateChoices(θ, step);
            foreach (Rotations choice in choices)
            {
                ε = error(targetNormal, targetX, θ);
                if (ε < ε_min)
                {
                    ε_min = ε;
                    choice_min = choice;
                }
            }
            
            θ = choice_min;

            if (
                // (θ.θ_2 < σ.Group2Min) ||
                // (θ.θ_5 < σ.Group5Min) ||
                // (θ.θ_7 < σ.Group7Min) ||
                // (θ.θ_2 > σ.Group2Max) ||
                // (θ.θ_5 > σ.Group5Max) ||
                // (θ.θ_7 > σ.Group7Max) ||
                step < stepMin
            ){
                // GD.Print("re-roll");
                rollCount += 1;
                θ = new Rotations(
                    ut.Uniform(σ.Group2Min, σ.Group2Max),
                    ut.Uniform(σ.Group5Min, σ.Group5Max),
                    ut.Uniform(σ.Group7Min, σ.Group7Max)
                );
                step.Degrees = originalStep.Degrees;
            }
            
            if(ε_last <= ε_min)
            {
                step.Degrees = step.Degrees / 2;
            }
            if (ε_min.Value < 0.00025)
            {
                string φ = "0.00000";
                GD.Print("Final:   "+θ.ToString());
                GD.Print("  Final error:    "+ε_min.ToString());
                GD.Print("  TargetY -->| "+targetNormal.ToString(φ)+"  ShotY -->| "+ShotNormal(θ).ToString(φ)+"\tεY -->| "+(targetNormal-ShotNormal(θ)).Magnitude.ToString(φ));
                GD.Print("  TargetX -->| "+targetX.ToString(φ)+"  ShotX -->| "+ShotX(θ).ToString(φ)+"\tεX -->| "+(targetX-ShotX(θ)).Magnitude.ToString(φ));
                GD.Print(
                    "finished in "+iterations+" iterations; "+
                    "with "+rollCount.ToString()+" rolls."
                );
                return θ;
            }
            
            if (iterations > safety_limit)
            {
                GD.Print("No Solution found");
                throw new Exception("Safety limit exceeded.");
            }
        }

        
    }
}