using System;
using System.Text;
using Godot;

public class Scalar
{
    double π = Math.PI;
    double _units;
    public Scalar(short scalar)
    {
        _units = (double)scalar;
    }
    public Scalar(int scalar)
    {
        _units = (double)scalar;
    }
    public Scalar(double scalar)
    {
        _units = scalar;
    }
    public Scalar(float scalar)
    {
        _units = scalar;
    }
    public Scalar(Angle scalar)
    {
        _units = scalar.Degrees*π/180.0f;
    }
    public float Value
    {
        get
        {
            return (float)_units;
        }
        set
        {
            _units = value;
        }
    }
    public override string ToString()
    {
        return Value.ToString()+" [rad]";
    }
    public string ToString(string format)
    {
        return Value.ToString(format)+" [rad]";
    }
    public static Scalar operator + (Scalar λ, Scalar ρ){return new Scalar(λ.Value + ρ.Value);}
    public static Scalar operator * (Scalar λ, Scalar ρ){return new Scalar(λ.Value * ρ.Value);}
    public static Scalar operator * (Scalar λ, float ρ){return new Scalar(λ.Value * ρ);}
    public static Vector3 operator * (Scalar λ, Vector3 ρ){return ρ*λ.Value;}
    public static Scalar operator / (Scalar λ, Scalar ρ){return new Scalar(λ.Value / ρ.Value);}
    public static Scalar operator - (Scalar λ, Scalar ρ){return new Scalar(λ.Value - ρ.Value);}
    public static Scalar operator - (int λ, Scalar ρ){return new Scalar(λ - ρ.Value);}
    public static Scalar operator - (Scalar λ, int ρ){return new Scalar(λ.Value - ρ);}
    public static Scalar operator - (Scalar λ, float ρ){return new Scalar(λ.Value - ρ);}
    public static Scalar operator - (float λ, Scalar ρ){return new Scalar(λ - ρ.Value);}
    public static Scalar operator / (Scalar λ, float ρ){return new Scalar(λ.Value / ρ);}
    public static Scalar operator / (float λ, Scalar ρ){return new Scalar(λ / ρ.Value);}
    public static Scalar operator - (Scalar σ){return new Scalar(-σ.Value);}
    public static Scalar operator + (Scalar λ, float ρ){return new Scalar(λ.Value + ρ);}
    public static Scalar operator + (float λ, Scalar ρ){return new Scalar(λ + ρ.Value);}
    public static bool operator > (Scalar λ, Scalar ρ){return λ.Value > ρ.Value;}
    public static bool operator < (Scalar λ, Scalar ρ){return λ.Value < ρ.Value;}
    public static bool operator <= (Scalar λ, Scalar ρ){return λ.Value <= ρ.Value;}
    public static bool operator >= (Scalar λ, Scalar ρ){return λ.Value >= ρ.Value;}
}
