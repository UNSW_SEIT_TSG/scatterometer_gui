using System;
using System.Text;

public class Rotations
{
    private Angle _θ_2;
    private Angle _θ_5;
    private Angle _θ_7;
    public Rotations()
    {
        _θ_2 = new Angle();
        _θ_5 = new Angle();
        _θ_7 = new Angle();
    }
    public Rotations(Angle θ_2, Angle θ_5, Angle θ_7)
    {
        _θ_2 = new Angle();
        _θ_5 = new Angle();
        _θ_7 = new Angle();
        _θ_2 = θ_2;
        _θ_5 = θ_5;
        _θ_7 = θ_7;
    }
    public override string ToString()
    {
        return
            "θ_2:"+_θ_2.Degrees.ToString("0.000")+" "+
            "θ_5:"+_θ_5.Degrees.ToString("0.000")+" "+
            "θ_7:"+_θ_7.Degrees.ToString("0.000")
        ;
    }
    public Angle θ_2
    {
        get
        {
            return _θ_2;
        }
        set
        {
            _θ_2 = value;
        }
    }
    public Angle θ_5
    {
        get
        {
            return _θ_5;
        }
        set
        {
            _θ_5 = value;
        }
    }
    public Angle θ_7
    {
        get
        {
            return _θ_7;
        }
        set
        {
            _θ_7 = value;
        }
    }
}
