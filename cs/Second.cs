using System;
using System.Text;

public class Second
{
    float π = (float)Math.PI;
    float _seconds;
    public Second(double second)
    {
        _seconds = (float)second;
    }
    public Second(float second)
    {
        _seconds = second;
    }
    public float Seconds
    {
        get
        {
            return _seconds;
        }
        set
        {
            _seconds = value;
        }
    }
    public float Milliseconds
    {
        get
        {
            return _seconds*1000;
        }
        set
        {
            _seconds = value/1000;
        }
    }
    public override string ToString()
    {
        return Seconds.ToString()+" [s]";
    }
    public static Second operator + (Second left, Second right)
    {
        return new Second(left.Seconds + right.Seconds);
    }
    public static Second operator - (Second left, Second right)
    {
        return new Second(left.Seconds - right.Seconds);
    }
    public static Second operator - (Second r)
    {
        return new Second(-r.Seconds);
    }
}