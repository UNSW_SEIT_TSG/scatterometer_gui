using Godot;
using System;
using System.Collections.Generic;
public class Experiment
{
    private bool _readingTakenAtThisPosition;
    private bool _locked;
    private string _experimentName;
    private Glx _glx;
    private Util _util;
    private bool _valueChanged;
    //private bool _alphaBetaValueChanged;
    private Angle _tolerance;
    private Scalar _toleranceScalar;
    private Angle _arcInterval;
    private Angle _arcSpan;
    private Angle _arcStart;
    private Angle _arcFinal;
    private Angle _θ;
    private Angle _φ;
    private Angle _arcInclination;
    private Angle _arcRotationAboutYAxisInXZPlane;
    private Angle _alpha;
    private Angle _beta;
    private int _readingCount;
    private int _readingCurrentIndex;
    private int _readingReceivedCurrentIndex;
    public List<Angle> readingSeqArc;
    public List<Vector3> readingListAsXYZs;
    public List<Result> results;
    public Scalar ToleranceScalar
    {
        get
        {
            return _toleranceScalar;
        }
    }
    public Angle Tolerance
    {
        get
        {
            return _tolerance;
        }
    }
    public bool ValueChanged
    {
        get
        {
            return _valueChanged;
        }
        set
        {
            _valueChanged = value;
        }
    }
    // public bool AlphaBetaValueChanged
    // {
    //     get
    //     {
    //         return _alphaBetaValueChanged;
    //     }
    //     set
    //     {
    //         _alphaBetaValueChanged = value;
    //     }
    // }
    public Angle TargetPositionDeg
    {
        get
        {
            UpdateArcSpan();
            UpdateSequence();
            UpdateVector3ReadingSequenceFromAngularReadingSequence();
            Angle result = new Angle();
            result = readingSeqArc[ReadingCurrentIndex];
            return result;
        }
    }
    public bool ReadyToTakeReading
    {
        get
        {
            return _util.EqualWithinTolerance(ArcCurrentAngleDeg,TargetPositionDeg,this.Tolerance);
        }
    }
    public Angle ArcCurrentAngleDeg
    {
        get
        {
            Angle result = new Angle();
            result = _glx.stack.rotaryStages["group2"].PositionGUI;
            return result;
        }
    }
    public void TakeReading(
        string intensityReadingMean,
        string intensityReadingStdDev)
    {
        results.Add(
            new Result(
                ArcCurrentAngleDeg,
                ArcRotationAboutYAxisInXZPlane,
                ArcInclination,
                intensityReadingMean,
                intensityReadingStdDev
            )
        );
        _readingTakenAtThisPosition = true;
        ReadingReceivedCurrentIndex += 1;
        ValueChanged = true;
    }
    public int ReadingCurrentIndex
    {
        get
        {
            return _readingCurrentIndex;
        }
        set
        {
            _readingCurrentIndex = value;
            ValueChanged = true;
        }
    }
    public int ReadingReceivedCurrentIndex
    {
        get
        {
            return _readingReceivedCurrentIndex;
        }
        set
        {
            _readingReceivedCurrentIndex = value;
            ValueChanged = true;
        }
    }
    public int ReadingCount
    {
        get
        {
            return _readingCount;
        }
        set
        {
            _readingCount = value;
            UpdateArcInterval();
            UpdateSequence();
            ValueChanged = true;
        }
    }
    public Angle ArcInterval
    {
        // ArcInterval property of Experiment
        get
        {
            return _arcInterval;
        }
        set
        {
            _arcInterval = value;
        }
    }
    public void UpdateArcInterval()
    {
        UpdateArcSpan(); // Ensures arc span is updated first.
        ArcInterval = ArcSpan/(ReadingCount-1);
    }
    public void UpdateArcSpan()
    {
        ArcSpan = _arcFinal-_arcStart;
    }
    public Angle ArcSpan
    {
        // ArcSpan property of Experiment
        get
        {
            return _arcSpan;
        }
        set
        {
            _arcSpan = value;
        }
    }
    public Angle ArcFinal
    {
        // ArcFinal property of Experiment
        get
        {
            return _arcFinal;
        }
        set
        {
            _arcFinal = value; // Assigns new value to Angle
            UpdateArcSpan(); // Update Arc Span now that the final is different.
            UpdateArcInterval(); // Updates Interval now that the span has changed.
            UpdateSequence(); // Update sequence or measurement points on arc.
            ValueChanged = true; // Set flag to tell GUI to re-render as values have changed.
        }
    }
    public Angle ArcStart
    {
        // ArcStart property of Experiment
        get
        {
            return _arcStart;
        }
        set{
            _arcStart = value;
            UpdateArcSpan();
            UpdateArcInterval();
            UpdateSequence();
            ValueChanged = true;
        }
    }
    public void UpdateSequence()
    {
        // Generate angle intervals within selected scattering plane
        readingSeqArc = new List<Angle>(ReadingCount);
        UpdateArcInterval();
        for (int q = 0; q < ReadingCount; q++)
        {
            readingSeqArc.Add(ArcInterval*q+ArcStart);
        }
    }
    public Angle θ
    {
        get
        {
            return _θ;
        }
        set
        {
            _θ = value;
        }
    }
    public Angle φ
    {
        get
        {
            return _φ;
        }
        set
        {
            _φ = value;
        }
    }
    public Angle ArcRotationAboutYAxisInXZPlane
    {
        // ArcRotationAboutYAxisInXZPlane property of Experiment.
        get
        {
            return _arcRotationAboutYAxisInXZPlane;
        }
        set
        {
            _arcRotationAboutYAxisInXZPlane = value; // Update value.
            UpdateSequence();  // Update Sequence as rotation about Y has changed.
            //jjk: moved this flag change to ScatterPlane class, on_signal proc.
            // ValueChanged = true; // Flag needed for update to GUI.
        }
    }
    public Angle ArcInclination
    {
        // ArcInclination property of Experiment
        get
        {
            return _arcInclination;
        }
        set
        {
            _arcInclination = value;  // Assign new value
            
            // As the value has changed update the measurement reading sequence.
            UpdateSequence();

            //jjk: moved this flag change to ScatterPlane class, on_signal proc.
            // ValueChanged = true; // Set flag for GUI to update scene.
        }
    }
    public Angle Alpha
    {
        // ArcRotationAboutYAxisInXZPlane property of Experiment.
        get
        {
            return _alpha;
        }
        set
        {
            _alpha = value; // Update value.
            //UpdateSequence();  // Update Sequence as rotation about Y has changed.
            //ValueChanged = true; // Flag needed for update to GUI.
        }
    }
    public Angle Beta
    {
        // ArcRotationAboutYAxisInXZPlane property of Experiment.
        get
        {
            return _beta;
        }
        set
        {
            _beta = value; // Update value.
            //UpdateSequence();  // Update Sequence as rotation about Y has changed.
            //ValueChanged = true; // Flag needed for update to GUI.
        }
    }
    public void UpdateVector3ReadingSequenceFromAngularReadingSequence()
    {
        Angle readingSeqArcOffset = new Angle();
        
        readingListAsXYZs = new List<Vector3>(ReadingCount);
        for (int q = 0; q < ReadingCount; q++)
        {
            Angle arc = new Angle();
            arc = readingSeqArc[q]+readingSeqArcOffset;

            Vector3 beforeRotationAboutYAxisInXZPlaneRotation = new Vector3(
                _util.Sin(arc).Value * _util.Cos(ArcInclination).Value,
                _util.Sin(arc).Value * _util.Sin(ArcInclination).Value,
                _util.Cos(arc).Value
            );

            readingListAsXYZs.Add(
                beforeRotationAboutYAxisInXZPlaneRotation.Rotated(
                    new Vector3(0,1,0), 
                    ArcRotationAboutYAxisInXZPlane.Radians
                )
            );
        }
    }
    public void UpdateXYZs(List<Vector3> XYZs)
    {
        readingListAsXYZs = new List<Vector3>(_readingCount);
        for (int i = 0; i < ReadingCount; i++)
        {
            readingListAsXYZs.Add(XYZs[i]);
        }
    }
    public string TodoDoingDone(int index)
    {
        string result = "";
        if (index < ReadingReceivedCurrentIndex)
        {
            result = "done";
        }
        else if (index > ReadingReceivedCurrentIndex)
        {
            result = "todo";
        }
        else
        {
            result = "doing";
        }
        return result;
    }
    public void PrintResults()
    {
        foreach (Result result in results)
        {
            GD.Print(result.ToString());
        }
    }
    public void MoveToNext(string tag)
    {
        ReadingCurrentIndex += 1;
        _glx.stack.rotaryStages["group2"].Move(TargetPositionDeg, tag);
        _readingTakenAtThisPosition=false;
        ValueChanged = true;
    }
    public bool Locked
    {
        get
        {
            return _locked;
        }
        set
        {
            _locked = value;
            _readingTakenAtThisPosition = false;
            ValueChanged = true;
        }
    }
    public void SaveResultsToFile()
    {
        string resultsString = "";
        
        foreach (Result result in results)
        {
            resultsString += result.ToString()+"\n";
        }
        string timeStamp = DateTime.Now.ToString("yyyyMMddHHmmssffff");
        System.IO.File.WriteAllText("results_"+timeStamp+".data", resultsString);
    }
    public Experiment(Glx glx)
    {
        // Experiment Constructor.

        _util = new Util();
        _glx = glx;

        // Instantiate new Angle objects
        _tolerance = new Angle(); // Margin within which two Angles can be considered equal.
        _arcInterval = new Angle(); // Arc of an interval between two measurement points.
        _arcSpan = new Angle(); // Arc of entire experiment from start to end.
        _arcStart = new Angle(); // Experiment start Angle
        _arcFinal = new Angle(); // Experiment final Angle
        _θ = new Angle(); // Angle between scatterplane normal and sample z axis.
        _φ = new Angle();
        _arcInclination = new Angle();
        _arcRotationAboutYAxisInXZPlane = new Angle();
        _alpha=new Angle();// jjk
        _beta=new Angle();

        _arcStart.Degrees = -20.0f; // Arbitrary default start angle
        _arcFinal.Degrees = 60.0f; // Arbitrary default final angle
        _readingCount = 9;
        _arcInclination.Degrees = 10.0f; // Arbitrary default arc inclination
        _arcRotationAboutYAxisInXZPlane.Degrees = 5.0f; // Arbitrary default.
        _alpha.Degrees = 0;
        _beta.Degrees = 0;

        // Initialised to -1 to represent readings have not started.
        ReadingCurrentIndex = -1; 

        // Initialised to -1 to represent readings have not started.
        ReadingReceivedCurrentIndex = -1;

        // Empty List for holding Result objects as they are collected.
        results = new List<Result>(); 

        _tolerance.Degrees = 0.005f;

        UpdateArcSpan();
        UpdateSequence();   // Initialise sequence from defaults.
        UpdateVector3ReadingSequenceFromAngularReadingSequence();
        _readingTakenAtThisPosition = false;
        ValueChanged = true;
    }
}
