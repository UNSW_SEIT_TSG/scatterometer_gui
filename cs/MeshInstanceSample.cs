using Godot;
using System;

public class MeshInstanceSample : MeshInstance
{
    // Member variables here, example:
    // private int a = 2;
    // private string b = "textvar";
    [Signal]
    public delegate void MySignal();
    private float oldG2, oldG5, oldG7;
    private Vector3 axisG2;
    private Vector3 axisG5;
    private Vector3 axisG7;
    private const double tau = 6.283185307179586476925286767;
    public override void _Ready()
    {
        // Called every time the node is added to the scene.
        // Initialization here
        oldG2 = 0;
        oldG5 = 0;
        oldG7 = 0;
    }

//    public override void _Process(float delta)
//    {
//        // Called every frame. Delta is time since last frame.
//        // Update game logic here.
//        
//    }
    private void initialiseAxes(){
        axisG2 = new Vector3(0,1,0);
        axisG5 = new Vector3(1,0,0);
        axisG7 = new Vector3(0,1,0);
    }
    private void updateAxes(float g2, float g5, float g7){
        updateAxisG2(g2);
        updateAxisG5(g5);
        updateAxisG7(g7);
        // GD.Print("");
        // GD.Print("G2: "+g2.ToString()+", G5: "+g5.ToString()+", G7: "+g7.ToString());
    }
    private void _on_SpinBoxGroup2_value_changed(float val){
        oldG2 = val;
        updateAxes(val, oldG5, oldG7);
        applyRotations();
    }

    private void _on_SpinBoxGroup5_value_changed(float val){
        oldG5 = val;
        updateAxes(oldG2, val, oldG7);
        applyRotations();
    }
    private void _on_SpinBoxGroup7_value_changed(float val){
        GD.Print("MeshInstanceSample::_on_SpinBoxGroup7_value_changed()::"+val.ToString());
        oldG7 = val;
        updateAxes(oldG2, oldG5, val);
        applyRotations();
    }
    private void updateAxisG2(float val){
        initialiseAxes();
    }
    private void updateAxisG5(float val){
        initialiseAxes();
        axisG5 = axisG5.Rotated(axisG2, degreesToRadians(oldG2));
    }
    private void updateAxisG7(float val){
        initialiseAxes();
        axisG5 = axisG5.Rotated(axisG2, degreesToRadians(oldG2));
        axisG7 = axisG7.Rotated(axisG2, degreesToRadians(oldG2));
        axisG7 = axisG7.Rotated(axisG5, degreesToRadians(oldG5));
    }
    private void applyRotations(){
        this.SetRotationDegrees(new Vector3(-90,0,0));
        this.Rotate(axisG2, degreesToRadians(oldG2));
        this.Rotate(axisG5, degreesToRadians(oldG5));
        this.Rotate(axisG7, degreesToRadians(oldG7));
        EmitSignal(nameof(MySignal), axisG2, axisG5, axisG7, oldG2, oldG5, oldG7);
    }
    private float degreesToRadians(float degrees){
        return (float)(degrees*tau/360.0);
    }
}
