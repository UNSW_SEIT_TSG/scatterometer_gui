using System;
using System.Text;

public class StandardBasis
{
    public UnitVector3 i;
    public UnitVector3 j;
    public UnitVector3 k;
    public StandardBasis()
    {
        i = new UnitVector3(1, 0, 0);
        j = new UnitVector3(0, 1, 0);
        k = new UnitVector3(0, 0, 1);
    }
}
